package shepard.backend.player

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.achievement.details.AchievementDetails
import shepard.backend.service.achievement.manager.AchievementManager
import shepard.backend.service.leaderboard.details.LeaderboardDetails
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.backend.service.matchmaking.util.Glicko2
import shepard.backend.service.player.Player
import shepard.backend.service.stats.details.StatDetails
import shepard.backend.service.stats.manager.StatManager
import spock.lang.Stepwise

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 21/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class PlayerSpec extends OneStageSpec
{
    def "Query player status for a empty system"()
    {
        setup:
        def player = Actor.getReference(Player, "random-player")

        expect:
        player.currentGame.join() == null
        player.currentLobby.join() == null
        player.achievementsList.join().size() == 0
        player.ranksList.join().size() == 0
        player.statList.join().size() == 0
        player.skill.join().rating.join() == Actor.getReference(Glicko2).defaultRating.join()
        player.skill.join().ratingDeviation.join() == Actor.getReference(Glicko2).defaultRatingDeviation.join()
        player.skill.join().volatility.join() == Actor.getReference(Glicko2).defaultVolatility.join()
    }

    def "Add some services"()
    {
        def ad = new AchievementDetails(name: "Test Achivement")
        def am = Actor.getReference(AchievementManager)
        am.registerService(ad).join()

        def sd = new StatDetails(name: "Test Stat")
        Actor.getReference(StatManager).registerService(sd).join()

        def ld = new LeaderboardDetails(name: "Test Leaderboard")
        Actor.getReference(LeaderboardManager).registerService(ld).join()

        def yd = new RoomDetails(name: "Test Lobby Service")
        Actor.getReference(RoomManager).registerService(yd).join()

        expect:
        Actor.getReference(AchievementManager).allEnabledServices.join().first().getServiceDetails().join() == ad
        Actor.getReference(StatManager).allEnabledServices.join().first().getServiceDetails().join() == sd
        Actor.getReference(LeaderboardManager).allEnabledServices.join().first().getServiceDetails().join() == ld
        Actor.getReference(RoomManager).allEnabledServices.join().first().getServiceDetails().join() == yd
    }

    def "Query player status for the available services"()
    {
        setup:
        def player = Actor.getReference(Player, "random-player-2")
        def lobby = Actor.getReference(RoomManager).getAllEnabledServices().join().first().getEntry(player).join()

        expect:
        player.currentGame.join() == null
        player.currentLobby.join() == lobby
        player.achievementsList.join().size() == 1
        player.ranksList.join().size() == 1
        player.statList.join().size() == 1
        player.skill.join().rating.join() == Actor.getReference(Glicko2).defaultRating.join()
        player.skill.join().ratingDeviation.join() == Actor.getReference(Glicko2).defaultRatingDeviation.join()
        player.skill.join().volatility.join() == Actor.getReference(Glicko2).defaultVolatility.join()
    }
}
