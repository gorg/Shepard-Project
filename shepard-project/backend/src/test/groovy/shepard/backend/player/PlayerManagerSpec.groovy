package shepard.backend.player

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.player.manager.PlayerManager
import spock.lang.Shared
import spock.lang.Stepwise
import spock.lang.Unroll

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 28/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class PlayerManagerSpec extends OneStageSpec
{
    @Shared PlayerManager manager = Actor.getReference(PlayerManager)

    @Unroll
    def "invalid player"()
    {
        when:
        def player = manager.registerPlayer(name,pass).join()

        then:
        player == null

        where:
        name    | pass
        null    | null
        null    | ''
        ''      | null
        ''      | ''
        'asdf'  | null
        'asdf'  | ''
        null    | 'asdf'
        ''      | 'asdf'
    }

    @Unroll
    def "register player"()
    {
        given:
        def player = manager.registerPlayer(name,pass).join()

        when:
        def valid = player.validatePlayer(name,pass).join()

        then:
        valid == player

        when:
        def another = manager.registerPlayer(name,pass).join()

        then:
        another == player
        another == valid

        when:
        //Player is already registered and name + pass don't match
        def invalid = manager.registerPlayer(name, pass.concat('qwerty')).join()

        then:
        invalid == null

        where:
        name    | pass
        'a'     | '1'
        'b'     | '2'
        'c'     | '3'
        'd'     | '4'
    }

    @Unroll
    def "change password"()
    {
        when:
        def player = manager.changePassword(name,pass,newPass).join()

        then:
        player.validatePlayer(name,newPass).join() == player

        where:
        name    | pass  | newPass
        'a'     | '1'   | '5'
        'b'     | '2'   | '6'
        'c'     | '3'   | '7'
        'd'     | '4'   | '8'
    }


    @Unroll
    def "find by name"()
    {
        when:
        def player = manager.findByName(name).join()

        then:
        assert (willBeNull ? player == null : player.identity == name)


        where:
        name |  willBeNull
        'a'  |  false
        'b'  |  false
        'c'  |  false
        'd'  |  false
        'z'  |  true
        'x'  |  true
        'y'  |  true
    }

}
