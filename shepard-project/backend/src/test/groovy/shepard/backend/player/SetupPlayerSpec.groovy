package shepard.backend.player

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.player.Player

import java.util.concurrent.CompletionException

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 21/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class SetupPlayerSpec extends OneStageSpec
{
    def "Check a random player"()
    {
        def player = Actor.getReference(Player, "random-player")

        expect:
        !player.isPlayerSet().join()

        when:
        player.registerPlayer("random-player", "random-password").join()

        then:
        player.isPlayerSet().join()

        and:
        def p = player.validatePlayer("random-player","random-password").join()
        assert p == player
    }

    def "Player name and actor identity must be a match"()
    {
        def player = Actor.getReference(Player, "random")

        expect:
        !player.isPlayerSet().join()

        when:
        player.registerPlayer("not so random", "random-password").join()

        then:
        def e = thrown(CompletionException)
        assert e.cause.message == "Username doesn't match actor identity"
        assert e.cause.class == IllegalArgumentException
    }
}
