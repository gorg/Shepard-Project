package shepard.backend.clustering

import cloud.orbit.actors.Actor
import cloud.orbit.actors.Stage
import cloud.orbit.actors.runtime.NodeCapabilities
import shepard.backend.ManyStageSpec
import shepard.backend.service.matchmaking.RoomService
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.details.Rules
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.backend.service.player.Player
import spock.lang.Shared
import spock.lang.Stepwise

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 08/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class ThreeStageLobbyTest extends ManyStageSpec
{
    @Shared Stage stage1
    @Shared Stage stage2
    @Shared Stage stage3
    @Shared int lobbies = 1
    @Shared int playersCount = 64
    @Shared RoomService ls
    @Shared Rules rules = new Rules()
    @Shared String cluster = "Three Stage Lobby"

    def "Start stages"()
    {
        stage1 = startStage(cluster)
        stage2 = startStage(cluster)
        stage3 = startStage(cluster)
        stage3.bind()

        expect:
        stage1.state == NodeCapabilities.NodeState.RUNNING
        stage2.state == NodeCapabilities.NodeState.RUNNING
        stage3.state == NodeCapabilities.NodeState.RUNNING
    }

    def "Register a lobby service"()
    {
        setup:
        def manager = Actor.getReference(RoomManager)

        def details = new RoomDetails
        (
                name: "Test lobby",
                rules: rules
        )
        ls = manager.registerService(details).join()

        expect:
        manager.allEnabledServices.join().size() == 1
    }

    def "Check with players"()
    {
        expect:
        (1..playersCount).each
        {
            def player = Actor.getReference(Player, "$it")
            assert player.getCurrentLobby().join() == null
        }
    }

    def "Kill one stage"()
    {
        when:
        stage1.stop().join()

        then:
        stage1.state == NodeCapabilities.NodeState.STOPPED
    }


    def "Add players to the lobbies"()
    {
        expect:
        (1..playersCount).each
        {
            def player = Actor.getReference(Player, "$it")
            def lobby = ls.getEntry(player).join()
            assert player.getCurrentLobby().join() == lobby
        }

        assert ls.lobbies.join().size() == (playersCount/rules.players).intValue()
    }

    def "Kill another stage"()
    {
        when:
        stage2.stop().join()

        then:
        stage2.state == NodeCapabilities.NodeState.STOPPED
    }

    def "Check lobbies with counter"()
    {
        expect:
        def counter = -1
        (1..playersCount).each {

            if(it%rules.players == 1)
                counter++

            def player = Actor.getReference(Player, "$it")
            assert player.getCurrentLobby().join().identity.contains("$counter")
        }
    }

    def "Add a new stage"()
    {
        setup:
        stage1 = startStage(cluster)

        expect:
        stage1.state == NodeCapabilities.NodeState.RUNNING
    }

    def "Check that players are in the lobbies again"()
    {
        expect:
        (1..playersCount).each
        {
            def player = Actor.getReference(Player, "$it")
            def lobby = ls.getEntry(player).join()
            assert 1 == 1

        }
    }
}
