package shepard.backend.clustering

import cloud.orbit.actors.Actor
import cloud.orbit.actors.Stage
import cloud.orbit.actors.runtime.NodeCapabilities
import shepard.backend.ManyStageSpec
import shepard.backend.service.leaderboard.LeaderboardService
import shepard.backend.service.leaderboard.details.LeaderboardDetails
import shepard.backend.service.leaderboard.details.LeaderboardSort
import shepard.backend.service.leaderboard.details.LeaderboardType
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.backend.service.player.Player
import spock.lang.Shared
import spock.lang.Stepwise

import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 08/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class ThreeStageLeaderboardTest extends ManyStageSpec
{
    @Shared Stage stage1
    @Shared Stage stage2
    @Shared Stage stage3
    @Shared int leaderboardCount = 10
    @Shared int playersCount = 100
    @Shared String cluster = "Three Leaderboard Test"

    def "Start stages"()
    {
        stage1 = startStage(cluster)
        stage2 = startStage(cluster)
        stage3 = startStage(cluster)
        stage3.bind()

        expect:
        stage1.state == NodeCapabilities.NodeState.RUNNING
        stage2.state == NodeCapabilities.NodeState.RUNNING
        stage3.state == NodeCapabilities.NodeState.RUNNING
    }

    def "Register leaderboards"()
    {
        setup:
        def manager = Actor.getReference(LeaderboardManager)

        (1..leaderboardCount).each {

            def details = new LeaderboardDetails(
                    name: "Test Leaderboard $it",
                    lowerLimit: -100,
                    upperLimit: 1000,
                    type: LeaderboardType.ABSOLUTE,
                    sort: LeaderboardSort.HIGH,
                    icon: null
            )

            manager.registerService(details).join()
        }

        expect:
        manager.allEnabledServices.join().size() == leaderboardCount
    }

    def "Check with players"()
    {
        expect:
        (1..leaderboardCount).each
        {
            def leaderboard = Actor.getReference(LeaderboardService, name2Id("Test Leaderboard $it"))
            (1..playersCount).each
            {
                //Rank is 0 because no score was ever posted to the leaderboard
                def player = Actor.getReference(Player, "$it")
                assert leaderboard.getEntry(player).join().getRank().join().toInteger() == 0
                assert player.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 0
            }
        }
    }

    def "Kill one stage"()
    {
        when:
        stage1.stop().join()

        then:
        stage1.state == NodeCapabilities.NodeState.STOPPED
    }

    def "Check again with the players"()
    {
        expect:
        (1..leaderboardCount).each
        {
            def leaderboard = Actor.getReference(LeaderboardService, name2Id("Test Leaderboard $it"))
            (1..playersCount).each
            {
                //Rank is 0 because no score was ever posted to the leaderboard
                def player = Actor.getReference(Player, "$it")
                assert leaderboard.getEntry(player).join().getRank().join().toInteger() == 0
                assert player.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 0
            }
        }
    }

    def "Post score to all leaderboards for all players"()
    {
        expect:
        (1..leaderboardCount).each
        {
            def leaderboard = Actor.getReference(LeaderboardService, name2Id("Test Leaderboard $it"))
            (1..playersCount).each
            {
                def player = Actor.getReference(Player, "$it")
                assert leaderboard.insertScore(player,(2*playersCount-it).toLong()).join()
                assert player.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 0
            }
        }
    }

    def "Perform ranking operation for each leaderboard"()
    {
        expect:
        (1..leaderboardCount).each
        {
            def leaderboard = Actor.getReference(LeaderboardService, name2Id("Test Leaderboard $it"))
            leaderboard.rank().join()
            (1..playersCount).each
            {
                def player = Actor.getReference(Player, "$it")
                assert player.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() != 0

            }
        }
    }

    def "Check with ranking list 0"()
    {
        expect:
        (1..leaderboardCount).each {

            def leaderboard = Actor.getReference(LeaderboardService, name2Id("Test Leaderboard $it"))
            def top = leaderboard.getRankedTop(playersCount).join()

            (1..playersCount).each { p ->
                def player = Actor.getReference(Player, "$p")
                top.get(p-1).player.join() == player
                top.get(p-1).score.join().rawScore.join() == (2*playersCount-p).toLong()
            }
        }
    }

    def "Kill another stage"()
    {
        when:
        stage2.stop().join()

        then:
        stage2.state == NodeCapabilities.NodeState.STOPPED
    }

    def "Check ranking for every player and leaderboard"()
    {
        expect:
        (1..leaderboardCount).each
        {
            def leaderboard = Actor.getReference(LeaderboardService, name2Id("Test Leaderboard $it"))
            (1..playersCount).each
            {
                def player = Actor.getReference(Player, "$it")
                assert leaderboard.getEntry(player).join().getRank().join().toInteger() == it
                assert player.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == it
                assert player.getRankForLeaderboard(leaderboard).join().getScore().join().rawScore.join().toInteger() == (2*playersCount-it)
            }
        }
    }

    def "Add a new stage"()
    {
        setup:
        stage1 = startStage(cluster)

        expect:
        stage1.state == NodeCapabilities.NodeState.RUNNING
    }

    def "Post another score to all leaderboards for all players"()
    {
        expect:
        (1..leaderboardCount).each
        {
            def leaderboard = Actor.getReference(LeaderboardService, name2Id("Test Leaderboard $it"))
            (1..playersCount).each
            {
                def player = Actor.getReference(Player, "$it")
                assert leaderboard.insertScore(player,(4*playersCount-it).toLong()).join()
                assert player.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == it
            }
        }
    }

    def "Check with ranking list 1"()
    {
        expect:
        (1..leaderboardCount).each {

            def leaderboard = Actor.getReference(LeaderboardService, name2Id("Test Leaderboard $it"))
            def top = leaderboard.getRankedTop(playersCount).join()

            (1..playersCount).each { p ->
                def player = Actor.getReference(Player, "$p")
                top.get(p-1).player.join() == player
                top.get(p-1).score.join().rawScore.join() == (4*playersCount-p).toLong()
            }
        }
    }

    def "Perform ranking for each leaderboard"()
    {
        expect:
        (1..leaderboardCount).each
        {
            def leaderboard = Actor.getReference(LeaderboardService, name2Id("Test Leaderboard $it"))
            leaderboard.rank().join()
            (1..playersCount).each
            {
                def player = Actor.getReference(Player, "$it")
                assert player.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() != 0
            }
        }
    }

    def "Check with ranking list 2 "()
    {
        expect:
        (1..leaderboardCount).each {

            def leaderboard = Actor.getReference(LeaderboardService, name2Id("Test Leaderboard $it"))
            def top = leaderboard.getRankedTop(playersCount).join()

            (1..playersCount).each { p ->
                def player = Actor.getReference(Player, "$p")
                top.get(p-1).player.join() == player
                top.get(p-1).score.join().rawScore.join() == (4*playersCount-p).toLong()
            }
        }
    }

    def "Check again the ranking for every player and leaderboard"()
    {
        expect:
        (1..leaderboardCount).each
        {
            def leaderboard = Actor.getReference(LeaderboardService, name2Id("Test Leaderboard $it"))
            (1..playersCount).each
            {
                def player = Actor.getReference(Player, "$it")
                assert leaderboard.getEntry(player).join().getRank().join().toInteger() == it
                assert player.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == it
                assert player.getRankForLeaderboard(leaderboard).join().getScore().join().rawScore.join().toInteger() == (4*playersCount-it)
            }
        }
    }

    def "Check with ranking list 3"()
    {
        expect:
        (1..leaderboardCount).each {

            def leaderboard = Actor.getReference(LeaderboardService, name2Id("Test Leaderboard $it"))
            def top = leaderboard.getRankedTop(playersCount).join()

            (1..playersCount).each { p ->
                def player = Actor.getReference(Player, "$p")
                top.get(p-1).player.join() == player
                top.get(p-1).score.join().rawScore.join() == (4*playersCount-p).toLong()
            }
        }
    }
}
