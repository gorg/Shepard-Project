package shepard.backend.clustering

import cloud.orbit.actors.Actor
import cloud.orbit.actors.Stage
import cloud.orbit.actors.runtime.NodeCapabilities
import shepard.backend.ManyStageSpec
import shepard.backend.service.player.Player
import shepard.backend.service.stats.StatService
import shepard.backend.service.stats.details.StatDetails
import shepard.backend.service.stats.manager.StatManager
import spock.lang.Shared
import spock.lang.Stepwise

import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 08/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class ThreeStageStatTest extends ManyStageSpec
{
    @Shared Stage stage1
    @Shared Stage stage2
    @Shared Stage stage3
    @Shared int statCount = 2
    @Shared int playersCount = 10
    @Shared String cluster = "Three Stage Stat"

    def "Start stages"()
    {
        stage1 = startStage(cluster)
        stage2 = startStage(cluster)
        stage3 = startStage(cluster)
        stage3.bind()

        expect:
        stage1.state == NodeCapabilities.NodeState.RUNNING
        stage2.state == NodeCapabilities.NodeState.RUNNING
        stage3.state == NodeCapabilities.NodeState.RUNNING
    }

    def "Register stats"()
    {
        setup:
        def manager = Actor.getReference(StatManager)

        (1..statCount).each {

            def details = new StatDetails(
                    name: "Test Stat $it",
                    defaultValue: 0,
                    maxValue: 5,
                    minValue: 0,
                    incrementOnly: true,
                    maxChange: 1,
            )
            manager.registerService(details).join()
        }

        expect:
        manager.allEnabledServices.join().size() == statCount
    }

    def "Check with players"()
    {
        expect:
        (1..statCount).each
        {
            def stat = Actor.getReference(StatService, name2Id("Test Stat $it"))
            (1..playersCount).each
            {
                def player = Actor.getReference(Player, "$it")
                assert stat.getEntry(player).join().statValue.join() == null
                assert player.getStat(stat).join().statValue.join() == null
            }
        }
    }

    def "Kill one stage"()
    {
        when:
        stage1.stop().join()

        then:
        stage1.state == NodeCapabilities.NodeState.STOPPED
    }

    def "Check again with the players"()
    {
        expect:
        (1..statCount).each
        {
            def stat = Actor.getReference(StatService, name2Id("Test Stat $it"))
            (1..playersCount).each
            {
                def player = Actor.getReference(Player, "$it")
                assert stat.getEntry(player).join().statValue.join() == null
                assert player.getStat(stat).join().statValue.join() == null
            }
        }
    }

    def "Kill another stage"()
    {
        when:
        stage2.stop().join()

        then:
        stage2.state == NodeCapabilities.NodeState.STOPPED
    }

    def "Post a stat update for all stats and all players"()
    {
        expect:
        (1..statCount).each
        {
            def stat = Actor.getReference(StatService, name2Id("Test Stat $it"))
            (1..playersCount).each
            {
                def player = Actor.getReference(Player, "$it")
                stat.update(1,player).join().statValue.join() as Integer == 1
                assert player.getStat(stat).join().statValue.join() as Integer == 1
            }
        }
    }

    def "Add a new stage"()
    {
        setup:
        stage1 = startStage(cluster)

        expect:
        stage1.state == NodeCapabilities.NodeState.RUNNING
    }

    def "Post another stat update for all stats and all players"()
    {
        expect:
        (1..statCount).each
        {
            def stat = Actor.getReference(StatService, name2Id("Test Stat $it"))
            (1..playersCount).each
            {
                def player = Actor.getReference(Player, "$it")
                stat.update(2,player).join().statValue.join() as Integer == 2
            }
        }
    }



    def "Check stat values with all players"()
    {
        expect:
        (1..statCount).each
        {
            def stat = Actor.getReference(StatService, name2Id("Test Stat $it"))
            (1..playersCount).each
            {
                def player = Actor.getReference(Player, "$it")
                assert stat.getEntry(player).join().statValue.join() as Integer == 2
            }
        }
    }

}
