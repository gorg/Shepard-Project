package shepard.backend.clustering

import cloud.orbit.actors.Actor
import cloud.orbit.actors.Stage
import cloud.orbit.actors.runtime.NodeCapabilities
import shepard.backend.ManyStageSpec
import shepard.backend.service.achievement.AchievementService
import shepard.backend.service.achievement.details.AchievementDetails
import shepard.backend.service.achievement.details.AchievementType
import shepard.backend.service.achievement.manager.AchievementManager
import shepard.backend.service.player.Player
import spock.lang.Shared
import spock.lang.Stepwise

import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 08/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class ThreeStageAchievementTest extends ManyStageSpec
{
    @Shared Stage stage1
    @Shared Stage stage2
    @Shared Stage stage3
    @Shared int achievementsCount = 15
    @Shared int playersCount = 100
    @Shared String cluster = "Three Achievement Test"

    def "Start stages"()
    {
        stage1 = startStage(cluster)
        stage2 = startStage(cluster)
        stage3 = startStage(cluster)
        stage3.bind()

        expect:
        stage1.state == NodeCapabilities.NodeState.RUNNING
        stage2.state == NodeCapabilities.NodeState.RUNNING
        stage3.state == NodeCapabilities.NodeState.RUNNING
    }

    def "Register achievements"()
    {
        setup:
        def manager = Actor.getReference(AchievementManager)

        (1..achievementsCount).each {

            def details = new AchievementDetails(
                    name: "Test Achievement $it",
                    description: "Multi stage testing achievement",
                    type: AchievementType.HIDDEN,
                    icon: null
            )

            manager.registerService(details).join()
        }

        expect:
        manager.allEnabledServices.join().size() == achievementsCount
    }

    def "Check with players"()
    {
        expect:
        (1..achievementsCount).each
        {
            def achievement = Actor.getReference(AchievementService, name2Id("Test Achievement $it"))
            (1..playersCount).each
            {
                def player = Actor.getReference(Player, "$it")
                assert !achievement.isCompleted(player).join()
                assert !player.isAchievementCompleted(achievement).join().isCompleted().join()
            }
        }
    }

    def "Kill one stage"()
    {
        when:
        stage1.stop().join()

        then:
        stage1.state == NodeCapabilities.NodeState.STOPPED
    }

    def "Check again with the players"()
    {
        expect:
        (1..achievementsCount).each
        {
            def achievement = Actor.getReference(AchievementService, name2Id("Test Achievement $it"))
            (1..playersCount).each
            {
                def player = Actor.getReference(Player, "$it")
                assert !achievement.isCompleted(player).join()
                assert !player.isAchievementCompleted(achievement).join().isCompleted().join()
            }
        }
    }

    def "Complete all achievements for all players"()
    {
        expect:
        (1..achievementsCount).each
        {
            def achievement = Actor.getReference(AchievementService, name2Id("Test Achievement $it"))
            (1..playersCount).each
            {
                def player = Actor.getReference(Player, "$it")
                assert achievement.completeAchievement(player).join()
            }
        }
    }

    def "Kill another stage"()
    {
        when:
        stage2.stop().join()

        then:
        stage2.state == NodeCapabilities.NodeState.STOPPED
    }

    def "Check that all achievements are completed"()
    {
        expect:
        (1..achievementsCount).each
        {
            def achievement = Actor.getReference(AchievementService, name2Id("Test Achievement $it"))
            (1..playersCount).each
            {
                def player = Actor.getReference(Player, "$it")
                assert achievement.isCompleted(player).join()
                assert player.isAchievementCompleted(achievement).join().isCompleted().join()
            }
        }
    }

    def "Add a new stage"()
    {
        setup:
        stage1 = startStage(cluster)

        expect:
        stage1.state == NodeCapabilities.NodeState.RUNNING
    }

    def "Check again that all achievements are completed"()
    {
        expect:
        (1..achievementsCount).each
        {
            def achievement = Actor.getReference(AchievementService, name2Id("Test Achievement $it"))
            (1..playersCount).each
            {
                def player = Actor.getReference(Player, "$it")
                assert achievement.isCompleted(player).join()
                assert player.isAchievementCompleted(achievement).join().isCompleted().join()
            }
        }
    }

}
