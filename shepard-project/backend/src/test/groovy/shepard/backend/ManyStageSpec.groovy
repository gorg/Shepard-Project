package shepard.backend

import cloud.orbit.actors.Stage
import cloud.orbit.actors.extensions.json.InMemoryJSONStorageExtension
import cloud.orbit.actors.runtime.NodeCapabilities
import com.ea.async.Async
import spock.lang.Specification

import java.util.concurrent.ConcurrentHashMap

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/11/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class ManyStageSpec extends Specification
{
    private static ArrayList<Stage> stages = new ArrayList<>()
    private static int counter = 0
    private static ConcurrentHashMap<Object, Object> fakeDatabase = new ConcurrentHashMap<>()

    def setupSpec()
    {
        Async.init()
    }

    Stage startStage(String clusterName)
    {
        Stage stage = new Stage.Builder()
            .extensions(new InMemoryJSONStorageExtension(fakeDatabase))
            .mode(Stage.StageMode.HOST)
            .nodeName("stage-$counter")
            .clusterName(clusterName)
            .build()

        stage.start().join()
        counter++
        stages.add(stage)
        return stage
    }

    def cleanupSpec()
    {
        stages.each {
           if (it.state == NodeCapabilities.NodeState.RUNNING)
               it.stop().join()
        }
    }
}


