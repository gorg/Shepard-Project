package shepard.backend

import cloud.orbit.actors.Stage
import cloud.orbit.actors.extensions.json.InMemoryJSONStorageExtension
import com.ea.async.Async
import spock.lang.Specification

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 10/27/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class OneStageSpec extends Specification
{
    static Stage stage

    def setupSpec()
    {
        Async.init()
        stage = new Stage()
        def id = Long.toHexString(System.currentTimeMillis())
        stage.setClusterName(id)
        stage.setNodeName(id)
        stage.addExtension(new InMemoryJSONStorageExtension())
        stage.setMode(Stage.StageMode.HOST)
        stage.start().join()
        stage.bind()
    }

    def cleanupSpec()
    {
        stage.stop().join()
    }
}
