package shepard.backend.service.matchmaking

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.details.Rules
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.backend.service.player.Player
import spock.lang.Shared

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 02/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
//@Stepwise
class LobbyAssigment extends OneStageSpec
{
    @Shared RoomService roomOne
    @Shared RoomService roomTwo
    @Shared Player player

    def "Setup 2 lobbies"()
    {
        def manager = Actor.getReference(RoomManager)

        def room1 = new RoomDetails(name: '1vs1', rules: new Rules(players: 2, teams: 2))
        def room2 = new RoomDetails(name: '2vs2', rules: new Rules(players: 4, teams: 2))

        roomOne = manager.registerService(room1).join()
        roomTwo = manager.registerService(room2).join()

        expect:
        manager.getAllEnabledServices().join().size() == 2
    }

    def "Add players to the lobbies"()
    {
        def lobby1 = roomOne.getEntry(Actor.getReference(Player,'p1')).join()
        def lobby2 = roomOne.getEntry(Actor.getReference(Player,'p2')).join()

        def lobby3 = roomOne.getEntry(Actor.getReference(Player,'p3')).join()

        def lobby4 = roomTwo.getEntry(Actor.getReference(Player,'p4')).join()
        def lobby5 = roomTwo.getEntry(Actor.getReference(Player,'p5')).join()
        def lobby6 = roomTwo.getEntry(Actor.getReference(Player,'p6')).join()
        def lobby7 = roomTwo.getEntry(Actor.getReference(Player,'p7')).join()

        def lobby8 = roomTwo.getEntry(Actor.getReference(Player,'p8')).join()
        def lobby9 = roomTwo.getEntry(Actor.getReference(Player,'p9')).join()

        def all = [lobby1,lobby2,lobby3,lobby4,lobby5,lobby6,lobby7,lobby8,lobby9]

        expect:
        lobby1 == lobby2

        def allWithoutFirst = all - [lobby1,lobby2]
        allWithoutFirst.each {
            it != lobby1
            it != lobby2
        }

        def allWithoutThird = all - [lobby3]
        allWithoutThird.each {
            it != lobby3
        }

        lobby4 == lobby5
        lobby5 == lobby6
        lobby6 == lobby7

        def allWithoutFifth = all - [lobby4,lobby5,lobby6,lobby7]
        allWithoutFifth.each {
            it != lobby4
            it != lobby5
            it != lobby6
            it != lobby7
        }

        lobby8 == lobby9
        def allWithoutEighth = all - [lobby8,lobby9]
        allWithoutEighth.each {
            it != lobby8
            it != lobby9
        }
    }
}
