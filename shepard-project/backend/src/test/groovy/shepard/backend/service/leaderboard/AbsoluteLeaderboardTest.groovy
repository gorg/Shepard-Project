package shepard.backend.service.leaderboard

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.leaderboard.details.LeaderboardDetails
import shepard.backend.service.leaderboard.details.LeaderboardSort
import shepard.backend.service.leaderboard.details.LeaderboardType
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.backend.service.player.Player
import spock.lang.Shared

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/8/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class AbsoluteLeaderboardTest extends OneStageSpec
{
    @Shared LeaderboardManager manager
    @Shared LeaderboardService leaderboard
    @Shared Player player1
    @Shared Player player2
    @Shared Player player3

    def "Set the test leaderboard"()
    {
        manager = Actor.getReference(LeaderboardManager)

        leaderboard = manager.registerService(
                new LeaderboardDetails(
                        name: "Test Leaderboard",
                        icon: null,
                        lowerLimit: 0,
                        upperLimit: 100,
                        type: LeaderboardType.ABSOLUTE,
                        sort: LeaderboardSort.HIGH
                )).join()

        player1 = Actor.getReference(Player, "player1")
        player2 = Actor.getReference(Player, "player2")
        player3 = Actor.getReference(Player, "player3")

        expect:
        //Used for actor activation
        assert player1.getCurrentGame().join() == null
        assert player2.getCurrentGame().join() == null
        assert player3.getCurrentGame().join() == null
    }


    def "Activating player actors"()
    {
        setup:
        assert player1.getCurrentGame().join() == null
        assert player2.getCurrentGame().join() == null
        assert player3.getCurrentGame().join() == null

        expect:
        player1.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 0
        player3.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 0
        player2.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 0
    }

    def "Publishing scores to the leaderboard and rank calculation"()
    {
        setup:
        leaderboard.insertScore(player1, 10).join()
        leaderboard.insertScore(player2, 20).join()
        leaderboard.insertScore(player3, 30).join()

        //Rank hasn't change cause is not calculated yet
        expect:
        player1.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 0
        player2.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 0
        player3.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 0

        //But scores are set
        player1.getScoreForLeaderboard(leaderboard).join().getRawScore().join().toInteger() == 10
        player2.getScoreForLeaderboard(leaderboard).join().getRawScore().join().toInteger() == 20
        player3.getScoreForLeaderboard(leaderboard).join().getRawScore().join().toInteger() == 30

        when:
        leaderboard.rank().join()

        //Ranks are now set
        then:
        player1.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 3
        player2.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 2
        player3.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 1

    }

    def "Changing scores and rank"()
    {
        setup:
        leaderboard.insertScore(player1, 60).join()
        leaderboard.insertScore(player2, 50).join()
        leaderboard.insertScore(player3, 40).join()

        //Rank hasn't change cause is not calculated yet
        expect:
        player1.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 3
        player2.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 2
        player3.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 1

        //But scores are set
        player1.getScoreForLeaderboard(leaderboard).join().getRawScore().join().toInteger() == 60
        player2.getScoreForLeaderboard(leaderboard).join().getRawScore().join().toInteger() == 50
        player3.getScoreForLeaderboard(leaderboard).join().getRawScore().join().toInteger() == 40

        when:
        leaderboard.rank().join()

        //New ranks are ready
        then:
        player1.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 1
        player2.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 2
        player3.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 3

    }


    def "Publishing an invalid score (under the lower limit)"()
    {
        //Leaderboard limits are from 0 to 100 points
        setup:
        leaderboard.insertScore(player1, -10).join()

        //An invalid score was tried to be published into the leaderboard. The score wasn't change
        expect:
        player1.getScoreForLeaderboard(leaderboard).join().getRawScore().join().toInteger() == 60
    }

    def "Publishing an invalid score (above the upper limit)"()
    {
        //Leaderboard limits are from 0 to 100 points
        setup:
        leaderboard.insertScore(player1, 110).join()

        //An invalid score was tried to be published into the leaderboard. The score wasn't change
        expect:
        player1.getScoreForLeaderboard(leaderboard).join().getRawScore().join().toInteger() == 60
    }
}
