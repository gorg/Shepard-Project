package shepard.backend.service.stats

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.player.Player
import shepard.backend.service.stats.cache.StatCache
import shepard.backend.service.stats.details.StatDetails
import shepard.backend.service.stats.manager.StatManager
import spock.lang.Shared
import spock.lang.Stepwise

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/17/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
@Stepwise
class SimpleStatTest extends OneStageSpec
{
    @Shared StatManager manager
    @Shared StatService stat
    @Shared StatService stat2

    def setup()
    {
        manager = Actor.getReference(StatManager)
    }

    def "Create a new stat"()
    {
        setup:
        stat = manager.registerService(new StatDetails(name: "Kills")).join()
        expect:
        manager.getAllEnabledServices().join().size() == 1
        manager.getAllEnabledServices().join().first() == stat
    }

    def "Cache lookup"()
    {
        setup:
        def stats = Actor.getReference(StatCache).retrieve().join()

        expect:
        assert stats.size() == 1
        assert stats.contains(stat)

    }

    def "Cache notification system"()
    {
        setup:
        stat2 = manager.registerService(new StatDetails(name: "More kills")).join()
        def stats = Actor.getReference(StatCache).retrieve().join()

        expect:
        assert stats.size() == 2
        assert stats.contains(stat)
        assert stats.contains(stat2)
    }

    def "Player with stats"()
    {
        setup:
        def player = Actor.getReference(Player,"random-player")

        expect:
        assert player.getStat(stat).join().getStatValue().join() == null
        assert player.getStat(stat2).join().getStatValue().join() == null

        when:
        stat.update(10, player).join()
        stat2.update(1f, player).join()

        then:
        assert player.getStat(stat2).join().getStatValue().join() == 1f
        assert player.getStat(stat).join().getStatValue().join() == 10

    }

    def "Query service for player"()
    {
        setup:
        def player = Actor.getReference(Player,"random-player")

        expect:
        stat.getEntry(player).join().statValue.join() == 10
        stat2.getEntry(player).join().statValue.join() == 1f
    }

}

