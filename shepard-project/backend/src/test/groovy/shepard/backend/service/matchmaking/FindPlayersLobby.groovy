package shepard.backend.service.matchmaking

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.details.Rules
import shepard.backend.service.matchmaking.entry.Lobby
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.backend.service.player.Player
import spock.lang.Shared
import spock.lang.Stepwise

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 07/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class FindPlayersLobby extends OneStageSpec
{
    @Shared RoomService room
    @Shared RoomService secondRoom
    @Shared Lobby lobby1
    @Shared Lobby lobby2
    @Shared Lobby lobby3
    @Shared Lobby lobby4
    @Shared Lobby lobby5
    @Shared Lobby lobby6


    def "Create one room"()
    {
        def details = new RoomDetails(name: 'Test Room', rules: new Rules(players: 2, teams: 2))
        room = Actor.getReference(RoomManager).registerService(details).join()

        expect:
        Actor.getReference(RoomManager).allEnabledServices.join().size() == 1
    }


    def "Add players"()
    {
        def player1 = Actor.getReference(Player, 'p1')
        def player2 = Actor.getReference(Player, 'p2')
        def player3 = Actor.getReference(Player, 'p3')
        def player4 = Actor.getReference(Player, 'p4')
        def player5 = Actor.getReference(Player, 'p5')

        Lobby lobbyOne = room.getEntry(player1).join()
        Lobby lobbyTwo = room.getEntry(player2).join()

        assert lobbyOne == lobbyTwo
        assert lobbyTwo != null
        lobby1 = lobbyTwo

        lobbyOne = room.getEntry(player3).join()
        lobbyTwo = room.getEntry(player4).join()

        assert lobbyOne == lobbyTwo
        assert lobbyTwo != null
        lobby2 = lobbyTwo

        lobby3 = room.getEntry(player5).join()
        assert lobby3 != lobby1
        assert lobby3 != lobby1
        assert lobby3 != null

        expect:
        room.getLobbies().join().size() == 3
    }

    def "Find players with manager"()
    {
        def manager = Actor.getReference(RoomManager)

        expect:
        manager.findPlayer('p1').join() == lobby1
        manager.findPlayer('p2').join() == lobby1
        manager.findPlayer('p3').join() == lobby2
        manager.findPlayer('p4').join() == lobby2
        manager.findPlayer('p5').join() == lobby3

        and:
        manager.findPlayer('not there').join() == null
    }


    def "Add a second room"()
    {
        def details = new RoomDetails(name: 'Test Room 2', rules: new Rules(players: 2, teams: 2))
        secondRoom = Actor.getReference(RoomManager).registerService(details).join()

        expect:
        Actor.getReference(RoomManager).allEnabledServices.join().size() == 2
    }

    def "Try again to find players with manager"()
    {
        def manager = Actor.getReference(RoomManager)

        expect:
        manager.findPlayer('p1').join() == lobby1
        manager.findPlayer('p2').join() == lobby1
        manager.findPlayer('p3').join() == lobby2
        manager.findPlayer('p4').join() == lobby2
        manager.findPlayer('p5').join() == lobby3

        and:
        manager.findPlayer('not there').join() == null
    }

    def "Add players to the second room"()
    {
        def player1 = Actor.getReference(Player, 'p6')
        def player2 = Actor.getReference(Player, 'p7')
        def player3 = Actor.getReference(Player, 'p8')
        def player4 = Actor.getReference(Player, 'p9')
        def player5 = Actor.getReference(Player, 'p10')

        Lobby lobbyOne = secondRoom.getEntry(player1).join()
        Lobby lobbyTwo = secondRoom.getEntry(player2).join()

        assert lobbyOne == lobbyTwo
        assert lobbyTwo != null
        lobby4 = lobbyTwo

        lobbyOne = secondRoom.getEntry(player3).join()
        lobbyTwo = secondRoom.getEntry(player4).join()

        assert lobbyOne == lobbyTwo
        assert lobbyTwo != null
        lobby5 = lobbyTwo

        lobby6 = secondRoom.getEntry(player5).join()
        assert lobby6 != lobby4
        assert lobby6 != lobby5
        assert lobby6 != null

        def firstRoomLobbies = [lobby1,lobby2,lobby3]
        def secondRoomLobbies = [lobby4,lobby5,lobby6]

        expect:
        firstRoomLobbies.each { first ->
            secondRoomLobbies.each { second ->
                assert first != second
            }
        }

        and:
        secondRoom.getLobbies().join().size() == 3
    }

    def "Find all the players with manager"()
    {
        def manager = Actor.getReference(RoomManager)

        expect:
        manager.findPlayer('p1').join() == lobby1
        manager.findPlayer('p2').join() == lobby1
        manager.findPlayer('p3').join() == lobby2
        manager.findPlayer('p4').join() == lobby2
        manager.findPlayer('p5').join() == lobby3
        manager.findPlayer('p6').join() == lobby4
        manager.findPlayer('p7').join() == lobby4
        manager.findPlayer('p8').join() == lobby5
        manager.findPlayer('p9').join() == lobby5
        manager.findPlayer('p10').join() == lobby6

        and:
        manager.findPlayer('not there').join() == null
    }
}
