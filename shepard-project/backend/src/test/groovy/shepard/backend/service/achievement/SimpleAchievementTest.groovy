package shepard.backend.service.achievement

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.achievement.details.AchievementDetails
import shepard.backend.service.achievement.details.AchievementType
import shepard.backend.service.achievement.entry.Achievement
import shepard.backend.service.achievement.manager.AchievementManager
import shepard.backend.service.player.Player
import spock.lang.Shared
import spock.lang.Stepwise

import java.time.LocalDateTime
import java.util.concurrent.CompletionException

import static shepard.backend.util.id.GenerateId.generateId

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 20/10/16.
 * email: gueorgui.tzvetoslavov@gmail.com
 */
@Stepwise
class SimpleAchievementTest extends OneStageSpec
{
    @Shared AchievementManager manager
    @Shared AchievementService achievement1
    @Shared AchievementService achievement2
    @Shared AchievementService achievement3
    @Shared Player player1
    @Shared Player player2

    def "Set some achievements"()
    {
        manager = Actor.getReference(AchievementManager)
        def details1 = new AchievementDetails(
                name:  "test achievements",
                description:  "some random description",
                type:  AchievementType.REVEALED)

        achievement1 = manager.registerService(details1).join()

        def details2 = new AchievementDetails(
                name:  "test achievements 2",
                description:  "some random description 2",
                type: AchievementType.HIDDEN)

        achievement2 = manager.registerService(details2).join()

        expect:
        manager.getAllEnabledServices().join().size() == 2
    }


    def "Player gets available achievements upon activation"()
    {
        setup:
        player1 = Actor.getReference(Player, "player1")

        expect:
        !player1.isAchievementCompleted(achievement1).join().isCompleted().join()
        !player1.isAchievementCompleted(achievement2).join().isCompleted().join()
    }


    def "Another achievement is created"()
    {
        setup:
        def details = new AchievementDetails(
                            name: "Another test achievement",
                            description:  "Some description",
                            type:  AchievementType.HIDDEN)
        achievement3 = manager.registerService(details).join()

        player2 = Actor.getReference(Player, "player2")

        expect:
        !player1.isAchievementCompleted(achievement1).join().isCompleted().join()
        !player1.isAchievementCompleted(achievement2).join().isCompleted().join()
        !player1.isAchievementCompleted(achievement3).join().isCompleted().join()

        !player2.isAchievementCompleted(achievement1).join().isCompleted().join()
        !player2.isAchievementCompleted(achievement2).join().isCompleted().join()
        !player2.isAchievementCompleted(achievement3).join().isCompleted().join()
    }

    def "Completing achievements"()
    {
        when:
        achievement1.completeAchievement(player1).join()

        then:
        achievement1.isCompleted(player1).join()
        player1.isAchievementCompleted(achievement1).join().isCompleted().join()

        when:
        achievement2.completeAchievement(player2).join()

        then:
        achievement2.isCompleted(player2).join()
        player2.isAchievementCompleted(achievement2).join().isCompleted().join()

        expect:
        player1.isAchievementCompleted(achievement1).join().isCompleted().join()
        !player1.isAchievementCompleted(achievement2).join().isCompleted().join()
        !player1.isAchievementCompleted(achievement3).join().isCompleted().join()

        !player2.isAchievementCompleted(achievement1).join().isCompleted().join()
        player2.isAchievementCompleted(achievement2).join().isCompleted().join()
        !player2.isAchievementCompleted(achievement3).join().isCompleted().join()
    }

    def "Retrieving information from the achievements"()
    {
        setup:
        def details = achievement1.getServiceDetails().join()

        expect:
        assert details.icon == null
        assert details.name == "test achievements"
        assert details.description == "some random description"
        assert details.type == AchievementType.REVEALED

    }

    def "Illegal achievement access"()
    {
        when:
        def entry = Actor.getReference(Achievement, generateId(achievement1,player1))
        entry = entry.setupEntry(achievement2,player2).join()

        then:
        thrown (CompletionException)
    }

    def "When was completed"()
    {
        when:
        def time = achievement1.whenWasCompleted(player1).join()
        def now = LocalDateTime.now()


        then:
        time.dayOfMonth == now.dayOfMonth
        time.dayOfWeek == now.dayOfWeek
        time.dayOfYear == now.dayOfYear
        time.hour == now.hour
        time.minute == now.minute
        time.month == now.month
    }
}
