package shepard.backend.service.leaderboard

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.leaderboard.details.LeaderboardDetails
import shepard.backend.service.leaderboard.details.LeaderboardSort
import shepard.backend.service.leaderboard.details.LeaderboardType
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.backend.service.player.Player
import spock.lang.Shared
import spock.lang.Stepwise

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/8/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
@Stepwise
class IncrementalLeaderboardTest extends OneStageSpec
{
    @Shared LeaderboardManager manager
    @Shared LeaderboardService leaderboard
    @Shared Player player1
    @Shared Player player2
    @Shared Player player3

    def "Setup a test leaderboard"()
    {
        manager = Actor.getReference(LeaderboardManager)
        leaderboard = manager.registerService(
                new LeaderboardDetails(
                name:"test-leaderboard",
                icon:null,
                lowerLimit:0,
                upperLimit:100,
                type:LeaderboardType.INCREMENTAL,
                sort:LeaderboardSort.HIGH)).join()

        player1 = Actor.getReference(Player, "player1")
        player2 = Actor.getReference(Player, "player2")
        player3 = Actor.getReference(Player, "player3")

        expect:
        //Used for actor activation
        assert player1.getCurrentGame().join() == null
        assert player2.getCurrentGame().join() == null
        assert player3.getCurrentGame().join() == null

    }

    def "Activating player actors"()
    {
        setup:
        assert player1.getCurrentGame().join() == null
        assert player2.getCurrentGame().join() == null
        assert player3.getCurrentGame().join() == null

        expect:
        player1.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 0
        player3.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 0
        player2.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 0
    }

    def "Publishing scores to the leaderboard and rank calculation"()
    {
        setup:
        leaderboard.insertScore(player1, 10).join()
        leaderboard.insertScore(player2, 20).join()
        leaderboard.insertScore(player3, 30).join()

        //Rank hasn't change cause is not calculated yet
        expect:
        player1.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 0
        player2.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 0
        player3.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 0

        //But scores are set
        player1.getScoreForLeaderboard(leaderboard).join().getRawScore().join().toInteger() == 10
        player2.getScoreForLeaderboard(leaderboard).join().getRawScore().join().toInteger() == 20
        player3.getScoreForLeaderboard(leaderboard).join().getRawScore().join().toInteger() == 30

        when:
        leaderboard.rank().join()

        //Ranks are now set
        then:
        player1.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 3
        player2.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 2
        player3.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 1

    }

    def "Changing scores and rank"()
    {
        setup:
        leaderboard.insertScore(player1, 10).join()
        leaderboard.insertScore(player2, 10).join()
        leaderboard.insertScore(player3, -10).join()

        //Rank hasn't change cause is not calculated yet
        expect:
        player1.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 3
        player2.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 2
        player3.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 1

        //But scores are set
        player1.getScoreForLeaderboard(leaderboard).join().getRawScore().join().toInteger() == 20
        player2.getScoreForLeaderboard(leaderboard).join().getRawScore().join().toInteger() == 30
        player3.getScoreForLeaderboard(leaderboard).join().getRawScore().join().toInteger() == 20

        when:
        leaderboard.rank().join()

        //New ranks are ready
        then:
        player1.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 2
        player2.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 1
        player3.getRankForLeaderboard(leaderboard).join().getRank().join().toInteger() == 3

    }

    def "Decrementing score"()
    {
        setup:
        leaderboard.insertScore(player1, -10).join()

        expect:
        player1.getScoreForLeaderboard(leaderboard).join().getRawScore().join().toInteger() == 10
    }


    def "Decrementing score under the lower limit"()
    {
        //Leaderboard limits are from 0 to 100 points.
        //Current player score after previous test is 10
        //10 - 20 = -10 < lowerLimit
        setup:
        leaderboard.insertScore(player1, -20).join()

        //The score wasn't change
        expect:
        player1.getScoreForLeaderboard(leaderboard).join().getRawScore().join().toInteger() == 10
    }

    def "Incrementing score above the upper limit"()
    {
        //Leaderboard limits are from 0 to 100 points
        //Current player score after previous test is 30
        //30 + 80 = 110 > upperLimit
        setup:
        leaderboard.insertScore(player2, 80).join()

        //An invalid score was tried to be published into the leaderboard. The score wasn't change
        expect:
        player2.getScoreForLeaderboard(leaderboard).join().getRawScore().join().toInteger() == 30
    }
}
