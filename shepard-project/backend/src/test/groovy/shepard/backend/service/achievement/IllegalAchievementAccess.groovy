package shepard.backend.service.achievement

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.achievement.details.AchievementDetails
import shepard.backend.service.achievement.entry.Achievement
import shepard.backend.service.achievement.manager.AchievementManager
import shepard.backend.service.player.Player
import spock.lang.Stepwise

import java.util.concurrent.CompletionException

import static shepard.backend.util.id.GenerateId.generateId
import static spock.util.Exceptions.getRootCause

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 30/01/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class IllegalAchievementAccess extends OneStageSpec
{
    def "Set up the achievement and player entry" ()
    {
        setup:
        def manager = Actor.getReference(AchievementManager)
        def details = new AchievementDetails(name: "test achievements")

        when:
        def service = manager.registerService(details).join()

        then:
        manager.isEnabled(service).join()
    }

    def "Manually setup service entry"()
    {
        setup:
        def manager = Actor.getReference(AchievementManager)
        def service = manager.getAllEnabledServices().join().first()
        def player1 = Actor.getReference(Player, "player 1")
        def player2 = Actor.getReference(Player, "player 2")

        when:
        def falseAchievement = Actor.getReference(Achievement, generateId(service,player1)).setupEntry(service,player2).join()

        then:
        def ex = thrown(CompletionException)
        assert getRootCause(ex).class == IllegalAccessError
        assert getRootCause(ex).message == "Actor identity is not matching the arguments"


        when:
        def achievement = service.getEntry(player1).join()

        then:
        !achievement.isCompleted().join()
        achievement.player.join() != player2
        achievement.player.join() == player1
    }
}
