package shepard.backend.service.matchmaking

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.details.Rules
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.backend.service.player.Player
import spock.lang.Shared

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 12/13/16.
 * email: gueorgui.tzvetoslavov@gmail.com*/
class LobbyTest extends OneStageSpec
{
    @Shared lobbies = []
    @Shared RoomService lobbyFinder
    @Shared rating = 1500d
    @Shared rd = 200d
    @Shared Rules rules = new Rules(2,8,true)

    def "Players in range"()
    {
        lobbyFinder = Actor.getReference(RoomManager).registerService(new RoomDetails(name: "test lobby",rules: rules)).join()

        def player = Actor.getReference(Player, "random-player1")
        player.getSkill().join().setRating(rating).join()
        player.getSkill().join().setRatingDeviation(rd).join()
        lobbies << lobbyFinder.getEntry(player).join()
        assert lobbies[0].getLobbyRating().join() == player.getSkill().join().getRating().join()
        assert lobbies[0].getLobbyRatingDevation().join() == player.getSkill().join().getRatingDeviation().join()
        rating++
        rd++

        def player2 = Actor.getReference(Player, "random-player2")
        player2.getSkill().join().setRating(rating).join()
        player2.getSkill().join().setRatingDeviation(rd).join()
        lobbies <<  lobbyFinder.getEntry(player2).join()
        rating++
        rd++

        def player3 = Actor.getReference(Player, "random-player3")
        player3.getSkill().join().setRating(rating).join()
        player3.getSkill().join().setRatingDeviation(rd).join()
        lobbies << lobbyFinder.getEntry(player3).join()
        rating++
        rd++

        def player4 = Actor.getReference(Player, "random-player4")
        player4.getSkill().join().setRating(rating).join()
        player4.getSkill().join().setRatingDeviation(rd).join()
        lobbies << lobbyFinder.getEntry(player4).join()
        rating++
        rd++


        def player5 = Actor.getReference(Player, "random-player5")
        player5.getSkill().join().setRating(rating).join()
        player5.getSkill().join().setRatingDeviation(rd).join()
        lobbies << lobbyFinder.getEntry(player5).join()
        rating++
        rd++

        def player6 = Actor.getReference(Player, "random-player6")
        player6.getSkill().join().setRating(rating).join()
        player6.getSkill().join().setRatingDeviation(rd).join()
        lobbies << lobbyFinder.getEntry(player6).join()
        rating++
        rd++


        def player7 = Actor.getReference(Player, "random-player7")
        player7.getSkill().join().setRating(rating).join()
        player7.getSkill().join().setRatingDeviation(rd).join()
        lobbies << lobbyFinder.getEntry(player7).join()
        rating++
        rd++

        //All the players are in the same lobby
        expect:
        lobbies.each { first ->
            lobbies.each { second ->
                assert first == second
            }
        }
    }

    def "Player out of range"()
    {
        when:
        def player8 = Actor.getReference(Player, "random-player8")
        player8.getSkill().join().setRating(1000).join()
        player8.getSkill().join().setRatingDeviation(20).join()
        def lobby = lobbyFinder.getEntry(player8).join()

        //Player 8 has a new lobby because his skill doesn't match the skill of the other players
        then:
        lobbies.each { assert it != lobby }
    }

    def "Completing the lobby"()
    {
        when:
        def player9 = Actor.getReference(Player, "random-player9")
        player9.getSkill().join().setRating(rating).join()
        player9.getSkill().join().setRatingDeviation(rd).join()
        def player9lobby = lobbyFinder.getEntry(player9).join()

        then:
        lobbies.each { assert it == player9lobby }
    }

    def "Re entry"()
    {
        //If a player is already in a lobby it won't assign him to a new one
        when:
        def lobby = lobbyFinder.getEntry(Actor.getReference(Player, "random-player3")).join()

        then:
        lobbies.each { assert it == lobby }
    }
}
