package shepard.backend.service.matchmaking

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.details.Rules
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.backend.service.player.Player
import spock.lang.Shared
import spock.lang.Stepwise

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 04/05/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class LobbyEntryAndReEntry extends OneStageSpec
{
    @Shared RoomService room
    @Shared Player player

    def "setup a room"()
    {
        def manager = Actor.getReference(RoomManager)

        room = manager.registerService(new RoomDetails(name: "Test Room", rules: new Rules(players: 2,teams: 2))).join()

        expect:
        manager.getAllEnabledServices().join().size() == 1
        manager.getAllEnabledServices().join().first() == room
    }

    def "player enter the room"()
    {
        given:
        player = Actor.getReference(Player, 'random-player')

        when:
        def lobby = room.enterLobby(player).join()

        then:
        lobby.getLobbyPLayers().join().contains(player)

        and:
        room.lobbies.join().size() == 1
        player.getCurrentLobby().join() == lobby
    }

    def "player exits the lobby"()
    {
        when:
        room.exitLobby(player).join()

        then:
        room.lobbies.join().size() == 0

        and:
        player.getCurrentLobby().join() == null
    }

    def "player enters again"()
    {
        when:
        def lobby = room.enterLobby(player).join()

        then:
        lobby.getLobbyPLayers().join().contains(player)

        and:
        room.lobbies.join().size() == 1
        player.getCurrentLobby().join() == lobby
    }
}
