package shepard.backend.service.matchmaking

import cloud.orbit.actors.Actor
import cloud.orbit.actors.Stage
import cloud.orbit.actors.extensions.json.InMemoryJSONStorageExtension
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.details.Rules
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.backend.service.player.Player
import spock.lang.Shared
import spock.lang.Specification

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 12/26/16.
 * email: gueorgui.tzvetoslavov@gmail.com*/
class LobbyTestWith3Teams extends Specification
{
    @Shared lobbyFinder
    @Shared rating = 1500d
    @Shared rd = 200d
    @Shared Rules rules
    @Shared player1
    @Shared player2
    @Shared player3
    @Shared player4
    @Shared player5
    @Shared player6
    @Shared player7
    @Shared player8
    @Shared player9

    @Shared stage
    def setup()
    {
        stage = new Stage()
        stage.setClusterName("Basic Test Setup")
        stage.setMode(Stage.StageMode.HOST)
        stage.addExtension(new InMemoryJSONStorageExtension())
        stage.start().join()
        stage.bind()
        rules = new Rules(3,9,true,0.75)
        def details = new RoomDetails(name: "test lobby",rules: rules)
        lobbyFinder = Actor.getReference(RoomManager).registerService(details).join()
    }

    def cleanup()
    {
        stage.stop().join()
    }

    def "Players in lobby"()
    {
        player1 = Actor.getReference(Player, "random-player1")
        player1.getSkill().join().setRating(rating).join()
        player1.getSkill().join().setRatingDeviation(rd).join()
        lobbyFinder.getEntry(player1).join()
        rating++
        rd++

        player2 = Actor.getReference(Player, "random-player2")
        player2.getSkill().join().setRating(rating).join()
        player2.getSkill().join().setRatingDeviation(rd).join()
        lobbyFinder.getEntry(player2).join()
        rating++
        rd++

        player3 = Actor.getReference(Player, "random-player3")
        player3.getSkill().join().setRating(rating).join()
        player3.getSkill().join().setRatingDeviation(rd).join()
        lobbyFinder.getEntry(player3).join()
        rating++
        rd++

        player4 = Actor.getReference(Player, "random-player4")
        player4.getSkill().join().setRating(rating).join()
        player4.getSkill().join().setRatingDeviation(rd).join()
        lobbyFinder.getEntry(player4).join()
        rating++
        rd++


        player5 = Actor.getReference(Player, "random-player5")
        player5.getSkill().join().setRating(rating).join()
        player5.getSkill().join().setRatingDeviation(rd).join()
        lobbyFinder.getEntry(player5).join()
        rating++
        rd++

        player6 = Actor.getReference(Player, "random-player6")
        player6.getSkill().join().setRating(rating).join()
        player6.getSkill().join().setRatingDeviation(rd).join()
        lobbyFinder.getEntry(player6).join()
        rating++
        rd++


        player7 = Actor.getReference(Player, "random-player7")
        player7.getSkill().join().setRating(rating).join()
        player7.getSkill().join().setRatingDeviation(rd).join()
        lobbyFinder.getEntry(player7).join()
        rating++
        rd++

        player8 = Actor.getReference(Player, "random-player8")
        player8.getSkill().join().setRating(rating).join()
        player8.getSkill().join().setRatingDeviation(rd).join()
        lobbyFinder.getEntry(player8).join()
        rating++
        rd++

        player9 = Actor.getReference(Player, "random-player9")
        player9.getSkill().join().setRating(rating).join()
        player9.getSkill().join().setRatingDeviation(rd).join()
        lobbyFinder.getEntry(player9).join()
        def lobby = lobbyFinder.getEntry(player9).join()


        expect:
        !lobby.hasFreeSpot().join()

        //Two teams are created but there aren't any players in the teams
        when:
        def teams = lobby.getTeams().join()

        then:
        teams.size() == rules.teams
        teams.each {
            it.teamSize().join() == 0
        }

        when:
        lobby.populateTeams().join()
        teams = lobby.getTeams().join()

        then:
        teams.size() == rules.teams
        teams.each {
            it.teamSize().join() == ( rules.players / rules.teams ).toInteger()
        }
    }

    def "Players in lobby, some  of them in teams"()
    {
        player1 = Actor.getReference(Player, "random-player1")
        player1.getSkill().join().setRating(rating).join()
        player1.getSkill().join().setRatingDeviation(rd).join()
        lobbyFinder.getEntry(player1).join()
        rating++
        rd++

        player2 = Actor.getReference(Player, "random-player2")
        player2.getSkill().join().setRating(rating).join()
        player2.getSkill().join().setRatingDeviation(rd).join()
        lobbyFinder.getEntry(player2).join()
        rating++
        rd++

        player3 = Actor.getReference(Player, "random-player3")
        player3.getSkill().join().setRating(rating).join()
        player3.getSkill().join().setRatingDeviation(rd).join()
        lobbyFinder.getEntry(player3).join()
        rating++
        rd++

        player4 = Actor.getReference(Player, "random-player4")
        player4.getSkill().join().setRating(rating).join()
        player4.getSkill().join().setRatingDeviation(rd).join()
        lobbyFinder.getEntry(player4).join()
        rating++
        rd++


        player5 = Actor.getReference(Player, "random-player5")
        player5.getSkill().join().setRating(rating).join()
        player5.getSkill().join().setRatingDeviation(rd).join()
        lobbyFinder.getEntry(player5).join()
        rating++
        rd++

        player6 = Actor.getReference(Player, "random-player6")
        player6.getSkill().join().setRating(rating).join()
        player6.getSkill().join().setRatingDeviation(rd).join()
        lobbyFinder.getEntry(player6).join()
        rating++
        rd++


        player7 = Actor.getReference(Player, "random-player7")
        player7.getSkill().join().setRating(rating).join()
        player7.getSkill().join().setRatingDeviation(rd).join()
        lobbyFinder.getEntry(player7).join()
        rating++
        rd++

        player8 = Actor.getReference(Player, "random-player8")
        player8.getSkill().join().setRating(rating).join()
        player8.getSkill().join().setRatingDeviation(rd).join()
        lobbyFinder.getEntry(player8).join()
        rating++
        rd++

        player9 = Actor.getReference(Player, "random-player9")
        player9.getSkill().join().setRating(rating).join()
        player9.getSkill().join().setRatingDeviation(rd).join()
        lobbyFinder.getEntry(player9).join()
        def lobby = lobbyFinder.getEntry(player9).join()

        expect:
        !lobby.hasFreeSpot().join()

        //Two teams are created but there aren't any players in the teams
        when:
        lobby.selectTeam(player1, 0).join()
        lobby.selectTeam(player2, 0).join()
        lobby.selectTeam(player3, 1).join()
        lobby.selectTeam(player4, 1).join()
        lobby.selectTeam(player5, 2).join()
        lobby.selectTeam(player6, 2).join()
        def teams = lobby.getTeams().join()

        then:
        teams.size() == rules.teams
        teams.each {
            it.teamSize().join() == 2
        }

        when:
        lobby.populateTeams().join()
        teams = lobby.getTeams().join()

        then:
        teams.size() == rules.teams
        teams.get(0).isInTeam(player1).join()
        teams.get(0).isInTeam(player2).join()
        teams.get(1).isInTeam(player3).join()
        teams.get(1).isInTeam(player4).join()
        teams.get(2).isInTeam(player5).join()
        teams.get(2).isInTeam(player6).join()
    }



}
