package shepard.backend.service.leaderboard

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.leaderboard.score.Score
import spock.lang.Specification

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 31/05/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class ScoreTypeTest extends OneStageSpec
{

    def "Type Conversion Test"()
    {
        given:
        def score = Actor.getReference(Score, "random-score")
        score.setScore(3141592).join()

        expect:
        score.getRawScore().join() == 3141592
        score.getScoreAsCurrencyScore().join().getScoreLocale() == "\$3.14"
        score.getScoreAsTimeScore().join().getScoreLocale() == "00:52:21.592"
        score.getScoreAsNumericalScore().join().getScoreLocale() == "3,141,592"
        score.getScoreAsCurrencyScore().join().getScoreLocale(Locale.UK) == "£3.14"
        score.getScoreAsTimeScore().join().getScoreLocale(Locale.UK) == "00:52:21.592"
        score.getScoreAsNumericalScore().join().getScoreLocale(Locale.UK) == "3,141,592"
        score.getScoreAsCurrencyScore().join().getScoreLocale(Locale.FRANCE) == "3,14 €"
        score.getScoreAsTimeScore().join().getScoreLocale(Locale.FRANCE) == "00:52:21.592"
        score.getScoreAsNumericalScore().join().getScoreLocale(Locale.FRANCE) == "3 141 592"

    }
}
