package shepard.backend.service.matchmaking

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.matchmaking.cache.LobbyCache
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.manager.RoomManager
import spock.lang.Stepwise

import java.util.concurrent.CompletionException

import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 28/01/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class WorkflowRoomService extends OneStageSpec
{
    def "Get the Lobby Manager"()
    {
        setup:
        def manager = Actor.getReference(RoomManager)

        expect:
        manager.getAllEnabledServices().join().size() == 0
        manager.getAllDisabledServices().join().size() == 0
        manager.getServiceMap().join().size() == 0

    }

    def "Enable/Disable lobbies while the manager is empty" ()
    {
        setup:
        def manager = Actor.getReference(RoomManager)

        when:
        manager.enableService(Actor.getReference(RoomService, "random-service")).join()

        then:
        CompletionException ex = thrown()
        ex.cause.class == NoSuchElementException.class

        when:
        !manager.disableService(Actor.getReference(RoomService, "random-service")).join()

        then:
        ex = thrown()
        ex.cause.class == NoSuchElementException.class
    }

    def "Register our first lobby" ()
    {
        setup:
        def manager = Actor.getReference(RoomManager)
        def details = new RoomDetails(name: "Random lobby")

        when:
        def service = manager.registerService(details).join()

        then:
        service.getServiceDetails().join() == details

        expect:
        manager.getAllEnabledServices().join().size() == 1
        manager.getAllEnabledServices().join().contains(service)
        manager.getAllDisabledServices().join().size() == 0
        manager.getServiceMap().join().get(service)
    }

    def "Disable and enable a registered lobby"()
    {
        def manager = Actor.getReference(RoomManager)
        def service = Actor.getReference(RoomService,name2Id("Random lobby"))

        expect:
        manager.getAllEnabledServices().join().contains(service)
        !manager.getAllDisabledServices().join().contains(service)

        when:
        assert manager.disableService(service).join()

        then:
        manager.getAllEnabledServices().join().size() == 0
        !manager.getAllEnabledServices().join().contains(service)
        manager.getAllDisabledServices().join().size() == 1
        manager.getAllDisabledServices().join().contains(service)

        when:
        assert manager.enableService(service).join()

        then:
        manager.getAllEnabledServices().join().size() == 1
        manager.getAllEnabledServices().join().contains(service)
        manager.getAllDisabledServices().join().size() == 0
        !manager.getAllDisabledServices().join().contains(service)

    }

    def "Remove the lobby"()
    {
        setup:
        def manager = Actor.getReference(RoomManager)
        def service = Actor.getReference(RoomService,name2Id("Random lobby"))
        
        expect:
        !manager.removeService(Actor.getReference(RoomService,name2Id("Non valid lobby"))).join()

        when:
        assert manager.removeService(service).join()

        then:
        manager.getAllEnabledServices().join().size() == 0
        !manager.getAllEnabledServices().join().contains(service)
        manager.getAllDisabledServices().join().size() == 0
        !manager.getAllDisabledServices().join().contains(service)
    }

    def "Actually the lobby will always exist"()
    {
        setup:
        def service = Actor.getReference(RoomService,name2Id("Random lobby"))

        expect:
        service.getServiceDetails().join() == null
    }

    def "Activating the lobby cache"()
    {
        setup:
        def cache = Actor.getReference(LobbyCache)

        expect:
        cache.retrieve().join().size() == 0
    }

    def "Registering a new lobby and pushing it to the cache"()
    {
        setup:
        def manager = Actor.getReference(RoomManager)
        def details = new RoomDetails(name: "Another lobby")
        def service = manager.registerService(details).join()

        expect:
        manager.getAllEnabledServices().join().size() == 1
        manager.getAllEnabledServices().join().contains(service)

        when:
        def cache = Actor.getReference(LobbyCache)

        then:
        cache.retrieve().join().size() == 1
        cache.retrieve().join().contains(service)

    }

    def "A disabled lobby will not be shown in the cache"()
    {
        setup:
        def manager = Actor.getReference(RoomManager)
        def cache = Actor.getReference(LobbyCache)
        def service = Actor.getReference(RoomService, name2Id("Another lobby"))

        when:
        assert manager.disableService(service).join()

        then:
        cache.retrieve().join().size() == 0
        !cache.retrieve().join().contains(service)
    }

    def "An enabled lobby will be shown in the cache"()
    {
        setup:
        def manager = Actor.getReference(RoomManager)
        def cache = Actor.getReference(LobbyCache)
        def service = Actor.getReference(RoomService, name2Id("Another lobby"))

        when:
        assert manager.enableService(service).join()

        then:
        cache.retrieve().join().size() == 1
        cache.retrieve().join().contains(service)
    }

    def "If an enabled lobby is removed from the manager will not show in the cache"()
    {
        setup:
        def manager = Actor.getReference(RoomManager)
        def cache = Actor.getReference(LobbyCache)
        def service = Actor.getReference(RoomService, name2Id("Another lobby"))

        when:
        assert manager.removeService(service).join()

        then:
        cache.retrieve().join().size() == 0
        !cache.retrieve().join().contains(service)
    }

    def "If an disabled lobby is removed from the manager will not show in the cache"()
    {
        setup:
        def manager = Actor.getReference(RoomManager)
        def cache = Actor.getReference(LobbyCache)
        def details = new RoomDetails(name:  "Yet another lobby")
        def service = manager.registerService(details).join()

        when:
        assert manager.disableService(service).join()

        then:
        cache.retrieve().join().size() == 0
        !cache.retrieve().join().contains(service)

        when:
        assert manager.removeService(service).join()

        then:
        cache.retrieve().join().size() == 0
        !cache.retrieve().join().contains(service)
    }


    def "Multiple achievement propagation and lyfecycle"()
    {
        setup:
        def manager = Actor.getReference(RoomManager)
        def details1 = new RoomDetails(name: "lobby 1")
        def service1 = manager.registerService(details1).join()

        def details2 = new RoomDetails(name: "lobby 2")
        def service2 = manager.registerService(details2).join()

        def details3 = new RoomDetails(name: "lobby 3")
        def service3 = manager.registerService(details3).join()

        def achievement = [service1, service2, service3]

        when:
        def cache = Actor.getReference(LobbyCache)

        then:
        cache.retrieve().join().size() == 3
        achievement.each {
            assert cache.retrieve().join().contains(it)
        }

        when:
        assert manager.disableService(service3).join()
        assert manager.removeService(service1).join()
        def anotherCacheReference = Actor.getReference(LobbyCache)

        then:
        anotherCacheReference.retrieve().join().size() == 1
        anotherCacheReference.retrieve().join().contains(service2)
        !anotherCacheReference.retrieve().join().contains(service1)
        !anotherCacheReference.retrieve().join().contains(service3)

        expect:
        manager.allDisabledServices.join().contains(service3)
        manager.allDisabledServices.join().size() == 1
        manager.allEnabledServices.join().contains(service2)
        manager.allEnabledServices.join().size() == 1
    }
}
