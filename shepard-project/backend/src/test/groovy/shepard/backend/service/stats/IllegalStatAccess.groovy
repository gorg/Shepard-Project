package shepard.backend.service.stats

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.player.Player
import shepard.backend.service.stats.details.StatDetails
import shepard.backend.service.stats.entry.StatEntry
import shepard.backend.service.stats.manager.StatManager
import spock.lang.Stepwise

import java.util.concurrent.CompletionException

import static shepard.backend.util.id.GenerateId.generateId
import static spock.util.Exceptions.getRootCause

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 30/01/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class IllegalStatAccess extends OneStageSpec
{
    def "Set up the stat" ()
    {
        setup:
        def manager = Actor.getReference(StatManager)
        def details = new StatDetails(name: "Testing stat")

        when:
        def service = manager.registerService(details).join()

        then:
        manager.isEnabled(service).join()
    }

    def "Manually setup service entry"()
    {
        setup:
        def manager = Actor.getReference(StatManager)
        def service = manager.getAllEnabledServices().join().first()
        def player1 = Actor.getReference(Player, "player 1")
        def player2 = Actor.getReference(Player, "player 2")

        when:
        def falseStat = Actor.getReference(StatEntry, generateId(service,player1)).setupEntry(service,player2).join()

        then:
        def ex = thrown(CompletionException)
        assert getRootCause(ex).class == IllegalAccessError
        assert getRootCause(ex).message == "Actor identity is not matching the arguments"


        when:
        def stat = service.getEntry(player1).join()

        then:
        stat.getStatValue().join() == null
        stat.player.join() != player2
        stat.player.join() == player1
    }
}
