package shepard.backend.service.game

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.details.Rules
import shepard.backend.service.matchmaking.entry.Lobby
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.backend.service.player.Player
import spock.lang.Shared
import spock.lang.Stepwise

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 22/01/17.
 * email: gueorgui.tzvetoslavov@gmail.com*/
@Stepwise
class SimpleGameTest extends OneStageSpec
{
    @Shared lobbyFinder
    @Shared Rules rules = new Rules(ranked: true, teams: 2)
    @Shared Player player1
    @Shared Player player2
    @Shared Player player3
    @Shared Player player4
    @Shared Player player5
    @Shared Player player6
    @Shared Player player7
    @Shared Player player8
    @Shared Lobby lobby
    @Shared Game game

    def "Players in lobby"()
    {   lobbyFinder = Actor.getReference(RoomManager)
            .registerService(new RoomDetails(name: "game-test", rules: rules)).join()

        player1 = Actor.getReference(Player, "player1")
        lobbyFinder.getEntry(player1).join()

        player2 = Actor.getReference(Player, "player2")
        lobbyFinder.getEntry(player2).join()

        player3 = Actor.getReference(Player, "player3")
        lobbyFinder.getEntry(player3).join()

        player4 = Actor.getReference(Player, "player4")
        lobbyFinder.getEntry(player4).join()

        player5 = Actor.getReference(Player, "player5")
        lobbyFinder.getEntry(player5).join()

        player6 = Actor.getReference(Player, "player6")
        lobbyFinder.getEntry(player6).join()

        player7 = Actor.getReference(Player, "player7")
        lobbyFinder.getEntry(player7).join()

        player8 = Actor.getReference(Player, "player8")
        this.lobby = lobbyFinder.getEntry(player8).join()

        expect:
        !lobby.hasFreeSpot().join()

        when:
        def teams = lobby.getTeams().join()

        then:
        teams.size() == rules.teams
        teams.each {
            it.teamSize().join() == 0
        }

        when:
        //Team 0
        lobby.selectTeam(player1,0).join()
        lobby.selectTeam(player2,0).join()
        lobby.selectTeam(player3,0).join()
        lobby.selectTeam(player4,0).join()
        //Team 1
        lobby.selectTeam(player5,1).join()
        lobby.selectTeam(player6,1).join()
        lobby.selectTeam(player7,1).join()
        lobby.selectTeam(player8,1).join()

        then:
        lobby.getTeams().join().first().getTeamMembers().join().containsAll([player1,player2,player3,player4])
        lobby.getTeams().join().last().getTeamMembers().join().containsAll([player5,player6,player7,player8])
    }

    def "Starting a game"()
    {
        def players = [player1,player2,player3,player4,player5,player6,player7,player8]

        expect:
        players.each {
            assert it.getCurrentLobby().join() == lobby
        }

        when:
        game = lobby.startGame().join()

        then:
        players.each {
            assert it.getCurrentGame().join() == game
        }

    }

    def "Posting results"()
    {
        given:
        def teams = lobby.getTeams().join()

        when:
        game.addWin(teams.first()).join()
        game.addDraw().join()
        game.finishMatch().join()

        then:
        player1.getSkill().join().getRating().join().trunc(2) == 1703.65d
        player2.getSkill().join().getRating().join().trunc(2) == 1703.65d
        player3.getSkill().join().getRating().join().trunc(2) == 1703.65d
        player4.getSkill().join().getRating().join().trunc(2) == 1703.65d

        player5.getSkill().join().getRating().join().trunc(2) == 1296.34d
        player6.getSkill().join().getRating().join().trunc(2) == 1296.34d
        player7.getSkill().join().getRating().join().trunc(2) == 1296.34d
        player8.getSkill().join().getRating().join().trunc(2) == 1296.34d
    }


    def "Fetch results resume"()
    {
        given:
        def players = [player1,player2,player3,player4,player5,player6,player7,player8]

        when:
        def resume = game.getResume().join()

        then:
        players.each { assert resume.player.contains(it) }
        resume.each { assert it.rounds == 8 }

        and:
        players.each { assert game.getResumeRedux().join().player.contains(it)}
        game.getResumeRedux().join().each { assert it.rounds == 2}
    }

    def "Fetch team results"()
    {
        when:
        def resume = game.getTeamResume().join()

        then:
        resume.each { assert it.rounds == 8}

        and:
        game.getTeamResumeRedux().join().each { assert it.rounds == 2}
    }

    def "Leaving game and lobby"()
    {
        def players = [player1,player2,player3,player4,player5,player6,player7,player8]

        expect:
        players.each {
            assert it.getCurrentLobby().join() == null
            assert it.getCurrentGame().join() == null
        }
    }

    def "Is game finished?"()
    {
        expect:
        game.isFinished().join()
    }
}
