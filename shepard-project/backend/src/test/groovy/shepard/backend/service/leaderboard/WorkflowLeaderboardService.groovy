package shepard.backend.service.leaderboard

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.leaderboard.cache.LeaderboardCache
import shepard.backend.service.leaderboard.details.LeaderboardDetails
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.backend.service.player.Player
import spock.lang.Stepwise

import java.util.concurrent.CompletionException

import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 25/01/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 *
 **/
@Stepwise
class WorkflowLeaderboardService extends OneStageSpec
{
    def "Get the Leaderboard Manager"()
    {
        setup:
        def manager = Actor.getReference(LeaderboardManager)

        expect:
        manager.getAllEnabledServices().join().size() == 0
        manager.getAllDisabledServices().join().size() == 0
        manager.getServiceMap().join().size() == 0

    }

    def "Enable/Disable Leaderboards while the manager is empty" ()
    {
        setup:
        def manager = Actor.getReference(LeaderboardManager)

        when:
        !manager.enableService(Actor.getReference(LeaderboardService, "random-service")).join()

        then:
        CompletionException ex = thrown()
        ex.cause.class == NoSuchElementException.class

        when:
        !manager.disableService(Actor.getReference(LeaderboardService, "random-service")).join()

        then:
        ex = thrown()
        ex.cause.class == NoSuchElementException.class
    }

    def "Register our first leaderboard" ()
    {
        setup:
        def manager = Actor.getReference(LeaderboardManager)
        def details = new LeaderboardDetails(name: "Random leaderboard")

        when:
        def service = manager.registerService(details).join()

        then:
        service.getServiceDetails().join() == details

        expect:
        manager.getAllEnabledServices().join().size() == 1
        manager.getAllEnabledServices().join().contains(service)
        manager.getAllDisabledServices().join().size() == 0
        manager.getServiceMap().join().get(service)
    }

    def "Disable and enable a registered leaderboard"()
    {
        def manager = Actor.getReference(LeaderboardManager)
        def service = Actor.getReference(LeaderboardService,name2Id("Random leaderboard"))

        expect:
        manager.getAllEnabledServices().join().contains(service)
        !manager.getAllDisabledServices().join().contains(service)

        when:
        assert manager.disableService(service).join()

        then:
        manager.getAllEnabledServices().join().size() == 0
        !manager.getAllEnabledServices().join().contains(service)
        manager.getAllDisabledServices().join().size() == 1
        manager.getAllDisabledServices().join().contains(service)

        when:
        assert manager.enableService(service).join()

        then:
        manager.getAllEnabledServices().join().size() == 1
        manager.getAllEnabledServices().join().contains(service)
        manager.getAllDisabledServices().join().size() == 0
        !manager.getAllDisabledServices().join().contains(service)

    }

    def "Remove the leaderboard"()
    {
        setup:
        def manager = Actor.getReference(LeaderboardManager)
        def service = Actor.getReference(LeaderboardService,name2Id("Random leaderboard"))

        expect:
        !manager.removeService(Actor.getReference(LeaderboardService,name2Id("Non valid leaderboard"))).join()

        when:
        assert manager.removeService(service).join()

        then:
        manager.getAllEnabledServices().join().size() == 0
        !manager.getAllEnabledServices().join().contains(service)
        manager.getAllDisabledServices().join().size() == 0
        !manager.getAllDisabledServices().join().contains(service)
    }

    def "Actually leaderboard will always exist"()
    {
        setup:
        def service = Actor.getReference(LeaderboardService,name2Id("Random leaderboard"))

        expect:
        service.getServiceDetails().join() == null
    }

    def "Activating the leaderboard cache"()
    {
        setup:
        def cache = Actor.getReference(LeaderboardCache)

        expect:
        cache.retrieve().join().size() == 0
    }

    def "Registering a new leaderboard and pushing it to the cache"()
    {
        setup:
        def manager = Actor.getReference(LeaderboardManager)
        def details = new LeaderboardDetails(name: "Another leaderboard")
        def service = manager.registerService(details).join()

        expect:
        manager.getAllEnabledServices().join().size() == 1
        manager.getAllEnabledServices().join().contains(service)

        when:
        def cache = Actor.getReference(LeaderboardCache)

        then:
        cache.retrieve().join().size() == 1
        cache.retrieve().join().contains(service)

    }

    def "A disabled leaderboard will not be shown in the cache"()
    {
        setup:
        def manager = Actor.getReference(LeaderboardManager)
        def cache = Actor.getReference(LeaderboardCache)
        def service = Actor.getReference(LeaderboardService, name2Id("Another leaderboard"))

        when:
        assert manager.disableService(service).join()

        then:
        cache.retrieve().join().size() == 0
        !cache.retrieve().join().contains(service)
    }

    def "An enabled leaderboard will be shown in the cache"()
    {
        setup:
        def manager = Actor.getReference(LeaderboardManager)
        def cache = Actor.getReference(LeaderboardCache)
        def service = Actor.getReference(LeaderboardService, name2Id("Another leaderboard"))

        when:
        assert manager.enableService(service).join()

        then:
        cache.retrieve().join().size() == 1
        cache.retrieve().join().contains(service)
    }

    def "If an enabled leaderboard is removed from the manager will not show in the cache"()
    {
        setup:
        def manager = Actor.getReference(LeaderboardManager)
        def cache = Actor.getReference(LeaderboardCache)
        def service = Actor.getReference(LeaderboardService, name2Id("Another leaderboard"))

        when:
        assert manager.removeService(service).join()

        then:
        cache.retrieve().join().size() == 0
        !cache.retrieve().join().contains(service)
    }

    def "If an disabled leaderboard is removed from the manager will not show in the cache"()
    {
        setup:
        def manager = Actor.getReference(LeaderboardManager)
        def cache = Actor.getReference(LeaderboardCache)
        def details = new LeaderboardDetails(name: "Random leaderboard")
        def service = manager.registerService(details).join()

        when:
        assert manager.disableService(service).join()

        then:
        cache.retrieve().join().size() == 0
        !cache.retrieve().join().contains(service)

        when:
        assert manager.removeService(service).join()

        then:
        cache.retrieve().join().size() == 0
        !cache.retrieve().join().contains(service)
    }


    def "Multiple leaderboard propagation and lyfecycle"()
    {
        setup:
        def manager = Actor.getReference(LeaderboardManager)
        def details1 = new LeaderboardDetails(name: "Random leaderboard 1")
        def service1 = manager.registerService(details1).join()

        def details2 = new LeaderboardDetails(name: "Random leaderboard 2")
        def service2 = manager.registerService(details2).join()

        def details3 = new LeaderboardDetails(name: "Random leaderboard 3")
        def service3 = manager.registerService(details3).join()

        def leaderboards = [service1, service2, service3]

        when:
        def cache = Actor.getReference(LeaderboardCache)

        then:
        cache.retrieve().join().size() == 3
        leaderboards.each {
            assert cache.retrieve().join().contains(it)
        }

        when:
        assert manager.disableService(service3).join()
        assert manager.removeService(service1).join()
        def anotherCacheReference = Actor.getReference(LeaderboardCache)

        then:
        anotherCacheReference.retrieve().join().size() == 1
        anotherCacheReference.retrieve().join().contains(service2)
        !anotherCacheReference.retrieve().join().contains(service1)
        !anotherCacheReference.retrieve().join().contains(service3)

        expect:
        manager.allDisabledServices.join().contains(service3)
        manager.allDisabledServices.join().size() == 1
        manager.allEnabledServices.join().contains(service2)
        manager.allEnabledServices.join().size() == 1
    }

    def "Players are up to date"()
    {
        setup:
        def player = Actor.getReference(Player, "random-player-1")
        def manager = Actor.getReference(LeaderboardManager)


        expect:
        manager.allEnabledServices.join().each {
            assert player.getScoreForLeaderboard(it).join().getRawScore().join() == 0l
        }


        when:
        player.getScoreForLeaderboard(manager.allDisabledServices.join().first()).join().getRawScore().join()

        then:
        thrown(CompletionException)
    }
}
