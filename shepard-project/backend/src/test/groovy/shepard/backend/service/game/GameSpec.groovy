package shepard.backend.service.game

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.details.Rules
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.backend.service.player.Player
import spock.lang.Shared
import spock.lang.Stepwise
import spock.lang.Unroll

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 20/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class GameSpec extends OneStageSpec
{
    @Shared Game game
    @Shared Player p1
    @Shared Player p2

    def "setup lobby"()
    {
        given:
        def room = Actor.getReference(RoomManager).
                registerService(new RoomDetails(name: 'Test Room', rules: new Rules(teams: 2, players: 2))).join()

        p1 = Actor.getReference(Player, 'player-one')
        p2 = Actor.getReference(Player, 'player-two')

        def lobby = room.getEntry(p1).join()

        expect:
        lobby == room.getEntry(p2).join()

        and:
        lobby.selectTeam(p1,0).join()
        lobby.selectTeam(p2,1).join()

        when:
        game = lobby.startGame().join()

        then:
        lobby.getTeam(0).join().isInTeam(p1)
        lobby.getTeam(1).join().isInTeam(p2)
    }

    @Unroll
    def "post wins"()
    {

        when:
        def g = game.addWin(winner).join()

        then:
        g.getResults().join().getAllResults().size() == size

        and:
        if (winner == 0)
        {
            g.getResults().join().getResults(p1).winner == p1
            g.getResults().join().getResults(p1).loser == p2
        }

        where:
        winner  | size
        0       | 1
        1       | 2
        -1      | 2
        2       | 2
        50      | 2
        -50     | 2
    }

    @Unroll
    def "post draws"()
    {
        when:
        def g = game.addDraw().join()

        then:
        g.getResults().join().getAllResults().last().draw
    }


    @Unroll
    def "post wins with specific teams"()
    {

        when:
        def g = game.addWin(winner,loser).join()

        then:
        g.getResults().join().getAllResults().size() == size

        where:
        winner |loser   | size
        0      |1       | 4
        1      |0       | 5
        0      |2       | 5
        -5     |6       | 5
        1      |1       | 5
    }
}
