package shepard.backend.service.achievement

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.achievement.cache.AchievementCache
import shepard.backend.service.achievement.details.AchievementDetails
import shepard.backend.service.achievement.details.AchievementType
import shepard.backend.service.achievement.manager.AchievementManager
import shepard.backend.service.player.Player
import spock.lang.Stepwise

import java.util.concurrent.CompletionException

import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 25/01/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 *
 **/
@Stepwise
class WorkflowAchievementService extends OneStageSpec
{
    def "Get the Achievement Manager"()
    {
        setup:
        def manager = Actor.getReference(AchievementManager)

        expect:
        manager.getAllEnabledServices().join().size() == 0
        manager.getAllDisabledServices().join().size() == 0
        manager.getServiceMap().join().size() == 0

    }

    def "Enable/Disable Achievements while the manager is empty" ()
    {
        setup:
        def manager = Actor.getReference(AchievementManager)

        when:
        !manager.enableService(Actor.getReference(AchievementService, "random-service")).join()

        then:
        CompletionException ex = thrown()
        ex.cause.class == NoSuchElementException.class

        when:
        !manager.disableService(Actor.getReference(AchievementService, "random-service")).join()

        then:
        ex = thrown()
        ex.cause.class == NoSuchElementException.class

    }

    def "Register our first achievement" ()
    {
        setup:
        def manager = Actor.getReference(AchievementManager)
        def details = new AchievementDetails(
                name: "Random achievements",
                description:  "Nothing special here",
                type:  AchievementType.REVEALED,
                icon:  null)

        when:
        def service = manager.registerService(details).join()

        then:
        service.getServiceDetails().join() == details

        expect:
        manager.getAllEnabledServices().join().size() == 1
        manager.getAllEnabledServices().join().contains(service)
        manager.getAllDisabledServices().join().size() == 0
        manager.getServiceMap().join().get(service)
    }

    def "Disable and enable a registered achievement"()
    {
        def manager = Actor.getReference(AchievementManager)
        def service = Actor.getReference(AchievementService,name2Id("Random achievements"))

        expect:
        manager.getAllEnabledServices().join().contains(service)
        !manager.getAllDisabledServices().join().contains(service)

        when:
        assert manager.disableService(service).join()

        then:
        manager.getAllEnabledServices().join().size() == 0
        !manager.getAllEnabledServices().join().contains(service)
        manager.getAllDisabledServices().join().size() == 1
        manager.getAllDisabledServices().join().contains(service)

        when:
        assert manager.enableService(service).join()

        then:
        manager.getAllEnabledServices().join().size() == 1
        manager.getAllEnabledServices().join().contains(service)
        manager.getAllDisabledServices().join().size() == 0
        !manager.getAllDisabledServices().join().contains(service)

    }

    def "Remove the achievement"()
    {
        setup:
        def manager = Actor.getReference(AchievementManager)
        def service = Actor.getReference(AchievementService,name2Id("Random achievements"))

        expect:
        !manager.removeService(Actor.getReference(AchievementService,name2Id("Non valid achievements"))).join()

        when:
        assert manager.removeService(service).join()

        then:
        manager.getAllEnabledServices().join().size() == 0
        !manager.getAllEnabledServices().join().contains(service)
        manager.getAllDisabledServices().join().size() == 0
        !manager.getAllDisabledServices().join().contains(service)
    }

    def "Actually achievement will always exist"()
    {
        setup:
        def service = Actor.getReference(AchievementService,name2Id("Random achievements"))

        expect:
        service.getServiceDetails().join() == null
    }

    def "Activating the achievement cache"()
    {
        setup:
        def cache = Actor.getReference(AchievementCache)

        expect:
        cache.retrieve().join().size() == 0
    }

    def "Registering a new achievement and pushing it to the cache"()
    {
        setup:
        def manager = Actor.getReference(AchievementManager)
        def details = new AchievementDetails(
                name:  "Another achievements",
                description:  "Nothing special here",
                type:  AchievementType.REVEALED,
                icon:  null)
        def service = manager.registerService(details).join()

        expect:
        manager.getAllEnabledServices().join().size() == 1
        manager.getAllEnabledServices().join().contains(service)

        when:
        def cache = Actor.getReference(AchievementCache)

        then:
        cache.retrieve().join().size() == 1
        cache.retrieve().join().contains(service)

    }

    def "A disabled achievement will not be shown in the cache"()
    {
        setup:
        def manager = Actor.getReference(AchievementManager)
        def cache = Actor.getReference(AchievementCache)
        def service = Actor.getReference(AchievementService, name2Id("Another achievements"))

        when:
        assert manager.disableService(service).join()

        then:
        cache.retrieve().join().size() == 0
        !cache.retrieve().join().contains(service)
    }

    def "An enabled achievement will be shown in the cache"()
    {
        setup:
        def manager = Actor.getReference(AchievementManager)
        def cache = Actor.getReference(AchievementCache)
        def service = Actor.getReference(AchievementService, name2Id("Another achievements"))

        when:
        assert manager.enableService(service).join()

        then:
        cache.retrieve().join().size() == 1
        cache.retrieve().join().contains(service)
    }

    def "If an enabled achievement is removed from the manager will not show in the cache"()
    {
        setup:
        def manager = Actor.getReference(AchievementManager)
        def cache = Actor.getReference(AchievementCache)
        def service = Actor.getReference(AchievementService, name2Id("Another achievements"))

        when:
        assert manager.removeService(service).join()

        then:
        cache.retrieve().join().size() == 0
        !cache.retrieve().join().contains(service)
    }

    def "If an disabled achievement is removed from the manager will not show in the cache"()
    {
        setup:
        def manager = Actor.getReference(AchievementManager)
        def cache = Actor.getReference(AchievementCache)
        def details = new AchievementDetails(
                name:  "Yet another achievements",
                description:  "Nothing special here",
                type:  AchievementType.REVEALED,
                icon:  null)
        def service = manager.registerService(details).join()

        when:
        assert manager.disableService(service).join()

        then:
        cache.retrieve().join().size() == 0
        !cache.retrieve().join().contains(service)

        when:
        assert manager.removeService(service).join()

        then:
        cache.retrieve().join().size() == 0
        !cache.retrieve().join().contains(service)
    }


    def "Multiple achievement propagation and lyfecycle"()
    {
        setup:
        def manager = Actor.getReference(AchievementManager)
        def details1 = new AchievementDetails(
                name:  "Achievement 1",
                description:  "Nothing special here",
                type:  AchievementType.HIDDEN,
                icon:  null)
        def service1 = manager.registerService(details1).join()

        def details2 = new AchievementDetails(
                name:  "Achievement 2",
                description:  "Nothing special here",
                type:  AchievementType.REVEALED,
                icon:  null)
        def service2 = manager.registerService(details2).join()

        def details3 = new AchievementDetails(
                name:  "Achievement 3",
                description:  "Nothing special here",
                type:  AchievementType.REVEALED,
                icon:  null)
        def service3 = manager.registerService(details3).join()

        def achievement = [service1, service2, service3]

        when:
        def cache = Actor.getReference(AchievementCache)

        then:
        cache.retrieve().join().size() == 3
        achievement.each {
            assert cache.retrieve().join().contains(it)
        }

        when:
        assert manager.disableService(service3).join()
        assert manager.removeService(service1).join()
        def anotherCacheReference = Actor.getReference(AchievementCache)

        then:
        anotherCacheReference.retrieve().join().size() == 1
        anotherCacheReference.retrieve().join().contains(service2)
        !anotherCacheReference.retrieve().join().contains(service1)
        !anotherCacheReference.retrieve().join().contains(service3)

        expect:
        manager.allDisabledServices.join().contains(service3)
        manager.allDisabledServices.join().size() == 1
        manager.allEnabledServices.join().contains(service2)
        manager.allEnabledServices.join().size() == 1
    }

    def "Players are up to date"()
    {
        setup:
        def player = Actor.getReference(Player, "random-player-1")
        def manager = Actor.getReference(AchievementManager)


        expect:
        manager.allEnabledServices.join().each {
            assert !player.isAchievementCompleted(it).join().isCompleted().join()
        }

        when:
        player.isAchievementCompleted(manager.allDisabledServices.join().first()).join().isCompleted().join()

        then:
        thrown(CompletionException)


    }
}
