package shepard.backend.service.leaderboard

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.leaderboard.details.LeaderboardDetails
import shepard.backend.service.leaderboard.details.LeaderboardSort
import shepard.backend.service.leaderboard.details.LeaderboardType
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.backend.service.player.Player

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/4/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class BigLeaderboardTest extends OneStageSpec
{
    static LeaderboardManager manager
    static LeaderboardService leaderboard
    static int count = 1000//1000000

    def "Setup a leaderboard"()
    {
        manager = Actor.getReference(LeaderboardManager)
        leaderboard = manager.registerService(
                new LeaderboardDetails(
                        name: "Test Leaderboard",
                        icon: null,
                        lowerLimit: 0,
                        upperLimit: count,
                        type: LeaderboardType.ABSOLUTE,
                        sort: LeaderboardSort.HIGH
                )).join()

        expect:
        manager.getAllEnabledServices().join().size() == 1
    }


    def "Players leaderboard"()
    {
        setup:
        (0..(count-1)).each
        {
            Player player = Actor.getReference(Player,"player$it")
            player.getCurrentGame().join()
            leaderboard.insertScore(player,it).join()
        }

        when:
        leaderboard.rank().join()

        then:
        ((count-1)..0).each {
            assert Actor.getReference(Player, "player$it").getRankForLeaderboard(leaderboard).join().getRank().join() == (count-it).toLong()
        }
    }
}
