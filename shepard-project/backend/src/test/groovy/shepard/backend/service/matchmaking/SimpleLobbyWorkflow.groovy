package shepard.backend.service.matchmaking

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.game.Game
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.details.Rules
import shepard.backend.service.matchmaking.entry.Lobby
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.backend.service.player.Player
import spock.lang.Shared
import spock.lang.Stepwise

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 23/01/17.
 * email: gueorgui.tzvetoslavov@gmail.com*/
@Stepwise
class SimpleLobbyWorkflow extends OneStageSpec
{
    @Shared RoomService lobbyService
    @Shared Lobby lobby
    @Shared Player player1
    @Shared Player player2
    @Shared Game game

    def "Register a lobby service"()
    {
        def manager = Actor.getReference(RoomManager)
        def rules = new Rules(2,2,true,0.75)
        def details = new RoomDetails(name: "test lobby",rules: rules)
        lobbyService = manager.registerService(details).join()

        expect:
        manager.getAllEnabledServices().join().size() == 1
        manager.getAllEnabledServices().join().contains(lobbyService)
        lobbyService.getServiceDetails().join() == details
    }

    def "Get a lobby from the lobby service"()
    {
        player1 = Actor.getReference(Player, "player-1")
        assert player1.getCurrentLobby().join() == null
        assert player1.getCurrentGame().join() == null
        lobby =  lobbyService.getEntry(player1).join()

        expect:
        lobby.getLobbyPLayers().join().contains(player1)
        lobby.getLobbyRating().join() == player1.getSkill().join().getRating().join()

    }

    def "Fill the lobby and select teams"()
    {
        player2 = Actor.getReference(Player, "player-2")
        assert player2.getCurrentLobby().join() == null
        assert player2.getCurrentGame().join() == null
        def lobby = lobbyService.getEntry(player2).join()

        expect:
        this.lobby == lobby
        lobby.getLobbyPLayers().join().contains(player2)
        lobby.getLobbyPLayers().join().contains(player1)

        when:
        lobby.selectTeam(player1,0).join()
        lobby.selectTeam(player2,1).join()
        def teams = lobby.getTeams().join()

        then:
        teams[0].isInTeam(player1).join()
        teams[1].isInTeam(player2).join()

        expect:
        player1.getCurrentLobby().join() == lobby
        player2.getCurrentLobby().join() == lobby
    }

    def "Start a game and post results"()
    {
        when:
        game = lobby.startGame().join()
        def teams = lobby.getTeams().join()
        game.addWin(teams[0]).join()
        game.addWin(teams[1]).join()
        game.addWin(teams[0]).join()
        game.addDraw().join()

        then:
        player1.getCurrentGame().join() == game
        player2.getCurrentGame().join() == game
    }


    def "Finish game and compare stats"()
    {
        when:
        game.finishMatch().join()

        then:
        player1.getCurrentGame().join() == null
        player2.getCurrentGame().join() == null

        player1.getCurrentLobby().join() == null
        player2.getCurrentLobby().join() == null

        player1.skill.join().rating.join() > player2.skill.join().rating.join()
    }

    def "Make sure lobby isn't in the lobby service anymore"()
    {
        expect:
        lobbyService.getLobbies().join().size() == 0
    }
}
