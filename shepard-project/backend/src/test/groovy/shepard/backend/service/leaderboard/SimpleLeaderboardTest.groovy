package shepard.backend.service.leaderboard

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.leaderboard.details.LeaderboardDetails
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.backend.service.player.Player
import spock.lang.Stepwise

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 10/27/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
@Stepwise
class SimpleLeaderboardTest extends OneStageSpec
{
    static LeaderboardManager manager
    static Player player1
    static Player player2
    static Player player3
    static LeaderboardService leaderboard

    def setup()
    {
        manager = Actor.getReference(LeaderboardManager)
        player1 = Actor.getReference(Player,"player1")
        player2 = Actor.getReference(Player,"player2")
        player3 = Actor.getReference(Player,"player3")
    }

    def "Create leaderboard"()
    {
        setup:
        leaderboard = manager.registerService(new LeaderboardDetails(name: "Test leaderboardId")).join()

        expect:
        player1.getScoreForLeaderboard(leaderboard).join().getRawScore().join() == 0.toLong()
        player2.getScoreForLeaderboard(leaderboard).join().getRawScore().join() == 0.toLong()
        player3.getScoreForLeaderboard(leaderboard).join().getRawScore().join() == 0.toLong()

    }


    def "Publish score to leaderboard"()
    {
        setup:
        leaderboard.insertScore(player1, 10).join()
        leaderboard.insertScore(player2, 20).join()
        leaderboard.insertScore(player3, 30).join()

        expect:
        player1.getScoreForLeaderboard(leaderboard).join().getRawScore().join() == 10.toLong()
        player2.getScoreForLeaderboard(leaderboard).join().getRawScore().join() == 20.toLong()
        player3.getScoreForLeaderboard(leaderboard).join().getRawScore().join() == 30.toLong()

        when:
        leaderboard.rank().join()
        def ranking = leaderboard.getRankedLeaderboard().join()

        then:
        ranking.size() == 3
        ranking.first().getPlayer().join() == player3
        ranking.last().getPlayer().join() == player1
    }

    def "Add a new leaderboard and scores to the new leaderboard"()
    {
        setup:
        def newLeaderboard = manager.registerService(new LeaderboardDetails(name: "another leaderboard")).join()

        expect:
        player1.getRankForLeaderboard(newLeaderboard).join().getRank().join().toInteger() == 0
        player2.getRankForLeaderboard(newLeaderboard).join().getRank().join().toInteger() == 0
        player3.getRankForLeaderboard(newLeaderboard).join().getRank().join().toInteger() == 0

        when:
        newLeaderboard.insertScore(player1, 10).join()
        newLeaderboard.insertScore(player2, 20).join()
        newLeaderboard.rank().join()

        then:
        player1.getRankForLeaderboard(newLeaderboard).join().getRank().join().toInteger() == 2
        player2.getRankForLeaderboard(newLeaderboard).join().getRank().join().toInteger() == 1

        //Player 3 has a rank of 0, because he hasn't posted a score to the leaderboard
        player3.getRankForLeaderboard(newLeaderboard).join().getRank().join().toInteger() == 0

        when:
        def list = newLeaderboard.getRankedLeaderboard().join()

        then:
        list.size() == 2
        list.first().getPlayer().join().equals(player2)
        list.last().getPlayer().join().equals(player1)

    }
}
