package shepard.backend.service.matchmaking.glicko

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.details.Rules
import shepard.backend.service.matchmaking.entry.Lobby
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.backend.service.matchmaking.util.Glicko2
import shepard.backend.service.matchmaking.util.Skill
import shepard.backend.service.player.Player
import spock.lang.Shared
import spock.lang.Stepwise

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/25/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
@Stepwise
class SkillTest extends OneStageSpec
{
    @Shared Player player
    @Shared Glicko2 rankingSystem
    @Shared Lobby lobby

    def setup()
    {
        rankingSystem = Actor.getReference(Glicko2)
        player = Actor.getReference(Player, "player-1")
    }

    def "Default values"()
    {
        setup:
        Skill skill = player.getSkill().join()

        expect:
        assert skill.getRating().join() == rankingSystem.getDefaultRating().join()
        assert skill.getRatingDeviation().join() == rankingSystem.getDefaultRatingDeviation().join()
        assert skill.getVolatility().join() == rankingSystem.getDefaultVolatility().join()

        assert skill.getSkillRange95().join().low == skill.getRating().join()-(2*skill.getRatingDeviation().join())
        assert skill.getSkillRange95().join().high == skill.getRating().join()+(2*skill.getRatingDeviation().join())

        assert skill.getSkillRange95().join().low == 800d
        assert skill.getSkillRange95().join().high == 2200d
        assert skill.getNumberOfResults().join() == 0

    }

    def "Creating a one man teams"()
    {
        setup:
        def manager = Actor.getReference(RoomManager)
        def rules = new Rules(2,2,true,0.75)
        def details = new RoomDetails(name: "lobby-test", rules: rules)
        def lobbyService = manager.registerService(details).join()
        lobby = lobbyService.getEntry(player).join()

        def player2 = Actor.getReference(Player, "player-2")
        Lobby lobby1 = lobbyService.getEntry(player2).join()

        expect:
        lobby == lobby1

        when:
        lobby.selectTeam(player,0).join()
        lobby.selectTeam(player2,1).join()

        then:
        assert lobby.getTeam(0).join().isInTeam(player).join()
        assert !lobby.getTeam(0).join().isInTeam(player2).join()
        assert !lobby.getTeam(1).join().isInTeam(player).join()
        assert lobby.getTeam(1).join().isInTeam(player2).join()
    }

    def "Creating a match"()
    {
        setup:
        def game = lobby.startGame().join()
        def team1 = lobby.getTeam(0).join()
        def team2 = lobby.getTeam(1).join()

        game.addWin(team1).join()

        when:
        1 == 1
        //game.finishMatch().join()

        then:
        2 == 2
        //assert player.skill.join().rating.join() > Actor.getReference(Glicko2).defaultRating.join()
        //assert player.skill.join().ratingDeviation.join() < Actor.getReference(Glicko2).defaultRatingDeviation.join()

    }
}
