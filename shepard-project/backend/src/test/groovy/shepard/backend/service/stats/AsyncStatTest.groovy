package shepard.backend.service.stats

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.player.Player
import shepard.backend.service.stats.details.StatDetails
import shepard.backend.service.stats.manager.StatManager
import spock.lang.Shared
import spock.lang.Stepwise

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 31/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class AsyncStatTest extends OneStageSpec
{
    @Shared StatService service

    def "Create a stat"()
    {
        def manager = Actor.getReference(StatManager)
        def details = new StatDetails(name: "Async", minValue: 0, maxValue: 20, maxChange: 0)
        service = manager.registerService(details).join()

        expect:
        manager.getAllEnabledServices().join().size() == 1
    }

    def "Update score"()
    {
        def player = Actor.getReference(Player, "random-player")
        def entry = service.update(10,player).join()

        expect:
        entry.getStatValue().join() == 10
    }

    def "Update score above limit"()
    {
        def player = Actor.getReference(Player, "random-player")
        def entry = service.update(25,player).join()

        expect:
        entry.getStatValue().join() == 10
    }
}
