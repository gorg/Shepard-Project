package shepard.backend.service.matchmaking

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.backend.service.player.Player
import spock.lang.Shared
import spock.lang.Stepwise

import java.util.concurrent.CompletionException

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 02/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class EnterTwoDifferentLobbies extends OneStageSpec
{
    @Shared RoomService roomOne
    @Shared RoomService roomTwo
    @Shared Player player

    def "Setup 2 lobbies"()
    {
        def manager = Actor.getReference(RoomManager)

        def room1 = new RoomDetails(name: 'Room one')
        def room2 = new RoomDetails(name: 'Room two')

        roomOne = manager.registerService(room1).join()
        roomTwo = manager.registerService(room2).join()

        expect:
        manager.getAllEnabledServices().join().size() == 2
    }

    def "Player is not in a lobby"()
    {
        def playerId = "random-player"
        player = Actor.getReference(Player, playerId)


        expect:
        player.currentLobby.join() == null
    }

    def "Get lobby from the first room"()
    {
        expect:
        roomOne.checkForEntry(player).join() == null
        roomTwo.checkForEntry(player).join() == null

        when:
        def lobby = roomOne.getEntry(player).join()
        
        then:
        player.currentLobby.join() == lobby
        
        and:
        roomOne.getLobbies().join().size() == 1
    }

    
    def "Get lobby from the second room"()
    {
        when:
        def lobby = roomTwo.getEntry(player).join()

        then:
        def ex = thrown(CompletionException)
        ex.cause.class == IllegalArgumentException

        and:
        roomTwo.getLobbies().join().size() == 0
        roomOne.getLobbies().join().size() == 1
        roomTwo.checkForEntry(player).join() == null
        roomOne.checkForEntry(player).join() == roomOne.getLobbies().join().first()
    }

}
