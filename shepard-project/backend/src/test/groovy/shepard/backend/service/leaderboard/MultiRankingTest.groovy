package shepard.backend.service.leaderboard

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.leaderboard.details.LeaderboardDetails
import shepard.backend.service.leaderboard.details.LeaderboardSort
import shepard.backend.service.leaderboard.details.LeaderboardType
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.backend.service.player.Player
import spock.lang.Shared
import spock.lang.Stepwise

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 01/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class MultiRankingTest extends OneStageSpec
{
    @Shared LeaderboardService service

    def "Setup a test leaderboard"()
    {
        def manager = Actor.getReference(LeaderboardManager)

        def details = new LeaderboardDetails(
                name: "Test leaderboard",
                type: LeaderboardType.ABSOLUTE,
                upperLimit: 1000,
                lowerLimit: 0,
                sort: LeaderboardSort.HIGH
        )

        service = manager.registerService(details).join()


        expect:
        manager.getAllEnabledServices().join().size() == 1
        manager.getAllEnabledServices().join().contains(service)
    }


    def "Post scores"()
    {

        expect:
        def player = Actor.getReference(Player,playerID)
        service.insertScore(player, score).join().getRawScore().join() == score as Long

        where:
        playerID    | score 
        'p1'        | 1     
        'p2'        | 2     
        'p3'        | 3     
        'p4'        | 4     
        'p5'        | 5     
        'p6'        | 6     
        'p7'        | 7     
        'p8'        | 8     
        'p9'        | 9     
        'p10'       | 10    
    }

    def "Perform ranking"()
    {
        service.rank().join()


        expect:
        service.getRankedTop(10).join().size() == 10

        (10..1).each {
            def player = "p$it"
            service.getEntry(Actor.getReference(Player,player)).join().getRank().join() == it
        }
    }

    def "Post next round of scores"()
    {

        expect:
        def player = Actor.getReference(Player,playerID)
        service.insertScore(player, score).join().getRawScore().join() == score as Long

        where:
        playerID    | score  
        'p1'        | 10     
        'p2'        | 20     
        'p3'        | 30     
        'p4'        | 40     
        'p5'        | 50     
        'p6'        | 60     
        'p7'        | 70     
        'p8'        | 80     
        'p9'        | 90     
        'p10'       | 100    
    }

    def "Perform next ranking"()
    {
        service.rank().join()


        expect:
        service.getRankedTop(10).join().size() == 10

        (10..1).each {
            def player = "p$it"
            service.getEntry(Actor.getReference(Player,player)).join().getRank().join() == it*10
        }
    }

    def "Check ranking again"()
    {
        service.rank().join()

        expect:
        service.getRankedTop(10).join().size() == 10

        (10..1).each {
            def player = "p$it"
            service.getEntry(Actor.getReference(Player,player)).join().getRank().join() == it*10
        }
    }

    def "And again"()
    {
        expect:
        (10..1).each {
            def player = "p$it"
            service.getEntry(Actor.getReference(Player,player)).join().getRank().join() == it*10
        }
    }

    def "Post another round of scores"()
    {

        expect:
        def player = Actor.getReference(Player,playerID)
        service.insertScore(player, score).join().getRawScore().join() == score as Long

        where:
        playerID    | score
        'p1'        | 10
        'p2'        | 9
        'p3'        | 8
        'p4'        | 7
        'p5'        | 6
        'p6'        | 5
        'p7'        | 4
        'p8'        | 3
        'p9'        | 2
        'p10'       | 1
    }

    def "Check ranking"()
    {
        service.rank().join()

        expect:
        service.getRankedTop(10).join().size() == 10

        (1..10).each {
            def player = "p$it"
            service.getEntry(Actor.getReference(Player,player)).join().getRank().join() == it
        }
    }
}
