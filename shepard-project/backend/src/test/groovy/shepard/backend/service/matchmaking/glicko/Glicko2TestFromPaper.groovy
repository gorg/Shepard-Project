package shepard.backend.service.matchmaking.glicko

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.details.Rules
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.backend.service.matchmaking.util.Glicko2
import shepard.backend.service.player.Player

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/27/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */

class Glicko2TestFromPaper extends OneStageSpec
{
    def "Recreating Glicko-2 test from the original paper"()
    {
        setup:
        Actor.getReference(Glicko2).setup(0.06, 0.5).join()
        def manager = Actor.getReference(RoomManager)
        def rules = new Rules(8,5,true,0.75)
        def details = new RoomDetails(name: "glicko 2 lobby",rules: rules)

        def lobbyFinder = manager.registerService(details).join()

        def player1 = Actor.getReference(Player, "player1")
        lobbyFinder.getEntry(player1).join()

        def player2 = Actor.getReference(Player, "player2")
        lobbyFinder.getEntry(player2).join()

        def player3 = Actor.getReference(Player, "player3")
        lobbyFinder.getEntry(player3).join()

        def player4 = Actor.getReference(Player, "player4")
        lobbyFinder.getEntry(player4).join()

        def player5 = Actor.getReference(Player, "player5")
        def lobby = lobbyFinder.getEntry(player5).join()

        def lobbyPlayers = lobby.getLobbyPLayers().join()
        def players = [player1, player2, player3, player4, player5]

        expect:
        players.each {
            assert lobbyPlayers.contains(it)
        }

        when:
        player1.skill.join().setRating(1500).join()
        player1.skill.join().setRatingDeviation(200).join()

        player2.skill.join().setRating(1400).join()
        player2.skill.join().setRatingDeviation(30).join()

        player3.skill.join().setRating(1550).join()
        player3.skill.join().setRatingDeviation(100).join()

        player4.skill.join().setRating(1700).join()
        player4.skill.join().setRatingDeviation(300).join()

        lobby.selectTeam(player1,0).join()
        lobby.selectTeam(player2,1).join()
        lobby.selectTeam(player3,2).join()
        lobby.selectTeam(player4,3).join()
        lobby.selectTeam(player5,4).join()

        def game = lobby.startGame().join()
        def teams = lobby.getTeams().join()

        game.addWin(teams[0],teams[1]).join()
        game.addWin(teams[2],teams[0]).join()
        game.addWin(teams[3],teams[0]).join()
        game.addParticipant(teams[4]).join()
        game.finishMatch().join()
        def rating  = Actor.getReference(Glicko2)

        then:
        assert player5.skill.join().getRating().join() == rating.defaultRating.join()
        assert player5.skill.join().getRatingDeviation().join() > rating.defaultRatingDeviation.join()
        assert player5.skill.join().getVolatility().join() == rating.defaultVolatility.join()


        assert player1.skill.join().getRating().join().round(2) == 1464.05d
        assert player1.skill.join().getRatingDeviation().join().round(2) == 151.52d
        assert player1.skill.join().volatility.join().trunc(5) == 0.05999d
    }




}
