package shepard.backend.service.stats

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.player.Player
import shepard.backend.service.stats.cache.StatCache
import shepard.backend.service.stats.details.StatDetails
import shepard.backend.service.stats.manager.StatManager
import spock.lang.Stepwise

import java.util.concurrent.CompletionException

import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 25/01/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 *
 **/
@Stepwise
class WorkflowStatsService extends OneStageSpec
{
    def "Get the Stat Manager"()
    {
        setup:
        def manager = Actor.getReference(StatManager)

        expect:
        manager.getAllEnabledServices().join().size() == 0
        manager.getAllDisabledServices().join().size() == 0
        manager.getServiceMap().join().size() == 0

    }

    def "Enable/Disable Stats while the manager is empty" ()
    {
        setup:
        def manager = Actor.getReference(StatManager)

        when:
        !manager.enableService(Actor.getReference(StatService, "random-service")).join()

        then:
        CompletionException ex = thrown()
        ex.cause.class == NoSuchElementException.class

        when:
        !manager.disableService(Actor.getReference(StatService, "random-service")).join()

        then:
        ex = thrown()
        ex.cause.class == NoSuchElementException.class
    }

    def "Register our first stat" ()
    {
        setup:
        def manager = Actor.getReference(StatManager)
        def details = new StatDetails(
                name: "Random stat",
                maxValue: 1000,
                minValue: -1000,
                defaultValue: 0,
                maxChange: 0
        )

        when:
        def service = manager.registerService(details).join()

        then:
        service.getServiceDetails().join() == details

        expect:
        manager.getAllEnabledServices().join().size() == 1
        manager.getAllEnabledServices().join().contains(service)
        manager.getAllDisabledServices().join().size() == 0
        manager.getServiceMap().join().get(service)
    }

    def "Disable and enable a registered stat"()
    {
        def manager = Actor.getReference(StatManager)
        def service = Actor.getReference(StatService,name2Id("Random stat"))

        expect:
        manager.getAllEnabledServices().join().contains(service)
        !manager.getAllDisabledServices().join().contains(service)

        when:
        assert manager.disableService(service).join()

        then:
        manager.getAllEnabledServices().join().size() == 0
        !manager.getAllEnabledServices().join().contains(service)
        manager.getAllDisabledServices().join().size() == 1
        manager.getAllDisabledServices().join().contains(service)

        when:
        assert manager.enableService(service).join()

        then:
        manager.getAllEnabledServices().join().size() == 1
        manager.getAllEnabledServices().join().contains(service)
        manager.getAllDisabledServices().join().size() == 0
        !manager.getAllDisabledServices().join().contains(service)

    }

    def "Remove the stat"()
    {
        setup:
        def manager = Actor.getReference(StatManager)
        def service = Actor.getReference(StatService,name2Id("Random stat"))

        expect:
        !manager.removeService(Actor.getReference(StatService,name2Id("Non valid stat"))).join()

        when:
        assert manager.removeService(service).join()

        then:
        manager.getAllEnabledServices().join().size() == 0
        !manager.getAllEnabledServices().join().contains(service)
        manager.getAllDisabledServices().join().size() == 0
        !manager.getAllDisabledServices().join().contains(service)
    }

    def "Actually stats will always exist"()
    {
        setup:
        def service = Actor.getReference(StatService,name2Id("Random stat"))

        expect:
        service.getServiceDetails().join() == null
    }

    def "Activating the stats cache"()
    {
        setup:
        def cache = Actor.getReference(StatCache)

        expect:
        cache.retrieve().join().size() == 0
    }

    def "Registering a new stat and pushing it to the cache"()
    {
        setup:
        def manager = Actor.getReference(StatManager)
        def details = new StatDetails(
                name: "Another Stat",
                defaultValue: 0,
                maxChange: 5,
                minValue: -100,
                maxValue: 100
        )

        def service = manager.registerService(details).join()

        expect:
        manager.getAllEnabledServices().join().size() == 1
        manager.getAllEnabledServices().join().contains(service)

        when:
        def cache = Actor.getReference(StatCache)

        then:
        cache.retrieve().join().size() == 1
        cache.retrieve().join().contains(service)

    }

    def "A disabled stat will not be shown in the cache"()
    {
        setup:
        def manager = Actor.getReference(StatManager)
        def cache = Actor.getReference(StatCache)
        def service = Actor.getReference(StatService, name2Id("Another stat"))

        when:
        assert manager.disableService(service).join()

        then:
        cache.retrieve().join().size() == 0
        !cache.retrieve().join().contains(service)
    }

    def "An enabled stat will be shown in the cache"()
    {
        setup:
        def manager = Actor.getReference(StatManager)
        def cache = Actor.getReference(StatCache)
        def service = Actor.getReference(StatService, name2Id("Another stat"))

        when:
        assert manager.enableService(service).join()

        then:
        cache.retrieve().join().size() == 1
        cache.retrieve().join().contains(service)
    }

    def "If an enabled stat is removed from the manager will not show in the cache"()
    {
        setup:
        def manager = Actor.getReference(StatManager)
        def cache = Actor.getReference(StatCache)
        def service = Actor.getReference(StatService, name2Id("Another stat"))

        when:
        assert manager.removeService(service).join()

        then:
        cache.retrieve().join().size() == 0
        !cache.retrieve().join().contains(service)
    }

    def "If an disabled stat is removed from the manager will not show in the cache"()
    {
        setup:
        def manager = Actor.getReference(StatManager)
        def cache = Actor.getReference(StatCache)
        def details = new StatDetails(
                name: "Yet another Stat",
                defaultValue: 0,
                maxChange: 5,
                minValue: -100,
                maxValue: 100
        )
        def service = manager.registerService(details).join()

        when:
        assert manager.disableService(service).join()

        then:
        cache.retrieve().join().size() == 0
        !cache.retrieve().join().contains(service)

        when:
        assert manager.removeService(service).join()

        then:
        cache.retrieve().join().size() == 0
        !cache.retrieve().join().contains(service)
    }


    def "Multiple stat propagation and lyfecycle"()
    {
        setup:
        def manager = Actor.getReference(StatManager)
        def details1 = new StatDetails(name: "Another stat 1")
        def service1 = manager.registerService(details1).join()

        def details2 = new StatDetails(name: "Another stat 2")
        def service2 = manager.registerService(details2).join()

        def details3 = new StatDetails(name: "Another stat 3")
        def service3 = manager.registerService(details3).join()

        def achievement = [service1, service2, service3]

        when:
        def cache = Actor.getReference(StatCache)

        then:
        cache.retrieve().join().size() == 3
        achievement.each {
            assert cache.retrieve().join().contains(it)
        }

        when:
        assert manager.disableService(service3).join()
        assert manager.removeService(service1).join()
        def anotherCacheReference = Actor.getReference(StatCache)

        then:
        anotherCacheReference.retrieve().join().size() == 1
        anotherCacheReference.retrieve().join().contains(service2)
        !anotherCacheReference.retrieve().join().contains(service1)
        !anotherCacheReference.retrieve().join().contains(service3)

        expect:
        manager.allDisabledServices.join().contains(service3)
        manager.allDisabledServices.join().size() == 1
        manager.allEnabledServices.join().contains(service2)
        manager.allEnabledServices.join().size() == 1
    }

    def "Players are up to date"()
    {
        setup:
        def player = Actor.getReference(Player, "random-player-1")
        def manager = Actor.getReference(StatManager)

        expect:
        manager.allEnabledServices.join().each {
            player.getStat(it).join().statValue.join() == 0.0f
        }


        when:
        player.getStat(manager.allDisabledServices.join().first()).join().statValue.join()

        then:
        thrown(CompletionException)
    }
}
