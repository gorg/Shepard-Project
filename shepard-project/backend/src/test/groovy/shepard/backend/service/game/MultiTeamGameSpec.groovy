package shepard.backend.service.game

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.details.Rules
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.backend.service.player.Player
import spock.lang.Shared
import spock.lang.Stepwise

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 20/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class MultiTeamGameSpec extends OneStageSpec
{

    @Shared Game game
    @Shared Player p1
    @Shared Player p2
    @Shared Player p3
    @Shared Player p4
    @Shared Player p5
    @Shared Player p6
    @Shared Player p7
    @Shared Player p8


    def "setup lobby"()
    {
        given:
        def room = Actor.getReference(RoomManager).
                registerService(new RoomDetails(name: 'Test Room', rules: new Rules(teams: 4, players: 8))).join()

        p1 = Actor.getReference(Player, 'player-1')
        p2 = Actor.getReference(Player, 'player-2')
        p3 = Actor.getReference(Player, 'player-3')
        p4 = Actor.getReference(Player, 'player-4')
        p5 = Actor.getReference(Player, 'player-5')
        p6 = Actor.getReference(Player, 'player-6')
        p7 = Actor.getReference(Player, 'player-7')
        p8 = Actor.getReference(Player, 'player-8')

        def players = [p1, p2, p3, p4, p5, p6, p7, p8]

        players.each { room.getEntry(it).join() }

        expect: 'all players are in the same lobby'
        players.each { a ->
            players.each { b ->
                a.getCurrentLobby().join() == b.getCurrentLobby().join()
            }
        }

        and:
        room.getLobbies().join().size() == 1

        when:
        game = room.getLobbies().join().first().startGame().join()

        then: 'all players are in the same game'
        players.each { it.getCurrentGame().join() == game }
    }

    def "post a victory"()
    {
        when: 'team 0 wins over the other three teams'
        def g = game.addWin(0).join()

        then:
        g.getResults().join().getAllResults().size() == 12

        and:
        g.getLobby().join().getTeam(0).join().getTeamMembers().join().each
        {
            g.getResults().join().getResults(it).each { res -> assert res.winner == it }
        }
    }

    def "post a draw"()
    {
        when:
        def g = game.addDraw().join()

        then:
        g.getResults().join().getAllResults().size() == 24

        and:
        g.getLobby().join().getLobbyPLayers().join().each {
            assert g.getResults().join().getResults(it).last().draw
        }
    }

    def "finish game"()
    {
        given:
        def players = [p1, p2, p3, p4, p5, p6, p7, p8]

        when:
        game.finishMatch().join()

        then:
        players.each { it.getCurrentGame().join() == null }
        players.each { it.getCurrentLobby().join() == null }
    }

}
