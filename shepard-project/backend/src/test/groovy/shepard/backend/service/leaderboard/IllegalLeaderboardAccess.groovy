package shepard.backend.service.leaderboard

import cloud.orbit.actors.Actor
import shepard.backend.OneStageSpec
import shepard.backend.service.leaderboard.details.LeaderboardDetails
import shepard.backend.service.leaderboard.details.LeaderboardSort
import shepard.backend.service.leaderboard.details.LeaderboardType
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.backend.service.leaderboard.ranking.Rank
import shepard.backend.service.player.Player

import java.util.concurrent.CompletionException

import static shepard.backend.util.id.GenerateId.generateId
import static spock.util.Exceptions.getRootCause

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 30/01/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class IllegalLeaderboardAccess extends OneStageSpec
{
    def "Set up the leaderboard and player entry" ()
    {
        setup:
        def manager = Actor.getReference(LeaderboardManager)
        def details = new LeaderboardDetails(
                name: "test leaderboard", 
                sort:LeaderboardSort.HIGH,
                type:LeaderboardType.ABSOLUTE,
                lowerLimit:-100,
                upperLimit:100,
                icon:null)
                

        when:
        def service = manager.registerService(details).join()

        then:
        manager.isEnabled(service).join()
    }

    def "Manually setup service entry"()
    {
        setup:
        def manager = Actor.getReference(LeaderboardManager)
        def service = manager.getAllEnabledServices().join().first()
        def player1 = Actor.getReference(Player, "player 1")
        def player2 = Actor.getReference(Player, "plater 2")

        when:
        Actor.getReference(Rank, generateId(service,player1)).setupEntry(service,player2).join()

        then:
        def ex = thrown(CompletionException)
        assert getRootCause(ex).class == IllegalAccessError
        assert getRootCause(ex).message == "Rank ID is not matching the arguments"


        when:
        def rank = service.getEntry(player1).join()

        then:
        rank.getScore().join().rawScore.join() == 0l
        rank.player.join() != player2
        rank.player.join() == player1
    }
}
