package shepard.backend

import cloud.orbit.actors.Stage
import cloud.orbit.actors.extensions.json.InMemoryJSONStorageExtension
import com.ea.async.Async

import static com.ea.async.Async.await

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 10/12/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class BackendMain
{
    static void main(String[] args)
    {
        Async.init()
        Stage stage = new Stage()
        stage.addExtension(new InMemoryJSONStorageExtension())
        stage.setClusterName("shepard-service")
        stage.setNodeName("shepard-backend")
        stage.setMode(Stage.StageMode.HOST)
        await(stage.start())
        stage.bind()
    }
}
