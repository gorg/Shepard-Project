package shepard.backend.service.matchmaking.util

import cloud.orbit.actors.runtime.AbstractActor
import cloud.orbit.concurrent.Task
import shepard.backend.service.player.Player

import static com.ea.async.Async.await

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/25/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class SkillActor extends AbstractActor<State> implements Skill
{
    static class State
    {
        boolean set = false
        String player

        double rating
        double ratingDeviation
        double volatility
        int numberOfResults = 0
    }

    private double workingRating
    private double workingRatingDeviation
    private double workingVolatility

    def ratingSystem = getReference(Glicko2)

    @Override
    Task<Skill> setup(Player player)
    {
        if(!state.set)
        {
            state.player = player.identity
            state.set = true
            state.rating = await(ratingSystem.defaultRating)
            state.ratingDeviation = await(ratingSystem.defaultRatingDeviation)
            state.volatility = await(ratingSystem.defaultVolatility)
            await(super.writeState())
        }
        Task.fromValue(this)
    }

    @Override
    Task<Double> getRating()
    {
        Task.fromValue(state.rating)
    }

    @Override
    Task<Double> getGlicko2Rating()
    {
        Task.fromValue(
                await(ratingSystem.convertRatingToGlicko2Scale(state.rating)))
    }

    @Override
    Task<Void> setGlicko2Rating(double glicko2Rating)
    {
        state.rating = await(
                ratingSystem.convertRatingToOriginalGlickoScale(glicko2Rating))
        await(super.writeState())
        Task.done()
    }

    @Override
    Task<Void> setRating(double rating)
    {
        state.rating = rating
        await(super.writeState())
        Task.done()
    }

    @Override
    Task<Double> getRatingDeviation()
    {
        Task.fromValue(state.ratingDeviation)
    }

    @Override
    Task<Double> getGlicko2RatingDeviation()
    {
        Task.fromValue(
                await(ratingSystem.convertRatingDeviationToGlicko2Scale(state.ratingDeviation)))
    }

    @Override
    Task<Void> setGlicko2RatingDeviation(double glicko2RatingDeviation)
    {
        state.ratingDeviation = await(
                ratingSystem.convertRatingDeviationToOriginalGlickoScale(glicko2RatingDeviation))
        await(super.writeState())
        Task.done()
    }

    @Override
    Task<Void> setRatingDeviation(double ratingDeviation)
    {
        state.ratingDeviation = ratingDeviation
        await(super.writeState())
        Task.done()
    }

    @Override
    Task<Double> getVolatility()
    {
        Task.fromValue(state.volatility)
    }

    @Override
    Task<Void> setVolatility(double volatility)
    {
        state.volatility = volatility
        await(super.writeState())
        Task.done()
    }

    @Override
    Task<Void> incrementNumberOfResults(int increment)
    {
        state.numberOfResults += increment
        await(super.writeState())
        Task.done()
    }

    @Override
    Task<Integer> getNumberOfResults()
    {
        Task.fromValue(state.numberOfResults)
    }

    @Override
    Task<Void> setWorkingRating(double workingRating)
    {
        this.workingRating = workingRating
        Task.done()
    }

    @Override
    Task<Void> setWorkingRatingDeviation(double workingRatingDeviation)
    {
        this.workingRatingDeviation = workingRatingDeviation
        Task.done()
    }

    @Override
    Task<Void> setWorkingVolatility(double workingVolatility)
    {
        this.workingVolatility = workingVolatility
        Task.done()
    }

    @Override
    Task<Void> finishRating()
    {
        await(this.setGlicko2Rating(this.workingRating))
        await(this.setGlicko2RatingDeviation(this.workingRatingDeviation))
        await(this.setVolatility(this.workingVolatility))

        this.workingRating = 0
        this.workingRatingDeviation = 0
        this.workingVolatility = 0

        Task.done()
    }

    @Override
    Task<SkillRange> getSkillRange68()
    {
        def range = new SkillRange
        (
            low: state.rating-state.ratingDeviation,
            high: state.rating+state.ratingDeviation
        )
        Task.fromValue(range)
    }

    @Override
    Task<SkillRange> getSkillRange95()
    {
        def range = new SkillRange
        (
            low: state.rating-2*state.ratingDeviation,
            high: state.rating+2*state.ratingDeviation
        )
        Task.fromValue(range)
    }

    @Override
    Task<SkillRange> getSkillRange99()
    {
        def range = new SkillRange
        (
            low: state.rating-3*state.ratingDeviation,
            high: state.rating+3*state.ratingDeviation
        )
        Task.fromValue(range)
    }

    @Override
    Task<String> resume()
    {
        def sb = new StringBuilder()
        sb.append("Rating: ")
        sb.append(state.rating)
        sb.append("\nRD: ")
        sb.append(state.ratingDeviation)
        sb.append("\nVolatility: ")
        sb.append(state.volatility)
        sb.append("\nMatches: ")
        sb.append(state.numberOfResults)
        Task.fromValue(sb.toString())
    }
}
