package shepard.backend.service.generic

import cloud.orbit.actors.Actor
import cloud.orbit.actors.runtime.AbstractActor
import cloud.orbit.actors.streams.AsyncStream
import cloud.orbit.concurrent.Task
import shepard.backend.service.player.Player

import static com.ea.async.Async.await

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/16/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
abstract class AbstractServiceManager<S extends Service, D extends ServiceDetails>
        extends AbstractActor<State> implements ServiceManager<S,D>
{
    protected AsyncStream<S> stream

    static class State
    {
        HashMap<String, Boolean> enabledReferences = new HashMap<>()
    }

    abstract protected S add(D details)

    protected S pushService(S service)
    {
        //Save to state
        state().enabledReferences.put(service.identity,true)

        //Persist changes
        await(super.writeState())

        //Notify all players that there is new leaderboard to track
        await(stream.publish(service))

        return service
    }

    @Override
    Task<S> registerService(D details)
    {
        Task.fromValue(add(details))
    }

    @Override
    Task<Boolean> disableService(S actor)
    {
        def service = actor.identity

        if (state().enabledReferences.containsKey(service))
        {
            //Only disables a service if it's enabled
            if (state().enabledReferences.get(service))
            {
                state().enabledReferences.put(service, false)
                await(stream.publish(actor))
                await(super.writeState())
                Task.fromValue(true)
            }
            else
                Task.fromValue(false)
        }
        else
            throw new NoSuchElementException()
    }

    @Override
    Task<Boolean> enableService(S actor)
    {
        def service = actor.identity

        if (state().enabledReferences.containsKey(service))
        {
            //Only enables a service if it's disabled
            if (!state().enabledReferences.get(service))
            {
                state().enabledReferences.put(service, true)
                await(stream.publish(actor))
                await(super.writeState())
                Task.fromValue(true)
            }
            else
                Task.fromValue(false)
        }
        else
            throw new NoSuchElementException()
    }

    @Override
    Task<Boolean> isEnabled(S service)
    {
        Task.fromValue(state().enabledReferences.get(service.identity))
    }

    @Override
    Task<List<Entry>> getAllEntries(String playerId)
    {
        def player = getReference(Player, playerId)
        Task.fromValue(state().enabledReferences.keySet().collect{ await(getService(it).getEntry(player)) })
    }

    @Override
    Task<Boolean> removeService(S actor)
    {
        def service = actor.identity

        if (state().enabledReferences.containsKey(service))
        {
            //If the achievements is enabled
            //It has to be pushed to the caches for a second time to remove it from caching
            if (state().enabledReferences.get(service))
            {
                await(stream.publish(actor))
            }

            state().enabledReferences.remove(service)
            actor.removeService().join()
            await(super.writeState())
            Task.fromValue(true)
        }
        else
            Task.fromValue(false)
    }

    @Override
    Task<Boolean> isValidId(S service)
    {
        Task.fromValue (state().enabledReferences.get(service.identity) != null)
    }

    abstract S getService(String id)

    @Override
    Task<Set<S>> getAllEnabledServices()
    {
        Task.fromValue(state().enabledReferences
                .findAll {k,v -> v }
                .collect {k,v -> getService(k)} as Set)
    }

    @Override
    Task<Set<S>> getAllDisabledServices()
    {
        Task.fromValue(state().enabledReferences
                .findAll {k,v -> !v }
                .collect {k,v -> getService(k)} as Set)
    }

}
