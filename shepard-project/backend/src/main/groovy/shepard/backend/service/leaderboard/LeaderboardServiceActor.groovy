package shepard.backend.service.leaderboard

import cloud.orbit.concurrent.Task
import cloud.orbit.tuples.Pair
import shepard.backend.service.generic.AbstractService
import shepard.backend.service.leaderboard.comparators.FirstHighComparator
import shepard.backend.service.leaderboard.comparators.FirstLowComparator
import shepard.backend.service.leaderboard.details.LeaderboardDetails
import shepard.backend.service.leaderboard.details.LeaderboardSort
import shepard.backend.service.leaderboard.details.LeaderboardType
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.backend.service.leaderboard.ranking.Rank
import shepard.backend.service.leaderboard.score.Score
import shepard.backend.service.leaderboard.score.updater.AbsoluteScoreUpdater
import shepard.backend.service.leaderboard.score.updater.IncrementalScoreUpdater
import shepard.backend.service.leaderboard.score.updater.OnlyHigher
import shepard.backend.service.leaderboard.score.updater.OnlyLower
import shepard.backend.service.player.Player

import java.util.function.Function
import java.util.stream.Collectors

import static com.ea.async.Async.await
import static shepard.backend.util.id.GenerateId.generateId

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 10/26/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class LeaderboardServiceActor
    extends AbstractService<LeaderboardService, LeaderboardDetails, LeaderboarData, Rank>
    implements LeaderboardService
{
    @Override
    Task<LeaderboardService> setupService(LeaderboardDetails details)
    {
        state().details = details
        state().data = new LeaderboarData()
        state().data.players = new HashSet<>()
        state().data.ranking = new ArrayList<>()

        if (details.sort == LeaderboardSort.HIGH)
            state().data.comparator = new FirstHighComparator()
        else
            state().data.comparator = new FirstLowComparator()

        def lowerLimit = state().details.lowerLimit
        def upperLimit = state().details.upperLimit

        switch (details.type)
        {
            case LeaderboardType.ABSOLUTE:
                state().data.updater = new AbsoluteScoreUpdater(lowerLimit: lowerLimit, upperLimit: upperLimit)
                break
            case LeaderboardType.INCREMENTAL:
                state().data.updater = new IncrementalScoreUpdater(lowerLimit: lowerLimit, upperLimit: upperLimit)
                break
            case LeaderboardType.ONLY_HIGHER:
                state().data.updater = new OnlyHigher(lowerLimit: lowerLimit, upperLimit: upperLimit)
                break
            case LeaderboardType.ONLY_LOWER:
                state().data.updater = new OnlyLower(lowerLimit: lowerLimit, upperLimit: upperLimit)
                break
        }
        await(super.writeState())
        Task.fromValue(this)
    }

    @Override
    Task<Rank> getEntry(Player player)
    {
        entry(player)
    }

    @Override
    Task<Long> rank()
    {
        state().data.ranking.clear()

        state().data.players.parallelStream()
        .map(new Function<String,Player>()
        {
            @Override
            Player apply(String s) {
                getReference(Player,s)
            }
        })
        .map(new Function<Player,Rank>()
        {
            @Override
            Rank apply(Player player)
            {
                await(entry(player))
            }
        }).map(new Function<Rank,Pair<Rank,Score>>()
        {
            @Override
            Pair<Rank, Score> apply(Rank rank)
            {
                Pair.of(rank,await(rank.getScore()))
            }
        }).sorted(state().data.comparator).collect(Collectors.toList()).eachWithIndex
        { pair, index ->
            def rank = ((Rank) pair.left)
            def score = await(((Score) pair.right).rawScore)
            await(rank.updateScore(score))
            rank = await(rank.changeRank(index+1))
            state().data.ranking << rank.identity
        }
        await(super.writeState())
        Task.fromValue(state().data.ranking.size())
    }

    @Override
    Task<Score> insertScore(Player player, long points)
    {
        //Stores player reference
        if(state().data.players.add(player.identity))
            await(super.writeState())

        //Updates score
        state().data.updater.updateScore(entry(player), points)
    }

    @Override
    Task<List<Rank>> getRankedLeaderboard()
    {
        getRankList(state().data.ranking)
    }

    @Override
    Task<List<Rank>> getRankedTop(int end)
    {
        if (end > state().data.ranking.size())
            getRankedLeaderboard()
        else
            getRankList(state().data.ranking.subList(0,end))
    }

    @Override
    Task<List<Rank>> getRankedRelativeLeaderboard(int start, int end)
    {
        def size = state().data.ranking.size()

        if (start > end)
            throw new IllegalArgumentException("Relative rank query $start > $end")
        if (start > size)
            throw new IllegalArgumentException("Relative rank query $start > rank.size()")
        if (end > size)
            end = size
        if (start < 0)
            start = 0
        getRankList(state().data.ranking.subList(start,end))
    }

    private static Task<List<Rank>> getRankList(List<String> rankIdentities)
    {
       Task.fromValue(rankIdentities.collect{ getReference(Rank,it) })
    }

    private Task<Rank> entry(Player player)
    {
        getReference(Rank, generateId(this,player)).setupEntry(this,player)
    }

    @Override
    Task<Boolean> isEnabled()
    {
        getReference(LeaderboardManager).isEnabled(this)
    }
}
