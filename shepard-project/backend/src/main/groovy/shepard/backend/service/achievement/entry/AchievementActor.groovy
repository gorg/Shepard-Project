package shepard.backend.service.achievement.entry

import cloud.orbit.actors.runtime.AbstractActor
import cloud.orbit.concurrent.Task
import shepard.backend.service.achievement.AchievementService
import shepard.backend.service.player.Player

import java.time.LocalDateTime

import static com.ea.async.Async.await
import static shepard.backend.util.id.GenerateId.generateId

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/8/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class AchievementActor extends AbstractActor<State> implements Achievement
{
    static class State
    {
        boolean isCompleted = false
        boolean set = false
        String timestamp
        String service
        Player player
    }

    @Override
    Task<Achievement> setupEntry(AchievementService service, Player player)
    {
        if(generateId(service,player) != this.getIdentity())
        {
            throw new IllegalAccessError("Actor identity is not matching the arguments")
        }

        if (!state().set)
        {
            state().player = player
            state().service = service.identity
            state().isCompleted = false
            state().set = true
            await(super.writeState())
        }
        Task.fromValue(this)
    }

    @Override
    Task<Boolean> complete()
    {
        if (state().isCompleted)
        {
            Task.fromValue(false)
        }
        else
        {
            state().isCompleted = true
            state().timestamp = LocalDateTime.now().toString()
            await(super.writeState())
            Task.fromValue(true)
        }
    }

    @Override
    Task<Boolean> isCompleted()
    {
        Task.fromValue(state().isCompleted)
    }

    @Override
    Task<LocalDateTime> whenWasCompleted()
    {
        LocalDateTime time = null
        if (state().timestamp != null)
            time = LocalDateTime.parse(state().timestamp)

        Task.fromValue(time)
    }

    @Override
    Task<AchievementService> getService()
    {
        Task.fromValue(getReference(AchievementService,state().service))
    }

    @Override
    Task<Player> getPlayer()
    {
        Task.fromValue(state().player)
    }

    @Override
    Task<Map> collectEntry()
    {
        def stateMap = [:]

        if (state.set)
        {
            stateMap['player'] = state().player.identity
            stateMap['achievement'] = state().service
            stateMap['completed'] = state().isCompleted

            if(state().timestamp)
                stateMap['when'] = state().timestamp
        }
        Task.fromValue(stateMap)
    }

}
