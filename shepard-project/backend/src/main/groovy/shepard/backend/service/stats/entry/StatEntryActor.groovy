package shepard.backend.service.stats.entry

import cloud.orbit.actors.runtime.AbstractActor
import cloud.orbit.concurrent.Task
import shepard.backend.service.player.Player
import shepard.backend.service.stats.StatService

import java.util.function.Function
import java.util.function.Supplier

import static com.ea.async.Async.await
import static shepard.backend.util.id.GenerateId.generateId

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/18/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class StatEntryActor extends AbstractActor<State> implements StatEntry
{
    static class State
    {
        BigDecimal value
        String player
        String service
        boolean set = false
    }

    @Override
    Task<Map> collectEntry()
    {
        if (state.value == null)
            state.value = await(await(getService()).getServiceDetails()).getDefaultValue()

        def stateMap = [:]
        if (state.set)
        {
            stateMap['stat'] = state().service
            stateMap['player'] = state().player
            stateMap['score'] = state().value
        }
        Task.fromValue(stateMap)
    }

    @Override
    Task<StatEntryActor> setupEntry(StatService service, Player player)
    {
        if(generateId(service,player) != this.getIdentity())
        {
            throw new IllegalAccessError("Actor identity is not matching the arguments")
        }

        if(!state().set)
        {
            state().service = service.identity
            state().player = player.identity
            state().set = true

            await(super.writeState())
        }
        Task.fromValue(this)
    }

    @Override
    Task<BigDecimal> getStatValue()
    {
        Task.fromValue(state().value)
    }

    @Override
    Task<StatEntry> updateStatValue(BigDecimal newValue)
    {
        if (state.value == null)
            state.value = await(await(getService()).getServiceDetails()).getDefaultValue()

         getService()
            .thenCompose({ it.test(newValue,state.value) } as Function)
            .thenApply{ if(it) setValue(newValue) }
            .thenReturn( {this} as Supplier<StatEntry> )
    }

    private void setValue(BigDecimal value)
    {
        state().value = value
        await(super.writeState())
    }

    @Override
    Task<StatService> getService()
    {
        Task.fromValue(getReference(StatService,state().service))
    }

    @Override
    Task<Player> getPlayer()
    {
        Task.fromValue(getReference(Player,state.player))
    }
}
