package shepard.backend.service.stats

import cloud.orbit.actors.annotation.Reentrant
import cloud.orbit.concurrent.Task
import shepard.backend.service.generic.AbstractService
import shepard.backend.service.player.Player
import shepard.backend.service.stats.details.StatDetails
import shepard.backend.service.stats.entry.StatEntry
import shepard.backend.service.stats.manager.StatManager

import java.util.function.Function

import static shepard.backend.util.id.GenerateId.generateId

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/17/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class StatServiceActor
        extends AbstractService<StatService, StatDetails, StatData, StatEntry>
        implements StatService
{
    @Override
    Task<StatEntry> getEntry(Player player)
    {
        getReference(StatEntry, generateId(this, player)).setupEntry(this,player)
    }

    @Override
    @Reentrant
    Task<StatEntry> update(BigDecimal newValue, Player player)
    {
        getEntry(player).thenCompose({it.updateStatValue(newValue)} as Function)
    }

    @Override
    @Reentrant
    Task<Boolean> test(BigDecimal newValue, BigDecimal oldValue)
    {
        Task.fromValue(state().details.test(newValue,oldValue))
    }

    @Override
    Task<Boolean> isEnabled()
    {
        getReference(StatManager).isEnabled(this)
    }
}
