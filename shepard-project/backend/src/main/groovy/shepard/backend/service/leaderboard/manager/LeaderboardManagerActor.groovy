package shepard.backend.service.leaderboard.manager

import cloud.orbit.actors.streams.AsyncStream
import cloud.orbit.concurrent.Task
import shepard.backend.service.generic.AbstractServiceManager
import shepard.backend.service.leaderboard.LeaderboardService
import shepard.backend.service.leaderboard.details.LeaderboardDetails

import static com.ea.async.Async.await

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 23/10/16.
 * email: gueorgui.tzvetoslavov@gmail.com
 */
class LeaderboardManagerActor
        extends AbstractServiceManager<LeaderboardService, LeaderboardDetails>
        implements LeaderboardManager
{
    @Override
    Task<?> activateAsync()
    {
        //Used to notify every player currently subscribed that there was a new achievements created
        stream = AsyncStream.getStream(LeaderboardService, "leaderboard-async-stream")
        return super.activateAsync()
    }


    @Override
    protected LeaderboardService add(LeaderboardDetails details)
    {
        if (!details.validate())
            throw new IllegalArgumentException("Bad leaderboard details")

        LeaderboardService leaderboard

        if(state().enabledReferences.containsKey(details.id()))
            leaderboard = getReference(LeaderboardService, details.id())
        else
        {
            leaderboard = await(getReference(LeaderboardService, details.id()).setupService(details))
            pushService(leaderboard)
        }
        return leaderboard
    }

    @Override
    LeaderboardService getService(String id)
    {
        getReference(LeaderboardService,id)
    }

    @Override
    Task<Map<LeaderboardService, Boolean>> getServiceMap()
    {
        Task.fromValue(
                state.enabledReferences
                .collectEntries {k,v -> [(getReference(LeaderboardService,k)):v] }
                as Map<LeaderboardService, Boolean>
        )
    }
}
