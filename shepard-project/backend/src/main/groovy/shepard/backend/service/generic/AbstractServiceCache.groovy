package shepard.backend.service.generic

import cloud.orbit.actors.runtime.AbstractActor
import cloud.orbit.actors.streams.AsyncStream
import cloud.orbit.actors.streams.StreamSubscriptionHandle
import cloud.orbit.concurrent.Task

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/14/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
abstract class AbstractServiceCache<E extends Service>
        extends AbstractActor
        implements ServiceCache<E>
{
    protected HashSet<String> cache
    protected static AsyncStream<E> stream
    protected StreamSubscriptionHandle<E> streamHandle

    @Override
    Task activateAsync()
    {
        this.getLogger().info("Activating cache")
        return super.activateAsync()
    }

    @Override
    Task deactivateAsync()
    {
        this.getLogger().info("Deactivating cache")
        return super.deactivateAsync()
    }

    protected void addToCache(E service)
    {
        if (cache.contains(service.identity))
        {
            getLogger().info("$service disabled")
            cache.remove(service.identity)
        }
        else
        {
            getLogger().info("$service cached")
            cache.add(service.identity)
        }
    }

}
