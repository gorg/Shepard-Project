package shepard.backend.service.matchmaking.entry

import cloud.orbit.actors.runtime.AbstractActor
import cloud.orbit.concurrent.Task
import shepard.backend.service.game.Game
import shepard.backend.service.matchmaking.RoomService
import shepard.backend.service.matchmaking.details.Rules
import shepard.backend.service.matchmaking.util.Team
import shepard.backend.service.player.Player
import shepard.util.statistics.IntersectionArea

import java.util.function.Consumer
import java.util.function.Function
import java.util.function.Predicate
import java.util.stream.Collectors

import static com.ea.async.Async.await
import static shepard.backend.util.id.TeamIdGenerator.generateTeamID

/*
 *
 * Created by Gueorgui Tzvetoslavov Topalski on 12/13/16.
 * email: gueorgui.tzvetoslavov@gmail.com
 *
 */
class LobbyActor extends AbstractActor<State> implements Lobby
{
    static class State
    {
        String service
        Boolean set = false
        Boolean game = false
        HashSet<String> lobbyPlayers = new HashSet<>()
        ArrayList<String> teams = new ArrayList<>()

        Rules lobbyRules
        double lobbyMu = 0
        double lobbySigma = 0

        def addTeam(Team team)
        {
            teams.add(team.identity)
        }

        def hasTeam(Team team)
        {
            teams.contains(team)
        }

        def getTeam(int team)
        {
            teams.get(team) ? getReference(Team, teams.get(team)) : null
        }

        def getTeamList()
        {
            teams.stream().map( new Function<String, Team>() {
                @Override
                Team apply(String teamId) {
                    getReference(Team, teamId)
                }
            }).collect(Collectors.toList()) as ArrayList<Team>
        }

        def selectTeam(Player player, int team)
        {
            if (team < teams.size() && team >= 0)
            {
                leaveTeam(player)
                await(getTeam(team).addTeamMember(player))
            }
        }

        def leaveTeam(Player player)
        {
            teams.stream()
                    .map{ getReference(Team, it) }
                    .filter{ await(it.isInTeam(player)) }
                    .allMatch{ await(it.removeTeamMember(player))}
        }

        def addPlayer(Player player)
        {
            lobbyPlayers.add(player.identity)
        }

        def hasPlayer(Player player)
        {
            lobbyPlayers.contains(player.identity)
        }

        def removePlayer(Player player)
        {
            lobbyPlayers.remove(player.identity)
        }

        def hasFreeSpot()
        {
            lobbyPlayers.size() < lobbyRules.players
        }

        def getPlayerSet()
        {
            lobbyPlayers.stream()
                    .map(new Function<String, Player>()
            {
                @Override
                Player apply(String playerId)
                {
                    getReference(Player, playerId)
                }
            }).collect(Collectors.toSet()) as HashSet<Player>
        }

    }


    @Override
    Task<Map> collectEntry()
    {
        def stateMap = [:]
        if(state.set)
        {
            stateMap['id'] = this.identity
            stateMap['room'] = state().service
            //stateMap['rules'] = state().lobbyRules
            stateMap['rating'] = state().lobbyMu
            stateMap['deviation'] = state().lobbySigma
            stateMap['players'] = state().lobbyPlayers
            //stateMap['teams'] = state().teams
        }
        Task.fromValue(stateMap)
    }

    @Override
    Task<Lobby> setupEntry(RoomService service, Player player, Rules rules)
    {
        if (!state.set)
        {
            state.set = true
            state.service = service.identity
            setupLobby(rules)
        }
        addPlayerToLobby(player)
        await(super.writeState())
        Task.fromValue(this)
    }

    @Override
    Task<Lobby> setupEntry(RoomService service, Player player)
    {
        if (!state.set)
        {
            state.service = service.identity
            setupLobby(new Rules())
        }
        addPlayerToLobby(player)
        await(super.writeState())
        Task.fromValue(this)
    }

    @Override
    Task<Boolean> addPlayer(Player player)
    {
        Task.fromValue(addPlayerToLobby(player))
    }

    @Override
    Task<Void> setRules(Rules rules)
    {
        state.lobbyRules = rules
        Task.done()
    }

    @Override
    Task<Rules> getLobbyRules()
    {
        Task.fromValue(state.lobbyRules)
    }

    @Override
    Task<Boolean> removePlayer(Player player)
    {
        if(state.hasPlayer(player))
        {
            state.removePlayer(player)
            state.getTeamList().stream().forEach{ team -> await(team.removeTeamMember(player)) }
            await(player.setCurrentLobby(null))

            if (state.playerSet.size())
            {
                updateMuAndSigma()
                await(super.writeState())
            }
            Task.fromValue(true)
        }
        else
            Task.fromValue(false)
    }

    private void updateMuAndSigma()
    {
        state.lobbyMu =  state.getLobbyPlayers().stream()
                .map{ id -> getReference(Player,id) }
                .map{ p -> await(p.getSkill()) }
                .mapToDouble{ skill -> await(skill.getRating()) }
                .sum() / state.lobbyPlayers.size()

        state.lobbySigma =  state.getLobbyPlayers().stream()
                .map{ id -> getReference(Player,id) }
                .map{ p -> await(p.getSkill()) }
                .mapToDouble{ skill -> await(skill.getRatingDeviation()) }
                .sum() / state.lobbyPlayers.size()
    }

    @Override
    Task<Double> getLobbyRating()
    {
        Task.fromValue(state.lobbyMu)
    }

    @Override
    Task<Double> getLobbyRatingDevation()
    {
        Task.fromValue(state.lobbySigma)
    }

    @Override
    Task<Boolean> canPlay(Player player)
    {
        Task.fromValue(isInRange(player))
    }

    @Override
    Task<Double> getArea(Player player)
    {
        Task.fromValue(getOverlappingArea(player))
    }

    @Override
    Task<Boolean> hasFreeSpot()
    {
        Task.fromValue(state.hasFreeSpot())
    }

    @Override
    Task<Boolean> selectTeam(Player player, int team)
    {
        Task.fromValue(state.selectTeam(player,team))
    }

    @Override
    Task<Boolean> leaveTeam(Player player)
    {
        if (state.hasPlayer(player))
        {
            state.leaveTeam(player)
            Task.fromValue(true)
        }
        else Task.fromValue(false)
    }

    @Override
    Task<Set<Player>> getLobbyPLayers()
    {
        Task.fromValue(state.getPlayerSet())
    }

    @Override
    Task<Void> finishMatch()
    {
        def lobbyService = getReference(RoomService, state.service)
        await(lobbyService.removeLobby(this, state.getPlayerSet()))
        Task.done()
    }

    @Override
    Task<Game> startGame()
    {
        await(populateTeams())
        def game = await(getReference(Game, this.getIdentity()).setup(this))
        state.getPlayerSet().each
        {
            await(it.joinGame(game))
        }
        state.game = true
        await(super.writeState())
        Task.fromValue(game)
    }

    @Override
    Task<Boolean> isGameStarted()
    {
        Task.fromValue(state.game)
    }

    @Override
    Task<Game> getGame()
    {
        state.game ? Task.fromValue(getReference(Game,this.identity)) : Task.fromValue(null)
    }

    @Override
    Task<List<Team>> getTeams()
    {
        Task.fromValue(state.teamList)
    }

    @Override
    Task<List<Team>> getOtherTeams(Team team)
    {
        Task.fromValue((state.teamList - [team]))
    }

    @Override
    Task<Set<Player>> getOpponents(Team team)
    {
        def otherTeams = state.teamList - [team]
        Task.fromValue(
            otherTeams
            .stream()
            .flatMap{await(it.teamMembers).stream()}
            .collect(Collectors.toSet()) as Set<Player>
        )
    }

    @Override
    Task<Team> getTeam(int team)
    {
       Task.fromValue(state.getTeam(team))
    }

    @Override
    Task<Void> populateTeams()
    {
        HashSet<Player> assigned = new HashSet<>()
        state.teamList.each {
            it.getTeamMembers().join().stream().forEach(new Consumer<Player>()
            {
                @Override
                void accept(Player player)
                {
                    assigned.add(player)
                }
            })
        }

        state.getPlayerSet().stream()
                .filter(new Predicate<Player>()
        {
            @Override
            boolean test(Player player)
            {
                return !assigned.contains(player)
            }
        }).forEach(new Consumer<Player>()
        {
            @Override
            void accept(Player player)
            {
                smallerTeam().addTeamMember(player).join()
            }
        })

        Task.done()
    }

    private void setupLobby(Rules rules)
    {
        state.lobbyRules = rules
        (0..<rules.teams).each {
            state.addTeam(getReference(Team, generateTeamID(this, it)))
        }
    }

    private boolean addPlayerToLobby(Player player)
    {
        def result = false

        if(state.hasPlayer(player)) throw new IllegalArgumentException("$player.identity is allready in the lobby")

        //First player sets lobby rating
        if(!state.lobbyPlayers.size())
        {
            def skill = await(player.getSkill())
            state.addPlayer(player)
            state.lobbyMu = await(skill.getRating())
            state.lobbySigma = await(skill.getRatingDeviation())
            result = true
        } else
        {
            if(state.hasFreeSpot()) if(isInRange(player))
            {
                state.addPlayer(player)
                updateLobbySkill()
                result = true
            }
        }
        return result
    }

    private Team smallerTeam()
    {
        state.teamList.stream().sorted(new Comparator<Team>() {
            @Override
            int compare(Team o1, Team o2)
            {
                def o1Size = await(o1.teamSize())
                def o2Size = await(o2.teamSize())

                if(o1Size == o2Size) return 0 else if(o2Size > o2Size) return 1 else return -1
            }
        }).findFirst().get()
    }

    private void updateLobbySkill()
    {
        state.lobbyMu = 0
        state.lobbySigma = 0

        state.playerSet.each {
            def skill = await(it.getSkill())
            state.lobbyMu += await(skill.getRating())
            state.lobbySigma += await(skill.getRatingDeviation())
        }
        state.lobbyMu = state.lobbyMu / state.lobbyPlayers.size()
        state.lobbySigma = state.lobbySigma / state.lobbyPlayers.size()
    }

    private boolean isInRange(Player player)
    {
        (getOverlappingArea(player) >= state.lobbyRules.overlap)
    }

    private Double getOverlappingArea(Player player)
    {
        def skill = await(player.getSkill())
        def area = IntersectionArea.intersect(state.lobbyMu,
                state.lobbySigma,
                await(skill.getRating()),
                await(skill.getRatingDeviation()))

        return area
    }

    @Override
    Task<RoomService> getService()
    {
        Task.fromValue(getReference(RoomService,state.service))
    }

    @Override
    Task<Player> getPlayer()
    {
        return null
    }
}
