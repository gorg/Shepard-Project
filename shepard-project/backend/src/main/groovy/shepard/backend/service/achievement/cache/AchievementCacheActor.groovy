package shepard.backend.service.achievement.cache

import cloud.orbit.actors.streams.AsyncObserver
import cloud.orbit.actors.streams.AsyncStream
import cloud.orbit.actors.streams.StreamSequenceToken
import cloud.orbit.concurrent.Task
import shepard.backend.service.achievement.AchievementService
import shepard.backend.service.achievement.manager.AchievementManager
import shepard.backend.service.generic.AbstractServiceCache

import java.util.function.Function
import java.util.stream.Collectors

import static com.ea.async.Async.await

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/8/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class AchievementCacheActor extends AbstractServiceCache<AchievementService> implements AchievementCache
{
    private static final String streamId = "achievements-async-stream"

    @Override
    Task activateAsync()
    {
        //Get the async stream
        stream = AsyncStream.getStream(AchievementService, streamId)

        cache = new HashSet<>()

        //Retrieve all available achievements
        await(getReference(AchievementManager).getAllEnabledServices()).each
        {
            cache.add(it.identity)
        }

        //Subscribe for new achievements
        streamHandle = stream.subscribe(new AsyncObserver<AchievementService>() {
            @Override
            Task<Void> onNext(AchievementService data, StreamSequenceToken sequenceToken)
            {
                add(data)
                Task.done()
            }
        }).join()

        return super.activateAsync()
    }

    @Override
    Task deactivateAsync()
    {
        stream.unsubscribe(streamHandle).join()
        return super.deactivateAsync()
    }

    private void add(AchievementService service)
    {
        addToCache(service)
    }

    @Override
    Task<List<AchievementService>> retrieve()
    {
        Task.fromValue(
            cache.stream().map(new Function<String,AchievementService>()
            {
                @Override
                AchievementService apply(String identity) {
                    getReference(AchievementService,identity)
                }
            }).collect(Collectors.toList())
        )
    }
}
