package shepard.backend.service.player.manager

import cloud.orbit.actors.Actor
import cloud.orbit.actors.runtime.AbstractActor
import cloud.orbit.concurrent.Task
import shepard.backend.service.player.Player

import static com.ea.async.Async.await
import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 28/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class PlayerManagerActor extends AbstractActor<State> implements PlayerManager
{
    static class State
    {

    }

    @Override
    Task<Player> registerPlayer(String playerName, String password)
    {
        if (playerName && password)
        {
            def player = Actor.getReference(Player, name2Id(playerName))
            if (!await(player.isPlayerSet()))
            {
                player = await(player.registerPlayer(playerName,password))
            }
            else
            {
                player = await(player.validatePlayer(playerName,password))
            }
            Task.fromValue(player)
        }
        else
            Task.fromValue(null)

    }

    @Override
    Task<Player> changePassword(String playerName, String oldPassword, String newPassword)
    {
        def player = await(getReference(Player, name2Id(playerName)).validatePlayer(playerName,oldPassword))
        if (player)
        {
            player = await(player.changePassword(oldPassword,newPassword))
        }
        Task.fromValue(player)
    }

    @Override
    Task<Player> findByName(String playerName)
    {
        Player player = null
        if (playerName)
        {
            def id = name2Id(playerName)
            player = getReference(Player, id)
            if (!await(player.isPlayerSet()))
                player = null
        }
        return Task.fromValue(player)
    }

    @Override
    Task<Player> findById(String id)
    {
        Player player = null
        if (id)
        {
            player = getReference(Player,id)
            if (!await(player.isPlayerSet()))
                player = null
        }
        Task.fromValue(player)
    }

    @Override
    Task<Boolean> removePlayer(String playerName, String password)
    {
        if (playerName && password)
        {
            def id = name2Id(playerName)
            def player = getReference(Player, id)
            Task.fromValue((await(player.isPlayerSet())) ? await(player.removePlayer(playerName,password)) : false)
        }
        else Task.fromValue(false)
    }
}
