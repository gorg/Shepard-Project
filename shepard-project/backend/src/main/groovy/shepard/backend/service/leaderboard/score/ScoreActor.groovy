package shepard.backend.service.leaderboard.score

import cloud.orbit.actors.runtime.AbstractActor
import cloud.orbit.concurrent.Task
import shepard.backend.service.leaderboard.score.details.CurrencyScore
import shepard.backend.service.leaderboard.score.details.NumericalScore
import shepard.backend.service.leaderboard.score.details.TimeScore

import static com.ea.async.Async.await

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/5/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class ScoreActor extends AbstractActor<State> implements Score
{
    static class State
    {
        Long score = 0
    }

    @Override
    Task<Score> setScore(long score)
    {
        state().score = score
        await(super.writeState())
        Task.fromValue(this)
    }

    @Override
    Task<Score> getScore()
    {
        Task.fromValue(this)
    }

    @Override
    Task<Long> getRawScore()
    {
        Task.fromValue(state().score)
    }

    @Override
    Task<NumericalScore> getScoreAsNumericalScore()
    {
        Task.fromValue(new NumericalScore(state().score))
    }

    @Override
    Task<CurrencyScore> getScoreAsCurrencyScore()
    {
        Task.fromValue(new CurrencyScore(state().score))
    }

    @Override
    Task<TimeScore> getScoreAsTimeScore()
    {
        Task.fromValue(new TimeScore(state().score))
    }
}
