package shepard.backend.service.matchmaking.util

import cloud.orbit.actors.runtime.AbstractActor
import cloud.orbit.concurrent.Task
import shepard.backend.service.player.Player

import java.util.function.Function
import java.util.stream.Collectors

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/25/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class TeamActor extends AbstractActor<State> implements Team
{
    static class State
    {
        HashSet<String> team = new HashSet<>()

        def addPlayer(Player player)
        {
            team.add(player.identity)
        }

        def removePlayer(Player player)
        {
            team.remove(player.identity)
        }

        def hasPlayer(Player player)
        {
            team.contains(player.identity)
        }

        def getPlayerSet()
        {
            team.stream().map(new Function<String,Player>() {

                @Override
                Player apply(String playerId)
                {
                    getReference(Player,playerId)
                }
            }).collect(Collectors.toSet()) as HashSet<Player>
        }

    }

    @Override
    Task<Boolean> addTeamMember(Player player)
    {
        Task.fromValue(state.addPlayer(player))
    }

    @Override
    Task<Boolean> removeTeamMember(Player player)
    {
        Task.fromValue(state.removePlayer(player))
    }

    @Override
    Task<Boolean> isInTeam(Player player)
    {
        Task.fromValue(state.hasPlayer(player))
    }

    @Override
    Task<Set<Player>> getTeamMembers()
    {
        Task.fromValue(state.playerSet)
    }

    @Override
    Task<Integer> teamSize()
    {
        Task.fromValue(state.team.size())
    }
}
