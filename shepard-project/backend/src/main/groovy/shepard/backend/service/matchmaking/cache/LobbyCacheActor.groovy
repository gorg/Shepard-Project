package shepard.backend.service.matchmaking.cache

import cloud.orbit.actors.streams.AsyncObserver
import cloud.orbit.actors.streams.AsyncStream
import cloud.orbit.actors.streams.StreamSequenceToken
import cloud.orbit.concurrent.Task
import shepard.backend.service.generic.AbstractServiceCache
import shepard.backend.service.matchmaking.RoomService
import shepard.backend.service.matchmaking.manager.RoomManager

import java.util.function.Function
import java.util.stream.Collectors

import static com.ea.async.Async.await

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 28/01/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class LobbyCacheActor
        extends AbstractServiceCache<RoomService>
        implements LobbyCache
{
    private static final String streamId = "lobby-async-stream"

    @Override
    Task activateAsync()
    {
        //Get the async stream
        stream = AsyncStream.getStream(RoomService, streamId)

        cache = new HashSet<>()
        //Retrieve all available cache
        await(getReference(RoomManager).getAllEnabledServices()).each
        {
            cache.add(it.identity)
        }


        //Subscribe to leaderboard stream
        streamHandle =
                stream.subscribe(
                        new AsyncObserver<RoomService>() {
                            @Override
                            Task<Void> onNext(RoomService data, StreamSequenceToken sequenceToken)
                            {
                                add(data)
                                Task.done()
                            }
                        })
                        .join()

        return super.activateAsync()
    }

    @Override
    Task deactivateAsync()
    {
        stream.unsubscribe(streamHandle).join()
        return super.deactivateAsync()
    }

    private void add(RoomService lobby)
    {
        addToCache(lobby)
    }

    @Override
    Task<List<RoomService>> retrieve()
    {
        Task.fromValue(
            cache.stream().map(new Function<String,RoomService>()
            {
                @Override
                RoomService apply(String identity) {
                    getReference(RoomService,identity)
                }
            }).collect(Collectors.toList())
        )
    }
}
