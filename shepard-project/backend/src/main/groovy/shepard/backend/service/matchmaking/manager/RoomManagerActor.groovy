package shepard.backend.service.matchmaking.manager

import cloud.orbit.actors.streams.AsyncStream
import cloud.orbit.concurrent.Task
import shepard.backend.service.generic.AbstractServiceManager
import shepard.backend.service.matchmaking.RoomService
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.entry.Lobby
import shepard.backend.service.player.Player

import static com.ea.async.Async.await

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 23/01/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class RoomManagerActor
        extends AbstractServiceManager<RoomService, RoomDetails>
        implements RoomManager
{
    @Override
    Task<?> activateAsync()
    {
        stream = AsyncStream.getStream(RoomService, "lobby-async-stream")
        return super.activateAsync()
    }


    @Override
    protected RoomService add(RoomDetails details)
    {
        if(!details.validate())
            throw new IllegalArgumentException("Invalid lobby details")

        RoomService room

        if(state().enabledReferences.containsKey(details.id()))
            room = getReference(RoomService, details.id())
        else
        {
            room = await(getReference(RoomService, details.id()).setupService(details))
            pushService(room)
        }
        return room
    }

    @Override
    RoomService getService(String id)
    {
        getReference(RoomService,id)
    }

    @Override
    Task<Map<RoomService, Boolean>> getServiceMap()
    {
        Task.fromValue(state
                .enabledReferences
                .collectEntries {k,v -> [(getReference(RoomService,k)):v]}
                as Map<RoomService,Boolean>
        )
    }

    @Override
    Task<Lobby> findPlayer(String playerId)
    {
        def player = getReference(Player,playerId)

        def opt = state().enabledReferences.keySet()
            .stream()
            .map { id -> getReference(RoomService,id) }
            .map { service -> service.checkForPlayer(player) }
            .map { task -> await(task) }
            .peek{ println it }
            .filter { lobby -> lobby != null }
            .findAny()

        try
        {
            def lobby = opt.get()
            Task.fromValue(lobby)
        }catch (NoSuchElementException e )
        {
            Task.fromValue(null)
        }

    }
}
