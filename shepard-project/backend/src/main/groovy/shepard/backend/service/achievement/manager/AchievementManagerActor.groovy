package shepard.backend.service.achievement.manager

import cloud.orbit.actors.streams.AsyncStream
import cloud.orbit.concurrent.Task
import shepard.backend.service.achievement.AchievementService
import shepard.backend.service.achievement.details.AchievementDetails
import shepard.backend.service.generic.AbstractServiceManager

import static com.ea.async.Async.await

/**
*  Created by Gueorgui Tzvetoslavov Topalski on 16/10/16.
*  email: gueorgui.tzvetoslavov@gmail.com
*/

//Creates achievements and notify players when a new achievements is created
class AchievementManagerActor
        extends AbstractServiceManager<AchievementService,AchievementDetails>
        implements AchievementManager
{

    @Override
    Task<?> activateAsync() {
        //Used to notify every player currently subscribed that there was a new achievements created
        stream = AsyncStream.getStream(AchievementService, "achievements-async-stream")
        return super.activateAsync()
    }

    @Override
    protected AchievementService add(AchievementDetails details)
    {
        if (!details.validate())
            throw new IllegalArgumentException("Bad achievement details")

        AchievementService achievement
        if(state().enabledReferences.containsKey(details.id()))
            achievement = (getReference(AchievementService, details.id()))
        else
        {
            achievement = await(getReference(AchievementService, details.id()).setupService(details))
            pushService(achievement)
        }
        return achievement
    }

    @Override
    AchievementService getService(String id)
    {
        getReference(AchievementService, id)
    }

    @Override
    Task<Map<AchievementService, Boolean>> getServiceMap()
    {
        Task.fromValue(state
                .enabledReferences
                .collectEntries { k, v -> [(getReference(AchievementService,k)):v] }
                as Map<AchievementService, Boolean>
        )
    }
}
