package shepard.backend.service.game

import cloud.orbit.actors.runtime.AbstractActor
import cloud.orbit.concurrent.Task
import shepard.backend.service.matchmaking.entry.Lobby
import shepard.backend.service.matchmaking.util.Glicko2
import shepard.backend.service.matchmaking.util.Team
import shepard.backend.service.player.Player
import shepard.backend.util.glicko2.ResultPeriod
import shepard.backend.util.glicko2.ResultResume
import shepard.backend.util.glicko2.TeamResultResume

import java.util.function.BiFunction
import java.util.function.Function

import static com.ea.async.Async.await

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 10/12/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class GameActor extends AbstractActor<State> implements Game
{
    static class State
    {
        String lobby
        ResultPeriod res
        boolean canUpdate

        Lobby getLobby()
        {
            getReference(Lobby,lobby)
        }
    }

    @Override
    Task<Game> addWin(Integer winner)
    {
        def teams = await(state.getLobby().getLobbyRules()).teams
        if (winner != null && winner >= 0 && winner < teams)
            return state.getLobby().getTeam(winner).thenCompose({ addWin(it) } as Function)
        else
            return Task.fromValue(this)
    }

    @Override
    Task<Game> addWin(Team winner)
    {
        if(state.canUpdate)
        {
            await(winner.getTeamMembers()
                  .thenCombine(state.getLobby().getOpponents(winner),
                  {a,b -> [a,b].combinations { comb -> state.res.addResult(comb[0], comb[1])}} as BiFunction))
            await(super.writeState())
        }
        Task.fromValue(this)
    }

    @Override
    Task<Game> addWin(Team winner, Team loser)
    {
        if(state.canUpdate)
        {
            await( winner.getTeamMembers()
                    .thenCombine(loser.getTeamMembers(),
                    {a,b -> [a,b].combinations{comb -> state.res.addResult(comb[0],comb[1])} } as BiFunction))
            await(super.writeState())
        }
        Task.fromValue(this)
    }

    @Override
    Task<Game> addWin(Integer winner, Integer loser)
    {
        def teams = await(state.getLobby().getLobbyRules()).teams
        if (winner != null && loser != null && winner >=0 && winner < teams && loser >=0 && loser < teams && winner != loser)
            return state.getLobby().getTeam(winner)
                    .thenCompose({addWin(it, await(state.getLobby().getTeam(loser)))} as Function)
        else
            return Task.fromValue(this)
    }

    @Override
    Task<Game> addParticipant(Team team)
    {
        if(state.canUpdate)
        {
            team.getTeamMembers().join().each
            {
                state.res.addParticipant(it)
            }
            await(super.writeState())
        }
        Task.fromValue(this)
    }

    @Override
    Task<Game> addDraw()
    {
        if(state.canUpdate)
        {
            def teams = await(state.getLobby().getTeams())
            def teamCombination = [teams.first(), teams-teams.first()].combinations()
            teamCombination.each
            { List<Team> comb ->
                [await(comb.first().getTeamMembers()), await(comb.last().getTeamMembers())].combinations
                {List<Player> players ->
                    state.res.addDraw(players.first(),players.last())
                }
            }
            await(super.writeState())

            /*
            await(state.getLobby().getTeams()
                .thenApply({ List<Team> teams -> [teams.get(0), teams-teams.get(0)].combinations
                {List<Team> comb ->
                    comb[0].getTeamMembers().thenCombine(
                        comb[1].getTeamMembers(),
                            {playersA, playersB -> [playersA,playersB].combinations{List<Player> players -> state.res.addDraw(players[0],players[1])}} as BiFunction)
                }} as Function))
            await(super.writeState())
            */

        }
        Task.fromValue(this)
    }

    @Override
    Task<List<ResultResume>> getResume()
    {
        def resume = null
        if (state.res.results.size() > 0)
        {
            resume = state.res.getResume()
        }
        Task.fromValue(resume)
    }

    @Override
    Task<List<ResultResume>> getResumeRedux()
    {
        def resume = null
        if (state.res.results.size() > 0)
        {
            def rules = await(await(getLobby()).getLobbyRules())
            def redux = (rules.players / rules.teams).toInteger()
            resume = state.res.getResume().collect { it.redux(redux) }
        }
        Task.fromValue(resume)
    }

    @Override
    Task<List<TeamResultResume>> getTeamResume()
    {
        def resume = null
        if (state.res.results.size() > 0)
        {
            def teams =  await(await(getLobby()).getTeams())
            resume = teams.collect { team -> state.res.getTeamResume(team) }
        }
        Task.fromValue(resume)
    }

    @Override
    Task<List<TeamResultResume>> getTeamResumeRedux()
    {
        def resume = null
        if (state.res.results.size() > 0)
        {
            def rules = await(await(getLobby()).getLobbyRules())
            def redux = (rules.players / rules.teams).toInteger()
            resume = (await(await(getLobby()).getTeams()).collect{ state.res.getTeamResume(it) }.collect{ it.redux(redux)})
        }
        Task.fromValue(resume)
    }

    @Override
    Task<Game> finishMatch()
    {
        state.canUpdate = false
        await(getReference(Glicko2).updateRating(state.res))
        await(state.getLobby().getLobbyPLayers()).each
        {
            await(it.leaveGame(this))
            await(it.setCurrentLobby(null))
        }
        await(state.getLobby().finishMatch())
        await(super.writeState())
        Task.fromValue(this)
    }

    @Override
    Task<ResultPeriod> getResults()
    {
        Task.fromValue(state.res)
    }

    @Override
    Task<Lobby> getLobby()
    {
        Task.fromValue(state.getLobby())
    }

    Task<Boolean> isFinished()
    {
        Task.fromValue(!state.canUpdate)
    }

    @Override
    Task<Game> setup(Lobby lobby)
    {
        state.lobby = lobby.identity
        state.res = new ResultPeriod()
        state.canUpdate = true
        await(super.writeState())
        Task.fromValue(this)
    }
}
