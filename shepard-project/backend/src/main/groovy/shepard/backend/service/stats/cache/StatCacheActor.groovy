package shepard.backend.service.stats.cache

import cloud.orbit.actors.streams.AsyncObserver
import cloud.orbit.actors.streams.AsyncStream
import cloud.orbit.actors.streams.StreamSequenceToken
import cloud.orbit.concurrent.Task
import shepard.backend.service.generic.AbstractServiceCache
import shepard.backend.service.stats.StatService
import shepard.backend.service.stats.manager.StatManager

import java.util.function.Function
import java.util.stream.Collectors

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/18/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class StatCacheActor extends AbstractServiceCache<StatService> implements StatCache
{
    private static final String streamId = "stat-async-stream"

    @Override
    Task activateAsync()
    {
        //Get the async stream
        stream = AsyncStream.getStream(StatService, streamId)

        cache = new HashSet<>()
        getReference(StatManager).getAllEnabledServices().join().each
        {
            cache.add(it.identity)
        }

        streamHandle =
                stream.subscribe(
                        new AsyncObserver<StatService>() {
                            @Override
                            Task<Void> onNext(StatService data, StreamSequenceToken sequenceToken)
                            {
                                add(data)
                                Task.done()
                            }
                        }
                ).join()

        return super.activateAsync()
    }

    @Override
    Task deactivateAsync()
    {
        stream.unsubscribe(streamHandle).join()
        return super.deactivateAsync()
    }

    private void add(StatService stat)
    {
        addToCache(stat)
    }

    @Override
    Task<List<StatService>> retrieve()
    {
        Task.fromValue(
            cache.stream().map(new Function<String,StatService>()
            {
                @Override
                StatService apply(String identity) {
                    getReference(StatService,identity)
                }
            }).collect(Collectors.toList())
        )
    }
}
