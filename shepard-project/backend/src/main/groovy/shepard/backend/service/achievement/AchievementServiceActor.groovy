package shepard.backend.service.achievement

import cloud.orbit.concurrent.Task
import shepard.backend.service.achievement.details.AchievementDetails
import shepard.backend.service.achievement.entry.Achievement
import shepard.backend.service.achievement.manager.AchievementManager
import shepard.backend.service.generic.AbstractService
import shepard.backend.service.player.Player

import java.time.LocalDateTime

import static shepard.backend.util.id.GenerateId.generateId

/**
*  Created by Gueorgui Tzvetoslavov Topalski on 16/10/16.
*  email: gueorgui.tzvetoslavov@gmail.com
*/
class AchievementServiceActor
        extends AbstractService<AchievementService, AchievementDetails, AchievementServiceData, Achievement>
        implements AchievementService
{
    private Task<Achievement> entry(Player player)
    {
        getReference(Achievement, generateId(this, player)).setupEntry(this, player)
    }

    @Override
    Task<Boolean> completeAchievement(Player player)
    {
        entry(player).thenComposeAsync{ it.complete() }
    }

    @Override
    Task<Boolean> isCompleted(Player player)
    {
        entry(player).thenComposeAsync{ it.isCompleted() }
    }

    @Override
    Task<LocalDateTime> whenWasCompleted(Player player)
    {
        entry(player).thenComposeAsync{ it.whenWasCompleted() }
    }

    @Override
    Task<Boolean> isEnabled()
    {
        getReference(AchievementManager).isEnabled(this)
    }

    @Override
    Task<Achievement> getEntry(Player player)
    {
        entry(player)
    }

}
