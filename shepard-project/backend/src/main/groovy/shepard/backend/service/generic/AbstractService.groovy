package shepard.backend.service.generic

import cloud.orbit.actors.runtime.AbstractActor
import cloud.orbit.concurrent.Task

import static com.ea.async.Async.await

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 26/01/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 */
abstract class AbstractService<S extends Service, D extends ServiceDetails, Z extends ServiceData, E extends Entry>
    extends AbstractActor<State<D,Z>>
    implements Service<S,D,Z,E>
{
    static class State<D,Z> implements Serializable
    {
        D details
        Z data
    }

    @Override
    Task<S> setupService(D serviceDetails)
    {
        state().details = serviceDetails
        await(super.writeState())
        Task.fromValue(this as S)
    }

    @Override
    Task<S> updateServiceDetails(D serviceDetails)
    {
        state().details = serviceDetails
        await(super.writeState())
        Task.fromValue(this as S)
    }

    @Override
    Task<D> getServiceDetails()
    {
        Task.fromValue(state().details)
    }

    @Override
    Task<Void> removeService()
    {
        state().details = null
        await(super.writeState())
        Task.done()
    }
}
