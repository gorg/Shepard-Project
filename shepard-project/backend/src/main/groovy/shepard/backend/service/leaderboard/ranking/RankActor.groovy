package shepard.backend.service.leaderboard.ranking

import cloud.orbit.actors.runtime.AbstractActor
import cloud.orbit.concurrent.Task
import shepard.backend.service.leaderboard.LeaderboardService
import shepard.backend.service.leaderboard.score.Score
import shepard.backend.service.player.Player

import static com.ea.async.Async.await
import static shepard.backend.util.id.GenerateId.generateId

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 10/27/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class RankActor extends AbstractActor<State> implements Rank
{
    static class State
    {
        String player
        String leaderboard
        long currentRank
        long delta
        boolean set = false
    }


    //Setup an empty rank entry for a leaderboard
    @Override
    Task<Rank> setupEntry(LeaderboardService service, Player player)
    {
        //Forgery protection
        if(generateId(service,player) != this.getIdentity())
        {
            throw new IllegalAccessError("Rank ID is not matching the arguments")
        }

        if(!state().set)
        {
            def score = getReference(Score,this.getIdentity())
            await(score.setScore(0))

            state().player = player.identity
            state().leaderboard = service.identity
            state().currentRank = 0
            state().delta = 0
            state().set = true
            await(super.writeState())
        }
        Task.fromValue(this)
    }

    @Override
    Task<Score> updateScore(long points)
    {
        getReference(Score,this.getIdentity()).setScore(points)
    }

    @Override
    Task<Rank> changeRank(long newRank)
    {
        state().delta = state().currentRank - newRank
        state().currentRank = newRank
        await(super.writeState())
        Task.fromValue(this)
    }

    @Override
    Task<Score> getScore()
    {
        Task.fromValue(getReference(Score,this.getIdentity()))
    }

    @Override
    Task<Long> getRank()
    {
        Task.fromValue(state().currentRank)
    }

    @Override
    Task<LeaderboardService> getService()
    {
        Task.fromValue(getReference(LeaderboardService,state().leaderboard))
    }

    @Override
    Task<Player> getPlayer()
    {
        Task.fromValue(getReference(Player,state().player))
    }

    @Override
    Task<Long> getDelta()
    {
        Task.fromValue(state().delta)
    }

    @Override
    Task<Map> collectEntry()
    {
        def stateMap = [:]
        if (state.set)
        {
            def score = await(await(getScore()).rawScore)
            stateMap['player'] = state().player
            stateMap['leaderboard'] = state().leaderboard
            stateMap['score'] = score
            stateMap['rank'] = state().currentRank
            stateMap['delta'] = state().delta
        }
        Task.fromValue(stateMap)
    }

}
