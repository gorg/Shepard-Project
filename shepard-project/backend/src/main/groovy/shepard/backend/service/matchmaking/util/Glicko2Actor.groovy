package shepard.backend.service.matchmaking.util

import cloud.orbit.actors.runtime.AbstractActor
import cloud.orbit.concurrent.Task
import shepard.backend.service.player.Player
import shepard.backend.util.glicko2.Result
import shepard.backend.util.glicko2.ResultPeriod

import static com.ea.async.Async.await
import static java.lang.Math.*

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/25/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class Glicko2Actor extends AbstractActor implements Glicko2
{
    private final static double DEFAULT_RATING =  1500.0
    private final static double DEFAULT_DEVIATION =  350
    private final static double DEFAULT_VOLATILITY =  0.06
    private final static double DEFAULT_TAU =  0.75
    private final static double MULTIPLIER =  173.7178
    private final static double CONVERGENCE_TOLERANCE =  0.000001

    private static double tau = DEFAULT_TAU
    private static volatility = DEFAULT_VOLATILITY

    @Override
    Task<Void> setup(double tau, double volatility)
    {
        this.tau = tau
        this.volatility = volatility
        Task.done()
    }

    @Override
    Task<Double> getDefaultRating()
    {
       Task.fromValue(DEFAULT_RATING)
    }

    @Override
    Task<Double> getDefaultRatingDeviation()
    {
        Task.fromValue(DEFAULT_DEVIATION)
    }

    @Override
    Task<Double> getDefaultVolatility()
    {
        Task.fromValue(DEFAULT_VOLATILITY)
    }

    @Override
    Task<Void> updateRating(ResultPeriod results)
    {
        //TeamMatch must be ended before updating players rating
        results.getParticipants().each
        {
            player ->
            if ( results.getResults(player).size() > 0 )
            {

                calculateNewRating(player, results.getResults(player))
            } else
            {
                // if a player does not compete during the rating period, then only Step 6 applies.
                // the player's rating and volatility parameters remain the same but deviation increases
                Skill skill = await(player.getSkill())

                await(skill.setWorkingRating(await(skill.getGlicko2Rating())))
                await(skill.setWorkingRatingDeviation(calculateNewRD(await(skill.getGlicko2RatingDeviation()), await(skill.getVolatility()))))
                await(skill.setWorkingVolatility(await(skill.getVolatility())))
            }
        }

        // now iterate through the participants and confirm their new ratings
        results.getParticipants().each
        {
            player -> await(await(player.getSkill()).finishRating())
        }

        // lastly, clear the result set down in anticipation of the next rating period
        results.clear()
        Task.done()
    }

    @Override
    Task<Double> convertRatingToGlicko2Scale(double rating)

    {
        Task.fromValue(( rating  - DEFAULT_RATING ) / MULTIPLIER )
    }

    @Override
    Task<Double> convertRatingToOriginalGlickoScale(double rating)
    {
        Task.fromValue(( rating  * MULTIPLIER ) + DEFAULT_RATING )
    }

    @Override
    Task<Double> convertRatingDeviationToGlicko2Scale(double ratingDeviation)
    {
        Task.fromValue(ratingDeviation / MULTIPLIER )
    }

    @Override
    Task<Double> convertRatingDeviationToOriginalGlickoScale(double ratingDeviation)
    {
        Task.fromValue(ratingDeviation * MULTIPLIER)
    }


    private static void calculateNewRating(Player player, List<Result> results) {

        def skill = await(player.getSkill())


        double phi = await(skill.getGlicko2RatingDeviation())
        double sigma = await(skill.getVolatility())
        double a = log( pow(sigma, 2))

        double delta = delta(player, results)

        double v = funcV(player, results)

        // step 5.2 - set the initial values of the iterative algorithm to come in step 5.4
        double A = a
        double B = 0.0
        if ( pow(delta, 2) > pow(phi, 2) + v ) {
            B = log( pow(delta, 2) - pow(phi, 2) - v )
        } else {
            double k = 1
            B = a - ( k * abs(tau))

            while ( funcF(B , delta, phi, v, a, tau) < 0 ) {
                k++
                B = a - ( k * abs(tau))
            }
        }

        // step 5.3
        double fA = funcF(A , delta, phi, v, a, tau)
        double fB = funcF(B , delta, phi, v, a, tau)

        // step 5.4
        while ( Math.abs(B - A) > CONVERGENCE_TOLERANCE ) {
            double C = A + (( (A-B)*fA ) / (fB - fA))
            double fC = funcF(C , delta, phi, v, a, tau)

            if ( fC * fB < 0 ) {
                A = B
                fA = fB
            } else {
                fA = fA / 2.0
            }

            B = C
            fB = fC
        }

        double newSigma = exp( A/2.0 )

        await(skill.setWorkingVolatility(newSigma))

        // Step 6
        double phiStar = calculateNewRD( phi, newSigma )

        // Step 7
        double newPhi = 1.0 / sqrt(( 1.0 / pow(phiStar, 2) ) + ( 1.0 / v ))

        // note that the newly calculated rating values are stored in a "working" area in the Rating object
        // this avoids us attempting to calculate subsequent participants' ratings against a moving target
        skill.setWorkingRating(
                skill.getGlicko2Rating().join()
                        + ( pow(newPhi, 2) * outcomeBasedRating(player, results))).join()

        skill.setWorkingRatingDeviation(newPhi).join()
        skill.incrementNumberOfResults(results.size()).join()


    }

    private static double funcF(double x, double delta, double phi, double v, double a, double tau) {
        return ( exp(x) * ( pow(delta, 2) - pow(phi, 2) - v - exp(x) ) /
                (2.0 * pow( pow(phi, 2) + v + exp(x), 2) )) -
                ( ( x - a ) / pow(tau, 2) )
    }

    private static double funcG(double deviation) {
        return 1.0 / ( sqrt( 1.0 + ( 3.0 * pow(deviation, 2) / pow(PI,2) )))
    }

    private static double funcE(double playerRating, double opponentRating, double opponentDeviation) {

        return 1.0 / (1.0 + exp( -1.0 * funcG(opponentDeviation) * ( playerRating - opponentRating )))
    }

    private static double funcV(Player player, List<Result> results) {
        double v = 0.0d
        def playerRating = await(await(player.getSkill()).getGlicko2Rating())

        results.each
        {
            result ->
            def opRating = await(await(result.getOpponent(player).getSkill()).getGlicko2Rating())
            def opRatingDeviation = await(await(result.getOpponent(player).getSkill()).getGlicko2RatingDeviation())
            v += (( pow(funcG(opRatingDeviation), 2)) * funcE(playerRating,opRating,opRatingDeviation) * ( 1.0 - funcE(playerRating,opRating,opRatingDeviation)))
        }
        return pow(v, -1)
    }

    private static delta(Player player, List<Result> results) {

        return funcV(player, results) * outcomeBasedRating(player, results)
    }

    private static double outcomeBasedRating(Player player, List<Result> results) {
        double outcomeBasedRating = 0.0d

        def playerRating = await(await(player.getSkill()).getGlicko2Rating())

        results.each
        {
            result ->
            def opRating = await(await(result.getOpponent(player).getSkill()).getGlicko2Rating())
            def opRatingDeviation = await(await(result.getOpponent(player).getSkill()).getGlicko2RatingDeviation())

            outcomeBasedRating +=
                    ( funcG(opRatingDeviation)*( result.getScore(player) - funcE(playerRating,opRating,opRatingDeviation))
            )
        }

        return outcomeBasedRating
    }

    private static double calculateNewRD(double phi, double sigma) {
        return sqrt( pow(phi, 2) + pow(sigma, 2) )
    }


}
