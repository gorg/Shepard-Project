package shepard.backend.service.matchmaking

import cloud.orbit.concurrent.Task
import shepard.backend.service.generic.AbstractService
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.entry.Lobby
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.backend.service.player.Player

import static com.ea.async.Async.await
import static shepard.backend.util.id.LobbyIdGenerator.generateLobbyId

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 12/13/16.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/

class RoomServiceActor
        extends AbstractService<RoomService, RoomDetails, RoomData, Lobby>
        implements RoomService
{
    @Override
    Task<RoomService> setupService(RoomDetails serviceDetails)
    {
        state().details = serviceDetails
        state().data =  new RoomData()
        await(super.writeState())
        Task.fromValue(this)
    }

    //Query if a player is in a lobby in this room service
    @Override
    Task<Lobby> checkForEntry(Player player)
    {
        checkForPlayer(player)
    }

    @Override
    Task<Lobby> exitLobby(Player player)
    {
        def lobby = state().data.getPlayersLobby(player)
        if (lobby)
        {
            await(lobby.removePlayer(player))
            state.data.removePlayer(player)
            if (await(lobby.getLobbyPLayers()).isEmpty())
            {
                state.data.removeLobby(lobby)
                state.data.counter--
                await(super.writeState())
            }
        }
        Task.fromValue(lobby)
    }

    @Override
    Task<Lobby> checkForPlayer(Player player)
    {
        Task.fromValue(state().data.getPlayersLobby(player))
    }

    @Override
    Task<Lobby> enterLobby(Player player)
    {
        getEntry(player)
    }

    //Create a entry for a player in this room service
    @Override
    Task<Lobby> getEntry(Player player)
    {
        def lobby = state().data.getPlayersLobby(player)
        lobby = lobby ?: getOrCreateLobby(player)
        Task.fromValue(lobby)
    }


    private Lobby getOrCreateLobby(Player player)
    {
        def assignedLobby = null
        def found = false
        def skill = await(player.getSkill())

        if (await(player.getCurrentLobby()) != null)
            throw new IllegalArgumentException()

        //Ranked lobbies use smaller skill range
        def skillRange = state().details.rules.ranked ? await(skill.getSkillRange68()) : await(skill.getSkillRange95())
        def subTree = state().data.lobbies.subMap(skillRange.low, skillRange.high)

        def iterator = subTree.entrySet().iterator()
        while(iterator.hasNext() && !found)
        {
            def entry = iterator.next()
            def rating = entry.key
            def lobby = getReference(Lobby,entry.value)

            if (await(lobby.hasFreeSpot()))
            {
                if(await(lobby.canPlay(player)))
                {
                    if (await(lobby.addPlayer(player)))
                    {
                        def mu = await(lobby.getLobbyRating())
                        state().data.lobbies.remove(rating)
                        state().data.addLobby(mu,lobby)
                        assignedLobby = lobby
                        found = true
                    }
                }
            }
        }
        //There was no lobby in the range
        if (!found)
        {
            def id = generateLobbyId(this.identity, state().data.counter)
            assignedLobby = await(getReference(Lobby, id).setupEntry(this, player, state().details.rules))
            def rating = await(assignedLobby.getLobbyRating())
            state().data.addLobby(rating,assignedLobby)
            state().data.counter++
        }
        await(player.setCurrentLobby(assignedLobby))
        state().data.addPlayer(player,assignedLobby)
        await(super.writeState())
        return assignedLobby
    }

    @Override
    Task<List<Lobby>> getLobbies()
    {
        Task.fromValue(state.data.getLobbyList())
    }

    @Override
    Task<Map<Double, Lobby>> getRatingsAndLobbies()
    {
        Task.fromValue(state.data.getRatingLobbyMap())
    }

    @Override
    Task<Void> removeLobby(Lobby lobby, Set<Player> lobbyPlayers)
    {
        state.data.removeLobby(lobby)
        state.data.counter --
        lobbyPlayers.each { player -> state.data.removePlayer(player) }
        await(super.writeState())
        Task.done()
    }


    @Override
    Task<Boolean> isEnabled()
    {
        getReference(RoomManager).isEnabled(this)
    }
}
