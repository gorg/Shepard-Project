package shepard.backend.service.stats.manager

import cloud.orbit.actors.streams.AsyncStream
import cloud.orbit.concurrent.Task
import shepard.backend.service.generic.AbstractServiceManager
import shepard.backend.service.stats.StatService
import shepard.backend.service.stats.details.StatDetails

import static com.ea.async.Async.await

class StatManagerActor
        extends AbstractServiceManager<StatService, StatDetails>
        implements StatManager
{
    @Override
    Task<?> activateAsync()
    {
        //Used to notify every player currently subscribed that there was a new stat created
        stream = AsyncStream.getStream(StatService, "stat-async-stream")
        return super.activateAsync()
    }


    @Override
    protected StatService add(StatDetails details)
    {
        if(!details.validate())
            throw new IllegalArgumentException("Invalid stat details")

        StatService stat

        if(state().enabledReferences.containsKey(details.id()))
            stat = getReference(StatService, details.id())
        else
        {
            stat = await(getReference(StatService, details.id()).setupService(details))
            pushService(stat)
        }
        return stat
    }

    @Override
    StatService getService(String id)
    {
        getReference(StatService,id)
    }

    @Override
    Task<Map<StatService, Boolean>> getServiceMap()
    {
        Task.fromValue(state
                .enabledReferences
                .collectEntries { k,v -> [(getReference(StatService,k)):v] }
                as Map<StatService,Boolean>
        )
    }
}
