package shepard.backend.service.leaderboard.cache

import cloud.orbit.actors.streams.AsyncObserver
import cloud.orbit.actors.streams.AsyncStream
import cloud.orbit.actors.streams.StreamSequenceToken
import cloud.orbit.concurrent.Task
import shepard.backend.service.generic.AbstractServiceCache
import shepard.backend.service.leaderboard.LeaderboardService
import shepard.backend.service.leaderboard.manager.LeaderboardManager

import java.util.function.Function
import java.util.stream.Collectors

import static com.ea.async.Async.await

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/8/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class LeaderboardCacheActor
        extends AbstractServiceCache<LeaderboardService>
        implements LeaderboardCache
{
    private static final String streamId = "leaderboard-async-stream"

    @Override
    Task activateAsync()
    {
        //Get the async stream
        stream = AsyncStream.getStream(LeaderboardService, streamId)

        cache = new HashSet<>()

        //Retrieve all available cache
        await(getReference(LeaderboardManager).getAllEnabledServices()).each {
            cache.add(it.identity)
        }


        //Subscribe to leaderboard stream
        streamHandle =
                stream.subscribe(
                        new AsyncObserver<LeaderboardService>() {
                            @Override
                            Task<Void> onNext(LeaderboardService data, StreamSequenceToken sequenceToken)
                            {
                                add(data)
                                Task.done()
                            }
                        })
                        .join()

        return super.activateAsync()
    }

    @Override
    Task deactivateAsync()
    {
        stream.unsubscribe(streamHandle).join()
        return super.deactivateAsync()
    }

    private void add(LeaderboardService leaderboard)
    {
        addToCache(leaderboard)
    }

    @Override
    Task<List<LeaderboardService>> retrieve()
    {
        Task.fromValue(
            cache.stream().map(new Function<String,LeaderboardService>()
            {
                @Override
                LeaderboardService apply(String identity) {
                    getReference(LeaderboardService,identity)
                }
            }).collect(Collectors.toList())
        )
    }
}
