package shepard.backend.service.player

import cloud.orbit.actors.runtime.AbstractActor
import cloud.orbit.actors.streams.AsyncObserver
import cloud.orbit.actors.streams.AsyncStream
import cloud.orbit.actors.streams.StreamSequenceToken
import cloud.orbit.actors.streams.StreamSubscriptionHandle
import cloud.orbit.concurrent.Task
import shepard.backend.service.achievement.AchievementService
import shepard.backend.service.achievement.cache.AchievementCache
import shepard.backend.service.achievement.entry.Achievement
import shepard.backend.service.game.Game
import shepard.backend.service.leaderboard.LeaderboardService
import shepard.backend.service.leaderboard.cache.LeaderboardCache
import shepard.backend.service.leaderboard.ranking.Rank
import shepard.backend.service.leaderboard.score.Score
import shepard.backend.service.matchmaking.entry.Lobby
import shepard.backend.service.matchmaking.util.Skill
import shepard.backend.service.stats.StatService
import shepard.backend.service.stats.cache.StatCache
import shepard.backend.service.stats.entry.StatEntry

import java.util.stream.Collectors

import static com.ea.async.Async.await
import static shepard.util.hashing.BCHash.check
import static shepard.util.hashing.BCHash.hash
import static shepard.util.string.StringUtils.name2Id

/**
*  Created by Gueorgui Tzvetoslavov Topalski on 15/10/16.
*  email: gueorgui.tzvetoslavov@gmail.com
*/
class PlayerActor extends AbstractActor<State> implements Player {

    static class State
    {
        Boolean set = false
        def name
        def password


        def lobby
    }

    private static final AsyncStream<LeaderboardService> leaderboardAsyncStream = AsyncStream.getStream(LeaderboardService, "leaderboard-async-stream")
    private StreamSubscriptionHandle<LeaderboardService> leaderboardStreamHandle
    private HashMap<LeaderboardService, Rank> rankingMap =  new HashMap<>()

    private static final AsyncStream<AchievementService> achievementAsyncStream = AsyncStream.getStream(AchievementService, "achievements-async-stream")
    private StreamSubscriptionHandle<AchievementService> achievementStreamHandle
    private HashMap<AchievementService, Achievement> achievementMap = new HashMap<>()

    private static final AsyncStream<StatService> statAsyncStream = AsyncStream.getStream(StatService, "stat-async-stream")
    private StreamSubscriptionHandle<StatService> statStreamHandle
    private HashMap<StatService, StatEntry> statMap = new HashMap<>()

    private Lobby lobby
    private Skill skill
    private Game currentGame

    //*****************************************************************************************************************
    //
    //                                  Leaderboards, Scores And Ranking
    //
    //*****************************************************************************************************************

    @Override
    Task<Rank> getRankForLeaderboard(LeaderboardService leaderboard)
    {
        if (rankingMap.get(leaderboard) == null)
        {
            if (await(leaderboard.isEnabled()))
            {
                def leaderboardId = leaderboard.getIdentity()
                this.getLogger().warn("$identity: $leaderboardId was not notified by the cache")
                addToLeaderboardMap(leaderboard)
            }
            else
            {
                throw new InvalidObjectException("Leaderboard is disabled")
            }
        }
        Task.fromValue(rankingMap.get(leaderboard))
    }

    @Override
    Task<Score> getScoreForLeaderboard(LeaderboardService leaderboard)
    {
        if (rankingMap.get(leaderboard) == null)
        {
            if (await(leaderboard.isEnabled()))
            {
                def leaderboardId = leaderboard.getIdentity()
                this.getLogger().warn("$identity: $leaderboardId was not notified by the cache")
                addToLeaderboardMap(leaderboard)
            }
            else
            {
                throw new InvalidObjectException("Leaderboard is disabled")
            }
        }
        Task.fromValue(rankingMap.get(leaderboard).getScore().join())
    }

    private void getLeaderboards()
    {
        //Retrieve actual cache
        await (getReference(LeaderboardCache).retrieve()).each {
            Rank rank = await (it.getEntry(this))
            rankingMap.put(it, rank)
        }

        leaderboardStreamHandle =
                leaderboardAsyncStream.subscribe(
                        new AsyncObserver<LeaderboardService>() {
                            @Override
                            Task<Void> onNext(LeaderboardService data, StreamSequenceToken sequenceToken)
                            {
                                addToLeaderboardMap(data)
                                Task.done()
                            }
                        })
                        .join()
    }

    private void addToLeaderboardMap(LeaderboardService leaderboard)
    {
        def rank = leaderboard.getEntry(this).join()
        rankingMap.put(leaderboard, rank)
    }



    //*****************************************************************************************************************
    //
    //                                              Achievements
    //
    //*****************************************************************************************************************

    @Override
    Task<Achievement> isAchievementCompleted(AchievementService achievement)
    {
        if (achievementMap.get(achievement) == null)
        {
            if (await(achievement.isEnabled()))
            {
                this.getLogger().warn("$identity: New achievements was not notified ($achievement)")
                addToAchievementMap(achievement)
            }
            else
            {
                throw new InvalidObjectException("Achievement is disabled")
            }

        }
        Task.fromValue(achievementMap.get(achievement))
    }

    @Override
    Task<Void> setCurrentLobby(Lobby lobby)
    {
        this.lobby = lobby
        state.lobby = (lobby == null) ? null : lobby.identity
        await(super.writeState())
        Task.done()
    }

    @Override
    Task<Lobby> getCurrentLobby()
    {
        if (lobby == null)
            getLobby()

        Task.fromValue(lobby)
    }


    private void getLobby()
    {

        if(state().lobby != null)
            lobby = getReference(Lobby, state.lobby)
    }

    private void getAchievements()
    {
        //Retrieve actual achievements
        await( getReference(AchievementCache).retrieve() ).each {
            addToAchievementMap(it)
        }

        //Subscribe for future achievements
        achievementStreamHandle =
                achievementAsyncStream.subscribe(
                        new AsyncObserver<AchievementService>() {
                            @Override
                            Task<Void> onNext(AchievementService data, StreamSequenceToken sequenceToken)
                            {
                                addToAchievementMap(data)
                                Task.done()
                            }
                        }
                ).join()
    }

    private void addToAchievementMap(AchievementService achievement)
    {
        def entry = achievement.getEntry(this).join()
        achievementMap.put(achievement, entry)
    }

    //*****************************************************************************************************************
    //
    //                                              Stats
    //
    //*****************************************************************************************************************

    @Override
    Task<StatEntry> getStat(StatService stat)
    {
        if (statMap.get(stat) == null)
        {
            if (await(stat.isEnabled()))
            {
                this.getLogger().warn("$identity: New stat was not notified ($stat)")
                addToStatMap(stat)
            }
            else
            {
                throw new InvalidObjectException("Stat is disabled")
            }
        }
        Task.fromValue(statMap.get(stat))
    }

    private void getStats()
    {
        await(getReference(StatCache).retrieve()).each
        {
            addToStatMap(it)
        }

        statStreamHandle =
                statAsyncStream.subscribe(
                        new AsyncObserver<StatService>() {
                            @Override
                            Task<Void> onNext(StatService data, StreamSequenceToken sequenceToken)
                            {
                                addToStatMap(data)
                                Task.done()
                            }
                        }
                ).join()

    }

    private void addToStatMap(StatService stat)
    {
        def entry = await(stat.getEntry(this))
        statMap.put(stat, entry)
    }



    //*****************************************************************************************************************
    //
    //                                                  Game
    //
    //*****************************************************************************************************************
    @Override
    Task<Game> getCurrentGame()
    {
        Task.fromValue(currentGame)
    }

    @Override
    Task<Void> joinGame(Game game)
    {
        currentGame = game
        Task.done()
    }

    @Override
    Task<Void> leaveGame(Game game)
    {
        if (game == currentGame)
        {
            currentGame = null
        }
        Task.done()
    }

    @Override
    Task<Skill> getSkill()
    {
        Task.fromValue(skill)
    }

    @Override
    Task<List<StatEntry>> getStatList()
    {
       Task.fromValue(statMap.values().stream().collect(Collectors.toList()) as List<StatEntry>)
    }

    @Override
    Task<List<Achievement>> getAchievementsList()
    {
        Task.fromValue(achievementMap.values().stream().collect(Collectors.toList()) as List<Achievement>)
    }

    @Override
    Task<List<Rank>> getRanksList()
    {
        Task.fromValue(rankingMap.values().stream().collect(Collectors.toList()) as List<Rank>)
    }

    @Override
    Task<Player> registerPlayer(String username, String password)
    {
        if (name2Id(username) == this.identity)
        {
            state().name = username
            state().password = hash(password)
            state().set = true
            await(super.writeState())
            Task.fromValue(this)
        }
        else
            throw new IllegalArgumentException("Username doesn't match actor identity")
    }

    @Override
    Task<Player> validatePlayer(String username, String password)
    {
        if (state.name == username && check(password,state.password) && state.set)
            Task.fromValue(this)
        else
            Task.fromValue(null)
     }

    @Override
    Task<Player> changePassword(String oldPassword, String newPassword)
    {
        if(check(oldPassword, state.password))
        {
            state.password = hash(newPassword)
            await(super.writeState())
        }
        Task.fromValue(this)
    }

    @Override
    Task<Boolean> removePlayer(String username, String password)
    {
        if (check(password,state.password))
        {
            state.password = null
            state.set = false
            state.name = null
            await(super.writeState())
            Task.fromValue(true)
        }
        else
            Task.fromValue(false)
    }

    @Override
    Task<Boolean> isPlayerSet()
    {
        Task.fromValue(state().set)
    }



    @Override
    Task activateAsync()
    {
        //TODO need to log??
        this.getLogger().info("Activating $identity")
        getAchievements()
        getLeaderboards()
        getStats()
        //getLobby()

        skill = getReference(Skill, this.getIdentity()).setup(this).join()
        return super.activateAsync()
    }

    @Override
    Task deactivateAsync()
    {
        await(leaderboardAsyncStream.unsubscribe(leaderboardStreamHandle))
        await(achievementAsyncStream.unsubscribe(achievementStreamHandle))
        await(statAsyncStream.unsubscribe(statStreamHandle))
        //TODO need to log??
        this.getLogger().info("Deactivating $identity")
        return super.deactivateAsync()
    }
}
