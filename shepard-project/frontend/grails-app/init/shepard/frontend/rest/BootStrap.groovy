package shepard.frontend.rest

import shepard.connector.ShepardConnect
class BootStrap {

    def init = { servletContext ->
        ShepardConnect.startAndLoad()
     }
    def destroy = {
        ShepardConnect.stop()
    }
}
