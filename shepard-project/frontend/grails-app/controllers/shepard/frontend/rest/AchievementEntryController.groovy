package shepard.frontend.rest

import shepard.frontend.controllers.EntryController

class AchievementEntryController extends EntryController<Achievement,AchievementEntry>
{
    AchievementEntryService achievementEntryService

    AchievementEntryController()
    {
        super(Achievement, AchievementEntry)
    }

    @Override
    protected AchievementEntryService getService()
    {
        return achievementEntryService
    }
}
