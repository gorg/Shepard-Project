package shepard.frontend.rest

class UrlMappings {

    static mappings = {

        //Achievements
        "/achievements"(resources: "achievement", excludes: ['create', 'edit', 'patch'])
        {
            put     "/activate"(action: 'enable')
            delete  "/activate"(action: 'disable')
        }
        get     "/achievements/enabled"  (controller: "achievement", action: "getEnabled")
        get     "/achievements/disabled" (controller: "achievement", action: "getDisabled")

        get     "/achievements/$achievementId/entries"          (controller: "achievementEntry", action: "index")
        get     "/achievements/$achievementId/player/$playerId" (controller: "achievementEntry", action: "show")
        post    "/achievements/$achievementId/player/$playerId" (controller: "achievementEntry", action: "save")
        put     "/achievements/$achievementId/player/$playerId" (controller: "achievementEntry", action: "update")

        //Leaderboards
        "/leaderboards"(resources: "leaderboard", excludes: ['create', 'edit', 'patch'])
        {
            put     "/activate"(action: 'enable')
            delete  "/activate"(action: 'disable')
            put     "/rank"(action: "rank")
            get     "/top10"(action: "top10")
            get     "/top100"(action: "top100")
            get     "/relative"(action: "relative")
        }
        get     "/leaderboards/$leaderboardId/entries"          (controller: "leaderboardEntry", action: "index")
        get     "/leaderboards/$leaderboardId/player/$playerId" (controller: "leaderboardEntry", action: "show")
        post    "/leaderboards/$leaderboardId/player/$playerId" (controller: "leaderboardEntry", action: "save")
        put     "/leaderboards/$leaderboardId/player/$playerId" (controller: "leaderboardEntry", action: "update")
        get     "/leaderboards/$leaderboardId/ranking"          (controller: "leaderboardEntry", action:  "ranking")

        //"/entries"(resources: "leaderboardEntry", includes: ['show','index','save','update'])


        get     "/leaderboards/enabled"  (controller: "leaderboard", action: "getEnabled")
        get     "/leaderboards/disabled" (controller: "leaderboard", action: "getDisabled")


        //Stats
        "/stats"(resources: "stat", excludes: ['create', 'edit', 'patch'])
        {
            put     "/activate"(action: 'enable')
            delete  "/activate"(action: 'disable')
        }
        get     "/stats/$statId/entries"            (controller: "statEntry", action: "index")
        get     "/stats/$statId/player/$playerId"   (controller: "statEntry", action: "show")
        post    "/stats/$statId/player/$playerId"   (controller: "statEntry", action: "save")
        put     "/stats/$statId/player/$playerId"   (controller: "statEntry", action: "update")
        get     "/stats/enabled"  (controller: "stat", action: "getEnabled")
        get     "/stats/disabled" (controller: "stat", action: "getDisabled")

        //Rooms and Lobbies
        "/rooms"(resources: "room",excludes: ['create', 'edit', 'patch'] )
        {
            put     "/activate"(action: 'enable')
            delete  "/activate"(action: 'disable')

            get     "/enter"(action: 'checkForPlayer')
            put     "/enter"(action: 'enterLobby')
            delete  "/enter"(action: 'exitLobby')

            //"/lobbies"(resources: "lobby", includes: ['index', 'show'])
        }
        get     "/rooms/enabled"  (controller: "room", action: "getEnabled")
        get     "/rooms/disabled" (controller: "room", action: "getDisabled")

        get     "/rooms/$roomId/lobbies"            (controller: "lobby", action: "index")
        //get     "/rooms/$roomId/player/$playerId"   (controller: "lobby", action: "show")
        //post    "/rooms/$roomId/player/$playerId"   (controller: "lobby", action: "save")
        //put     "/rooms/$roomId/player/$playerId"   (controller: "lobby", action: "update")
        put     "/rooms/$roomId/lobbies/$id"        (controller: "lobby", action: "show")
        get     "/rooms/$roomId/lobbies/$id"        (controller: "lobby", action: "show")

        get     "/rooms/$roomId/lobbies/$id/team"(controller: "lobby", action: 'showTeam')
        put     "/rooms/$roomId/lobbies/$id/team"(controller: "lobby", action: 'joinTeam')
        delete  "/rooms/$roomId/lobbies/$id/team"(controller: "lobby", action: 'leaveTeam')
        get     "/rooms/$roomId/lobbies/$id/game"(controller: "lobby", action: 'showGame')
        post    "/rooms/$roomId/lobbies/$id/game"(controller: "lobby", action: 'startGame')
        delete  "/rooms/$roomId/lobbies/$id/game"(controller: "lobby", action: 'finishGame')

        get     "/game/$id/" (controller: "game", action: 'show')
        put     "/game/$id/" (controller: "game", action: 'result')
        delete  "/game/$id/" (controller: "game", action: 'finish')

        //"/players"(resources: "player", excludes: ['index','create', 'edit', 'patch'])
        get     "/players/$id" (controller: "player", action: "show")
        post    "/players" (controller: "player", action: "save")
        put     "/players/$id" (controller: "player", action: "update")
        delete  "/players/$id" (controller: "player", action: "delete")



        "/"(controller: 'application', action:'index')
        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
