package shepard.frontend.rest

import groovy.json.JsonBuilder
import org.springframework.http.HttpStatus

import static org.springframework.http.HttpStatus.*

class GameController
{
	static responseFormats = ['json']
    static allowedMethods = [result: "PUT",
                             finish: "DELETE",
                             show: "GET"]


    GameService gameService

    def show()
    {
        def gameId = getGameId()
        if (gameId)
            respond gameService.showGame(gameId)

    }

    def result()
    {
        def gameId = getGameId()
        if (gameId)
        {
            def resume = gameService.results(gameId, params)
            if(resume) respond resume, [status: OK, model:'show']
            else badRequest()
        }
    }

    def finish()
    {
        def gameId = getGameId()
        if (gameId)
            respond gameService.finishGame(gameId)
    }


    def getGameId()
    {
        def gameId = params.id as String

        if (!gameId)
            throw new IllegalArgumentException()

        return gameId
    }

    def catchIllegalArgument(final IllegalArgumentException ex)
    {
        notFound()
    }

    def catchNullPointer(final NullPointerException ex)
    {
        badRequest()
    }

    def notFound = { render status: NOT_FOUND }

    def badRequest = { render status: BAD_REQUEST }
}
