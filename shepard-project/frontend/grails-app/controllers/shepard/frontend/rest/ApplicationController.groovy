package shepard.frontend.rest

class ApplicationController
{
    def index()
    {
        def achievements = Achievement.getAll()
        def leaderboards = Leaderboard.getAll()
        def rooms = Room.getAll()
        def stats = Stat.getAll()
        [achievements:achievements, leaderboards:leaderboards, stats:stats, rooms: rooms]
    }
}
