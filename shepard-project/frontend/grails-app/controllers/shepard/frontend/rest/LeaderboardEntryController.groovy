package shepard.frontend.rest

import grails.transaction.Transactional
import shepard.frontend.controllers.EntryController

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class LeaderboardEntryController extends EntryController<Leaderboard,LeaderboardEntry>
{
    LeaderboardEntryService leaderboardEntryService

    LeaderboardEntryController()
    {
        super(Leaderboard, LeaderboardEntry)
    }

    @Override
    protected LeaderboardEntryService getService()
    {
        return leaderboardEntryService
    }

    def ranking()
    {
        if (params.leaderboardId)
        {
            def leaderboard = Leaderboard.retrieve(params.leaderboardId as String)
            def start = params.int('start')
            def end = params.int('end')
            if(leaderboard == null)
            {
                render status: NOT_FOUND
                return
            }
            if (start == null || end == null)
            {
                render status: BAD_REQUEST
                return
            }
            if( end < start )
            {
                render status: UNPROCESSABLE_ENTITY
                return
            }
            start = (start < 0) ? 0 : start
            respond leaderboardEntryService.getRanking(leaderboard,start,end), [status: OK, view: 'index']
        }
        else render status: NOT_FOUND
    }
}
