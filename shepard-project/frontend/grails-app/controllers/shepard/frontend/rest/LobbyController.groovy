package shepard.frontend.rest

import org.springframework.http.HttpStatus
import shepard.frontend.controllers.EntryController
import shepard.frontend.services.EntryService

class LobbyController extends EntryController<Room,Lobby>
{
    LobbyService lobbyService
    GameService  gameService

    LobbyController()
    {
        super(Room, Lobby)
    }

    @Override
    protected EntryService getService()
    {
        return lobbyService
    }

    @Override
    def save() { badRequest() }

    @Override
    def update() { badRequest() }

    def showTeam()
    {
        def lobby = getLobby()
        respond getService().showTeam(lobby)
        //render status: HttpStatus.OK
    }

    def joinTeam()
    {
        def lobby = getLobby()
        def player = getPlayer()
        def team = params.int('team')

        if (team == null)
            throw new IllegalArgumentException()

        respond service.joinTeam(lobby,player,team)

        //render status: HttpStatus.OK
    }

    def leaveTeam()
    {
        def lobby = getLobby()
        def player = getPlayer()

        respond service.leaveTeam(lobby,player)

        //render status: HttpStatus.OK
    }

    def startGame()
    {
        def lobby = getLobby()
        params.clear()
        respond gameService.showGame(service.startGame(lobby)), [status: HttpStatus.OK, model: "../game/show"]
    }

    def showGame()
    {
        def lobby = getLobby()
        if (lobby.gameId == null)
            notFound()
        else
        {
            respond gameService.showGame(lobby.gameId), [status: HttpStatus.OK, model: "../game/show"]
        }
    }

    def finishGame()
    {
        def lobby = getLobby()
        def gameId = lobby.gameId ?: lobby.id
        respond gameService.finishGame(gameId), [status: HttpStatus.NO_CONTENT, model: "../game/show"]
    }

    private Lobby getLobby()
    {
        def entry = Lobby.retrieve(params.id as String)

        if (!entry)
        {
            def player = getPlayer()
            def room = Room.retrieve(params.roomId as String)

            if (player && room)
                entry = Lobby.findByRoomAndPlayer(room,player)
        }

        if (!entry) throw new NullPointerException()

        return entry
    }

    private Player getPlayer()
    {
        def player = Player.retrieve(params.playerId as String)

        if (!player)
            throw new NullPointerException()

        return player
    }


    def catchNullPointer(final NullPointerException ex)
    {
        notFound()
    }

}
