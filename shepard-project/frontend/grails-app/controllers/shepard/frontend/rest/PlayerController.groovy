package shepard.frontend.rest

import shepard.frontend.command.PlayerCommand

import static org.springframework.http.HttpStatus.*

class PlayerController {

    static responseFormats = ['json']
    static allowedMethods = [save: "POST",
                             update: ["PUT", "POST"],
                             delete: "DELETE",
                             show: "GET"]

    PlayerService playerService

    def show()
    {
        if (params.id)
        {
            def player = playerService.show(params.id as String)
            if(player) respond player, [status: OK, view: 'show']
            else notFound()
        }
        else notFound()
    }

    def save()
    {
        def data = getFromParams()
        if(data.user && data.password)
        {
            def player = playerService.save(data)
            if(player) respond player, [status: CREATED, view: 'show']
            else badRequest()
        }
        else badRequest()
    }

    def update()
    {
        def data = getFromParams()
        if (data.user && data.oldPassword && data.password)
        {
            def player = playerService.update(data)
            if(player) respond player, [status: OK, view: 'show']
            else notFound()
        }
        else badRequest()
    }

    def delete()
    {
        def data = getFromParams()
        if (data.user && data.password)
        {
            def deleted = playerService.delete(data)
            if (deleted) render status: NO_CONTENT
            else notFound()
        }
        else
            badRequest()
    }

    private PlayerCommand getFromParams()
    {
        def map = mergeBodyAndParams()
        new PlayerCommand(
            user: map.user,
            password: map.password,
            oldPassword: map.oldPassword
        )
    }

    private Map mergeBodyAndParams()
    {
        def values = [:]
        request.JSON.each { k,v -> if (v != null) values[k] = v }
        params.each { k,v -> if (v != null) values[k] = v }
        return values
    }

    def notFound = { render status: NOT_FOUND }

    def badRequest = { render status: BAD_REQUEST}
}