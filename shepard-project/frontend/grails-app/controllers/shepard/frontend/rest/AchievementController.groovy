package shepard.frontend.rest

import grails.transaction.Transactional
import shepard.frontend.controllers.ShepardServiceController

@Transactional(readOnly = true)
class AchievementController extends ShepardServiceController<Achievement>
{
    AchievementService achievementService

    AchievementController()
    {
        super(Achievement)
    }

    @Override
    AchievementService getService()
    {
        return achievementService
    }
}
