package shepard.frontend.rest

import shepard.frontend.controllers.EntryController
import shepard.frontend.services.EntryService

class StatEntryController extends EntryController<Stat,StatEntry>
{
    StatEntryService statEntryService

    StatEntryController()
    {
        super(Stat,StatEntry)
    }

    @Override
    protected EntryService getService()
    {
        return statEntryService
    }
}
