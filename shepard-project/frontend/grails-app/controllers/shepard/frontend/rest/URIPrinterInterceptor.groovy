package shepard.frontend.rest


class URIPrinterInterceptor {

    URIPrinterInterceptor()
    {
        matchAll()
    }

    boolean before() {
        params.each { k,v -> println "$k --> $v"}
        println "[$response.status] $request.method $request.requestURI -> $request.forwardURI"
        //println "$request.method $request.requestURI"
        true
    }

    boolean after()
    {
        //println "[$response.status] $request.method $request.requestURI"
        true
    }

    void afterView() {
        // no-op
    }
}
