package shepard.frontend.rest

import grails.transaction.Transactional
import shepard.frontend.controllers.ShepardServiceController

import static org.springframework.http.HttpStatus.BAD_REQUEST
import static org.springframework.http.HttpStatus.NOT_FOUND

@Transactional(readOnly = true)
class LeaderboardController extends ShepardServiceController<Leaderboard>
{
    LeaderboardService leaderboardService

    LeaderboardController()
    {
        super(Leaderboard)
    }

    @Override
    LeaderboardService getService()
    {
        return leaderboardService
    }

    def rank()
    {
        def id = getId()
        if(id)
        {
            def howMany = leaderboardService.rank(id)
            if(howMany == null) notFound() else respond(view: 'rank', model: [howMany: howMany])
        } else notFound()
    }

    def top10()
    {
        if (params.id)
        {
            def leaderboard = Leaderboard.get(params.id)
            if(leaderboard == null)
            {
                render status: NOT_FOUND
                return
            }
            params.remove('id')
            params.start = 0
            params.end = 10
            params.leaderboardId = leaderboard.id
            forward(controller: "leaderboardEntry", action: "ranking", params:params)
        }
        else
            render status: NOT_FOUND
    }

    def top100()
    {
        if (params.id)
        {
            def leaderboard = Leaderboard.get(params.id)
            if(leaderboard == null)
            {
                render status: NOT_FOUND
                return
            }
            params.remove('id')
            params.start = 0
            params.end = 100
            params.leaderboardId = leaderboard.id
            forward(controller: "leaderboardEntry", action: "ranking", params:params)
        }
        else
            render status: NOT_FOUND
    }

    def relative()
    {
        if (params.id && params.start && params.end)
        {
            def leaderboard = Leaderboard.get(params.id)
            if(leaderboard == null)
            {
                render status: NOT_FOUND
                return
            }
            if (params.int('end') < params.int('start'))
            {
                render status: BAD_REQUEST
                return
            }
            params.remove('id')
            params.leaderboardId = leaderboard.id
            forward(controller: "leaderboardEntry", action: "ranking", params:params)
        }
        else
            render status: NOT_FOUND
    }


}
