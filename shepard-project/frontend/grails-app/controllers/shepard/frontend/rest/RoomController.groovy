package shepard.frontend.rest

import grails.transaction.Transactional
import shepard.frontend.controllers.ShepardServiceController

@Transactional(readOnly = true)
class RoomController extends ShepardServiceController<Room>
{
    RoomService roomService
    LobbyService lobbyService

    RoomController()
    {
        super(Room)
    }

    @Override
    RoomService getService()
    {
        return roomService
    }

    def update()
    {
        show()
    }

    def checkForPlayer()
    {
        def player = Player.retrieve(params.playerId as String)
        def room = Room.retrieve(params.roomId as String)
        if (room && player)
        {
            def lobby = roomService.checkForPlayer(room,player)
            (lobby) ? forward2Lobby(lobby.identity) : notFound()
        }
        else notFound()
    }

    def enterLobby()
    {
        def player = Player.retrieve(params.playerId as String)
        def room = Room.retrieve(params.roomId as String)
        if (room && player)
        {
            def lobby = roomService.enterLobby(room,player)
            lobby ? forward2Lobby(lobby.identity) : notFound()
        }
        else notFound()
    }

    def exitLobby()
    {
        def player = Player.retrieve(params.playerId as String)
        def room = Room.retrieve(params.roomId as String)
        if (room && player)
        {
            def lobby = roomService.exitLobby(room,player)
            lobby ? forward2Lobby(lobby.identity) : notFound()
        }
        else notFound()
    }

    def forward2Lobby(String lobbyId)
    {
        respond lobbyService.getLobby(lobbyId)
    }
}
