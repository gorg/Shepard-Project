package shepard.frontend.rest

import grails.transaction.Transactional
import shepard.frontend.controllers.ShepardServiceController

@Transactional(readOnly = true)
class StatController extends ShepardServiceController<Stat>
{
    StatService statService

    StatController()
    {
        super(Stat)
    }

    @Override
    StatService getService()
    {
        return statService
    }
}
