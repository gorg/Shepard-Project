package shepard.frontend.rest

import cloud.orbit.actors.Actor
import groovy.transform.ToString
import shepard.backend.service.achievement.AchievementService
import shepard.backend.service.achievement.details.AchievementDetails
import shepard.backend.service.achievement.details.AchievementType
import shepard.backend.service.achievement.manager.AchievementManager
import shepard.frontend.domain.RetrieveActor

import static com.ea.async.Async.await
import static shepard.util.string.StringUtils.name2Id

@ToString
class Achievement implements RetrieveActor<AchievementService>
{
    String id
    String name
    String description
    AchievementType type
    String icon
    boolean enabled

    static mapping = {
        id generator: 'assigned'
        type enumType: 'string'
    }

    def beforeValidate()
    {
        if(id == null && name != null)
            id = name2Id(name)
        type = type ?: AchievementType.REVEALED
        enabled = enabled ?: false
    }

    static constraints = {
        name blank: false, nullable: false, updateable: false
        id validator:  {val, obj -> val == name2Id(obj.name) }
        icon blank: true, nullable: true
        description blank: true, nullable: true
        type blank: false, nullable: false, inList: AchievementType.values().toList()
        enabled blank: false, nullable: false
    }

    def beforeInsert()
    {
        def actor = await(Actor.getReference(AchievementManager).registerService(this.generateDetailsFromDomain()))
        this.enabled = await(actor.isEnabled())
    }

    def afterUpdate()
    {
        await(getActor().updateServiceDetails(this.generateDetailsFromDomain()))
    }

    def afterDelete()
    {
        await(Actor.getReference(AchievementManager).removeService(getActor()))
    }

    AchievementDetails generateDetailsFromDomain()
    {
        new AchievementDetails(
            name: this.name,
            description: this.description,
            icon: this.icon,
            type: this.type
        )
    }

    @Override
    AchievementService getActor()
    {
        Actor.getReference(AchievementService, this.id)
    }

    static Achievement retrieve(String entryId)
    {
        Achievement achievement = null
        if (entryId)
        {
            if (exists(entryId))
                achievement = get(entryId)
            else
            {
                achievement = fromActor(Actor.getReference(AchievementService, entryId))
                achievement = achievement?.validate() ? achievement.save(flush: true) : null
            }
        }
        return achievement
    }

    static Achievement retrieve(AchievementService actor)
    {
        (actor) ? retrieve(actor.identity) : null
    }

    private static Achievement fromActor(AchievementService actor)
    {
        Achievement achievement = null
        def details = await(actor.getServiceDetails())
        if (details)
        {
            achievement = new Achievement()
            achievement.name = details.name
            achievement.id = details.id()
            achievement.description = details.description
            achievement.type = details.type
            achievement.enabled = await(actor.isEnabled())
            achievement.icon = details.icon
        }
        return achievement
    }
}
