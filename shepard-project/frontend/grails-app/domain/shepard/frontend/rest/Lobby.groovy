package shepard.frontend.rest

import cloud.orbit.actors.Actor
import groovy.transform.ToString
import shepard.backend.service.matchmaking.entry.Lobby as BackendLobby
import shepard.frontend.domain.RetrieveActor
import shepard.frontend.domain.Updatable

import java.util.function.Predicate

import static com.ea.async.Async.await

@ToString(includeNames = true, includePackage = false)
class Lobby implements RetrieveActor<BackendLobby>,Updatable
{
    String id
    Room room
    Set<Player> players
    Double rating
    Double deviation
    String gameId

    static mapping = {
        id generator: 'assigned'
    }

    static constraints = {
        id          nullable: false, blank: false, unique: true
        room        nullable: false, blank: false
        rating      nullable: true,  blank: true
        deviation   nullable: true,  blank: true
        gameId      nullable: true,  blank: true
    }

    void updateFromBackend()
    {
        rating = await(getActor().lobbyRating)
        deviation = await(getActor().lobbyRatingDevation)
        players = await(getActor().lobbyPLayers).collect{ Player.retrieve(it.identity) } as Set
    }

    static Lobby findByRoomAndPlayer(Room room, Player player)
    {
        findAllByRoom(room).stream().filter(new Predicate<Lobby>()
        {
            @Override
            boolean test(Lobby lobby) {
                lobby.players.contains(player)
            }
        }).findAny().get()
    }

    @Override
    BackendLobby getActor()
    {
        Actor.getReference(BackendLobby, this.id)
    }

    static Lobby fromActor(BackendLobby actor)
    {
        (actor) ?  new Lobby(await(actor.collectEntry())) : null
    }

    static Lobby retrieve(String id)
    {
        Lobby entry = null
        if (id)
        {
            if (exists(id))
            {
                entry = get(id)
                entry.updateFromBackend()
                entry = entry.save(flush:true)
            }
            else
            {
                entry = fromActor(Actor.getReference(BackendLobby, id))
                entry.id = id
                entry = entry.validate() ? entry.save(flush:true) : null
            }
        }
        return entry
    }

    static Lobby retrieve(BackendLobby back, Room service)
    {
        Lobby lobby = null
        if(back)
        {
            if(exists(back.identity))
                lobby = get(back.identity)
            else
            {
                lobby = new Lobby()
                lobby.id = back.identity
                lobby.room = service
            }
            lobby.updateFromBackend()
            lobby = lobby.save(flush:true)
        }
        return lobby
    }
}
