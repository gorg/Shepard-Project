package shepard.frontend.rest

import cloud.orbit.actors.Actor
import shepard.backend.service.player.Player as BackendPlayer
import shepard.frontend.domain.RetrieveActor
import shepard.frontend.domain.Updatable

import static com.ea.async.Async.await

class Player implements RetrieveActor<BackendPlayer>, Updatable
{
    String id
    Double rating
    Integer games

    static mapping = {
        id generator: 'assigned'
    }

    static constraints = {
        id nullable: false, blank: false
        rating nullable: true, blank: true
        games nullable: true, blank: true
    }

    def beforeValidate() { }

    @Override
    BackendPlayer getActor()
    {
        Actor.getReference(shepard.backend.service.player.Player, id)
    }

    def beforeInsert()
    {
        updateFromBackend()
    }

    void updateFromBackend()
    {
        def skill = await(getActor().skill)
        rating = await(skill.rating)
        games = await(skill.numberOfResults)
    }

    static Player retrieve(String id)
    {
        Player player = null
        if (id)
        {
            if (exists(id))
            {
                player = get(id)
                player.updateFromBackend()
            }
            else
            {
                player = new Player()
                player.id = id
                player = player.save(flush:true)
            }
        }
        return player
    }

    static Player retrieve(BackendPlayer actor)
    {
        (actor) ? retrieve(actor.identity) : null
    }

}
