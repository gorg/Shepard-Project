package shepard.frontend.rest

import cloud.orbit.actors.Actor
import shepard.backend.service.leaderboard.LeaderboardService
import shepard.backend.service.leaderboard.details.LeaderboardDetails
import shepard.backend.service.leaderboard.details.LeaderboardSort
import shepard.backend.service.leaderboard.details.LeaderboardType
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.frontend.domain.RetrieveActor

import static com.ea.async.Async.await
import static shepard.util.string.StringUtils.name2Id

class Leaderboard implements RetrieveActor<LeaderboardService>
{
    String id
    String name
    Long upperLimit
    Long lowerLimit
    LeaderboardType leaderboardType
    LeaderboardSort leaderboardSort
    String icon
    boolean enabled

    static mapping = {
        id generator: 'assigned'
        leaderboardType enumType: 'string'
        leaderboardSort enumType: 'string'
    }

    static constraints = {
        id blank: false, nullable: false, updatable: false
        name blank: false, nullable: false, updatable: false
        icon blank: true, nullable: true
        leaderboardType nullable: true, inList: LeaderboardType.values().toList()
        leaderboardSort nullable: true, inList: LeaderboardSort.values().toList()
        upperLimit nullable: true, validator: {val, obj -> val > obj.lowerLimit }
        lowerLimit nullable: true, validator: {val, obj -> val < obj.upperLimit }
        enabled blank: false, nullable: false
    }

    def beforeValidate()
    {
        id = id ?: name2Id(name)
        enabled = enabled ?: false
        leaderboardType = leaderboardType ?: LeaderboardType.ABSOLUTE
        leaderboardSort = leaderboardSort ?: LeaderboardSort.HIGH

        if (lowerLimit == null)
            lowerLimit = 0

        if (upperLimit == null)
            upperLimit = Long.MAX_VALUE

        if (lowerLimit == upperLimit)
        {
            lowerLimit = 0
            upperLimit = Long.MAX_VALUE
        }
    }

    def beforeInsert()
    {
        def actor = await(Actor.getReference(LeaderboardManager).registerService(this.generateDetailsFromDomain()))
        this.enabled = await(actor.isEnabled())
    }

    def afterUpdate()
    {
        await(getActor().updateServiceDetails(this.generateDetailsFromDomain()))
    }

    def afterDelete()
    {
        await(Actor.getReference(LeaderboardManager).removeService(getActor()))
    }

    LeaderboardDetails generateDetailsFromDomain()
    {
        new LeaderboardDetails
        (
            name: this.name,
            icon: this.icon,
            type: this.leaderboardType,
            sort: this.leaderboardSort,
            lowerLimit: this.lowerLimit,
            upperLimit: this.upperLimit
        )
    }

    @Override
    LeaderboardService getActor()
    {
        Actor.getReference(LeaderboardService, this.id)
    }

    static Leaderboard retrieve(String id)
    {
        Leaderboard leaderboard = null
        if (id)
        {
            if (exists(id))
                leaderboard = get(id)
            else
            {
                leaderboard = fromActor(Actor.getReference(LeaderboardService,id))
                leaderboard = leaderboard?.validate() ? leaderboard.save(flush: true) : null
            }
        }
        return leaderboard
    }

    static Leaderboard retrieve(LeaderboardService actor)
    {
        (actor) ? retrieve(actor.identity) : null
    }

    private static Leaderboard fromActor(LeaderboardService actor)
    {
        def details = await(actor.getServiceDetails())
        if (details)
        {
            def leaderboard = new Leaderboard()
            leaderboard.id = details.id()
            leaderboard.name = details.name
            leaderboard.icon = details.icon
            leaderboard.leaderboardType = details.type
            leaderboard.leaderboardSort = details.sort
            leaderboard.upperLimit = details.upperLimit
            leaderboard.lowerLimit = details.lowerLimit
            leaderboard.enabled = await(actor.isEnabled())
            return leaderboard
        }
        else
            return null
    }

}
