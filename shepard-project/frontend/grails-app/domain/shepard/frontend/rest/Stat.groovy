package shepard.frontend.rest

import cloud.orbit.actors.Actor
import shepard.backend.service.stats.StatService
import shepard.backend.service.stats.details.StatDetails
import shepard.backend.service.stats.manager.StatManager
import shepard.frontend.domain.RetrieveActor

import static com.ea.async.Async.await
import static shepard.util.string.StringUtils.name2Id

class Stat implements RetrieveActor<StatService>
{
    String id
    String name
    boolean enabled
    BigDecimal maxChange
    BigDecimal minValue
    BigDecimal maxValue
    BigDecimal defaultValue
    boolean incrementOnly

    static mapping = {
        id generator: 'assigned'
    }

    static constraints = {
        name blank: false, nullable: false, updatable: false
        minValue blank: false, nullable: false, validator: {val,obj -> (val <= obj.defaultValue)&&(val < obj.maxValue)}
        maxValue blank: false, nullable: false, validator: {val,obj -> (val >= obj.defaultValue)&&(val > obj.minValue)}
        defaultValue blank: false, nullable: false, validator: {val,obj -> (val >= obj.minValue) && (val <= obj.maxValue) }
        maxChange blank: false, nullable: false, validator: {val,obj -> (val < obj.minValue.abs()+obj.maxValue.abs())}
        incrementOnly blank: false, nullable: false
    }

    def beforeValidate()
    {
        id = id ?: name2Id(name)

        if (minValue == null)
            minValue = 0

        if (maxValue == null)
            maxValue = BigDecimal.valueOf(Long.MAX_VALUE,4)

        if (minValue == maxValue)
        {
            minValue = 0
            maxValue = BigDecimal.valueOf(Long.MAX_VALUE,4)
        }

        enabled = enabled ?: false

        if (maxChange == null)
            maxChange = 0

        if (defaultValue == null)
            defaultValue = 0

        if (incrementOnly == null)
            incrementOnly = true
    }

    def beforeInsert()
    {
        def actor = await(Actor.getReference(StatManager).registerService(this.generateDetailsFromDomain()))
        this.enabled = await(actor.isEnabled())
    }

    def afterUpdate()
    {
        await(getActor().updateServiceDetails(this.generateDetailsFromDomain()))
    }

    def afterDelete()
    {
        await(Actor.getReference(StatManager).removeService(getActor()))
    }

    StatDetails generateDetailsFromDomain()
    {
        new StatDetails(
            name: this.name,
            maxChange: this.maxChange,
            defaultValue: this.defaultValue,
            minValue: this.minValue,
            maxValue: this.maxValue,
            incrementOnly: this.incrementOnly
        )
    }

    @Override
    StatService getActor()
    {
        Actor.getReference(StatService,this.id)
    }

    static Stat retrieve(String id)
    {
        Stat stat = null
        if (id)
        {
            if (exists(id))
                stat = get(id)
            else
            {
                stat = fromActor(Actor.getReference(StatService, id))
                stat = stat?.validate() ? stat.save(flush: true) : null
            }
        }
        return  stat
    }

    static Stat retrieve(StatService actor)
    {
        (actor) ? retrieve(actor.identity) : null
    }

    private static Stat fromActor(StatService actor)
    {
        def details = await(actor.getServiceDetails())
        if (details)
        {
            def stat = new Stat()
            stat.id = details.id()
            stat.name = details.name
            stat.defaultValue = details.defaultValue
            stat.minValue = details.minValue
            stat.maxValue = details.maxValue
            stat.incrementOnly = details.incrementOnly
            stat.maxChange = details.maxChange
            stat.enabled = await(actor.isEnabled())
            return stat
        }
        else
            return null
    }
}
