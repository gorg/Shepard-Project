package shepard.frontend.rest

import cloud.orbit.actors.Actor
import shepard.backend.service.matchmaking.RoomService
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.details.Rules
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.frontend.domain.RetrieveActor

import static com.ea.async.Async.await
import static shepard.util.string.StringUtils.name2Id

class Room implements RetrieveActor<RoomService>
{
    String id
    String name
    boolean enabled
    Integer teams
    Integer players
    Boolean ranked
    Double overlap

    static mapping = {
        id generator: 'assigned'
    }

    static constraints = {
        name    nullable: false, blank: false, updatable: false
        teams   nullable: false, blank: false, updatable: false, min:2, max: 8
        players nullable: false, blank: false, updatable: false, min:1, max:64
        ranked  nullable: false, blank: false, updatable: false
        overlap nullable: false, blank: false, updatable: false, validator: {val -> val > 0.25 && val < 1}
    }

    def beforeValidate()
    {
        id = id ?: name2Id(name)
        enabled = enabled ?: false
        teams = teams ?: 2
        players = players ?: 8
        overlap = overlap ?: 0.75d
        if (ranked == null) ranked = true
    }

    def beforeInsert()
    {
        def actor = await(Actor.getReference(RoomManager).registerService(this.generateDetailsFromDomain()))
        this.enabled = await(actor.isEnabled())
    }

    def afterDelete()
    {
        await(Actor.getReference(RoomManager).removeService(getActor()))
    }

    RoomDetails generateDetailsFromDomain()
    {
        new RoomDetails(
            name: this.name,
            rules: new Rules(
                teams: this.teams,
                players: this.players,
                ranked: this.ranked,
                overlap: this.overlap
            )
        )
    }

    @Override
    RoomService getActor()
    {
        Actor.getReference(RoomService, this.id)
    }

    static Room retrieve(String id)
    {
        Room room = null
        if (id)
        {
            if (exists(id))
                room = get(id)
            else
            {
                room = fromActor(Actor.getReference(RoomService, id))
                room = room?.validate() ? room.save(flush:true) : null
            }
        }
        return  room
    }

    static Room retrieve(RoomService actor)
    {
        (actor) ? retrieve(actor.identity) : null
    }

    static Room fromActor(RoomService actor)
    {
        def details =  await(actor.getServiceDetails())
        if (details)
        {
            def room = new Room()
            room.id = details.id()
            room.name = details.name
            room.players = details.rules.players
            room.teams = details.rules.teams
            room.ranked = details.rules.ranked
            room.overlap = details.rules.overlap
            room.enabled = await(actor.isEnabled())
            return room
        }
        else
            return null
    }
}
