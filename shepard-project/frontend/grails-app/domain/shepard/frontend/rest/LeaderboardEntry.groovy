package shepard.frontend.rest

import cloud.orbit.actors.Actor
import shepard.backend.service.leaderboard.ranking.Rank as BackendEntry
import shepard.frontend.domain.RetrieveActor
import shepard.frontend.domain.Updatable

import static com.ea.async.Async.await
import static shepard.backend.util.id.GenerateId.generateId

class LeaderboardEntry implements RetrieveActor<BackendEntry>, Updatable
{
    String id
    Long score
    Long rank
    Long delta
    Leaderboard leaderboard
    Player player

    static mapping = {
        id generator: 'assigned'
    }

    static constraints = {
        player nullable: false, blank: false, updatable: false
        leaderboard nullable: false, blank: false, updatable: false
        id nullable: false, blank: false, validator: { val,obj -> val == generateId(
                                                        obj.leaderboard.getActor(),obj.player.getActor()) }
        score nullable: false, blank: false
        rank  nullable: false, blank: false
        delta nullable: false, blank: false
    }

    def beforeValidate()
    {
        id = (leaderboard&&player) ? generateId(leaderboard.getActor(),player.getActor()) : null
        rank = rank ?: 0
        delta = delta ?: 0
        score = score ?: 0
    }

    void beforeInsert()
    {
        updateFromBackend()
    }

    void updateFromBackend()
    {
        def entry = await(leaderboard.getActor().getEntry(player.getActor()))
        this.score = await(await(entry.getScore()).getRawScore())
        this.rank = await(entry.rank)
        this.delta = await(entry.delta)
    }

    @Override
    BackendEntry getActor()
    {
        Actor.getReference(BackendEntry, this.id)
    }

    static LeaderboardEntry fromActor(BackendEntry actor)
    {
        (actor) ? new LeaderboardEntry(await(actor.collectEntry())) : null
    }

    static LeaderboardEntry retrieve(String id)
    {
        LeaderboardEntry entry = null
        if (id)
        {
            if (exists(id))
                entry = get(id)
            else
            {
                entry = fromActor(Actor.getReference(BackendEntry, id))
                entry = entry.validate() ? entry.save(flush:true) : null
            }
        }
        return entry
    }

}
