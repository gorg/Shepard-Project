package shepard.frontend.rest

import cloud.orbit.actors.Actor
import shepard.backend.service.achievement.entry.Achievement as BackendEntry
import shepard.frontend.domain.RetrieveActor
import shepard.frontend.domain.Updatable

import java.time.LocalDateTime

import static com.ea.async.Async.await
import static shepard.backend.util.id.GenerateId.generateId

class AchievementEntry implements RetrieveActor<BackendEntry>, Updatable
{
    String id
    Boolean completed
    LocalDateTime when
    Achievement achievement
    Player player

    static mapping = {
        id generator: 'assigned'
    }

    static constraints = {
        player nullable: false, blank: false, updateble: false
        achievement nullable: false, blank: false, updateble: false
        when nullable: true, blank: true, updateble: false, bindable: false
        completed nullable: false, blank: false, updateble: false, bindable: false
        id nullable: false, blank: false, validator: {val, obj -> val == generateId(obj.achievement.getActor(), obj.player.getActor()) }
    }

    def beforeValidate()
    {
        completed = completed ?: false
        id = (achievement&&player) ? generateId(achievement.getActor(),player.getActor()) : null
    }

    void beforeInsert()
    {
        def achievement = await(achievement.getActor().getEntry(player.getActor()))
        this.completed = await(achievement.isCompleted())
        this.when = await(achievement.whenWasCompleted())
    }

    @Override
    BackendEntry getActor()
    {
        Actor.getReference(BackendEntry, this.id)
    }

    @Override
    void updateFromBackend()
    {

    }

    static AchievementEntry retrieve(String id)
    {
        AchievementEntry entry
        if (id)
        {
            if (exists(id))
                entry = get(id)
            else
            {
                entry = fromActor(Actor.getReference(BackendEntry, id))
                entry = entry.validate() ? entry.save(flush:true) : null
            }
        }
        return entry
    }

    private static AchievementEntry fromActor(BackendEntry actor)
    {
        (actor) ? new AchievementEntry(await(actor.collectEntry())) : null
    }
}

