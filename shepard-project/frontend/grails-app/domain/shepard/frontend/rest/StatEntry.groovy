package shepard.frontend.rest

import cloud.orbit.actors.Actor
import shepard.backend.service.stats.entry.StatEntry as BackendEntry
import shepard.frontend.domain.RetrieveActor
import shepard.frontend.domain.Updatable

import static com.ea.async.Async.await
import static shepard.backend.util.id.GenerateId.generateId

class StatEntry implements RetrieveActor<BackendEntry>,Updatable
{

    String id
    Player player
    Stat stat
    BigDecimal score

    static mapping = {
        id generator: 'assigned'
    }

    static constraints = {
        player nullable: false, blank: false, updatable: false
        stat nullable: false, blank: false, updatable: false
        score nullable: true, blank: true, updatable: false, bindable: false
        id nullable: false, blank: false, validator: { val,obj -> val == generateId(
                obj.stat.getActor(),obj.player.getActor()) }
    }

    def beforeValidate()
    {
        id = (stat&&player) ? generateId(stat.getActor(),player.getActor()) : null
        score = score ?: stat?.defaultValue
    }

    void beforeInsert()
    {
        updateFromBackend()
    }

    void updateFromBackend()
    {
        def entry = await(stat.getActor().getEntry(player.getActor()))
        this.score = await(entry.getStatValue())
    }

    @Override
    BackendEntry getActor()
    {
        Actor.getReference(BackendEntry, this.id)
    }

    static StatEntry fromActor(BackendEntry actor)
    {
        (actor) ? new StatEntry(await(actor.collectEntry())) : null
    }

    static StatEntry retrieve(String id)
    {
        StatEntry entry = null
        if (id)
        {
            if (exists(id))
                entry = get(id)
            else
            {
                entry = fromActor(Actor.getReference(BackendEntry,id))
                entry = entry.validate() ? entry.save(flush:true) : null
            }
        }
        return entry
    }
}
