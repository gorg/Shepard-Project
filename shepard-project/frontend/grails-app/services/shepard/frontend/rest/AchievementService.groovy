package shepard.frontend.rest

import grails.transaction.Transactional
import shepard.backend.service.achievement.manager.AchievementManager
import shepard.frontend.services.ShepardServiceCommons

@Transactional
class AchievementService extends ShepardServiceCommons<Achievement, AchievementEntry, AchievementManager>
{
    AchievementService()
    {
        super(Achievement, AchievementEntry, AchievementManager)
    }
}
