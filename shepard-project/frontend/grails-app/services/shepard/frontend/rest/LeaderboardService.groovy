package shepard.frontend.rest

import cloud.orbit.actors.Actor
import grails.transaction.Transactional
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.backend.service.player.Player
import shepard.frontend.services.ShepardServiceCommons

import static com.ea.async.Async.await

@Transactional
class LeaderboardService extends ShepardServiceCommons<Leaderboard, LeaderboardEntry, LeaderboardManager>
{
    LeaderboardService()
    {
        super(Leaderboard, LeaderboardEntry, LeaderboardManager)
    }

    def setScore(Leaderboard leaderboard, String playerId, Long score)
    {
        def player = Actor.getReference(Player,playerId)
        await(leaderboard.getActor().insertScore(player,score))
    }

    Long rank(String id)
    {
        Long howMany = null
        if (id)
        {
            def leaderboard = Leaderboard.retrieve(id)
            if (leaderboard)
            {
                howMany =  await(leaderboard.actor.rank())
            }
        }
        return howMany
    }
}
