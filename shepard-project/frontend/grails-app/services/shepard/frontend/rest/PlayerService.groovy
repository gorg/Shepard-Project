package shepard.frontend.rest

import cloud.orbit.actors.Actor
import grails.transaction.Transactional
import shepard.backend.service.player.manager.PlayerManager
import shepard.frontend.command.PlayerCommand

import static com.ea.async.Async.await
import static shepard.util.string.StringUtils.name2Id

@Transactional
class PlayerService
{
    def show(String id)
    {
        Player.retrieve(await(Actor.getReference(PlayerManager).findById(id)))
    }

    def save(PlayerCommand data)
    {
        Player.retrieve(await(
                Actor.getReference(PlayerManager)
                .registerPlayer(data.user, data.password)))
    }

    def update(PlayerCommand data)
    {
        Player.retrieve(await(
                Actor.getReference(PlayerManager)
                .changePassword(data.user,data.oldPassword,data.password)))
    }

    def delete(PlayerCommand data)
    {
        if (await(Actor.getReference(PlayerManager).removePlayer(data.user,data.password)))
        {
            def player = Player.get(name2Id(data.user))
            if (player)
                player.delete(flush:true)

            return true
        }
        return false
    }
}
