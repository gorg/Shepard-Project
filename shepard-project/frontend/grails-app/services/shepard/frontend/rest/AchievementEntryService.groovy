package shepard.frontend.rest

import grails.transaction.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import shepard.frontend.services.EntryService

import static com.ea.async.Async.await

@Transactional
class AchievementEntryService implements EntryService<Achievement,AchievementEntry>
{
    @Override
    @Transactional
    AchievementEntry setEntry(Achievement achievement, Player player, GrailsParameterMap params)
    {
        if(player && achievement)
        {
            await(achievement.getActor().completeAchievement(player.getActor()))
            def entry = AchievementEntry.findByAchievementAndPlayer(achievement,player) ?: createEntry(achievement,player,params)
            entry.completed = true
            entry.when = await(achievement.actor.whenWasCompleted(player.actor))
            return  entry.save(flush: true)
        }
        else throw new IllegalArgumentException()
    }

    @Override
    AchievementEntry updateEntry(AchievementEntry entry, GrailsParameterMap params)
    {
        setEntry(entry.achievement,entry.player,params)
    }

    @Override
    @Transactional
    AchievementEntry createEntry(Achievement achievement, Player player, GrailsParameterMap params)
    {
        if(player && achievement)
        {
            def entry = new AchievementEntry()
            def backendEntry = achievement.actor.getEntry(player.actor).join()
            entry.id = backendEntry.identity
            entry.achievement = achievement
            entry.player = player
            if (entry.hasErrors())
            {
                transactionStatus.setRollbackOnly()
                throw new IllegalArgumentException()
            }
            return entry.save(flush: true)
        }
        else throw new IllegalArgumentException()
    }
}
