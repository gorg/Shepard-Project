package shepard.frontend.rest

import grails.transaction.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import shepard.frontend.services.EntryService

import static com.ea.async.Async.await

@Transactional
class StatEntryService implements EntryService<Stat,StatEntry>
{
    @Override
    @Transactional
    StatEntry setEntry(Stat stat, Player player, GrailsParameterMap params)
    {
        BigDecimal score = params.double('score')?.toBigDecimal()
        if (stat && player && score != null)
        {
            await(stat.getActor().update(score,player.getActor()))
            def entry = getOrCreate(stat,player)
            entry.updateFromBackend()
            return entry.save(flush:true)
        }else throw new IllegalArgumentException()
    }

    @Override
    StatEntry updateEntry(StatEntry entry, GrailsParameterMap params)
    {
        setEntry(entry.stat, entry.player, params)
    }

    @Override
    StatEntry createEntry(Stat service, Player player, GrailsParameterMap params)
    {
        getOrCreate(service,player)
    }

    @Transactional
    StatEntry getOrCreate(Stat stat, Player player)
    {
        def entry = StatEntry.findByStatAndPlayer(stat,player)
        if (entry == null)
        {
            entry = StatEntry.fromActor(await(stat.actor.getEntry(player.actor)))
            entry = entry.save(flush:true)
        }
        return entry
    }
}
