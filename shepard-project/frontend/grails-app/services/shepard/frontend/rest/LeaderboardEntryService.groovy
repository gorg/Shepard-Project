package shepard.frontend.rest

import grails.transaction.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import shepard.frontend.services.EntryService

import static com.ea.async.Async.await

@Transactional
class LeaderboardEntryService implements EntryService<Leaderboard,LeaderboardEntry>
{
    @Override
    @Transactional
    LeaderboardEntry setEntry(Leaderboard leaderboard, Player player, GrailsParameterMap params)
    {
        def score = params.long('score')
        if(leaderboard && player && score != null)
        {
            //Post new score to the leaderboard
            await(leaderboard.getActor().insertScore(player.getActor(),score))
            def entry = getOrCreate(leaderboard,player)
            entry.updateFromBackend()
            return entry.save(flush:true)
        }else throw new IllegalArgumentException()
    }

    @Override
    LeaderboardEntry updateEntry(LeaderboardEntry entry, GrailsParameterMap params)
    {
        setEntry(entry.leaderboard, entry.player, params)
    }

    @Override
    LeaderboardEntry createEntry(Leaderboard service, Player player, GrailsParameterMap params)
    {
        getOrCreate(service,player)
    }

    def getRanking(Leaderboard leaderboard, Integer start, Integer end)
    {
        try {
            def list = leaderboard.getActor().getRankedRelativeLeaderboard(start,end)
            return await(list).collect { getOrCreate(leaderboard, Player.get(await(it.player).identity)) }
        }
        catch (java.util.concurrent.CompletionException ex)
        {
            throw new IllegalArgumentException()
        }
    }

    @Transactional
    LeaderboardEntry getOrCreate(Leaderboard leaderboard, Player player)
    {
        def entry = LeaderboardEntry.findByLeaderboardAndPlayer(leaderboard,player)
        if (entry == null)
        {
            entry = LeaderboardEntry.fromActor(await(leaderboard.actor.getEntry(player.actor)))
            entry = entry.save(flush:true)
        }
        return entry
    }
}

