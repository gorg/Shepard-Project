package shepard.frontend.rest

import cloud.orbit.actors.Actor
import grails.transaction.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import shepard.backend.service.game.Game
import shepard.frontend.command.GameResume

import static com.ea.async.Async.await

@Transactional
class GameService
{
    GameResume showGame(String gameId)
    {
        getResume(Actor.getReference(Game, gameId))
    }

    GameResume finishGame(String gameId)
    {
        def game = Actor.getReference(Game, gameId)
        getResume(await(game.finishMatch()))
    }

    GameResume results(String gameId, GrailsParameterMap results)
    {
        def game = Actor.getReference(Game, gameId)
        getResume(postResults(game,results))
    }

    private static GameResume getResume(Game game)
    {
        new GameResume(game)
    }

    private static Game postResults(Game game, GrailsParameterMap params)
    {
        def draw = params.boolean('draw')
        def winner = params.int('winner')
        def loser = params.int('loser')

        if (draw == null && winner == null && loser == null)
            throw new NullPointerException()

        if (draw)
        {
            return await(game.addDraw())
        }
        else if (loser == null && winner >= 0)
        {
            return await(game.addWin(winner))
        }
        else if (loser >= 0 && winner >= 0)
        {
            return await(game.addWin(winner,loser))
        }
        else
            throw new NullPointerException()
    }
}




