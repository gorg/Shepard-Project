package shepard.frontend.rest

import grails.transaction.Transactional
import shepard.backend.service.stats.manager.StatManager
import shepard.frontend.services.ShepardServiceCommons

@Transactional
class StatService extends ShepardServiceCommons<Stat, StatEntry, StatManager>
{

    StatService()
    {
        super(Stat, StatEntry, StatManager)
    }
}
