package shepard.frontend.rest

import grails.transaction.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import shepard.backend.service.matchmaking.util.Team
import shepard.frontend.services.EntryService

import static com.ea.async.Async.await

@Transactional
class LobbyService implements EntryService<Room,Lobby>
{
    Lobby getLobby(String id)
    {
        Lobby.retrieve(id)
    }

    @Override
    Lobby setEntry(Room service, Player player, GrailsParameterMap params)
    {
       throw new IllegalArgumentException()
    }

    @Override
    Lobby updateEntry(Lobby entry, GrailsParameterMap params)
    {
        throw new IllegalArgumentException()
    }

    @Override
    Lobby createEntry(Room service, Player player, GrailsParameterMap params)
    {
        return null
    }

    def showTeam(Lobby lobby)
    {
        def teams = await(lobby.getActor().getTeams())
        def teamMap = [:]
        teamMap["lobby"] = lobby
        teams.eachWithIndex { Team entry, int i ->
            teamMap[i] = await(entry.getTeamMembers()).collect { Player.retrieve(it.identity) }
        }
        return teamMap
    }

    def joinTeam(Lobby lobby, Player player, int team)
    {
        def select = lobby.getActor().selectTeam(player.getActor(), team)
        if(select)
            return showTeam(lobby)
        else
            throw new IllegalArgumentException()
    }

    def leaveTeam(Lobby lobby, Player player)
    {
        (await(lobby.actor.leaveTeam(player.actor)))
        return showTeam(lobby)
    }

    @Transactional
    def startGame(Lobby lobby)
    {
        def game = await(lobby.actor.startGame())
        lobby.gameId = game.identity
        lobby.save(flush: true)
        return game.identity
    }

}


