package shepard.frontend.rest

import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.frontend.services.ShepardServiceCommons
import shepard.backend.service.matchmaking.entry.Lobby as BackendLobby

import static com.ea.async.Async.await

class RoomService extends ShepardServiceCommons<Room, Lobby, RoomManager>
{
    RoomService()
    {
        super(Room, Lobby, RoomManager)
    }

    @Override
    Room update(String id, Map params)
    {
        //Rooms can't be updated
        (id) ? Room.retrieve(id) : null
    }

    BackendLobby checkForPlayer(Room room, Player player)
    {
        (room && player) ? await(room.actor.checkForPlayer(player.actor)) :  null
    }

    BackendLobby enterLobby(Room room, Player player)
    {
        (room && player) ? await(room.actor.enterLobby(player.actor)) : null
    }

    BackendLobby exitLobby(Room room, Player player)
    {
        (room && player) ?  await(room.actor.exitLobby(player.actor)) : null
    }
}
