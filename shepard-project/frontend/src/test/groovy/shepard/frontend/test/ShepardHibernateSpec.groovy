package shepard.frontend.test

import cloud.orbit.actors.Stage
import cloud.orbit.actors.extensions.json.InMemoryJSONStorageExtension
import grails.test.hibernate.HibernateSpec

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 28/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class ShepardHibernateSpec extends HibernateSpec
{
    static Stage stage

    def setupSpec()
    {
        stage = new Stage()
        def id = Long.toHexString(System.currentTimeMillis())
        stage.setClusterName(id)
        stage.addExtension(new InMemoryJSONStorageExtension())
        stage.setNodeName(id)
        stage.setMode(Stage.StageMode.HOST)
        stage.start().join()
    }

    def cleanupSpec()
    {
        stage.stop().join()
    }

}
