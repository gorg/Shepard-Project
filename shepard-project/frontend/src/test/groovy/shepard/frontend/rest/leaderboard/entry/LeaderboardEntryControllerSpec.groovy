package shepard.frontend.rest.leaderboard.entry

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.frontend.rest.*
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Unroll

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 09/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Leaderboard, Player, LeaderboardEntry])
@TestFor(LeaderboardEntryController)
class LeaderboardEntryControllerSpec extends ShepardHibernateSpec
{
    def setup()
    {
        controller.transactionManager = transactionManager
        defineBeans {
            leaderboardEntryService(LeaderboardEntryService)
        }
    }
        /*
    @Unroll
    def "index"()
    {
        given:
        def leaderboard = Leaderboard.build(name: 'Leaderboard', id: 'leaderboard')
        12.times {
            def player = Player.build(id: "player-$it")
            def entry = LeaderboardEntry
                    .build(leaderboard: leaderboard, player: player, score: it, id: "$leaderboard.id:$player.id")
            assert entry.player == player
            assert entry.leaderboard == leaderboard
        }
        assert LeaderboardEntry.findAllByLeaderboard(leaderboard).size() == 12

        and:
        controller.params.leaderboardId = sendId ? leaderboard.id : null

        when:
        controller.index()

        then:
        if(sendId)
        {
            response.status == 200
            12.times {
                response.json[it].player.id == "player-$it"
                response.json[it].leaderboard.id == leaderboard.id
                response.json[it].score == it
                response.json[it].delta == 0
                response.json[it].rank == 0
            }
        }
        else response.status == 404

        where:
        sendId << [false,true]
    }
    */
    @Unroll
    def "show"()
    {
        given:
        def leaderboard = Leaderboard.build(name: 'Test', id: 'test')
        def player = Player.build(id: "player")
        def entry = LeaderboardEntry
                .build(player: player, leaderboard: leaderboard, score: 10, id: "$leaderboard.id:$player.id")

        and:
        controller.params.leaderboardId = sendLeaderboardID ? leaderboard.id : null
        controller.params.playerId = sendPlayerId ? player.id : null

        when:
        controller.show()

        then:
        if(sendLeaderboardID && sendPlayerId)
        {
            response.status == resStatus
            response.json.player.id == player.id
            response.json.leaderboard.id == leaderboard.id
            response.json.id == entry.id
            response.json.score == 10
            response.json.delta == 0
            response.json.rank == 0
        }
        else response.status == resStatus


        where:
        sendLeaderboardID | sendPlayerId || resStatus
        false             | false        || 404
        false             | true         || 404
        true              | false        || 404
        true              | true         || 200
    }

    @Unroll
    def "save score"()
    {
        given:
        def leaderboard = new Leaderboard(name: 'Test Leaderboard', id: 'test-leaderboard', lowerLimit: -10, upperLimit: 10).save(flush:true)
        def player = Player.build(id: "player")

        and:
        request.method = 'POST'
        controller.params.leaderboardId = sendLeaderboardiD ? leaderboard.id : null
        controller.params.playerId = sendPlayerId ? player.id : null
        controller.params.score = score

        when:
        controller.update()

        then:
        response.status == resStatus

        and:
        if (resStatus == 200)
        {
            assert response.json.player.id == player.id
            assert response.json.leaderboard.id == leaderboard.id
            assert response.json.score == 5     //Score of 15 would not be posted because of the upper limit
            assert response.json.rank == 0
            assert response.json.delta == 0
        }

        where:
        sendLeaderboardiD | sendPlayerId |score      || resStatus
        false             | false        |5          || 404
        false             | true         |5          || 404
        true              | false        |5          || 404
        true              | true         |null       || 400
        true              | true         |'a'        || 400
        true              | true         |5          || 200
        true              | true         |15         || 200
    }

    @Unroll
    def "update score with leaderboard and player"()
    {
        given:
        def leaderboard = new Leaderboard(name: 'Test Leaderboard', id: 'test-leaderboard', lowerLimit: -10, upperLimit: 10).save(flush:true)
        def player = Player.build(id: "player")


        and:
        request.method = 'PUT'
        controller.params.leaderboardId = sendLeaderboardiD ? leaderboard.id : null
        controller.params.playerId = sendPlayerId ? player.id : null
        controller.params.score = score
        controller.params.delta = delta
        controller.params.rank = rank

        when:
        controller.update()

        then:
        response.status == resStatus

        and:
        if (resStatus == 200)
        {
            assert response.json.player.id == player.id
            assert response.json.leaderboard.id == leaderboard.id
            assert response.json.score == score
            assert response.json.rank == 0      //Can't update rank from frontend
            assert response.json.delta == 0     //Rank and delta are update only after ranking operation
        }

        where:
        sendLeaderboardiD |sendPlayerId |score     |delta      |rank   || resStatus
        false             |false        |5         |null       |null   || 404
        false             |true         |5         |null       |null   || 404
        true              |false        |5         |null       |null   || 404
        true              |true         |5         |null       |null   || 200
        true              |true         |6         |null       |null   || 200
        true              |true         |7         |null       |1      || 200
        true              |true         |8         |1          |null   || 200
        true              |true         |9         |1          |2      || 200
    }

    @Unroll
    def "ranking"()
    {
        given:
        def leaderboard = Leaderboard.build(name: 'Test Leaderboard', id: 'test-leaderboard')
        def playerOne = Player.build(id: 'player-1')
        def entryOne = LeaderboardEntry.build(player: playerOne, leaderboard: leaderboard, score: 10, rank: 1, id: "$leaderboard.id:$playerOne.id")
        def playerTwo = Player.build(id: 'player-2')
        def entryTwo = LeaderboardEntry.build(player: playerTwo, leaderboard: leaderboard, score: 9, rank: 2, id: "$leaderboard.id:$playerTwo.id")
        def playerThree = Player.build(id: 'player-3')
        def entryThree = LeaderboardEntry.build(player: playerThree, leaderboard: leaderboard, score: 8, rank: 3,  id: "$leaderboard.id:$playerThree.id")
        controller.leaderboardEntryService = Mock(LeaderboardEntryService)
        controller.leaderboardEntryService.getRanking(_,_,_) >> [entryOne,entryTwo,entryThree]

        and:
        controller.params.leaderboardId = sendId ? leaderboard.id : null
        controller.params.start = sendStart ? start : null
        controller.params.end = sendEnd ? end : null

        when:
        controller.ranking()

        then:
        response.status == respStatus

        where:
        sendId  |sendStart  |start |sendEnd |end     || respStatus
        false   |false      |null  |false   |null    || 404
        true    |false      |null  |false   |null    || 400
        true    |true       |null  |false   |null    || 400
        true    |false      |null  |true    |null    || 400
        true    |true       |null  |true    |null    || 400
        true    |true       |20    |true    |10      || 422
        true    |true       |10    |true    |20      || 200
    }

}
