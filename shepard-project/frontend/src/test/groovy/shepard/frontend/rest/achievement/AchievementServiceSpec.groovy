package shepard.frontend.rest.achievement

import cloud.orbit.actors.Actor
import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.backend.service.achievement.details.AchievementType
import shepard.backend.service.achievement.manager.AchievementManager
import shepard.frontend.rest.Achievement
import shepard.frontend.rest.AchievementService
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Stepwise
import spock.lang.Unroll

@Stepwise
@Build(Achievement)
@TestFor(AchievementService)
class AchievementServiceSpec extends ShepardHibernateSpec
{

    def setup()
    {
        service.transactionManager = transactionManager
    }

    def index()
    {
        given:
        12.times {
            Achievement.build(name: "Test $it",id: "test-$it")
        }

        expect:
        Achievement.list().size() == 12

        when:
        def resp = service.index()

        then:
        resp.size() == 12

        and:
        12.times {
            def indx = it
            assert resp[indx].name == "Test $it"
            assert resp[indx].id == "test-$it"
            assert resp[indx].type == AchievementType.HIDDEN
        }
    }

    @Unroll
    def show()
    {
        given:
        def achievement = Achievement.build(name: 'Test', id:'test')
        def id = passId ? achievement.id : null

        when:
        def resp = service.show(id)

        then:
        passId ? resp == achievement : resp == null

        where:
        passId << [false, true]
    }

    @Unroll
    def save()
    {
        when:
        def resp = service.save(params)

        then:
        if (!valid) resp == null
        else
        {
            assert resp.validate()
            assert resp.name == params.name
            assert resp.description == ((params.description != null && !params.description?.empty) ? params.description : null)
            assert resp.type == ((params.type != null && !params.type?.empty) ? AchievementType.valueOf(params.type) : AchievementType.REVEALED)
            assert resp.icon == ((params.icon != null && !params.icon?.empty) ? params.icon : null)
        }

        where:
        params                                                                                                  ||valid
        [name: '']                                                                                              ||false
        [name: null]                                                                                            ||false
        [name: 'Test Achievement']                                                                              ||true
        [name: 'Test Achievement', description: '']                                                             ||true
        [name: 'Test Achievement', description: null]                                                           ||true
        [name: 'Test Achievement', description: 'asdf']                                                         ||true
        [name: 'Test Achievement', description: 'asdf', type: '']                                               ||true
        [name: 'Test Achievement', description: 'asdf', type: null]                                             ||true
        [name: 'Test Achievement', description: 'asdf', type: 'HIDDEN']                                         ||true
        [name: 'Test Achievement', description: 'asdf', type: 'REVEALED']                                       ||true
        [name: 'Test Achievement', description: 'asdf', type: 'REVEALED', icon: '']                             ||true
        [name: 'Test Achievement', description: 'asdf', type: 'REVEALED', icon: null]                           ||true
        [name: 'Test Achievement', description: 'asdf', type: 'REVEALED', icon: 'asdf']                         ||true
    }

    @Unroll
    def update()
    {
        given:
        def achievement = Achievement.build(name: 'Test Achievement', id: 'test-achievement')
        def id = sendId ? achievement.id : null
        def map = sendMap ? params : null

        when:
        def res = service.update(id, map)

        then:
        if (sendId && sendMap && params)
        {
            assert res.validate() == valid
            if (valid) assert res == achievement
        }
        else res == null

        where:
        sendId  |sendMap    |params                                             ||valid
        false   |false      |null                                               ||false
        false   |true       |null                                               ||false
        true    |false      |null                                               ||false
        true    |true       |null                                               ||false
        true    |true       |[:]                                                ||false
        true    |true       |[name: null]                                       ||false
        true    |true       |[name: '']                                         ||false
        true    |true       |[name: 'asdf']                                     ||false
        true    |true       |[description: null]                                ||true
        true    |true       |[description: '']                                  ||true
        true    |true       |[description: 'asdf']                              ||true
        true    |true       |[type: '']                                         ||true
        true    |true       |[type: null]                                       ||true
        true    |true       |[type: 'HIDDEN']                                   ||true
        true    |true       |[type: 'REVEALED']                                 ||true
        true    |true       |[icon: '']                                         ||true
        true    |true       |[icon: null]                                       ||true
        true    |true       |[icon: 'asdf']                                     ||true
        true    |true       |[description: 'asdf', type: 'REVEALED', icon: '2'] ||true
    }

    @Unroll
    def delete()
    {
        given:
        def achievement = Achievement.build(name: 'Test Achievement', id: 'test-achievement')
        def id = sendId ? achievement.id : null

        expect:
        Achievement.list().size() == 1

        when:
        def res = service.delete(id)

        then:
        res == isDeleted

        where:
        sendId  ||isDeleted
        false   ||false
        true    ||true
    }

    def enable()
    {
        given:
        def enabled = Achievement.build(name: 'Enabled achievement', id: 'enabled-achievement', enabled:true)
        def disabled = Achievement.build(name: 'Disabled achievement', id: 'disabled-achievement', enabled:false)

        expect:
        Achievement.list().size() == 2

        when:
        Actor.getReference(AchievementManager).disableService(disabled.actor).join()
        def res = service.enable(disabled.id)

        then:
        res == true

        when:
        res = service.enable(enabled.id)

        then:
        res == false
    }

    def disable()
    {
        given:
        def enabled = Achievement.build(name: 'Enabled achievement', id: 'enabled-achievement', enabled:true)
        def disabled = Achievement.build(name: 'Disabled achievement', id: 'disabled-achievement', enabled:false)

        expect:
        Achievement.list().size() == 2

        when:
        Actor.getReference(AchievementManager).enableService(enabled.actor).join()
        def res = service.disable(enabled.id)

        then:
        res == true

        when:
        res = service.disable(enabled.id)

        then:
        res == false
    }

    def getEnabled()
    {
        given:
        def achievement = Achievement.build(name: 'Achievement', id: 'achievement')

        expect:
        Achievement.list().size() == 1

        when:
        def res = service.getEnabled()

        then:
        res.size() == 1

        and:
        res.first() == achievement
    }

    def getDisabled()
    {
        given:
        def achievement = Achievement.build(name: 'Achievement', id: 'achievement')

        expect:
        Achievement.list().size() == 1

        when:
        service.disable(achievement.id)
        def res = service.getDisabled()

        then:
        res.size() == 1

        and:
        res.first() == achievement
    }

}
