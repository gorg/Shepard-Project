package shepard.frontend.rest.room

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.frontend.rest.Player
import shepard.frontend.rest.Room
import shepard.frontend.rest.RoomService
import shepard.frontend.test.ShepardHibernateSpec

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 07/05/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Room,Player])
@TestFor(RoomService)
class RoomServiceCustomSpec extends ShepardHibernateSpec
{
    def setup()
    {
        service.transactionManager = transactionManager
    }

    def "check for player"()
    {
        given:
        def room = Room.build(name: 'Room 1', id: 'room-1', overlap: 0.55)
        def player = Player.build(id: 'random-player')

        expect:
        Room.list().size() == 1

        when:
        def lobby = service.checkForPlayer(room,player)

        then:
        lobby == null
    }

    def "enter lobby"()
    {
        given:
        def room = Room.build(name: 'Room 1', id: 'room-1', overlap: 0.55)
        def player = Player.build(id: 'random-player')

        expect:
        Room.list().size() == 1

        when:
        def lobby = service.enterLobby(room,player)

        then:
        lobby.getLobbyPLayers().join().size() == 1
        lobby.getLobbyPLayers().join().first().identity == player.id
    }

    def "exit lobby"()
    {
        given:
        def room = Room.build(name: 'Room 1', id: 'room-1', overlap: 0.55)
        def player = Player.build(id: 'random-player')

        expect:
        Room.list().size() == 1

        when:
        def lobby = service.exitLobby(room,player)

        then:
        lobby.getLobbyPLayers().join().size() == 0
    }


}
