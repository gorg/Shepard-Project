package shepard.frontend.rest.leaderboard

import cloud.orbit.actors.Actor
import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.backend.service.leaderboard.details.LeaderboardSort
import shepard.backend.service.leaderboard.details.LeaderboardType
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.frontend.rest.Leaderboard
import shepard.frontend.rest.LeaderboardService
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Shared
import spock.lang.Stepwise
import spock.lang.Unroll

@Build(Leaderboard)
@TestFor(LeaderboardService)
class LeaderboardServiceSpec extends ShepardHibernateSpec
{
    def setup()
    {
        service.transactionManager = transactionManager
    }

    def index()
    {
        given:
        12.times {
            Leaderboard.build(name: "Test $it", id: "test-$it")
        }

        expect:
        Leaderboard.list().size() == 12

        when:
        def resp = service.index()

        then:
        resp.size() == 12

        and:
        12.times {
            assert resp[it].name == "Test $it"
            assert resp[it].id == "test-$it"
            assert resp[it].icon == null
            assert resp[it].lowerLimit == 0
            assert resp[it].upperLimit == Long.MAX_VALUE
            assert resp[it].leaderboardSort.name == "HIGH"
            assert resp[it].leaderboardType.name == "ABSOLUTE"
        }
    }

    @Unroll
    def show()
    {
        given:
        def leaderboard = Leaderboard.build(name: 'Test', id: 'test')
        def id = passId ? leaderboard.id : null

        when:
        def resp = service.show(id)

        then:
        if (passId)
        {
            assert resp == leaderboard
        }else assert resp == null

        where:
        passId << [false, true]
    }


    @Unroll
    def save()
    {
        when:
        def resp = service.save(params)

        then:
        if (!valid) assert resp == null
        else
        {
            assert resp.validate()
            assert resp.name == params.name
            assert resp.icon == params.icon
            assert resp.lowerLimit == (params.lowerLimit == null ? 0 : params.lowerLimit)
            assert resp.upperLimit == (params.upperLimit == null ? Long.MAX_VALUE : params.upperLimit)
            assert resp.leaderboardSort.name == (params.leaderboardSort == null ? "HIGH" : params.leaderboardSort.name)
            assert resp.leaderboardType.name == (params.leaderboardType == null ? "ABSOLUTE" : params.leaderboardType.name)
        }

        where:
        params                                                                                                                              ||valid
        [:]                                                                                                                                 ||false
        [name: '']                                                                                                                          ||false
        [name: null]                                                                                                                        ||false
        [name: 'test']                                                                                                                      ||true
        [name: 'test', lowerLimit: 10]                                                                                                      ||true
        [name: 'test', lowerLimit: 10, upperLimit: 100]                                                                                     ||true
        [name: 'test', lowerLimit: 10, upperLimit: 100, leaderboardSort: LeaderboardSort.LOW]                                               ||true
        [name: 'test', lowerLimit: 10, upperLimit: 100, leaderboardSort: LeaderboardSort.LOW, leaderboardType: LeaderboardType.ONLY_HIGHER] ||true
    }


    @Unroll
    def update()
    {
        given:
        def leaderboard = Leaderboard.build(name: 'Test', id: 'test', lowerLimit: 10, upperLimit: 20)
        def id = passId ? leaderboard.id : null
        def map = passMap ? params : null

        when:
        def res = service.update(id,map)

        then:
        if (passId && passMap && params)
        {
            assert res.validate() == valid
            if(valid) assert res == leaderboard
        }
        else res == null

        where:
        passId  |passMap    |params                 ||valid
        false   |false      |null                   ||false
        false   |true       |null                   ||false
        true    |false      |null                   ||false
        true    |true       |null                   ||false
        true    |true       |[:]                    ||false
        true    |true       |[name:'']              ||false
        true    |true       |[name:null]            ||false
        true    |true       |[name:'qwerty']        ||false
        true    |true       |[lowerLimit:0]         ||true
        true    |true       |[lowerLimit:50]        ||false
        true    |true       |[upperLimit:0]         ||false
        true    |true       |[upperLimit:50]        ||true
    }

    @Unroll
    def delete()
    {
        given:
        def leaderboard = Leaderboard.build(name: 'Delete me', id: 'delete-me')
        def id = passId ? leaderboard.id : null

        expect:
        Leaderboard.list().size() == 1

        when:
        def res = service.delete(id)

        then:
        res == isDeleted

        and:
        Leaderboard.list().size() == size

        where:
        passId  ||isDeleted |size
        false   ||false     |1
        true    ||true      |0
    }

    def enable()
    {
        given:
        def enabled = Leaderboard.build(name: 'Enable me', id: 'enable-me')
        def disabled = Leaderboard.build(name: 'Disable me', id: 'disable-me')

        expect:
        Leaderboard.list().size() == 2

        when:
        Actor.getReference(LeaderboardManager).disableService(disabled.actor).join()
        def res = service.enable(disabled.id)

        then:
        res == true

        when:
        res = service.enable(enabled.id)

        then:
        res == false
    }


    def disable()
    {
        given:
        def enabled = Leaderboard.build(name: 'Enable me', id: 'enable-me')
        def disabled = Leaderboard.build(name: 'Disable me', id: 'disable-me')

        expect:
        Leaderboard.list().size() == 2

        when:
        Actor.getReference(LeaderboardManager).enableService(enabled.actor).join()
        def res = service.disable(enabled.id)

        then:
        res == true

        when:
        res = service.disable(enabled.id)

        then:
        res == false
    }


    def getEnabled()
    {
        given:
        def stat = Leaderboard.build(name: 'Test me', id: 'test-me')

        expect:
        Leaderboard.list().size() == 1

        when:
        def res = service.getEnabled()

        then:
        res.size() == 1

        and:
        res.first() == stat
    }

    def getDisabled()
    {
        given:
        def stat = Leaderboard.build(name: 'Disable me', id: 'disable-me')

        expect:
        Leaderboard.list().size() == 1

        when:
        service.disable(stat.id)
        def res = service.getDisabled()

        then:
        res.size() == 1

        and:
        res.first() == stat
    }







}
