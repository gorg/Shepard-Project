package shepard.frontend.rest.achievement.entry

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.frontend.rest.Achievement
import shepard.frontend.rest.AchievementEntry
import shepard.frontend.rest.AchievementEntryService
import shepard.frontend.rest.Player
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Unroll

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 08/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Achievement,Player, AchievementEntry])
@TestFor(AchievementEntryService)
class AchievementEntryServiceSpec extends ShepardHibernateSpec
{
    def setup() {
        service.transactionManager = transactionManager
    }

    @Unroll
    def "Invalid complete achievement"()
    {
        given:
        def player = buildPlayer ? Player.build(id: 'random-player') : null
        def achievement = buildAchievement ? Achievement.build(name: 'Test', id: 'test') : null

        when:
        service.setEntry(achievement,player,null)

        then:
        thrown(IllegalArgumentException)

        where:
        buildAchievement    | buildPlayer 
        false               | false       
        false               | true        
        true                | false       
    }

    @Unroll
    def "Complete achievement"()
    {
        given:
        def player = Player.build(id: 'random-player')
        def achievement = Achievement.build(name: 'Test', id: 'test')

        when:
        def entry = service.setEntry(achievement,player,null)

        then:
        entry.achievement == achievement
        entry.player == player
        entry.completed == true
        entry.when == achievement.getActor().getEntry(player.getActor()).join().whenWasCompleted().join()
    }

}
