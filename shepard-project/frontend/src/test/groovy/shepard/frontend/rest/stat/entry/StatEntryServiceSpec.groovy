package shepard.frontend.rest.stat.entry

import cloud.orbit.actors.Actor
import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import grails.web.servlet.mvc.GrailsParameterMap
import org.grails.plugins.testing.GrailsMockHttpServletRequest
import shepard.backend.service.stats.manager.StatManager
import shepard.frontend.rest.Player
import shepard.frontend.rest.Stat
import shepard.frontend.rest.StatEntry
import shepard.frontend.rest.StatEntryService
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Shared
import spock.lang.Unroll

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 16/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Stat, StatEntry, Player])
@TestFor(StatEntryService)
class StatEntryServiceSpec extends ShepardHibernateSpec
{
    @Shared Stat stat

    def setup()
    {
        service.transactionManager = transactionManager
    }

    def "set a leaderboard"()
    {
        stat = Stat.build(name: "Test stat", id: "test-stat", minValue: 0, maxValue: 10000, defaultValue: 0)
        Stat.list().size() == 1

        expect:
        Actor.getReference(StatManager)
                .getAllEnabledServices().join().first() == stat.getActor()

        Actor.getReference(StatManager)
                .getAllEnabledServices().join().first()
                .getServiceDetails().join() == stat.generateDetailsFromDomain()
    }

    @Unroll
    def "set score"()
    {
        given:
        stat = Stat.build(name: "Test stat", id: "test-stat", minValue: 0, maxValue: 10000, defaultValue: 0)
        def map = new GrailsParameterMap([:], new GrailsMockHttpServletRequest())
        map.score = score

        def player = Player.build(id: "player-$id")

        when:
        def entry = service.setEntry(stat, player, map)

        then:
        entry.stat == stat
        entry.player == player
        entry.score == score


        where:
        id  | score
        0   | 1000
        1   | 900
        2   | 800
        3   | 700
        4   | 600
        5   | 400
        6   | 300
        7   | 200
        8   | 100
        9   | 90
        10  | 80
    }

}
