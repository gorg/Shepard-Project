package shepard.frontend.rest.room.lobby

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.frontend.rest.Lobby
import shepard.frontend.rest.Player
import shepard.frontend.rest.Room
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Unroll

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 18/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build(Player)
@TestFor(Lobby)
class LobbySpec extends ShepardHibernateSpec
{
    @Unroll
    void "Validates Lobby's instance that must be valid: #valid, with params: #params"()
    {
        given:
        Room room = new Room(name: 'Test', id: 'test', overlap: 0.55).save(flush:true)
        Player player = Player.build(id: 'random-player')
        Lobby lobby = new Lobby(params)

        expect:
        lobby.validate() == valid

        and:
        if (valid) {
            assert lobby.save()
            params.each { String key, Object value -> assert lobby."$key" == value }
        }

        where:
        params                                                                                  ||valid
        [:]                                                                                     ||false
        [room: 'test-room' ]                                                                    ||false
        [room: 'test-room', players: ['random-player']]                                         ||false
        [id: 'randomId', room: 'test-room', players: ['random-player']]                         ||false
        [id: 'randomId', room: 'test-room', players: ['random-player'], rating: 1320]           ||false
    }

    void "Retrieve a valid lobby instance"()
    {
        given:
        Room room = new Room(name: 'Test Room', id: 'test-room', overlap: 0.55).save(flush: true)
        Player player = Player.build(id: 'random-player-2')
        def backendLobby = room.getActor().getEntry(player.getActor()).join()

        when:
        def lobby = Lobby.retrieve(backendLobby, room)

        then:
        lobby.room == room
        lobby.players.contains(player)
    }


}
