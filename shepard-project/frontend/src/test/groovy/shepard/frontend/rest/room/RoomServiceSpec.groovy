package shepard.frontend.rest.room

import cloud.orbit.actors.Actor
import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.frontend.rest.Room
import shepard.frontend.rest.RoomService
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Unroll

@Build(Room)
@TestFor(RoomService)
class RoomServiceSpec extends ShepardHibernateSpec
{
    def setup()
    {
        service.transactionManager = transactionManager
    }

    def index()
    {
        given:
        12.times {
            Room.build(name: "Test $it", id: "test-$it", teams: 2, players: 8, overlap: 0.75)
        }

        expect:
        Room.list().size() == 12

        when:
        def resp = service.index()

        then:
        resp.size() == 12

        and:
        12.times {
            assert resp[it].name == "Test $it"
            assert resp[it].id == "test-$it"
            assert resp[it].overlap == 0.75
            assert resp[it].teams == 2
            assert resp[it].players == 8
            assert resp[it].ranked == false
        }
    }


    @Unroll
    def show()
    {
        given:
        def room = Room.build(name: 'Test', id: 'test', teams: 2, players: 8, overlap: 0.75)
        def id = passId ? room.id : null

        when:
        def resp = service.show(id)

        then:
        if (passId)
        {
            assert resp == room
        }else assert resp == null

        where:
        passId << [false, true]
    }


    @Unroll
    def save()
    {
        when:
        def resp = service.save(params)

        then:
        if (nu) assert resp == null
        else
        {
            assert resp.validate() == valid
            if (valid)
            {
                assert resp.name == params.name
                assert resp.teams == (params.teams ?: 2)
                assert resp.players == (params.players ?: 8)
                assert resp.ranked == (params.ranked == null ? true : params.ranked)
                assert resp.overlap == (params.overlap ?: 0.75)
            }
        }

        where:
        params                                                                      ||valid     |nu
        [:]                                                                         ||false     |true
        [name: null]                                                                ||false     |true
        [name: 'test']                                                              ||true      |false
        [name: 'test', teams: 1]                                                    ||false     |false
        [name: 'test', teams: 9]                                                    ||false     |false
        [name: 'test', teams: 2]                                                    ||true      |false
        [name: 'test', teams: 4, players: 65]                                       ||false     |false
        [name: 'test', teams: 4, players: 4]                                        ||true      |false
        [name: 'test', teams: 4, players: 4, ranked: false]                         ||true      |false
        [name: 'test', teams: 4, players: 4, ranked: true]                          ||true      |false
        [name: 'test', teams: 4, players: 4, ranked: true, overlap: 0.51]           ||true      |false
        [name: 'test', teams: 4, players: 4, ranked: true, overlap: 0.1]            ||false     |false
        [name: 'test', teams: 4, players: 4, ranked: true, overlap: 1.25]           ||false     |false
    }

    
    @Unroll
    def update()
    {
        given:
        def room = Room.build(name: 'Test', id: 'test', teams: 2, players: 8, overlap: 0.75)
        def id = passId ? room.id : null
        def map = passMap ? params : null

        //Rooms can't be updated
        when:
        def res = service.update(id,map)

        then:
        if (passId)
        {
            assert res == room
        }
        else res == null

        where:
        passId  |passMap    |params                 
        false   |false      |null                   
        false   |true       |null                   
        true    |false      |null                   
        true    |true       |null                   
        true    |true       |[:]                    
        true    |true       |[name:'']              
        true    |true       |[name:null]            
        true    |true       |[name:'qwerty']        
        true    |true       |[teams: 0]             
        true    |true       |[teams:6]              
        true    |true       |[teams:-5]             
        true    |true       |[upperLimit:50]        
    }
    
    
    @Unroll
    def delete()
    {
        given:
        def room = Room.build(name: 'Delete me', id: 'delete-me', teams: 2, players: 8, overlap: 0.75)
        def id = passId ? room.id : null

        expect:
        Room.list().size() == 1

        when:
        def res = service.delete(id)

        then:
        res == isDeleted

        and:
        Room.list().size() == size

        where:
        passId  ||isDeleted |size
        false   ||false     |1
        true    ||true      |0
    }
    

    def enable()
    {
        given:
        def enabled = Room.build(name: 'Enable me', id: 'enable-me', teams: 2, players: 8, overlap: 0.75)
        def disabled = Room.build(name: 'Disable me', id: 'disable-me', teams: 2, players: 8, overlap: 0.75)

        expect:
        Room.list().size() == 2

        when:
        Actor.getReference(RoomManager).disableService(disabled.actor).join()
        def res = service.enable(disabled.id)

        then:
        res == true

        when:
        res = service.enable(enabled.id)

        then:
        res == false
    }


    def disable()
    {
        given:
        def enabled = Room.build(name: 'Enable me', id: 'enable-me', teams: 2, players: 8, overlap: 0.75)
        def disabled = Room.build(name: 'Disable me', id: 'disable-me', teams: 2, players: 8, overlap: 0.75)

        expect:
        Room.list().size() == 2

        when:
        Actor.getReference(RoomManager).enableService(enabled.actor).join()
        def res = service.disable(enabled.id)

        then:
        res == true

        when:
        res = service.disable(enabled.id)

        then:
        res == false
    }

    def getEnabled()
    {
        given:
        def room = Room.build(name: 'Test me', id: 'test-me', teams: 2, players: 8, overlap: 0.75)

        expect:
        Room.list().size() == 1

        when:
        def res = service.getEnabled()

        then:
        res.size() == 1

        and:
        res.first() == room
    }

    def getDisabled()
    {
        given:
        def room = Room.build(name: 'Disable me', id: 'disable-me', teams: 2, players: 8, overlap: 0.75)

        expect:
        Room.list().size() == 1

        when:
        service.disable(room.id)
        def res = service.getDisabled()

        then:
        res.size() == 1

        and:
        res.first() == room
    }
}
