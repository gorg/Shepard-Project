package shepard.frontend.rest.game

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.backend.service.game.Game
import shepard.frontend.rest.*
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Shared
import spock.lang.Unroll

@Build([Room,Lobby,Player])
@TestFor(GameController)
class GameControllerSpec extends ShepardHibernateSpec
{
    @Shared Lobby lobby
    @Shared Game game
    @Shared Room room

    def setup()
    {
        defineBeans {
            gameService(GameService)
        }
    }

    def "setup players, room, lobby and start game"()
    {
        room = new Room(name: 'test', teams: 2, players: 2).save(flush:true)
        def player1 = Player.build(id: 'player-1')
        def player2 = Player.build(id: 'player-2')
        def lobby = room.getActor().getEntry(player1.getActor()).join()
        lobby.selectTeam(player1.actor,0).join()
        lobby = room.getActor().getEntry(player2.getActor()).join()
        lobby.selectTeam(player2.actor,1).join()
        game = room.actor.getLobbies().join().first().startGame().join()

        expect:
        !game.isFinished().join()
        game.lobby.join().getLobbyPLayers().join().collect { it.identity }.containsAll([player1.id, player2.id])
    }


    @Unroll
    def "show"()
    {
        given:
        def back = room.getActor().getLobbies().join().first()
        lobby = Lobby.retrieve(back,room)
        controller.params.id = sendId ? lobby.id : null

        when:
        controller.show()

        then:
        response.status == respStatus

        and:
        if (respStatus == 200)
        {
            assert response.json.finished == false
            assert response.json.lobby.id == lobby.id
        }

        where:
        sendId  || respStatus
        false   || 404
        true    || 200
    }

    @Unroll
    def "illegal result results"()
    {
        given:
        def back = room.getActor().getLobbies().join().first()
        lobby = Lobby.retrieve(back,room)
        controller.params.id = sendId ? lobby.id : null
        controller.params.draw = sendDraw ? draw : null
        controller.params.winner = sendWinner ? winer : null
        controller.params.loser = sendLoser ? loser : null
        request.method = 'PUT'

        when:
        controller.result()

        then:
        response.status == respStatus

        where:
        sendId  |sendDraw   |draw  |sendWinner |winer   |sendLoser  |loser  || respStatus
        false   |null       |null  |null       |null    |null       |null   || 404
        true    |null       |null  |null       |null    |null       |null   || 400
        true    |false      |null  |null       |null    |null       |null   || 400
        true    |true       |false |null       |null    |null       |null   || 400
        true    |null       |null  |true       |null    |null       |null   || 400
        true    |null       |null  |true       |'asdf'  |null       |null   || 400
        true    |null       |null  |true       |'-1'    |null       |null   || 400
        true    |null       |null  |true       |'0'     |true       |'-1'   || 400
    }

    @Unroll
    def "legal results"()
    {
        given:
        def back = room.getActor().getLobbies().join().first()
        lobby = Lobby.retrieve(back,room)
        controller.params.id = sendId ? lobby.id : null
        controller.params.draw = sendDraw ? draw : null
        controller.params.winner = sendWinner ? winer : null
        controller.params.loser = sendLoser ? loser : null
        request.method = 'PUT'

        when:
        controller.result()

        then:
        response.status == respStatus

        and:
        if (respStatus == 200)
        {
            assert response.json.finished == false
            assert response.json.lobby.id == lobby.id

            switch(iteraction) {
                case 0: //First draw
                    response.json.players.rounds.each { assert it == 1 }
                    response.json.players.draws.each { assert it == 1 }
                    response.json.players.wins.each { assert it == 0 }
                    response.json.players.loses.each { assert it == 0 }
                    response.json.players.score.each { assert it == 0.5 }
                    break

                case 1: //Win of team 0 over all
                    response.json.players.rounds.each { assert it == 2 }
                    def p1 = response.json.players."player-1"
                    assert p1.wins == 1
                    assert p1.loses == 0
                    assert p1.draws == 1
                    assert p1.score == 1.5


                    def p2 = response.json.players."player-2"
                    assert p2.wins == 0
                    assert p2.loses == 1
                    assert p2.draws == 1
                    assert p2.score == 0.5
                    break

                case 2: //Win of team 1 over all
                    response.json.players.rounds.each { assert it == 3 }
                    def p1 = response.json.players."player-1"
                    assert p1.wins == 1
                    assert p1.loses == 1
                    assert p1.draws == 1
                    assert p1.score == 1.5


                    def p2 = response.json.players."player-2"
                    assert p2.wins == 1
                    assert p2.loses == 1
                    assert p2.draws == 1
                    assert p2.score == 1.5
                    break

                case 3..4:
                    response.json.players.rounds.each { assert it == 4 }
                    def p1 = response.json.players."player-1"
                    assert p1.wins == 2
                    assert p1.loses == 1
                    assert p1.draws == 1
                    assert p1.score == 2.5


                    def p2 = response.json.players."player-2"
                    assert p2.wins == 1
                    assert p2.loses == 2
                    assert p2.draws == 1
                    assert p2.score == 1.5
                    break


                case 5: //Team 0 wins
                    response.json.players.rounds.each { assert it == 5 }
                    def p1 = response.json.players."player-1"
                    assert p1.wins == 3
                    assert p1.loses == 1
                    assert p1.draws == 1
                    assert p1.score == 3.5


                    def p2 = response.json.players."player-2"
                    assert p2.wins == 1
                    assert p2.loses == 3
                    assert p2.draws == 1
                    assert p2.score == 1.5
                    break


                case 6: //Draw
                    response.json.players.rounds.each { assert it == 6 }
                    def p1 = response.json.players."player-1"
                    assert p1.wins == 3
                    assert p1.loses == 1
                    assert p1.draws == 2
                    assert p1.score == 4.0


                    def p2 = response.json.players."player-2"
                    assert p2.wins == 1
                    assert p2.loses == 3
                    assert p2.draws == 2
                    assert p2.score == 2
                    break
            }
            
        }

        where:
        sendId  |sendDraw   |draw  |sendWinner |winer   |sendLoser  |loser  |iteraction     || respStatus
        true    |true       |true  |null       |null    |null       |null   |0              || 200      //Valid draw
        true    |null       |null  |true       |'0'     |null       |null   |1              || 200      //Valid win for team 0 over all the other teams
        true    |null       |null  |true       |'1'     |null       |null   |2              || 200      //Valid win for team 1 over all the other teams
        true    |null       |null  |true       |'0'     |true       |'asdf' |3              || 200      //Valid win for team 0 over all the other teams
        true    |null       |null  |true       |'0'     |true       |'0'    |4              || 200      //Score is not updated
        true    |null       |null  |true       |'0'     |true       |'1'    |5              || 200      //Valid win for team 0 over team 1
        true    |true       |true  |true       |'0'     |true       |'1'    |6              || 200      //It will be a draw
    }

    @Unroll
    def "finish"()
    {
        given:
        request.method = "DELETE"
        controller.params.id = sendId ? lobby.id : null

        when:
        controller.finish()

        then:
        response.status == respStatus

        and:
        if (respStatus == 200)
        {
            response.json.finished == true
            response.json.players.rounds.each { assert it == 6 }
            def p1 = response.json.players."player-1"
            assert p1.wins == 3
            assert p1.loses == 1
            assert p1.draws == 2
            assert p1.score == 4.0


            def p2 = response.json.players."player-2"
            assert p2.wins == 1
            assert p2.loses == 3
            assert p2.draws == 2
            assert p2.score == 2
        }

        where:
        sendId  || respStatus
        false   || 404
        true    || 200
    }
}
