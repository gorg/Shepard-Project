package shepard.frontend.rest.room

import cloud.orbit.actors.Actor
import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import org.springframework.http.HttpStatus
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.frontend.rest.*
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Unroll

import static shepard.util.string.StringUtils.name2Id

@Build([Room, Player])
@TestFor(RoomController)
class RoomControllerSpec extends ShepardHibernateSpec
{
    def setup()
    {
        defineBeans {
            lobbyService(LobbyService)
            roomService(RoomService)
        }
    }

    def "index"()
    {
        given:
        12.times {
            Room.build(name: "Test Room $it", id: "test-room-$it", enabled: true, overlap: 0.75)
        }

        when:
        controller.index()

        then:
        response.status == HttpStatus.OK.value()
        12.times {
            response.json[it].id == "test-room-$it"
            response.json[it].name == "Test Room $it"
            response.json[it].teams == 2
            response.json[it].players == 8
            response.json[it].ranked == true
            response.json[it].overlap == 0.75d
        }
    }

    @Unroll
    def "show"()
    {
        given:
        def room = Room.build(name: "Test Room", id: "test-room", overlap: 0.75)

        and:
        controller.params.id = sendId ? room.id : null

        when:
        controller.show()

        then:
        response.status == respStatus

        and:
        if (sendId)
        {
            response.json.id ==  room.id
            response.json.name == room.name
            response.json.teams == room.teams
            response.json.players ==  room.players
            response.json.ranked ==  room.ranked
            response.json.overlap == room.overlap
        }

        where:
        sendId   || respStatus
        false    || 404
        true     || 200
    }

    @Unroll
    def "save"()
    {
        given:
        request.method = method
        controller.params.name = name
        controller.params.id = id
        controller.params.teams = t
        controller.params.players = p
        controller.params.ranked = r
        controller.params.overlap = o


        when:
        controller.save()

        then:
        response.status == respStatus

        and:
        if (respStatus == 201)
        {
            assert response.json.name == name
            assert response.json.id == id?: name2Id(name)
            assert response.json.teams == t?:2
            assert response.json.players == p?:8
            assert response.json.ranked == (r == null) ? true : r
            assert response.json.overlap == o?:0.75
        }

        where:
        method  |name |id   |t    |p      |r      |o        || respStatus
        'GET'   |null |null |null |null   |null   |null     || 405
        'PUT'   |null |null |null |null   |null   |null     || 405
        'DELETE'|null |null |null |null   |null   |null     || 405
        'POST'  |null |null |null |null   |null   |null     || 400
        'POST'  |'t'  |null |null |null   |null   |null     || 201
        'POST'  |'t'  |'t'  |null |null   |null   |null     || 201
        'POST'  |null |'t'  |null |null   |null   |null     || 400
        'POST'  |'a'  |'b'  |null |null   |null   |null     || 201 //Bad id gets overwritten
        'POST'  |'t'  |'t'  |2    |null   |null   |null     || 201
        'POST'  |'t'  |'t'  |0    |null   |null   |null     || 201
        'POST'  |'t'  |'t'  |9    |null   |null   |null     || 422 // Teams > 8
        'POST'  |'t'  |'t'  |2    |2      |null   |null     || 201
        'POST'  |'t'  |'t'  |2    |0      |null   |null     || 201
        'POST'  |'t'  |'t'  |2    |65     |null   |null     || 422 // Players > 64
        'POST'  |'t'  |'t'  |2    |2      |true   |null     || 201
        'POST'  |'t'  |'t'  |2    |2      |false  |null     || 201
        'POST'  |'t'  |'t'  |2    |2      |true   |0        || 201
        'POST'  |'t'  |'t'  |2    |2      |true   |0.5      || 201
        'POST'  |'t'  |'t'  |2    |2      |true   |1.5      || 422 // overlap > 1
    }


    @Unroll
    def "remove"()
    {
        given:
        def room = Room.build(name: "Test Room", id: "test-room", overlap: 0.75)
        Room.list().size() == 1

        and:
        request.method = method
        controller.params.id = sendId ? room.id : null

        when:
        controller.delete()

        then:
        response.status == respStatus

        where:
        method  | sendId   || respStatus
        'GET'   | false    || 405
        'POST'  | true     || 405
        'PUT'   | true     || 405
        'DELETE'| false    || 404
        'DELETE'| true     || 204
    }

    def "get enabled"()
    {
        given:
        def enabled = Room.build(name: "Room 1", id: "room-1", enabled: true, overlap: 0.55)
        def disabled = Room.build(name: "Room 2", id: "room-2", enabled: false, overlap: 0.55)
        Room.list()
        Actor.getReference(RoomManager).disableService(disabled.actor).join()
        disabled.enabled = false
        disabled.save(flush: true)

        when:
        controller.getEnabled()

        then:
        response.status == HttpStatus.OK.value()
        (response.json as List<String>).size() == 1
        response.json[0].id == enabled.id
        response.json[0].enabled == true

    }


    def "get disabled"()
    {
        given:
        def enabled = Room.build(name: "Room 1", id: "room-1", enabled: true, overlap: 0.55)
        def disabled = Room.build(name: "Room 2", id: "room-2", enabled: false, overlap: 0.55)
        Room.list()
        Actor.getReference(RoomManager).disableService(disabled.actor).join()
        disabled.enabled = false
        disabled.save(flush: true)

        when:
        controller.getDisabled()

        then:
        response.status == HttpStatus.OK.value()
        (response.json as List<String>).size() == 1
        response.json[0].id == disabled.id
        response.json[0].enabled == false
    }

    @Unroll
    def "enable"()
    {
        given:
        def room = Room.build(name: "Test Room", id: "test-room", overlap: 0.75)
        if (sendId && !enabled)
            Actor.getReference(RoomManager).disableService(room.actor).join()
        if (sendId && enabled)
            Actor.getReference(RoomManager).enableService(room.actor).join()

        and:
        controller.params.id = sendId ? room.id : null

        when:
        controller.enable()

        then:
        response.status == respStatus

        where:
        sendId  | enabled || respStatus
        false   | false   || 404
        false   | true    || 404
        true    | false   || 200
        true    | true    || 304
    }

    @Unroll
    def "disable"()
    {
        given:
        def room = Room.build(name: "Test Room", id: "test-room", overlap: 0.75)
        if (sendId && !enabled)
            Actor.getReference(RoomManager).disableService(room.actor).join()
        if (sendId && enabled)
            Actor.getReference(RoomManager).enableService(room.actor).join()

        and:
        controller.params.id = sendId ? room.id : null

        when:
        controller.disable()

        then:
        response.status == respStatus

        where:
        sendId  | enabled  || respStatus
        false   | false    || 404
        false   | true     || 404
        true    | false    || 304
        true    | true     || 200
    }

    @Unroll
    def "update"()
    {
        //Rooms can't be updated. Controller will silently redirect to show()
        given:
        def room = Room.build(name: "t", id: "t", overlap: 0.75)

        and:
        request.method = method
        controller.params.id = id

        when:
        controller.update()

        then:
        response.status == respStatus

        and:
        if (respStatus == 200)
        {
            assert response.json.name == room.name
            assert response.json.id == room.id
            assert response.json.teams == room.teams
            assert response.json.players == room.players
            assert response.json.ranked == room.ranked
            assert response.json.overlap == room.overlap
        }

        where:
        method  |id   || respStatus
        'GET'   |null || 405
        'POST'  |null || 404
        'DELETE'|null || 405
        'PUT'   |null || 404
        'PUT'   |'t'  || 200
    }
    /*
    @Unroll
    def "enter a lobby"()
    {
        given:
        def room = new Room(name: "Test Room", id: "test-room", overlap: 0.75).save(flush: true)
        def player = Player.build(id: 'player')

        and:
        controller.params.roomId = sendId ? room.id : null
        controller.params.playerId = sendPlayerId ? player.id : null

        when:
        controller.enterLobby()

        then:
        response.status == respStatus

        and:
        response?.json?.each { println it }

        where:
        sendId  |sendPlayerId   ||respStatus
        false   |false          ||404
        false   |true           ||404
        true    |false          ||404
        true    |true           ||200
    }

    @Unroll
    def "check for player"()
    {
        given:
        def room = new Room(name: "Test Room", id: "test-room", overlap: 0.75).save(flush: true)
        def player = Player.build(id: 'player')

        and:
        controller.params.roomId = sendId ? room.id : null
        controller.params.playerId = sendPlayerId ? player.id : null

        when:
        controller.checkForPlayer()

        then:
        response.status == respStatus

        where:
        sendId  |sendPlayerId   ||respStatus
        false   |false          ||404
        false   |true           ||404
        true    |false          ||404
        true    |true           ||200
    }


    @Unroll
    def "exit lobby"()
    {
        given:
        def room = Room.build(name: "Test Room", id: "test-room", overlap: 0.75)
        def player = Player.build(id: 'player')

        and:
        controller.params.roomId = sendId ? room.id : null
        controller.params.playerId = sendPlayerId ? player.id : null

        when:
        controller.exitLobby()

        then:
        response.status == respStatus

        where:
        sendId  |sendPlayerId   ||respStatus
        false   |false          ||404
        false   |true           ||404
        true    |false          ||404
        true    |true           ||200
    }
    */


}