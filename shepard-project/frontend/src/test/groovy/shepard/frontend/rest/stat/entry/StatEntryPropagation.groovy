package shepard.frontend.rest.stat.entry

import cloud.orbit.actors.Actor
import grails.test.mixin.TestFor
import shepard.backend.service.stats.manager.StatManager
import shepard.frontend.rest.Player
import shepard.frontend.rest.Stat
import shepard.frontend.rest.StatEntry
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Stepwise

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 11/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
@TestFor(StatEntry)
class StatEntryPropagation extends ShepardHibernateSpec
{
    def "From backend to frontend test"()
    {
        given: 'a player and an stat'
        Stat stat = new Stat()
        stat.name = "Test Stat"
        stat.save(flush: true, insert: true)
        assert stat.id == stat.getActor().identity

        Player playerOne = new Player()
        playerOne.id = 'random-player'
        playerOne.save(flush: true, insert: true)
        assert playerOne.id == playerOne.getActor().identity


        expect: 'the stat service is running in the backend'
        Actor.getReference(StatManager)
                .getAllEnabledServices().join().size() == 1
        Actor.getReference(StatManager)
                .getAllEnabledServices().join().collect{ it.serviceDetails.join() }.name.contains(stat.name)

        when: 'the entry is completed'
        def backEntry = stat.getActor().getEntry(playerOne.getActor()).join()
        backEntry.updateStatValue(22.221).join()

        and: 'and a instance is created'
        def entry = new StatEntry(stat: stat, player: playerOne)
        entry.save(flush: true, insert: true)

        then: 'the instance will be completed'
        entry.stat == stat
        entry.player == playerOne
        entry.score == 22.221
    }

    def "From frontend to backend test"()
    {
        given: 'a player and an stat'
        Stat stat = new Stat()
        stat.name = "Test Stat 2"
        stat.save(flush: true, insert: true)
        assert stat.id == stat.getActor().identity

        Player playerOne = new Player()
        playerOne.id = 'random-player-2'
        playerOne.save(flush: true, insert: true)
        assert playerOne.id == playerOne.getActor().identity

        expect: 'the stat service is running in the backend'
        Actor.getReference(StatManager)
                .getAllEnabledServices().join().size() == 2
        Actor.getReference(StatManager)
                .getAllEnabledServices().join().collect{ it.serviceDetails.join() }.name.contains(stat.name)

        when:
        def entry = new StatEntry(stat: stat, player: playerOne)
        entry.score == 5151.253
        entry.save(flush: true, insert: true)
        def back = Actor.getReference(shepard.backend.service.stats.entry.StatEntry, entry.id)

        then:
        back.identity == entry.id
        back.player.join().identity == entry.player.id
        back.statValue.join() == entry.score
    }

}
