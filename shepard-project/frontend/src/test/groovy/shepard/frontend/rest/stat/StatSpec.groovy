package shepard.frontend.rest.stat

import grails.test.mixin.TestFor
import shepard.frontend.rest.Stat
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Stat)
class StatSpec extends Specification
{
    @Unroll
    void "Stat instance that must be valid: #valid, with params: #params"()
    {
        given:
        Stat stat = new Stat(params)

        expect:
        stat.validate() == valid

        and:
        if (valid) {
            assert stat.save()
            params.each { String key, Object value -> assert stat."$key" == value  }
        }

        where:
        params                                                                                                    ||valid
        [:]                                                                                                       ||false
        [name: null]                                                                                              ||false
        [name: '']                                                                                                ||false
        [id: null]                                                                                                ||false
        [id: '']                                                                                                  ||false
        [name: 'test']                                                                                            ||true
        [name: 'test', id: 'test']                                                                                ||true
        [name: 'test', id: 'test', defaultValue: 0]                                                               ||true
        [name: 'test', id: 'test', defaultValue: 0, minValue: 0]                                                  ||true
        [name: 'test', id: 'test', defaultValue: 0, minValue: 0, maxValue: 10, maxChange: 2]                      ||true
        [name: 'test', id: 'test', defaultValue: 0, minValue: 0, maxValue: 10, maxChange: 2, incrementOnly: false]||true
        [name: 'test', defaultValue: 0, minValue: 0, maxValue: 10, maxChange: 2, incrementOnly: false]            ||true
        [name: 'test', defaultValue: 0, minValue: 0, maxValue: 10, maxChange: 50, incrementOnly: false]           ||false
        [name: 'test', defaultValue: 0, minValue: 20, maxValue: 10, maxChange: 2, incrementOnly: false]           ||false
        [name: 'test', defaultValue: -50, minValue: 0, maxValue: 10, maxChange: 2, incrementOnly: false]          ||false



    }

    def "Stat instance with empty/null name"()
    {
        expect:
        !new Stat().validate(['name'])
        !new Stat().validate()
        !new Stat(name: null).validate(['name'])
        !new Stat(name: null).validate()
        !new Stat(name: "").validate(['name'])
        !new Stat(name: "").validate()
        !new Stat(id: null).validate()
    }

    @Unroll
    def "Stat instance with max: #max, min: #min and default: #dValue values, should be valid: #shouldBeValid"()
    {
        expect:
        new Stat(maxValue: max, minValue: min, defaultValue: dValue).validate(['minValue', 'maxValue', 'defaultValue']) == shouldBeValid

        where:
        min  | max   | dValue || shouldBeValid
        -100 | 10    | 0      || true
        0    | 99    | 0      || true
        0    | 0     | 0      || true
        0    | 500   | 0      || true
        -500 | 10000 | 0      || true
        null | null  | null   || true
        80   | 70    | 0      || false
        10   | 70    | 9      || false
        10   | 70    | 71     || false
        ""   | ""    | ""     || true
    }
}
