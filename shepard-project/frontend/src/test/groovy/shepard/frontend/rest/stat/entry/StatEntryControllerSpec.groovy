package shepard.frontend.rest.stat.entry

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.frontend.rest.*
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Shared
import spock.lang.Unroll

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 16/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Stat, StatEntry, Player])
@TestFor(StatEntryController)
class StatEntryControllerSpec extends ShepardHibernateSpec
{
    @Shared Stat stat

    def setup()
    {
        controller.transactionManager = transactionManager
        defineBeans {
            statEntryService(StatEntryService)
        }
        stat = Stat.build(name: 'Test', id:'test', minValue: -10, maxValue: 10,
                defaultValue: 5, maxChange: 0, incrementOnly: true)
    }

    @Unroll
    def "index"()
    {
        given:
        12.times {
            def player = Player.build(id: "player-$it")
            def entry = StatEntry
                    .build(stat: stat, player: player, score: it, id: "$stat.id:$player.id")
            assert entry.player == player
            assert entry.stat == stat
        }
        assert StatEntry.findAllByStat(stat).size() == 12

        and:
        controller.params.statId = sendId ? stat.id : null

        when:
        controller.index()

        then:
        if(sendId)
        {
            response.status == 200
            12.times {
                response.json[it].player.id == "player-$it"
                response.json[it].stat.id == stat.id
                response.json[it].score == it
            }
        }
        else response.status == 404

        where:
        sendId << [false,true]
    }

    @Unroll
    def "show"()
    {
        given:
        def player = Player.build(id: "player")
        def entry = StatEntry.build(player: player, stat: stat, score: 10, id: "$stat.id:$player.id")

        and:
        controller.params.statId = sendStatId ? stat.id : null
        controller.params.playerId = sendPlayerId ? player.id : null

        when:
        controller.show()

        then:
        if(sendStatId && sendPlayerId)
        {
            response.status == resStatus
            response.json.player.id == player.id
            response.json.stat.id == stat.id
            response.json.id == entry.id
            response.json.score == 10
        }
        else response.status == resStatus


        where:
        sendStatId | sendPlayerId || resStatus
        false      | false        || 404
        false      | true         || 404
        true       | false        || 404
        true       | true         || 200
    }

    @Unroll
    def "save score"()
    {
        given:
        def player = Player.build(id: "player")

        and:
        request.method = 'POST'
        controller.params.statId = sendStatId ? stat.id : null
        controller.params.playerId = sendPlayerId ? player.id : null
        controller.params.score = score

        when:
        controller.update()

        then:
        response.status == resStatus

        and:
        if (resStatus == 200)
        {
            assert response.json.player.id == player.id
            assert response.json.stat.id == stat.id
            assert response.json.score == 5.toBigDecimal()
        }

        where:
        sendStatId | sendPlayerId |score || resStatus
        false      | false        |5     || 404
        false      | true         |5     || 404
        true       | false        |5     || 404
        true       | true         |null  || 400
        true       | true         |'a'   || 400
        true       | true         |5     || 200
        true       | true         |15    || 200
    }

    @Unroll
    def "update score with leaderboard and player"()
    {
        given:
        def player = Player.build(id: "player")

        and:
        request.method = 'PUT'
        controller.params.statId = sendStatId ? stat.id : null
        controller.params.playerId = sendPlayerId ? player.id : null
        controller.params.score = score

        when:
        controller.update()

        then:
        response.status == resStatus

        and:
        if (resStatus == 200)
        {
            assert response.json.player.id == player.id
            assert response.json.stat.id == stat.id
            assert response.json.score == score
        }

        where:
        sendStatId |sendPlayerId |score || resStatus
        false      |false        |null  || 404
        false      |true         |null  || 404
        true       |false        |null  || 404
        true       |true         |null  || 400
        true       |true         |6     || 200
        true       |true         |7     || 200
        true       |true         |8     || 200
        true       |true         |9     || 200
    }
}
