package shepard.frontend.rest.stat.entry

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.frontend.rest.Player
import shepard.frontend.rest.Stat
import shepard.frontend.rest.StatEntry
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 16/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Stat, Player])
@TestFor(StatEntry)
class StatEntrySpec extends Specification
{
    @Unroll
    def "StatEntry instance must be #valid with params: #params"()
    {
        given:
        Stat stat = Stat.build(name: 'Test', id:'test', minValue: -10, maxValue: 10,
                defaultValue: 5, maxChange: 0, incrementOnly: true)
        Player player = Player.build(id: 'player')
        StatEntry entry = new StatEntry(params)

        expect:
        entry.validate() == valid

        and:
        if(valid)
        {
            entry = entry.save()
            entry.player == player
            entry.stat == stat
            entry.score == stat.defaultValue
        }

        where:
        params                                      ||valid
        [:]                                         ||false
        [stat:'test']                               ||false
        [player:'player']                           ||false
        [score: 4]                                  ||false
        [stat: 'test', player: 'player']            ||true
        [stat: 'test', player: 'player', score: 10] ||true //Score is not bindable. It will be set to the stat default
    }
}
