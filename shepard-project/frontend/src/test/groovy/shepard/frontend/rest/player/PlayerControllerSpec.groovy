package shepard.frontend.rest.player

import cloud.orbit.actors.Actor
import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.backend.service.player.manager.PlayerManager
import shepard.frontend.rest.Player
import shepard.frontend.rest.PlayerController
import shepard.frontend.rest.PlayerService
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Unroll

import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 29/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Player])
@TestFor(PlayerController)
class PlayerControllerSpec extends ShepardHibernateSpec
{
    def setup() {
        defineBeans {
            playerService(PlayerService)
        }
    }

    @Unroll
    def "save player #userName"()
    {
        given:
        request.method = method
        controller.params.user = userName
        controller.params.password = password

        when:
        controller.save()

        then:
        response.status == respStatus

        and:
        if (respStatus == 201)
        {
            def player = Actor.getReference(PlayerManager).findByName(userName).join()
            assert response.json.id == name2Id(userName)
            assert response.json.games == 0
            assert response.json.rating == 1500
            assert player.validatePlayer(userName,password).join() == player
        }

        where:
        method  |userName   |password   || respStatus
        'GET'   |null       |null       || 405
        'DELETE'|null       |null       || 405
        'PUT'   |null       |null       || 405
        'POST'  |null       |null       || 400
        'POST'  |'asdf'     |null       || 400
        'POST'  |null       |'asdf'     || 400
        'POST'  |'player 1' |'qwerty'   || 201
    }

    def "show player"()
    {
        given:
        controller.params.id = 'player-1'

        when:
        controller.show()

        then:
        response.status == 200
        response.json.id == name2Id('player 1')
        response.json.games == 0
        response.json.rating == 1500
    }

    @Unroll
    def "change player #userName password"()
    {
        given:
        request.method = method
        controller.params.user = userName
        controller.params.password = password
        controller.params.oldPassword = oldPassword

        when:
        controller.update()

        then:
        response.status == respStatus

        and:
        if (respStatus == 200)
        {
            def player = Actor.getReference(PlayerManager).findByName(userName).join()
            assert response.json.id == name2Id(userName)
            assert response.json.games == 0
            assert response.json.rating == 1500
            assert player.validatePlayer(userName,password).join() == player
            assert player.validatePlayer(userName,oldPassword).join() == null
        }

        where:
        method  |userName   |oldPassword    |password   || respStatus
        'GET'   |null       |null           |null       || 405
        'DELETE'|null       |null           |null       || 405
        'POST'  |null       |null           |null       || 400
        'PUT'   |null       |null           |null       || 400
        'PUT'   |'asdf'     |null           |null       || 400
        'PUT'   |null       |'asdf'         |null       || 400
        'PUT'   |'player 1' |'qwerty'       |'asdf'     || 200
    }

    @Unroll
    def "delete player #userName"()
    {
        given:
        Player.list().size() == 1
        request.method = method
        controller.params.user = userName
        controller.params.password = password

        when:
        controller.delete()

        then:
        response.status == respStatus

        and:
        if (respStatus == 204)
        {
            assert Player.list().size() == 0
        }


        where:
        method      |userName   |password   || respStatus
        'GET'       |null       |null       || 405
        'POST'      |null       |null       || 405
        'PUT'       |null       |null       || 405
        'DELETE'    |null       |null       || 400
        'DELETE'    |'asdf'     |null       || 400
        'DELETE'    |null       |'asdf'     || 400
        'DELETE'    |'player 1' |'asdf'     || 204
    }                       






}
