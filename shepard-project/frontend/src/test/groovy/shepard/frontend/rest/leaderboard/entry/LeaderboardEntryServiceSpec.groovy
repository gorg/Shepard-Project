package shepard.frontend.rest.leaderboard.entry

import cloud.orbit.actors.Actor
import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import grails.web.servlet.mvc.GrailsParameterMap
import org.grails.plugins.testing.GrailsMockHttpServletRequest
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.frontend.rest.Leaderboard
import shepard.frontend.rest.LeaderboardEntry
import shepard.frontend.rest.LeaderboardEntryService
import shepard.frontend.rest.Player
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Unroll

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 10/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Leaderboard, LeaderboardEntry, Player])
@TestFor(LeaderboardEntryService)
class LeaderboardEntryServiceSpec extends ShepardHibernateSpec
{
    static Leaderboard leaderboard

    def setup()
    {
        service.transactionManager = transactionManager
    }

    def "set a leaderboard"()
    {
        leaderboard = Leaderboard.build(name: "Test leaderboard", id: "test-leaderboard")
        expect:
        Leaderboard.list().size() == 1

        and:
        Actor.getReference(LeaderboardManager)
                .getAllEnabledServices().join().first() == leaderboard.getActor()

        Actor.getReference(LeaderboardManager)
                .getAllEnabledServices().join().first()
                .getServiceDetails().join() == leaderboard.generateDetailsFromDomain()
    }

    @Unroll
    def "set score"()
    {
        given:
        def map = new GrailsParameterMap([:], new GrailsMockHttpServletRequest())
        map.score = score
        leaderboard = Leaderboard.build(name: "Test leaderboard", id: "test-leaderboard")
        def player = Player.build(id: "player-$id")


        when:
        def entry = service.setEntry(leaderboard, player, map)

        then:
        entry.score == score
        entry.leaderboard == leaderboard
        entry.rank == 0
        entry.delta == 0

        where:
        id  | score
        0   | 1000
        1   | 900
        2   | 800
        3   | 700
        4   | 600
        5   | 400
        6   | 300
        7   | 200
        8   | 100
        9   | 90
        10  | 80
    }
    
    @Unroll
    def "get ranking"()
    {
        given:
        leaderboard = Leaderboard.build(name: "Test leaderboard", id: "test-leaderboard")         
        leaderboard.getActor().rank().join()
        11.times {
            Player.build(id: "player-$it")
        }

        when:
        def list = service.getRanking(leaderboard,start,end)

        then:
        list.eachWithIndex { entry, indx ->
            entry.player.id == "player-"+(indx+start)
            entry.leaderboard == leaderboard
            entry.rank == indx
        }

        and:
        //If the end is bigger it will be changed to the leaderboard size
        if (end == 50)
        {
            list.size() == 11
        }
        else list.size() == (end-start)

        where:
        start   |end
        0       |10
        5       |7
        2       |3
        0       |50
    }

}
