package shepard.frontend.rest.player

import cloud.orbit.actors.Actor
import grails.test.mixin.TestFor
import shepard.backend.service.player.manager.PlayerManager
import shepard.frontend.command.PlayerCommand
import shepard.frontend.rest.Player
import shepard.frontend.rest.PlayerService
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Unroll

import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 29/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@TestFor(PlayerService)
class PlayerServiceSpec extends ShepardHibernateSpec
{
    def show()
    {
        given:
        def name = 'Random Player'
        Actor.getReference(PlayerManager).registerPlayer(name, 'random-password').join()

        when:
        def r = service.show(name2Id(name))

        then:
        r.id == name2Id(name)
        r.rating == 1500
        r.games == 0

        and:
        def n = service.show('not registered player')

        then:
        n == null
    }

    @Unroll
    def "save [name:#name, pass:#pass] should be valid: #shouldBeValid"()
    {
        given:
        def data = new PlayerCommand(user: name, password: pass)

        when:
        def r = service.save(data)

        then:
        if (shouldBeValid)
        {
            assert r.id == name2Id(name)
            assert Player.retrieve(Actor.getReference(PlayerManager).findByName(name).join()) == r
        }
        else r == null


        where:
        name    |pass       ||shouldBeValid
        null    |null       ||false
        ''      |''         ||false
        null    |''         ||false
        ''      |null       ||false
        'a'     |null       ||false
        'a'     |''         ||false
        ''      |'a'        ||false
        null    |'a'        ||false
        'player'|'qwerty'   ||true
    }

    @Unroll
    def "update"()
    {
        given:
        def data = new PlayerCommand(user: name, password: pass, oldPassword: oldPass)

        when:
        def r = service.update(data)

        then:
        if (shouldBeValid)
        {
            assert r.id == name2Id(name)
            assert Player.retrieve(Actor.getReference(PlayerManager).findByName(name).join()) == r
            assert r.actor.validatePlayer(name,oldPass).join() == null
            assert r.actor.validatePlayer(name,pass).join() == r.actor
        }
        else r == null

        where:
        name    |oldPass    |pass       ||shouldBeValid
        'player'|'qwerty'   |'asdf'     ||true
        'player'|null       |null       ||false
        'player'|'qwerty'   |null       ||false
    }

    @Unroll
    def "delete"()
    {
        given:
        def data = new PlayerCommand(user: name, password: pass)

        when:
        def r = service.delete(data)

        then:
        r == shouldBeValid

        where:
        name     |pass       ||shouldBeValid
        'player' |'asdf'     ||true
        null     |'1231'     ||false
        'player' |null       ||false
        'random' |'random'   ||false
    }
}
