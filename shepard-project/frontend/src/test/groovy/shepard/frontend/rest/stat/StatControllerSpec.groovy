package shepard.frontend.rest.stat

import cloud.orbit.actors.Actor
import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import org.springframework.http.HttpStatus
import shepard.backend.service.stats.manager.StatManager
import shepard.frontend.rest.Player
import shepard.frontend.rest.Stat
import shepard.frontend.rest.StatController
import shepard.frontend.rest.StatService
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@Build([Stat,Player])
@TestFor(StatController)
class StatControllerSpec extends ShepardHibernateSpec
{
    def setup() {
        defineBeans {
            statService(StatService)
        }
    }

    def "index"()
    {
        given:
        12.times {
            Stat.build(name: "Stat $it", id: "stat-$it", minValue: 0 ,defaultValue: 0, maxValue: 10, maxChange: 0)
        }

        when:
        controller.index()

        then:
        response.status == HttpStatus.OK.value()
        12.times {
            response.json[it].name == "Stat $it"
            response.json[it].id == "stat-$it"
            response.json[it].minValue == 0
            response.json[it].maxValue == 10
            response.json[it].defaultvalue == 0
            response.json[it].maxChange == 0
        }
    }

    @Unroll
    def "show"()
    {

        given:
        def stat = Stat.build(name: "Test", id: "test", minValue: 0 ,defaultValue: 0, maxValue: 10, maxChange: 0)

        and:
        controller.params.id = sendId ? stat.id : null

        when:
        controller.show()

        then:
        response.status == respStatus

        where:
        sendId  || respStatus
        false   || 404
        true    || 200
    }


    @Unroll
    def "save"()
    {
        given:
        request.method = method
        controller.params.name = name
        controller.params.id = id
        controller.params.minValue = low
        controller.params.maxValue = up
        controller.params.defaultValue = d
        controller.params.incrementOnly = increment
        controller.params.maxChange = change


        when:
        controller.save()

        then:
        response.status == respStatus


        and:
        if (respStatus == 200)
        {
            response.json.id == id
            response.json.name == name
            response.json.defaultValue == d
            response.json.maxChange == change
            response.json.minValue == low
            response.json.incrementOnly == (increment == null) ? false : increment
            response.json.maxValue == up

        }

        where:
        method      |name   |id     |low   |up    |d       |change |increment  || respStatus
        'GET'       |null   |null   |null  |null  |null    |null   |null       || 405
        'PUT'       |null   |null   |null  |null  |null    |null   |null       || 405
        'DELETE'    |null   |null   |null  |null  |null    |null   |null       || 405
        'POST'      |null   |null   |null  |null  |null    |null   |null       || 400
        'POST'      |'t'    |null   |null  |null  |null    |null   |null       || 201
        'POST'      |'t'    |'t'    |null  |null  |null    |null   |null       || 201
        'POST'      |null   |'t'    |null  |null  |null    |null   |null       || 400
        'POST'      |'a'    |'b'    |null  |null  |null    |null   |null       || 201 //Bad id gets overwritten
        'POST'      |'t'    |'t'    |0     |0     |null    |null   |null       || 201
        'POST'      |'t'    |'t'    |0     |10    |null    |null   |null       || 201
        'POST'      |'t'    |'t'    |-10   |0     |null    |null   |null       || 201
        'POST'      |'t'    |'t'    |100   |10    |null    |null   |null       || 422 //maxValue < minValue
        'POST'      |'t'    |'t'    |0     |10    |0       |null   |null       || 201
        'POST'      |'t'    |'t'    |0     |10    |5       |null   |null       || 201
        'POST'      |'t'    |'t'    |0     |10    |11      |null   |null       || 422 //defaultValue > maxValue
        'POST'      |'t'    |'t'    |0     |10    |-1      |null   |null       || 422 //defaultValue < minValue
        'POST'      |'t'    |'t'    |0     |10    |0       |0      |null       || 201
        'POST'      |'t'    |'t'    |0     |10    |0       |5      |null       || 201
        'POST'      |'t'    |'t'    |0     |10    |0       |11     |null       || 422 //maxChange > abs(low)+abs(up)
        'POST'      |'t'    |'t'    |0     |10    |0       |0      |true       || 201
        'POST'      |'t'    |'t'    |0     |10    |0       |0      |false      || 201
    }
                                           
    @Unroll
    def "update"()
    {
        given:
        def stat = Stat.build(name: "Test", id: "test", minValue: 0 ,defaultValue: 0, maxValue: 10, maxChange: 0)

        and:
        request.method = method
        controller.params.name = name
        controller.params.id = id
        controller.params.minValue = low
        controller.params.maxValue = up
        controller.params.defaultValue = d
        controller.params.maxChange = change
        controller.params.incrementOnly = increment


        when:
        controller.update()

        then:
        response.status == respStatus

        and:
        if(respStatus == 200)
        {
            stat.name == name
            stat.id == id
            stat.minValue == low
            stat.maxValue == up
            stat.defaultValue == d
            stat.maxChange == change
            stat.incrementOnly == increment
        }

        where:
        method     |name   |id     |low   |up    |d       |change |increment  || respStatus
        'GET'      |null   |null   |null  |null  |null    |null   |null       || 405
        'POST'     |null   |null   |null  |null  |null    |null   |null       || 404
        'DELETE'   |null   |null   |null  |null  |null    |null   |null       || 405
        'PUT'      |null   |null   |null  |null  |null    |null   |null       || 404
        'PUT'      |'test' |null   |null  |null  |null    |null   |null       || 404
        'PUT'      |'test' |'test' |null  |null  |null    |null   |null       || 200
        'PUT'      |null   |'test' |null  |null  |null    |null   |null       || 200
        'PUT'      |'a'    |'b'    |null  |null  |null    |null   |null       || 404 
        'PUT'      |'test' |'test' |0     |0     |null    |null   |null       || 200
        'PUT'      |'test' |'test' |0     |10    |null    |null   |null       || 200
        'PUT'      |'test' |'test' |-10   |0     |null    |null   |null       || 200
        'PUT'      |'test' |'test' |100   |10    |null    |null   |null       || 422 //maxValue < minValue
        'PUT'      |'test' |'test' |0     |10    |0       |null   |null       || 200
        'PUT'      |'test' |'test' |0     |10    |5       |null   |null       || 200
        'PUT'      |'test' |'test' |0     |10    |11      |null   |null       || 422 //defaultValue > maxValue
        'PUT'      |'test' |'test' |0     |10    |-1      |null   |null       || 422 //defaultValue < minValue
        'PUT'      |'test' |'test' |0     |10    |0       |0      |null       || 200
        'PUT'      |'test' |'test' |0     |10    |0       |5      |null       || 200
        'PUT'      |'test' |'test' |0     |10    |0       |11     |null       || 422 //maxChange > abs(low)+abs(up)
        'PUT'      |'test' |'test' |0     |10    |0       |0      |true       || 200
        'PUT'      |'test' |'test' |0     |10    |0       |0      |false      || 200
    }
       
    @Unroll
    def "remove"()
    {
        given:
        def stat = Stat.build(name: "Test", id: "test", minValue: 0 ,defaultValue: 0, maxValue: 10, maxChange: 0)
        Stat.list().size() == 1

        and:
        request.method = method
        controller.params.id = sendId ? stat.id : null

        when:
        controller.delete()

        then:
        response.status == respStatus

        where:
        method      |sendId   || respStatus
        'GET'       |false    || 405
        'POST'      |true     || 405
        'PUT'       |true     || 405
        'DELETE'    |false    || 404
        'DELETE'    |true     || 204
    }


    def "get enabled"()
    {
        given:
        def enabled = Stat.build(name: "Test Stat", id: "test-stat",enabled: true, minValue: 0 ,defaultValue: 0, maxValue: 10, maxChange: 0)
        def disabled = Stat.build(name: "Test Stat 2", id: "test-stat-2",enabled: false, minValue: 0 ,defaultValue: 0, maxValue: 10, maxChange: 0)
        Stat.list()
        Actor.getReference(StatManager).disableService(disabled.actor).join()
        disabled.enabled = false
        disabled.save(flush:true)

        when:
        controller.getEnabled()

        then:
        response.status == HttpStatus.OK.value()
        (response.json as List<String>).size() == 1
        response.json[0].id == enabled.id
        response.json[0].enabled == true

    }


    def "get disabled"()
    {
        given:
        def enabled = Stat.build(name: "Test Stat", id: "test-stat",enabled: true, minValue: 0 ,defaultValue: 0, maxValue: 10, maxChange: 0)
        def disabled = Stat.build(name: "Test Stat 2", id: "test-stat-2",enabled: false, minValue: 0 ,defaultValue: 0, maxValue: 10, maxChange: 0)
        Stat.list()
        Actor.getReference(StatManager).disableService(disabled.actor).join()
        disabled.enabled = false
        disabled.save(flush:true)

        when:
        controller.getDisabled()

        then:
        response.status == HttpStatus.OK.value()
        (response.json as List<String>).size() == 1
        response.json[0].id == disabled.id
        response.json[0].enabled == false
    }


    @Unroll
    def "enable"()
    {
        given:
        def stat = Stat.build(name: "Test Stat", id: "test-stat",
                enabled: enabled, minValue: 0 ,defaultValue: 0, maxValue: 10, maxChange: 0)
        if (sendId && !enabled)
            Actor.getReference(StatManager).disableService(stat.actor).join()
        if (sendId && enabled)
            Actor.getReference(StatManager).enableService(stat.actor).join()

        and:
        controller.params.id = sendId ? stat.id : null

        when:
        controller.enable()

        then:
        response.status == respStatus

        where:
        sendId  | enabled || respStatus
        false   | false   || 404
        false   | true    || 404
        true    | false   || 200
        true    | true    || 304
    }

    @Unroll
    def "disable"()
    {
        given:
        def stat = Stat.build(name: "Test Stat", id: "test-stat",
                enabled: enabled, minValue: 0 ,defaultValue: 0, maxValue: 10, maxChange: 0)
        if (sendId && !enabled)
            Actor.getReference(StatManager).disableService(stat.actor).join()
        if (sendId && enabled)
            Actor.getReference(StatManager).enableService(stat.actor).join()

        and:
        controller.params.id = sendId ? stat.id : null

        when:
        controller.disable()

        then:
        response.status == respStatus

        where:
        sendId  | enabled  || respStatus
        false   | false    || 404
        false   | true     || 404
        true    | false    || 304
        true    | true     || 200
    }
    
}