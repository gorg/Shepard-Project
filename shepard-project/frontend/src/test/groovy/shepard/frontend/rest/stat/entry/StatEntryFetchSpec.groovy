package shepard.frontend.rest.stat.entry

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.backend.service.stats.entry.StatEntry as BackendEntry
import shepard.frontend.rest.Player
import shepard.frontend.rest.Stat
import shepard.frontend.rest.StatEntry
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Shared

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 24/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Stat,Player])
@TestFor(StatEntry)
class StatEntryFetchSpec extends ShepardHibernateSpec
{
    @Shared BackendEntry back
    @Shared Stat stat
    @Shared Player player

    def setup()
    {
        stat = Stat.build(name: 'Test Stat', id: 'test-stat', minValue: 0, maxValue: 10, defaultValue: 0)
        player = Player.build(id: 'random-player')

        assert Stat.list().size() == 1
        assert Player.list().size() == 1
    }

    def "setup an entry in the backend"()
    {
        when:
        back = stat.actor.getEntry(player.actor).join()

        then:
        back.player.join() == player.actor
        back.service.join() == stat.actor
    }

    def "map a backend entry to the frontend database"()
    {
        given:
        def params = back.collectEntry().join()

        when:
        def entry = new StatEntry(params)

        then:
        entry.validate()
        !entry.hasErrors()

        and:
        entry.stat == stat
        entry.player == player
        entry.score == 0
    }
}
