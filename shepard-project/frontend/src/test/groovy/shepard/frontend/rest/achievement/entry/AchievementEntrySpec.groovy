package shepard.frontend.rest.achievement.entry

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.frontend.rest.Achievement
import shepard.frontend.rest.AchievementEntry
import shepard.frontend.rest.Player
import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDateTime

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 09/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Player,Achievement])
@TestFor(AchievementEntry)
class AchievementEntrySpec extends Specification
{
    @Unroll
    def "AchievementEntry instance must be #valid with params: #params"()
    {
        given:
        Achievement achievement = Achievement.build(name: 'Test', id: 'test')
        Player player = Player.build(id: 'player')
        AchievementEntry entry = new AchievementEntry(params)

        expect:
        entry.validate() == valid

        and:
        if(valid)
        {
            def saved = entry.save()
            saved.achievement == achievement
            saved.player == player
            saved.when == null          //Can't bind
            saved.completed = false     //Can't bind
        }

        where:
        params                                                                                          || valid
        [:]                                                                                             || false
        [achievement:'test']                                                                            || false
        [player:'player']                                                                               || false
        [achievement:'test',player:'player']                                                            || true
        [achievement:'test',player:'player',completed:'true']                                           || true
        [achievement:'test',player:'player',completed:'true', when:'now']                               || true
        [achievement:'test',player:'player',completed:'true', when: LocalDateTime.now().toString()]     || true
    }
}
