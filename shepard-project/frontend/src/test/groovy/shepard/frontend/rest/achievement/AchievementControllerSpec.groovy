package shepard.frontend.rest.achievement

import cloud.orbit.actors.Actor
import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import org.springframework.http.HttpStatus
import shepard.backend.service.achievement.details.AchievementType
import shepard.backend.service.achievement.manager.AchievementManager
import shepard.frontend.rest.Achievement
import shepard.frontend.rest.AchievementController
import shepard.frontend.rest.AchievementService
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Unroll

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 28/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Achievement])
@TestFor(AchievementController)
class AchievementControllerSpec extends ShepardHibernateSpec
{
    def setup() {
        defineBeans {
            achievementService(AchievementService)
        }
    }

    def "index"()
    {
        given:
        12.times {
            Achievement.build(name: "Test Achievement $it", id: "test-achievement-$it")
        }

        when:
        controller.index()

        then:
        response.status == HttpStatus.OK.value()
        12.times {
            response.json[it].id == "test-achievement-$it"
            response.json[it].name == "Test Achievement $it"
        }
    }

    @Unroll
    def "show"()
    {
        given:
        Achievement.build(name: "Test Achievement", id: "test-achievement")

        and:
        controller.params.id = sendId ? 'test-achievement' : null

        when:
        controller.show()

        then:
        response.status == respStatus

        and:
        if (sendId)
            response.json.id == 'test-achievement'

        where:
        sendId   || respStatus
        false    || 404
        true     || 200
    }


    @Unroll
    def "save"()
    {
        given:
        request.method = method
        controller.params.name = name
        controller.params.id = id
        controller.params.description = desc
        controller.params.type = type
        controller.params.icon = icon

        when:
        controller.save()

        then:
        response.status == respStatus

        and:
        if (respStatus == 201)
        {
            assert response.json.id == (id == 'b') ? 'a' : (id) ? id : 't'
            assert response.json.name == name
            assert response.json.icon == icon
            assert response.json.description == desc
            assert response.json.type == (type) ?: 'REVEALED'
        }

        where:
        method  | name    |id     |desc   |type         |icon    || respStatus
        'GET'   | null    |null   |null   |null         |null    || 405
        'PUT'   | null    |null   |null   |null         |null    || 405
        'DELETE'| null    |null   |null   |null         |null    || 405
        'POST'  | null    |null   |null   |null         |null    || 400
        'POST'  | 't'     |null   |null   |null         |null    || 201
        'POST'  | 't'     |'t'    |null   |null         |null    || 201
        'POST'  | null    |'t'    |null   |null         |null    || 400
        'POST'  | 'a'     |'b'    |null   |null         |null    || 201 //Bad id gets overwritten
        'POST'  | 't'     |'t'    |'d'    |null         |null    || 201
        'POST'  | 't'     |'t'    |'d'    |'HIDDEN'     |null    || 201
        'POST'  | 't'     |'t'    |'d'    |'REVEALED'   |null    || 201
        'POST'  | 't'     |'t'    |'d'    |'asdf'       |null    || 422
        'POST'  | 't'     |'t'    |'d'    |'REVEALED'   |'a'     || 201 //Icon is an unchecked path/url to image
    }


    @Unroll
    def "remove"()
    {
        given:
        def achievement = Achievement.build(name: "Test Achievement", id: "test-achievement")
        Achievement.list().size() == 1

        and:
        request.method = method
        controller.params.id = sendId ? achievement.id : null

        when:
        controller.delete()

        then:
        response.status == respStatus

        where:
        method  | sendId   || respStatus
        'GET'   | false    || 405
        'POST'  | true     || 405
        'PUT'   | true     || 405
        'DELETE'| false    || 404
        'DELETE'| true     || 204
    }


    def "get enabled"()
    {
        given:
        def enabled = Achievement.build(name: "Test Achievement", id: "test-achievement", enabled: true)
        def disabled = Achievement.build(name: "Test Achievement 2", id: "test-achievement-2", enabled: false)
        Actor.getReference(AchievementManager).disableService(disabled.actor).join()
        disabled.enabled = false
        disabled.save(flush: true)

        when:
        controller.getEnabled()

        then:
        response.status == HttpStatus.OK.value()
        (response.json as List<String>).size() == 1
        response.json[0].id == enabled.id
        response.json[0].enabled == true
    }


    def "get disabled"()
    {
        given:
        def enabled = Achievement.build(name: "Test Achievement", id: "test-achievement", enabled: true)
        def disabled = Achievement.build(name: "Test Achievement 2", id: "test-achievement-2", enabled: false)
        Actor.getReference(AchievementManager).disableService(disabled.actor).join()
        disabled.enabled = false
        disabled.save(flush: true)

        when:
        controller.getDisabled()

        then:
        response.status == HttpStatus.OK.value()
        (response.json as List<String>).size() == 1
        response.json[0].id == disabled.id
        response.json[0].enabled == false
    }


    @Unroll
    def "enable"()
    {
        given:
        def achievement = Achievement.build(name: "Enabled Achievement", id: "enabled-achievement", enabled: enabled)
        if (sendId && !enabled)
            Actor.getReference(AchievementManager).disableService(achievement.actor).join()
        if (sendId && enabled)
            Actor.getReference(AchievementManager).enableService(achievement.actor).join()

        and:
        controller.params.id = sendId ? achievement.id : null

        when:
        controller.enable()

        then:
        response.status == respStatus

        where:
        sendId  | enabled || respStatus
        false   | false   || 404
        false   | true    || 404
        true    | false   || 200
        true    | true    || 304
    }

    @Unroll
    def "disable"()
    {
        given:
        def achievement = Achievement.build(name: "Disabled Achievement", id: "disabled-achievement", enabled: enabled)
        if (sendId && !enabled)
            Actor.getReference(AchievementManager).disableService(achievement.actor).join()
        if (sendId && enabled)
            Actor.getReference(AchievementManager).enableService(achievement.actor).join()

        and:
        controller.params.id = sendId ? achievement.id : null

        when:
        controller.disable()

        then:
        response.status == respStatus

        where:
        sendId  | enabled  || respStatus
        false   | false    || 404
        false   | true     || 404
        true    | false    || 304
        true    | true     || 200
    }

    @Unroll
    def "update"()
    {
        given:
        def achievement = Achievement.build(name: 'test', id: 'test')
        request.method = method
        controller.params.name = name
        controller.params.id = id
        controller.params.description = desc
        controller.params.type = type
        controller.params.icon = icon

        when:
        controller.update()

        then:
        response.status == respStatus

        and:
        if(respStatus == 200)
        {
            assert response.json.description == achievement.description
            assert response.json.type.name == achievement.type.name
            assert response.json.icon == achievement.icon
        }

        where:
        method  | name    | id     |desc   |type         |icon    || respStatus
        'GET'   | null    | null   |null   |null         |null    || 405
        'POST'  | null    | null   |null   |null         |null    || 404
        'DELETE'| null    | null   |null   |null         |null    || 405
        'PUT'   | null    | null   |null   |null         |null    || 404
        'PUT'   | 'test'  | null   |null   |null         |null    || 404
        'PUT'   | 'test'  | 'test' |null   |null         |null    || 200
        'PUT'   | 'test'  | 'test' |'a'    |null         |null    || 200
        'PUT'   | 'test'  | 'test' |'a'    |'HIDDEN'     |null    || 200
        'PUT'   | 'test'  | 'test' |'a'    |'REVEALED'   |null    || 200
        'PUT'   | 'test'  | 'test' |'a'    |'ads'        |null    || 422
        'PUT'   | 'test'  | 'test' |'a'    |'REVEALED'   |'a'     || 200
    }
}
