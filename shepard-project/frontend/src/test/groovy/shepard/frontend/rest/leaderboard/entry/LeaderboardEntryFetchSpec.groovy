package shepard.frontend.rest.leaderboard.entry

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.backend.service.leaderboard.ranking.Rank as BackendEntry
import shepard.frontend.rest.Leaderboard
import shepard.frontend.rest.LeaderboardEntry
import shepard.frontend.rest.Player
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Shared

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 24/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Leaderboard,Player])
@TestFor(LeaderboardEntry)
class LeaderboardEntryFetchSpec extends ShepardHibernateSpec
{
    @Shared BackendEntry back
    @Shared Leaderboard leaderboard
    @Shared Player player

    def setup()
    {
        leaderboard = Leaderboard.build(name: 'Test Leaderboard', id: 'test-leaderboard')
        player = Player.build(id: 'random-player')
    }

    def "setup an entry in the backend"()
    {
        when:
        back = leaderboard.actor.getEntry(player.actor).join()

        then:
        back.player.join() == player.actor
        back.service.join() == leaderboard.actor
    }

    def "map a backend entry to the frontend database"()
    {
        given:
        def params = back.collectEntry().join()

        when:
        def entry = new LeaderboardEntry(params)

        then:
        entry.validate()
        !entry.hasErrors()

        and:
        entry.leaderboard == leaderboard
        entry.player == player
        entry.score == 0
        entry.delta == 0
        entry.rank == 0
    }








}
