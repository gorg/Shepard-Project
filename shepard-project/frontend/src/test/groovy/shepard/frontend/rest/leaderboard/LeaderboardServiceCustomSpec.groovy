package shepard.frontend.rest.leaderboard

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.frontend.rest.Leaderboard
import shepard.frontend.rest.LeaderboardService
import shepard.frontend.rest.Player
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Shared

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 07/05/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Leaderboard, Player])
@TestFor(LeaderboardService)
class LeaderboardServiceCustomSpec extends ShepardHibernateSpec
{
    @Shared Integer count = 50

    def setup()
    {
        service.transactionManager = transactionManager
    }

    def "create a leaderboard, post scores and perform a rank"()
    {
        given:
        def leaderboard = Leaderboard.build(name: 'Test leaderboard', id: 'test-leaderboard', lowerLimit: 0, upperLimit: 100)

        count.times {
            Player.build(id: "player-$it")
        }

        Leaderboard.list().size() == 1
        Player.list().size() == count

        and:
        count.times {
            def score = leaderboard.actor.insertScore(Player.get("player-$it").actor, it).join()
            assert score.rawScore.join() == it
        }

        when:
        def howMany = service.rank(leaderboard.id)
        def board = leaderboard.actor.getRankedLeaderboard().join()

        then:
        howMany == count

        and:
        count.times {
            def number = count - (it+1)
            assert board[it].player.join().identity == "player-$number"
            assert board[it].rank.join() == it+1
            assert board[it].score.join().rawScore.join() as Integer == number
            assert Player.get("player-$number").actor == board[it].player.join()
        }
    }

























}
