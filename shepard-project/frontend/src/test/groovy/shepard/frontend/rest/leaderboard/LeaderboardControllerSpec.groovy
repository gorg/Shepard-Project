package shepard.frontend.rest.leaderboard

import cloud.orbit.actors.Actor
import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import org.springframework.http.HttpStatus
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.frontend.rest.Leaderboard
import shepard.frontend.rest.LeaderboardController
import shepard.frontend.rest.LeaderboardService
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Unroll

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 29/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Leaderboard])
@TestFor(LeaderboardController)
class LeaderboardControllerSpec extends ShepardHibernateSpec
{
    def setup()
    {
        defineBeans {
            leaderboardService(LeaderboardService)
        }
    }

    def "index"()
    {
        given:
        12.times {
            Leaderboard.build(name: "Leaderboard $it", id: "leaderboard-$it")
        }

        when:
        controller.index()

        then:
        response.status == HttpStatus.OK.value()
        12.times {
            response.json[it].name == "Leaderboard $it"
            response.json[it].id == "leaderboard-$it"
            response.json[it].icon == null
            response.json[it].lowerLimit == 0
            response.json[it].upperLimit == Long.MAX_VALUE
            response.json[it].leadeboardType == 'ABSOLUTE'
            response.json[it].leaderboardSort == 'HIGH'
        }
    }

    @Unroll
    def "show"()
    {
        given:
        def leaderboard = Leaderboard.build(name: 'Test', id: 'test')
        Leaderboard.list().size() == 1

        and:
        controller.params.id = (sendId) ? leaderboard.id : null

        when:
        controller.show()

        then:
        response.status == respStatus

        where:
        sendId  || respStatus
        false   || 404
        true    || 200
    }

    @Unroll
    def "save"()
    {
        given:
        request.method = method
        controller.params.name = name
        controller.params.id = id
        controller.params.lowerLimit = low
        controller.params.upperLimit = up
        controller.params.leaderboardType = type
        controller.params.leaderboardSort = sort

        when:
        controller.save()

        then:
        response.status == respStatus

        and:
        if (respStatus == 201)
        {
            response.json.id == id
            response.json.name == name
            response.json.leaderboardType == type
            response.json.leaderboardSort == sort
            response.json.lowerLimit == low
            response.json.upperLimit == up
        }

        where:
        method  |name    |id     |low    |up           |type           |sort      || respStatus
        'GET'   |null    |null   |null   |null         |null           |null      || 405
        'PUT'   |null    |null   |null   |null         |null           |null      || 405
        'DELETE'|null    |null   |null   |null         |null           |null      || 405
        'PUT'   |null    |null   |null   |null         |null           |null      || 405
        'POST'  |null    |null   |null   |null         |null           |null      || 400
        'POST'  |'t'     |null   |null   |null         |null           |null      || 201
        'POST'  |'t'     |'t'    |null   |null         |null           |null      || 201
        'POST'  |null    |'t'    |null   |null         |null           |null      || 400
        'POST'  |'a'     |'b'    |null   |null         |null           |null      || 201 //Bad id gets overwritten
        'POST'  |'t'     |'t'    |0      |0            |null           |null      || 201
        'POST'  |'t'     |'t'    |0      |10           |null           |null      || 201
        'POST'  |'t'     |'t'    |-10    |10           |null           |null      || 201
        'POST'  |'t'     |'t'    |-10    |-20          |null           |null      || 422 //upperLimit < lowerLimit
        'POST'  |'t'     |'t'    |0      |10           |'ABSOLUTE'     |null      || 201
        'POST'  |'t'     |'t'    |0      |10           |'INCREMENTAL'  |null      || 201
        'POST'  |'t'     |'t'    |0      |10           |'asdf'         |null      || 422  //bad enum
        'POST'  |'t'     |'t'    |0      |10           |'ABSOLUTE'     |'HIGH'    || 201
        'POST'  |'t'     |'t'    |0      |10           |'ABSOLUTE'     |'LOW'     || 201
        'POST'  |'t'     |'t'    |0      |10           |'ABSOLUTE'     |'asdf'    || 422  //bad enum
    }

    @Unroll
    def "update"()
    {
        given:
        def leaderboard = Leaderboard.build(name: 'test', id: 'test')
        request.method = method
        controller.params.name = name
        controller.params.id = id
        controller.params.lowerLimit = low
        controller.params.upperLimit = up
        controller.params.leaderboardType = type
        controller.params.leaderboardSort = sort

        when:
        controller.update()

        then:
        response.status == respStatus

        and:
        if(respStatus == 200)
        {
            leaderboard.name == name
            leaderboard.id == id
            leaderboard.lowerLimit == low
            leaderboard.upperLimit == up
            leaderboard.leaderboardType.toString() == type
            leaderboard.leaderboardSort.toString() == sort
        }

        where:
        method  |name    |id     |low    |up           |type           |sort      || respStatus
        'GET'   |null    |null   |null   |null         |null           |null      || 405
        'POST'  |null    |null   |null   |null         |null           |null      || 404
        'DELETE'|null    |null   |null   |null         |null           |null      || 405
        'PUT'   |null    |null   |null   |null         |null           |null      || 404
        'PUT'   |'test'  |null   |null   |null         |null           |null      || 404
        'PUT'   |'test'  |'test' |null   |null         |null           |null      || 200
        'PUT'   |null    |'test' |null   |null         |null           |null      || 200
        'PUT'   |'test'  |'test' |0      |5            |null           |null      || 200
        'PUT'   |'test'  |'test' |-5     |10           |null           |null      || 200
        'PUT'   |'test'  |'test' |0      |0            |null           |null      || 200
        'PUT'   |'test'  |'test' |20     |10           |null           |null      || 422 //upperLimit < lowerLimit
        'PUT'   |'test'  |'test' |0      |5            |'absolute'     |null      || 422 //bad enum
        'PUT'   |'test'  |'test' |0      |5            |'incremental'  |null      || 422 //bad enum
        'PUT'   |'test'  |'test' |0      |5            |'ABSOLUTE'     |null      || 200
        'PUT'   |'test'  |'test' |0      |5            |'INCREMENTAL'  |null      || 200
        'PUT'   |'test'  |'test' |0      |5            |'asdf'         |null      || 422 //bad enum
        'PUT'   |'test'  |'test' |0      |5            |'ABSOLUTE'     |'high'    || 422 //bad enum
        'PUT'   |'test'  |'test' |0      |5            |'ABSOLUTE'     |'low'     || 422 //bad enum
        'PUT'   |'test'  |'test' |0      |5            |'ABSOLUTE'     |'HIGH'    || 200
        'PUT'   |'test'  |'test' |0      |5            |'ABSOLUTE'     |'LOW'     || 200
        'PUT'   |'test'  |'test' |0      |5            |'ABSOLUTE'     |'asdf'    || 422 //bad enum
    }

    @Unroll
    def "remove"()
    {
        given:
        def leaderboard = Leaderboard.build(name: "Test", id: "test")
        Leaderboard.list().size() == 1

        and:
        request.method = method
        controller.params.id = sendId ? leaderboard.id : null

        when:
        controller.delete()

        then:
        response.status == respStatus

        where:
        method      |sendId   || respStatus
        'GET'       |false    || 405
        'POST'      |true     || 405
        'PUT'       |true     || 405
        'DELETE'    |false    || 404
        'DELETE'    |true     || 204
    }

    def "get enabled"()
    {
        setup:
        def enabled = Leaderboard.build(name: "Enabled Leaderboard", id: "enabled-leaderboard")
        def disabled = Leaderboard.build(name: "Disabled Leaderboard", id: "disabled-leaderboard")
        Leaderboard.list()      //Build plugin acts lazy

        and:
        Actor.getReference(LeaderboardManager).disableService(disabled.actor).join()
        disabled.enabled = false
        disabled.save(flush: true)

        when:
        controller.getEnabled()

        then:
        response.status == HttpStatus.OK.value()
        (response.json as List<String>).size() == 1
        response.json[0].id == enabled.id
        response.json[0].enabled == true

    }


    def "get disabled"()
    {
        given:
        def enabled = Leaderboard.build(name: "Enabled Leaderboard", id: "enabled-leaderboard")
        def disabled = Leaderboard.build(name: "Disabled Leaderboard", id: "disabled-leaderboard")
        Leaderboard.list() //Build plugin acts lazy

        and:
        Actor.getReference(LeaderboardManager).disableService(disabled.actor).join()
        disabled.enabled = false
        disabled.save(flush: true)

        when:
        controller.getDisabled()

        then:
        response.status == HttpStatus.OK.value()
        (response.json as List<String>).size() == 1
        response.json[0].id == disabled.id
        response.json[0].enabled == false
    }

    @Unroll
    def "enable"()
    {
        given:
        def leaderboard = Leaderboard.build(name: "Test Leaderboard", id: "test-leaderboard", enabled: enabled)
        if (sendId && !enabled)
            Actor.getReference(LeaderboardManager).disableService(leaderboard.actor).join()
        if (sendId && enabled)
            Actor.getReference(LeaderboardManager).enableService(leaderboard.actor).join()

        and:
        controller.params.id = sendId ? leaderboard.id : null

        when:
        controller.enable()

        then:
        response.status == respStatus

        where:
        sendId  | enabled || respStatus
        false   | false   || 404
        false   | true    || 404
        true    | false   || 200
        true    | true    || 304
    }

    @Unroll
    def "disable"()
    {
        given:
        def leaderboard = Leaderboard.build(name: "Test Leaderboard", id: "test-leaderboard", enabled: enabled)
        if (sendId && !enabled)
            Actor.getReference(LeaderboardManager).disableService(leaderboard.actor).join()
        if (sendId && enabled)
            Actor.getReference(LeaderboardManager).enableService(leaderboard.actor).join()

        and:
        controller.params.id = sendId ? leaderboard.id : null

        when:
        controller.disable()

        then:
        response.status == respStatus

        where:
        sendId  | enabled  || respStatus
        false   | false    || 404
        false   | true     || 404
        true    | false    || 304
        true    | true     || 200
    }

    @Unroll
    def "rank"()
    {
        given:
        def leaderboard = Leaderboard.build(name: 'Test', id: 'test')
        Leaderboard.list().size() == 1
        controller.leaderboardService = Mock(LeaderboardService)
        controller.leaderboardService.rank(_) >>> [ Long.valueOf('558877') ]

        and:
        controller.params.leaderboardId = sendId ? leaderboard.id : null

        when:
        controller.rank()

        then:
        response.status == respStatus

        and:
        if (respStatus == 200)
        {
            response.json.ranked == 558877
        }

        where:
        sendId  || respStatus
        false   || 404
        true    || 200
    }

    @Unroll
    def "top10"()
    {
        given:
        def leaderboard = Leaderboard.build(name: 'Test', id: 'test')

        and:
        controller.params.id = sendId ? leaderboard.id : null

        when:
        controller.top10()

        then:
        response.status == respStatus
        if (sendId)
        {
            response.redirectedUrl == "/leaderboards/$leaderboard.id/entries/ranking?start=0&end=10"
        }

        where:
        sendId  || respStatus
        false   || 404
        true    || 200
    }

    @Unroll
    def "top100"()
    {
        given:
        def leaderboard = Leaderboard.build(name: 'Test', id: 'test')

        and:
        controller.params.id = sendId ? leaderboard.id : null

        when:
        controller.top100()

        then:
        response.status == respStatus
        if (sendId)
        {
            response.redirectedUrl == "/leaderboards/$leaderboard.id/entries/ranking?start=0&end=100"
        }

        where:
        sendId  || respStatus
        false   || 404
        true    || 200
    }

    @Unroll
    def "relative"()
    {
        given:
        def leaderboard = Leaderboard.build(name: 'Test', id: 'test')

        and:
        controller.params.id = sendId ? leaderboard.id : null
        controller.params.start = sendStart ? start : null
        controller.params.end = sendEnd ? end : null

        when:
        controller.relative()

        then:
        response.status == respStatus
        if (response.status == 302)
        {
            response.redirectedUrl == "/leaderboards/$leaderboard.id/entries/ranking?start=$start&end=$end"
        }

        where:
        sendId  |sendStart  |start |sendEnd |end     || respStatus
        false   |false      |10    |false   |20      || 404
        true    |true       |10    |false   |20      || 404
        true    |false      |10    |true    |20      || 404
        true    |true       |10    |true    |20      || 200
        true    |true       |20    |true    |10      || 400
        true    |true       |null  |true    |null    || 404
    }
}
