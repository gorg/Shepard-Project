package shepard.frontend.rest.player

import cloud.orbit.actors.Actor
import grails.test.mixin.TestFor
import shepard.backend.service.player.Player as BackendPlayer
import shepard.frontend.rest.Player
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Shared
import spock.lang.Stepwise

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 17/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
@TestFor(Player)
class PlayerTransformSpec extends ShepardHibernateSpec
{
    @Shared BackendPlayer playerOne
    @Shared BackendPlayer playerTwo

    def "Create players in the backend"()
    {
        playerOne = Actor.getReference(BackendPlayer, 'player-one')
        playerOne.getSkill().join().setRating(1850d).join()
        playerOne.getSkill().join().setRatingDeviation(120d).join()
        playerOne.getSkill().join().incrementNumberOfResults(20).join()

        playerTwo = Actor.getReference(BackendPlayer, 'player-two')
        playerTwo.getSkill().join().setRating(1400d).join()
        playerTwo.getSkill().join().setRatingDeviation(157d).join()

        expect:
        playerOne.getCurrentLobby().join() == null
        playerOne.getCurrentGame().join() == null

        playerTwo.getCurrentLobby().join() == null
        playerTwo.getCurrentGame().join() == null

        and:
        Player.getAll().size() == 0     //Backend has 2 players but frontend doesn't know about them
    }

    def "Create a the first player"()
    {
        given:
        def player = new Player()
        player.id = 'player-one'

        when:
        player = player.save(flush: true, failOnError: true)

        then:
        player.rating == 1850d
        player.games == 20

        and:
        Player.getAll().size() == 1
    }


    def "Get the second player"()
    {
        given:
        def p1 = Player.retrieve('player-one')
        def p2 = Player.retrieve('player-two')

        expect:
        p1.rating == 1850d
        p2.rating == 1400d

        and:
        Player.getAll().size() == 2
    }

    def "Get a new player"()
    {
        given:
        def id = 'player-three'
        def p3 = Player.retrieve(id)

        expect:
        p3.rating == 1500

        and:
        Actor.getReference(BackendPlayer,id).getSkill().join().getRating().join() == p3.rating

        when:
        Actor.getReference(BackendPlayer,id).getSkill().join().setRating(1800d).join()

        then:
        p3.rating == 1500

        and:
        Player.retrieve(id).rating == 1800d
    }

}
