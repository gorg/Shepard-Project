package shepard.frontend.rest.achievement

import grails.test.mixin.TestFor
import shepard.backend.service.achievement.details.AchievementType
import shepard.frontend.rest.Achievement
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Achievement)
class AchievementSpec extends Specification {

    @Unroll
    void "Validates Achievement's instance that must be valid: #valid, with params: #params"()
    {
        given:
        Achievement achievement = new Achievement(params)

        expect:
        achievement.validate() == valid

        and:
        if (valid) {
            assert achievement.save()
            params.each { String key, String value -> assert achievement."$key" == value as String}
        }

        where:
        params                                                                                  ||valid
        [:]                                                                                     ||false
        [name: null]                                                                            ||false
        [name: '  ']                                                                            ||false
        [name: 'test']                                                                          ||true
        [name: 'test', id: 'test']                                                              ||true
        [name: 'test', id: 'test', description: 'Test']                                         ||true
        [name: 'test', id: 'test', description: 'Test', icon: "some-place"]                     ||true
    }

    void "Test empty achievement creation"()
    {
        when:
        def achievement = new Achievement().save()
        achievement.getId()

        then:
        thrown(NullPointerException)
    }

    void "Validates Achievement's instance based on the name parameter"()
    {
        expect:
        !new Achievement().validate(['name'])
        !new Achievement().validate()
        !new Achievement(name: null).validate(['name'])
        !new Achievement(name: null).validate()
        !new Achievement(name: "").validate(['name'])
        !new Achievement(name: "").validate()
        !new Achievement(id: null).validate()
    }

    @Unroll
    void "Validates Achievement's ID generation #realId with #name"()
    {
        given:
        Achievement achievement = new Achievement(id: id, name: name )

        expect:
        achievement.validate() == valid

        and:
        if (valid)
        {
            achievement.save().id == realId
        }

        where:
        id          | name                  | valid         | realId
        ""          | "Test achievement 2"  | true          | "test-achievement-2"
        null        | "Test achievement 1"  | true          | "test-achievement-1"
        "random"    | "Random Achievement"  | true          | "random-achievement"
        "new-player"| "New Player"          | true          | "new-player"
    }

    @Unroll
    void "Validates Achievement's type #persistedType, with params: #type"()
    {
        given:
        def achievement =  new Achievement(type: type)

        expect:
        achievement.validate(['type']) == shouldBeValid

        and:
        if (shouldBeValid)
        {
            new Achievement(name: "random", type: type).save().type == persistedType
        }


        where:
        type                     | shouldBeValid | persistedType
        AchievementType.HIDDEN   | true          | AchievementType.HIDDEN
        AchievementType.REVEALED | true          | AchievementType.REVEALED
        ""                       | true          | AchievementType.REVEALED
        null                     | true          | AchievementType.REVEALED
    }
}
