package shepard.frontend.rest.leaderboard.entry

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.frontend.rest.Leaderboard
import shepard.frontend.rest.LeaderboardEntry
import shepard.frontend.rest.Player
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 09/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Leaderboard, Player])
@TestFor(LeaderboardEntry)
class LeaderboardEntrySpec extends Specification
{
    @Unroll
    def "LeaderboardEntry instance must be #valid with params: #params"()
    {
        given:
        Leaderboard leaderboard = Leaderboard.build(name: 'Test', id: 'test')
        Player player = Player.build(id: 'player')
        LeaderboardEntry entry = new LeaderboardEntry(params)

        expect:
        entry.validate() == valid

        and:
        if(valid)
        {
            def saved = entry.save()
            saved.leaderboard == leaderboard
            saved.player == player
            saved.score == 0        // Can't bind score
            saved.rank == 0
            saved.delta == 0
        }

        where:
        params                                                                                      || valid
        [:]                                                                                         || false
        [leaderboard: 'test' ]                                                                      || false
        [player:'player']                                                                           || false
        [leaderboard:'test', player:'player']                                                       || true
        [leaderboard:'test', player:'player', score: '50']                                          || true
        [leaderboard:'test', player:'player', score: '50', delta: '2']                              || true
        [leaderboard:'test', player:'player', score: '50', delta: '2', rank: '14']                  || true
        [score: '50', delta: '2', rank: '14']                                                       || false
    }
}
