package shepard.frontend.rest.room.lobby

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.mock.web.MockHttpServletRequest
import shepard.frontend.rest.Lobby
import shepard.frontend.rest.LobbyService
import shepard.frontend.rest.Player
import shepard.frontend.rest.Room
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Shared
import spock.lang.Stepwise

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 18/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Room,Player, Lobby])
@TestFor(LobbyService)
@Stepwise
class LobbyServiceSpec extends ShepardHibernateSpec
{
    @Shared Room room

    def setup()
    {
        service.transactionManager = transactionManager
        room = new Room(name: 'test', teams: 2, players: 4).save(flush:true)
    }

    def "get entry"()
    {
        given:
        def player = Player.build(id: 'random-player')
        def map = new GrailsParameterMap([:], new MockHttpServletRequest())

        when:
        service.setEntry(room, player, map)

        then:
        thrown(IllegalArgumentException)

    }

    def "update entry"()
    {
        given:
        def player = Player.build(id: 'random-player')
        def lobby = Lobby.build(players: [player], room: room, id: 'random')
        def map = new GrailsParameterMap([:], new MockHttpServletRequest())

        when:
        service.updateEntry(lobby, map)

        then:
        thrown(IllegalArgumentException)
    }

    def "show team"()
    {
        given:
        def player = Player.build(id: 'random-player')
        def back = room.getActor().getEntry(player.getActor()).join()
        def lobby = Lobby.retrieve(back,room)

        when:
        def res = service.showTeam(lobby)

        then:
        res['lobby'] == lobby
        res[0] == []
        res[1] == []
    }

    def "join team"()
    {
        given:
        def player = Player.build(id: 'random-player')
        def back = room.getActor().getEntry(player.getActor()).join()
        def lobby = Lobby.retrieve(back,room)

        when:
        def res = service.joinTeam(lobby,player,0)

        then:
        res['lobby'] == lobby
        res[0] == [player]
        res[1] == []
    }

    def "leave team"()
    {
        given:
        def player = Player.build(id: 'random-player')
        def back = room.getActor().getEntry(player.getActor()).join()
        def lobby = Lobby.retrieve(back,room)

        when:
        def res = service.leaveTeam(lobby,player)

        then:
        res['lobby'] == lobby
        res[0] == []
        res[1] == []
    }

    def "start game"()
    {
        given:
        def player = Player.build(id: 'random-player')
        def back = room.getActor().getEntry(player.getActor()).join()
        def lobby = Lobby.retrieve(back,room)

        when:
        def res = service.startGame(lobby)

        then:
        res == lobby.id
        res == lobby.gameId
    }

}
