package shepard.frontend.rest.stat

import cloud.orbit.actors.Actor
import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.backend.service.stats.manager.StatManager
import shepard.frontend.rest.Stat
import shepard.frontend.rest.StatService
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Shared
import spock.lang.Unroll

@Build(Stat)
@TestFor(StatService)
class StatServiceSpec extends ShepardHibernateSpec
{

    def setup()
    {
        service.transactionManager = transactionManager
    }

    def index()
    {
        given:
        12.times {
            Stat.build(name: "Test $it",id: "test-$it", defaultValue: 0, maxValue: 10, minValue: 0, maxChange: 2)
        }

        expect:
        Stat.list().size() == 12

        when:
        def resp = service.index()

        then:
        resp.size() == 12

        and:
        12.times {
            def indx = it
            assert resp[indx].name == "Test $it"
            assert resp[indx].id == "test-$it"
            assert resp[indx].defaultValue == 0
            assert resp[indx].maxValue == 10
            assert resp[indx].minValue == 0
            assert resp[indx].maxChange == 2
            assert resp[indx].incrementOnly == false
        }
    }

    @Unroll
    def show()
    {
        given:
        def  stat = Stat.build(name: "Test stat",id: "test-stat", defaultValue: 0, maxValue: 10, minValue: 0, maxChange: 2)
        def id = passId ? stat.id : null

        when:
        def resp = service.show(id)

        then:
        passId ? resp == stat : resp == null

        where:
        passId << [false, true]
    }

    @Unroll
    def save()
    {
        when:
        def resp = service.save(params)

        then:
        if (!valid) resp == null
        else
        {
            assert resp.validate()
            assert resp.name == params.name
            assert resp.defaultValue == (params.defaultValue ?: 0)
            assert resp.minValue == (params.minValue ?: 0)
            assert resp.maxValue == (params.maxValue ?: BigDecimal.valueOf(Long.MAX_VALUE,4))
            assert resp.maxChange == (params.maxChange ?: 0)
            assert resp.incrementOnly == (params.incrementOnly == null ? false : params.incrementOnly)
        }

        where:
        params                                                                                              ||valid
        [:]                                                                                                 ||false
        [name: null]                                                                                        ||false
        [name: '']                                                                                          ||false
        [id: null]                                                                                          ||false
        [id: '']                                                                                            ||false
        [name: 'test']                                                                                      ||true
        [name: 'test', defaultValue: 0]                                                                     ||true
        [name: 'test', defaultValue: 0, minValue: 0]                                                        ||true
        [name: 'test', defaultValue: 0, minValue: 0, maxValue: 10, maxChange: 2]                            ||true
        [name: 'test', defaultValue: 0, minValue: 0, maxValue: 10, maxChange: 2, incrementOnly: false]      ||true
        [name: 'test', defaultValue: 0, minValue: 0, maxValue: 10, maxChange: 2, incrementOnly: false]      ||true
        [name: 'test', defaultValue: 0, minValue: 0, maxValue: 10, maxChange: 50, incrementOnly: false]     ||false
        [name: 'test', defaultValue: 0, minValue: 20, maxValue: 10, maxChange: 2, incrementOnly: false]     ||false
        [name: 'test', defaultValue: -50, minValue: 0, maxValue: 10, maxChange: 2, incrementOnly: false]    ||false
    }

    
    @Unroll
    def update()
    {
        given:
        def  stat = Stat.build(name: "Test stat",id: "test-stat", defaultValue: 0, maxValue: 10, minValue: 0, maxChange: 2)     
        def id = sendId ? stat.id : null
        def map = sendMap ? params : null

        when:
        def res = service.update(id, map)

        then:
        if (sendId && sendMap && params)
        {
            assert res.validate() == valid
            if(valid) assert res == stat
        }
        else res == null

        where:
        sendId  |sendMap    |params                 ||valid
        false   |false      |null                   ||false
        false   |true       |null                   ||false
        true    |false      |null                   ||false
        true    |true       |null                   ||false
        true    |true       |[:]                    ||false
        true    |true       |[name: null]           ||false
        true    |true       |[name: '']             ||false
        true    |true       |[name: 'asdf']         ||true
        true    |true       |[minValue: '']         ||true
        true    |true       |[minValue: null]       ||true
        true    |true       |[minValue: '50']       ||false
        true    |true       |[minValue: '-50']      ||true
        true    |true       |[maxValue: '']         ||true
        true    |true       |[maxValue: null]       ||true
        true    |true       |[maxValue: '-80']      ||false
        true    |true       |[maxValue: '100']      ||true
        true    |true       |[defaultValue: '']     ||true
        true    |true       |[defaultValue: null]   ||true
        true    |true       |[defaultValue: '-100'] ||false
        true    |true       |[defaultValue: '1000'] ||false
        true    |true       |[defaultValue: '5']    ||true

    }

    @Unroll
    def delete()
    {
        given:
        def  stat = Stat.build(name: "Test stat",id: "test-stat", defaultValue: 0, maxValue: 10, minValue: 0, maxChange: 2)
        def id = sendId ? stat.id : null

        expect:
        Stat.list().size() == 1

        when:
        def res = service.delete(id)

        then:
        res == isDeleted

        where:
        sendId  ||isDeleted
        false   ||false
        true    ||true
    }


    def enable()
    {
        given:
        def enabled = Stat.build(name: "Test stat",id: "test-stat", defaultValue: 0, maxValue: 10, minValue: 0, maxChange: 2)
        def disabled = Stat.build(name: "Test stat 2",id: "test-stat-2", defaultValue: 0, maxValue: 10, minValue: 0, maxChange: 2)

        expect:
        Stat.list().size() == 2

        when:
        Actor.getReference(StatManager).disableService(disabled.actor).join()
        def res = service.enable(disabled.id)

        then:
        res == true

        when:
        res = service.enable(enabled.id)

        then:
        res == false
    }


    def disable()
    {
        given:
        def enabled = Stat.build(name: "Test stat",id: "test-stat", defaultValue: 0, maxValue: 10, minValue: 0, maxChange: 2)
        def disabled = Stat.build(name: "Test stat 2",id: "test-stat-2", defaultValue: 0, maxValue: 10, minValue: 0, maxChange: 2)

        expect:
        Stat.list().size() == 2

        when:
        Actor.getReference(StatManager).enableService(enabled.actor).join()
        def res = service.disable(enabled.id)

        then:
        res == true

        when:
        res = service.disable(enabled.id)

        then:
        res == false
    }


    def getEnabled()
    {
        given:
        def stat = Stat.build(name: "Test stat 3",id: "test-stat-3", defaultValue: 0, maxValue: 10, minValue: 0, maxChange: 2)

        expect:
        Stat.list().size() == 1

        when:
        def res = service.getEnabled()

        then:
        res.size() == 1

        and:
        res.first() == stat
    }

    def getDisabled()
    {
        given:
        def stat = Stat.build(name: "Test stat 4",id: "test-stat-4", defaultValue: 0, maxValue: 10, minValue: 0, maxChange: 2)

        expect:
        Stat.list().size() == 1

        when:
        service.disable(stat.id)
        def res = service.getDisabled()

        then:
        res.size() == 1

        and:
        res.first() == stat
    }
}
