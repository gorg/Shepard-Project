package shepard.frontend.rest.room

import grails.test.mixin.TestFor
import shepard.frontend.rest.Room
import spock.lang.Specification
import spock.lang.Unroll

@TestFor(Room)
class RoomSpec extends Specification
{
    @Unroll
    void "Validates Room's instance that must be valid: #valid, with params: #params"()
    {
        given:
        Room room = new Room(params)

        expect:
        room.validate() == valid

        and:
        if (valid) {
            assert room.save()
            params.each { String key, Object value -> assert room."$key" == value }
        }

        where:
        params                                                                                  ||valid
        [:]                                                                                     ||false
        [name: null]                                                                            ||false
        [name: '  ']                                                                            ||false
        [name: 'test']                                                                          ||true
        [name: 'test', id: 'test']                                                              ||true
        [name: 'test', id: 'test', teams: 1]                                                    ||false
        [name: 'test', id: 'test', teams: 9]                                                    ||false
        [name: 'test', id: 'test', teams: 2]                                                    ||true
        [name: 'test', id: 'test', teams: 4, players: 65]                                       ||false
        [name: 'test', id: 'test', teams: 4, players: 4]                                        ||true
        [name: 'test', id: 'test', teams: 4, players: 4, ranked: false]                         ||true
        [name: 'test', id: 'test', teams: 4, players: 4, ranked: true]                          ||true
        [name: 'test', id: 'test', teams: 4, players: 4, ranked: true, overlap: 0.51]           ||true
        [name: 'test', id: 'test', teams: 4, players: 4, ranked: true, overlap: 0.1]            ||false
        [name: 'test', id: 'test', teams: 4, players: 4, ranked: true, overlap: 1.25]           ||false
    }

    def "Test empty lobby creation"()
    {
        when:
        def lobby = new Room().save()
        lobby.getId()

        then:
        thrown(NullPointerException)
    }

    def "Test lobby creation with empty/null name"()
    {
        expect:
        !new Room().validate(['name'])
        !new Room().validate()
        !new Room(name: null).validate(['name'])
        !new Room(name: null).validate()
        !new Room(name: "").validate(['name'])
        !new Room(name: "").validate()
        !new Room(id: null).validate()

    }

    @Unroll("A lobby with #players players should be valid: #shouldBeValid")
    "Lobby player assignment"()
    {
        expect:
        new Room(players: players).validate(['players']) == shouldBeValid

        where:
        players             || shouldBeValid
        0                   || true
        1                   || true
        2                   || true
        null                || true
        ""                  || true
        64                  || true
        -1                  || false
        Integer.MIN_VALUE   || false
        Integer.MAX_VALUE   || false
    }

    def "Lobby player limits"()
    {
        when:
        new Room(name: "random", players: Integer.MAX_VALUE).save().players

        then:
        thrown(NullPointerException)

        when:
        new Room(name: "random", players: Integer.MIN_VALUE).save().players

        then:
        thrown(NullPointerException)


        when:
        new Room(name: "random", players: -1).save().players

        then:
        thrown(NullPointerException)


        when:
        new Room(name: "random", players: 65).save().players

        then:
        thrown(NullPointerException)
    }

    @Unroll("A lobby with #team teams should be valid: #shouldBeValid")
    "Lobby team assignment"()
    {
        expect:
        new Room(teams: team).validate(['teams']) == shouldBeValid

        where:
        team                || shouldBeValid
        0                   || true
        1                   || false
        2                   || true
        null                || true
        ""                  || true
        64                  || false
        -1                  || false
        Integer.MIN_VALUE   || false
        Integer.MAX_VALUE   || false
    }

    def "Lobby team limits"()
    {
        when:
        new Room(name: "random", teams: Integer.MAX_VALUE).save().teams

        then:
        thrown(NullPointerException)

        when:
        new Room(name: "random", teams: Integer.MIN_VALUE).save().teams

        then:
        thrown(NullPointerException)


        when:
        new Room(name: "random", teams: 1).save().teams

        then:
        thrown(NullPointerException)
    }

    def "Lobby overlap limits 0.25 < overlap < 1"()
    {

        when:
        new Room(name: "random", overlap: 0.01).save().overlap

        then:
        thrown(NullPointerException)

        when:
        new Room(name: "random", overlap: 1).save().overlap

        then:
        thrown(NullPointerException)

        when:
        new Room(name: "random", overlap: 0.99).save().overlap

        then:
        noExceptionThrown()

        when:
        new Room(name: "random", overlap: 0.25).save().overlap

        then:
        thrown(NullPointerException)

        when:
        new Room(name: "random", overlap: 0.26).save().overlap

        then:
        noExceptionThrown()

        expect:
        new Room(name: "random", overlap: 0).save().overlap == 0.75d
    }
}
