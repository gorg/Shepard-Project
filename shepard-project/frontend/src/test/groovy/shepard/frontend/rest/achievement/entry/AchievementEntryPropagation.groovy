package shepard.frontend.rest.achievement.entry

import cloud.orbit.actors.Actor
import grails.test.mixin.TestFor
import shepard.backend.service.achievement.manager.AchievementManager
import shepard.frontend.rest.Achievement
import shepard.frontend.rest.AchievementEntry
import shepard.frontend.rest.Player
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Stepwise

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 11/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
@TestFor(AchievementEntry)
class AchievementEntryPropagation extends ShepardHibernateSpec
{
    def "From backend to frontend test"()
    {
        given: 'a player and an achievement'
        Achievement achievement = new Achievement()
        achievement.name = "Test Achievement"
        achievement.save(flush: true, insert: true)
        assert achievement.id == achievement.getActor().identity

        Player playerOne = new Player()
        playerOne.id = 'random-player'
        playerOne.save(flush: true, insert: true)
        assert playerOne.id == playerOne.getActor().identity


        expect: 'the achievement service is running in the backend'
        Actor.getReference(AchievementManager)
                .getAllEnabledServices().join().size() == 1
        Actor.getReference(AchievementManager)
                .getAllEnabledServices().join().collect{ it.serviceDetails.join() }.name.contains(achievement.name)

        when: 'the entry is completed'
        def backEntry = achievement.getActor().getEntry(playerOne.getActor()).join()
        backEntry.complete().join()

        and: 'and a instance is created'
        def entry = new AchievementEntry(achievement: achievement, player: playerOne)
        entry.save(flush: true, insert: true)

        then: 'the instance will be completed'
        entry.achievement == achievement
        entry.player == playerOne
        entry.completed == true
        entry.when != null
        entry.when == backEntry.whenWasCompleted().join()
    }

    def "From frontend to backend test"()
    {
        given: 'a player and an achievement'
        Achievement achievement = new Achievement()
        achievement.name = "Test Achievement 2"
        achievement.save(flush: true, insert: true)
        assert achievement.id == achievement.getActor().identity

        Player playerOne = new Player()
        playerOne.id = 'random-player-2'
        playerOne.save(flush: true, insert: true)
        assert playerOne.id == playerOne.getActor().identity

        expect: 'the achievement service is running in the backend'
        Actor.getReference(AchievementManager)
                .getAllEnabledServices().join().size() == 2
        Actor.getReference(AchievementManager)
                .getAllEnabledServices().join().collect{ it.serviceDetails.join() }.name.contains(achievement.name)

        when:
        def entry = new AchievementEntry(achievement: achievement, player: playerOne)
        entry.save(flush: true, insert: true)
        def back = Actor.getReference(shepard.backend.service.achievement.entry.Achievement, entry.id)

        then:
        back.identity == entry.id
        back.player.join().identity == entry.player.id
        back.isCompleted().join() == entry.completed
        back.whenWasCompleted().join() == entry.when
    }
}
