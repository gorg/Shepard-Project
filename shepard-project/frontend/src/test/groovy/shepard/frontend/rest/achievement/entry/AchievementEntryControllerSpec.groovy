package shepard.frontend.rest.achievement.entry

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import org.springframework.http.HttpStatus
import shepard.frontend.rest.*
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Unroll

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 08/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Achievement,AchievementEntry,Player])
@TestFor(AchievementEntryController)
class AchievementEntryControllerSpec extends ShepardHibernateSpec
{
    def setup()
    {
        defineBeans {
            achievementEntryService(AchievementEntryService)
        }
    }
           
    def "index"()
    {
        given:
        def achievement = Achievement.build(name: 'Test Achievement', id: 'test-achievement')
        12.times {
            def player = Player.build(id: "player-$it")
            def entry = AchievementEntry.build(player: player, achievement: achievement, id: "$achievement.id:$player.id")
            assert entry.achievement == achievement
        }
        assert AchievementEntry.findAllByAchievement(achievement).size() == 12

        and:
        controller.params.achievementId = achievement.id

        when:
        controller.index()

        then:
        response.status == HttpStatus.OK.value()
        12.times {
            response.json[it].player.id == "player-$it"
            response.json[it].achievement.id == achievement.id
            response.json[it].completed == false
        }
    }
                     
    @Unroll
    def "show"()
    {
        given:
        def achievement = Achievement.build(name: 'Test Achievement', id: 'test-achievement')
        def player = Player.build(id: "player")
        def entry = AchievementEntry.build(player: player, achievement: achievement, id: "$achievement.id:$player.id")
        assert entry.player == player
        assert entry.achievement == achievement
        assert AchievementEntry.findByAchievementAndPlayer(achievement,player) == entry

        and:
        controller.params.achievementId = sendAchievemntId ? achievement.id : null
        controller.params.playerId = sendPlayerId ? player.id : null

        when:
        controller.show()

        then:
        if(sendAchievemntId && sendPlayerId)
        {
            response.status == resStatus
            response.json.player.id == player.id
            response.json.achievement.id == achievement.id
            response.json.id == entry.id
            response.json.completed == false
        }
        else
        {
            response.status == resStatus
        }

        where:
        sendAchievemntId | sendPlayerId || resStatus
        false            | false        || 404
        false            | true         || 404
        true             | false        || 404
        true             | true         || 200
    }

            
    @Unroll
    def "save/update"()
    {
        given:
        def achievement = Achievement.build(name: 'Test Achievement', id: 'test-achievement')
        def player = Player.build(id: "player")

        and:
        request.method = 'POST'
        controller.params.achievementId = sendAchievemntId ? achievement.id : null
        controller.params.playerId = sendPlayerId ? player.id : null

        when:
        controller.update()

        then:
        response.status == resStatus
        if (resStatus == 200)
        {
            response.json.player.id == player.id
            response.json.achievement.id == achievement.id
            response.json.completed == true
        }

        where:
        sendAchievemntId | sendPlayerId || resStatus
        false            | false        || 404
        false            | true         || 404
        true             | false        || 404
        true             | true         || 200
    }
   
}
