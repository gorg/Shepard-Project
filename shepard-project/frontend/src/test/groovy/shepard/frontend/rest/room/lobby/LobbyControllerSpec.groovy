package shepard.frontend.rest.room.lobby

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.frontend.rest.*
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Shared
import spock.lang.Unroll

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 18/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Room, Player])
@TestFor(LobbyController)
class LobbyControllerSpec extends ShepardHibernateSpec
{
    @Shared Room room

    def setup()
    {
        controller.transactionManager = transactionManager
        defineBeans {
            lobbyService(LobbyService)
            gameService(GameService)
        }
        room = new Room(name: 'test', teams: 2, players: 4).save(flush:true)
    }

    def "index"()
    {
        given:
        12.times {
            def player = Player.build(id: "player-$it")
            def back = room.getActor().getEntry(player.getActor()).join()
            def lobby = Lobby.retrieve(back,room)
            assert lobby.id == back.identity
        }
        assert Lobby.findAllByRoom(room).size() == 3
        assert Room.get('test').is(room)

        and:
        controller.params.roomId = room.id

        when:
        controller.index()

        then:
        response.status == 200

        and:
        12.times {
            def index = (it)/4 as Integer
            assert response.json[index].room.id == room.id
            response.json[index].players.id.contains( "player-$it" )
        }
    }

    @Unroll
    def "show"()
    {
        given:
        def player = Player.build(id: "player")
        def back = room.getActor().getEntry(player.getActor()).join()
        def lobby = Lobby.retrieve(back,room)
        Lobby.list().size() == 1
        assert lobby.id == back.identity

        and:
        controller.params.roomId = sendRoomId ? room.id : null
        controller.params.playerId = sendPlayerId ? player.id : null

        when:
        controller.show()

        then:
        response.status == respStatus

        and:
        if(respStatus == 200)
        {
            response.json.room.id == room.id
            response.json.players.id.contains(player.id)
        }


        where:
        sendRoomId  |sendPlayerId   || respStatus
        false       |false          || 404
        true        |false          || 404
        false       |true           || 404
        true        |true           || 200
    }

    @Unroll
    def "save"()
    {
        //Can't create lobbies or update them from the frontend

        given:
        controller.request.method = method

        when:
        controller.save()

        then:
        response.status == respStatus

        where:
        method  |respStatus
        'GET'   |405
        'DELETE'|405
        'PUT'   |405
        'POST'  |400
    }

    @Unroll
    def "update"()
    {
        //Can't create lobbies or update them from the frontend

        given:
        controller.request.method = method

        when:
        controller.update()

        then:
        response.status == respStatus

        where:
        method  |respStatus
        'GET'   |405
        'DELETE'|405
        'POST'  |400
        'PUT'   |400
    }

    @Unroll
    def "join team"()
    {
        given:
        def player = Player.build(id: "random-player")
        def back = room.getActor().getEntry(player.getActor()).join()
        def lobby = Lobby.retrieve(back,room)
        assert lobby.id == back.identity

        and:
        controller.params.id = sendLobbyId ? lobby.id : null
        controller.params.roomId = sendRoomId ? room.id : null
        controller.params.playerId = sendPlayerId ? player.id : null
        controller.params.team = sendTeam ? 0 : null

        when:
        controller.joinTeam()

        then:
        response.status == respStatus

        and:
        if (respStatus == 200)
        {
            assert response.json.lobby.id == lobby.id
            assert response.json."0".first().id == player.id
        }

        where:
        sendRoomId  |sendPlayerId   |sendLobbyId    |sendTeam || respStatus
        false       |false          |false          |false    || 404
        false       |false          |false          |true     || 404
        false       |false          |true           |false    || 404
        false       |false          |true           |true     || 404
        false       |true           |false          |false    || 404
        false       |true           |false          |true     || 404
        false       |true           |true           |false    || 400
        false       |true           |true           |true     || 200
        true        |false          |false          |false    || 404
        true        |false          |false          |true     || 404
        true        |false          |true           |false    || 404
        true        |false          |true           |true     || 404
        true        |true           |false          |false    || 400
        true        |true           |false          |true     || 200
        true        |true           |true           |false    || 400
        true        |true           |true           |true     || 200
    }

    @Unroll
    def "get teams"()
    {
        given:
        def player = Player.build(id: "random-player")
        def back = room.getActor().getEntry(player.getActor()).join()
        def lobby = Lobby.retrieve(back,room)
        assert lobby.id == back.identity

        and:
        controller.params.id = sendLobbyId ? lobby.id : null
        controller.params.roomId = sendRoomId ? room.id : null
        controller.params.playerId = sendPlayerId ? player.id : null

        when:
        controller.showTeam()

        then:
        response.status == respStatus

        and:
        if (respStatus == 200)
        {
            assert response.json.lobby.id == lobby.id
            assert response.json."0".first().id == player.id
        }

        where:
        sendRoomId  |sendPlayerId   |sendLobbyId    || respStatus
        false       |false          |false          || 404
        true        |false          |false          || 404
        false       |true           |false          || 404
        true        |true           |false          || 200
        false       |false          |true           || 200
        true        |false          |true           || 200
        false       |true           |true           || 200
        true        |true           |true           || 200
    }

    @Unroll
    def "join other teams"()
    {
        given:
        def player = Player.build(id: "random-player")
        def back = room.getActor().getEntry(player.getActor()).join()
        def lobby = Lobby.retrieve(back,room)
        assert lobby.id == back.identity

        and:
        controller.params.id = lobby.id
        controller.params.playerId = player.id
        controller.params.team = team

        when:
        controller.joinTeam()

        then:
        response.status == respStatus

        and:
        if (respStatus == 200)
        {
            assert response.json.lobby.id == lobby.id
            switch (team) {
                case -1:
                    assert response.json."0".first().id == player.id
                    break
                case 0..1:
                    assert response.json."$team".first().id == player.id
                    break
                case 2..3:
                    assert response.json."1".first().id == player.id
            }
        }

        where:
        team     || respStatus
        -1       || 200
        0        || 200
        1        || 200
        2        || 200
        3        || 200
        ''       || 400
        null     || 400
    }

    @Unroll
    def "leave team"()
    {
        given:
        def player = Player.build(id: "random-player")
        def back = room.getActor().getEntry(player.getActor()).join()
        def lobby = Lobby.retrieve(back,room)
        assert lobby.id == back.identity

        and:
        controller.params.id = lobby.id
        controller.params.playerId = player.id

        when:
        controller.leaveTeam()

        then:
        response.status == 200

        and:
        response.json.lobby.id == lobby.id
        assert response.json."0".isEmpty()
        assert response.json."1".isEmpty()
    }

    def "start game"()
    {
        given:
        def player = Player.build(id: "random-player")
        def back = room.getActor().getEntry(player.getActor()).join()
        def lobby = Lobby.retrieve(back,room)
        assert lobby.id == back.identity
        assert lobby.gameId == null

        and:
        controller.params.id = lobby.id

        when:
        controller.startGame()

        then:
        response.status == 200
        response.json.finished == false
    }

    def "finish game"()
    {
        given:
        def player = Player.build(id: "random-player")
        def back = room.getActor().getEntry(player.getActor()).join()
        def lobby = Lobby.retrieve(back,room)
        assert lobby.id == back.identity

        and:
        controller.params.id = lobby.id

        when:
        controller.finishGame()

        then:
        response.status == 204
        response.json.finished == true
    }
}
