package shepard.frontend.rest.leaderboard.entry

import cloud.orbit.actors.Actor
import grails.test.mixin.TestFor
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.backend.service.leaderboard.ranking.Rank
import shepard.frontend.rest.Leaderboard
import shepard.frontend.rest.LeaderboardEntry
import shepard.frontend.rest.Player
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Stepwise

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 11/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
@TestFor(LeaderboardEntry)
class LeaderboardEntryPropagation extends ShepardHibernateSpec
{
    def "From backend to frontend test"()
    {
        given: 'a player and an leaderboard'
        Leaderboard leaderboard = new Leaderboard()
        leaderboard.name = "Test Leaderboard"
        leaderboard.save(flush: true, insert: true)
        assert leaderboard.id == leaderboard.getActor().identity

        Player playerOne = new Player()
        playerOne.id = 'random-player'
        playerOne.save(flush: true, insert: true)
        assert playerOne.id == playerOne.getActor().identity


        expect: 'the leaderboard service is running in the backend'
        Actor.getReference(LeaderboardManager)
                .getAllEnabledServices().join().size() == 1
        Actor.getReference(LeaderboardManager)
                .getAllEnabledServices().join().collect{ it.serviceDetails.join() }.name.contains(leaderboard.name)

        when: 'the entry is completed'
        def backEntry = leaderboard.getActor().getEntry(playerOne.getActor()).join()
        backEntry.updateScore(40).join()

        and: 'and a instance is created'
        def entry = new LeaderboardEntry(leaderboard: leaderboard, player: playerOne)
        entry.save(flush: true, insert: true)

        then: 'the instance will be completed'
        entry.leaderboard == leaderboard
        entry.player == playerOne
        entry.score == 40
    }

    def "From frontend to backend test"()
    {
        given: 'a player and an leaderboard'
        Leaderboard leaderboard = new Leaderboard()
        leaderboard.name = "Test Leaderboard 2"
        leaderboard.save(flush: true, insert: true)
        assert leaderboard.id == leaderboard.getActor().identity

        Player playerOne = new Player()
        playerOne.id = 'random-player-2'
        playerOne.save(flush: true, insert: true)
        assert playerOne.id == playerOne.getActor().identity

        expect: 'the leaderboard service is running in the backend'
        Actor.getReference(LeaderboardManager)
                .getAllEnabledServices().join().size() == 2
        Actor.getReference(LeaderboardManager)
                .getAllEnabledServices().join().collect{ it.serviceDetails.join() }.name.contains(leaderboard.name)

        when:
        def entry = new LeaderboardEntry(leaderboard: leaderboard, player: playerOne)
        entry.score = 20
        entry.save(flush: true, insert: true)
        def back = Actor.getReference(Rank, entry.id)

        then:
        back.identity == entry.id
        back.player.join().identity == entry.player.id
        back.score.join().rawScore.join() == entry.score
    }

}
