package shepard.frontend.rest.room.lobby

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.backend.service.matchmaking.entry.Lobby as BackendEntry
import shepard.frontend.rest.Lobby
import shepard.frontend.rest.Player
import shepard.frontend.rest.Room
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Shared
import spock.lang.Stepwise

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 24/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
@Build([Room, Player])
@TestFor(Lobby)
class LobbyFetchSpec extends ShepardHibernateSpec
{
    @Shared BackendEntry back
    @Shared Room room
    @Shared Player player

    def setup()
    {
        room = Room.build(name: 'Test Room', id: 'test-room', overlap: 0.75, teams: 2, players: 2, ranked: true)
        player = Player.build(id: 'random-player')
    }

    def "setup an entry in the backend"()
    {
        when:
        back = room.actor.setupService(room.generateDetailsFromDomain()).join().getEntry(player.actor).join()

        then:
        back.getLobbyPLayers().join().contains(player.actor)
        back.service.join() == room.actor
    }

    def "map a backend entry to the frontend database"()
    {
        given:
        def params = back.collectEntry().join()

        when:
        def entry = new Lobby(params)
        entry.id =  params.id

        then:
        entry.validate()
        !entry.hasErrors()

        and:
        entry.room == room
        entry.players.contains(player)
    }
}
