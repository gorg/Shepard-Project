package shepard.frontend.rest.game

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.backend.service.game.Game
import shepard.frontend.rest.GameService
import shepard.frontend.rest.Player
import shepard.frontend.rest.Room
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Shared

@Build([Player])
@TestFor(GameService)
class GameServiceSpec extends ShepardHibernateSpec
{
    @Shared Game game
    @Shared Room room

    def "setup players, room, lobby and start game"()
    {
        room = new Room(name: 'test', teams: 2, players: 2).save(flush:true)
        def player1 = Player.build(id: 'player-1')
        def player2 = Player.build(id: 'player-2')
        def lobby = room.getActor().getEntry(player1.getActor()).join()
        lobby.selectTeam(player1.actor,0).join()
        lobby = room.getActor().getEntry(player2.getActor()).join()
        lobby.selectTeam(player2.actor,1).join()
        game = room.actor.getLobbies().join().first().startGame().join()

        expect:
        !game.isFinished().join()
        game.lobby.join().getLobbyPLayers().join().collect { it.identity }.containsAll([player1.id, player2.id])
    }

    def "show game"()
    {
        when:
        def show = service.showGame(game.identity)

        then:
        !show.finished
    }

    def "finish game"()
    {
        when:
        def show = service.finishGame(game.identity)

        then:
        show.finished
    }

}
