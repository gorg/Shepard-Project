package shepard.frontend.rest.achievement.entry

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import shepard.backend.service.achievement.entry.Achievement as BackendAchievement
import shepard.frontend.rest.Achievement
import shepard.frontend.rest.AchievementEntry
import shepard.frontend.rest.Player
import shepard.frontend.test.ShepardHibernateSpec
import spock.lang.Shared

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 24/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Build([Achievement,Player])
@TestFor(AchievementEntry)
class AchievementEntryFetchSpec extends ShepardHibernateSpec
{
    @Shared BackendAchievement back
    @Shared Achievement achievement
    @Shared Player player

    def setup()
    {
        achievement = Achievement.build(name: 'Test Achievement', id: 'test-achievement')
        player = Player.build(id: 'random-player')
    }

    def "setup an entry in the backend"()
    {
        when:
        back = achievement.actor.getEntry(player.actor).join()

        then:
        back.player.join() == player.actor
        back.service.join() == achievement.actor
    }

    def "map a backend entry to the frontend database"()
    {
        given:
        def params = back.collectEntry().join()

        when:
        def entry = new AchievementEntry(params)

        then:
        entry.validate()
        !entry.hasErrors()

        and:
        entry.achievement == achievement
        entry.player == player
        entry.completed == false
        entry.when == null
    }
}
