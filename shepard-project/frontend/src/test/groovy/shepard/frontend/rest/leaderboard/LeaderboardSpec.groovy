package shepard.frontend.rest.leaderboard

import grails.test.mixin.TestFor
import shepard.backend.service.leaderboard.details.LeaderboardSort
import shepard.backend.service.leaderboard.details.LeaderboardType
import shepard.frontend.rest.Leaderboard
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Leaderboard)
class LeaderboardSpec extends Specification {

    @Unroll
    void "Leaderboard instance that must be valid: #valid, with params: #params"()
    {
        given:
        Leaderboard leaderboard = new Leaderboard(params)

        expect:
        leaderboard.validate() == valid

        and:
        if (valid) {
            assert leaderboard.save()
            params.each { String key, Object value -> assert leaderboard."$key" == value }
        }

        where:
        params                                                                                  ||valid
        [:]                                                                                     ||false
        [name: null]                                                                            ||false
        [name: '']                                                                              ||false
        [id: null]                                                                              ||false
        [id: '']                                                                                ||false
        [name: 'test']                                                                          ||true
        [name: 'test', id: 'test']                                                              ||true
        [name: 'test', id: 'test', lowerLimit: 10]                                              ||true
        [name: 'test', id: 'test', lowerLimit: 10, upperLimit: 100]                             ||true
        [name: 'test', id: 'test', lowerLimit: 100, upperLimit: 10]                             ||false
        [name: 'test', id: 'test', lowerLimit: 10, upperLimit: 100,
            leaderboardSort: LeaderboardSort.LOW]                                               ||true
        [name: 'test', id: 'test', lowerLimit: 10, upperLimit: 100,
            leaderboardSort: LeaderboardSort.LOW, leaderboardType: LeaderboardType.ONLY_HIGHER] ||true

    }

    @Unroll
    def "Leaderboard instance with type param: #assignedType and validated type: #persistedType"()
    {
        when:
        new Leaderboard(name: "random", leaderboardType: "not my type").save().type

        then:
        thrown(NullPointerException)

        expect:
        new Leaderboard(leaderboardType: assignedType).validate(['type']) == shouldBeValid
        new Leaderboard(name: "random", leaderboardType: assignedType).save().leaderboardType == persistedType


        where:
        assignedType                | shouldBeValid | persistedType
        LeaderboardType.ABSOLUTE    | true          | LeaderboardType.ABSOLUTE
        LeaderboardType.INCREMENTAL | true          | LeaderboardType.INCREMENTAL
        LeaderboardType.ONLY_HIGHER | true          | LeaderboardType.ONLY_HIGHER
        LeaderboardType.ONLY_LOWER  | true          | LeaderboardType.ONLY_LOWER
        ""                          | true          | LeaderboardType.ABSOLUTE
        null                        | true          | LeaderboardType.ABSOLUTE
    }

    @Unroll
    def "Leaderboard instance with sort param: #assignedSort and validated type: #persistedSort"()
    {
        when:
        new Leaderboard(name: "random", leaderboardSort: "not my type").save().type

        then:
        thrown(NullPointerException)

        expect:
        new Leaderboard(leaderboardSort: assignedSort).validate(['sort']) == shouldBeValid
        new Leaderboard(name: "random", leaderboardSort: assignedSort).save().leaderboardSort == persistedSort


        where:
        assignedSort         | shouldBeValid | persistedSort
        LeaderboardSort.HIGH | true          | LeaderboardSort.HIGH
        LeaderboardSort.LOW  | true          | LeaderboardSort.LOW
        ""                   | true          | LeaderboardSort.HIGH
        null                 | true          | LeaderboardSort.HIGH
    }

    def "Leaderboard instance with default limits"()
    {
        when:
        def leaderboard = new Leaderboard(name: "random").save()

        then:
        leaderboard.lowerLimit == 0
        leaderboard.upperLimit == Long.MAX_VALUE
    }

    @Unroll
    def "Leaderboard instance that must be valid: #shouldBeValid, with low: #low and high: #high limits"()
    {
        expect:
        new Leaderboard(lowerLimit: low, upperLimit: high).validate(['lowerLimit', 'upperLimit']) == shouldBeValid

        where:
        low         |high       |shouldBeValid
        5           |-5         |false
        -5          |-10        |false
        0           |0          |true
        0           |500        |true
        -500        |10000      |true
        null        |null       |true
    }
}
