package shepard.connector;

import cloud.orbit.actors.extensions.LifetimeExtension;
import cloud.orbit.exception.UncheckedException;
import groovy.transform.CompileStatic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Component;

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 27/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 *
 * Based on Johno Crawford gist
 * https://gist.github.com/johnou/48d744e1a63396e49e80cdf250ae25c9
 **/

@CompileStatic
@Component
public class ShepardLifeTimeExtension implements LifetimeExtension
{

    @Autowired
    private AutowireCapableBeanFactory autowireCapableBeanFactory;

    @Value("${orbit.actors.autowireMode:" + AutowireCapableBeanFactory.AUTOWIRE_BY_NAME + "}")
    private int autowireMode;

    @Override
    @SuppressWarnings("unchecked")
    public <T> T newInstance(Class<T> concreteClass) {
        try {
            T abstractActor = concreteClass.newInstance();
            autowireCapableBeanFactory.autowireBeanProperties(abstractActor, autowireMode, false);
            return (T) autowireCapableBeanFactory.initializeBean(abstractActor, abstractActor.getClass().getName());
        } catch (Exception ex) {
            throw new UncheckedException(ex);
        }
    }

}
