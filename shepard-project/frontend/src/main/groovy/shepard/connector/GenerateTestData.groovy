package shepard.connector

import cloud.orbit.actors.Actor
import shepard.backend.service.achievement.details.AchievementDetails
import shepard.backend.service.achievement.details.AchievementType
import shepard.backend.service.achievement.manager.AchievementManager
import shepard.backend.service.leaderboard.details.LeaderboardDetails
import shepard.backend.service.leaderboard.details.LeaderboardSort
import shepard.backend.service.leaderboard.details.LeaderboardType
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.details.Rules
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.backend.service.stats.details.StatDetails
import shepard.backend.service.stats.manager.StatManager

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 28/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class GenerateTestData
{
    static genAchievements()
    {
        def manager = Actor.getReference(AchievementManager)

        def achievementOne = new AchievementDetails(
                name: 'Welcome to Shepard',
                type: AchievementType.REVEALED,
                description: 'First of all achievements',
                icon: 'https://cdn3.iconfinder.com/data/icons/life-stories-01/100/Weekly_64x64_copy-06-512.png'
        )
        manager.registerService(achievementOne).join()

        def achievementTwo = new AchievementDetails(
                name: 'First round',
                type: AchievementType.HIDDEN,
                description: 'This one is a hidden achievements',
                icon: 'https://cdn3.iconfinder.com/data/icons/life-stories-01/100/Weekly_64x64_copy-06-512.png'
        )
        manager.registerService(achievementTwo).join()
    }

    static genLeaderboards()
    {
        def manager = Actor.getReference(LeaderboardManager)

        def leaderboardOne = new LeaderboardDetails(
            name: 'Local players leaderboard',
            icon: 'https://cdn3.iconfinder.com/data/icons/life-stories-01/100/Weekly_64x64_copy-06-512.png',
            lowerLimit: 0,
            upperLimit: 1500,
            sort: LeaderboardSort.HIGH,
            type: LeaderboardType.INCREMENTAL
        )
        manager.registerService(leaderboardOne).join()

        def leaderboardTwo = new LeaderboardDetails(
            name: 'Global player leaderboard',
            icon: 'https://cdn3.iconfinder.com/data/icons/life-stories-01/100/Weekly_64x64_copy-06-512.png',
            lowerLimit: 0,
            upperLimit: 1000,
            sort: LeaderboardSort.HIGH,
            type: LeaderboardType.ABSOLUTE
        )
        manager.registerService(leaderboardTwo).join()
    }

    static genStats()
    {
        def manager = Actor.getReference(StatManager)

        def statOne = new StatDetails(
            name: 'Gold',
            minValue: 0,
            maxValue: 5000,
            maxChange: 0,
            defaultValue: 100,
            incrementOnly: true
        )
        manager.registerService(statOne).join()

        def statTwo = new StatDetails(
            name: 'Silver',
            minValue: 0,
            maxChange: 10000,
            defaultValue: 100,
            incrementOnly: false
        )
        manager.registerService(statTwo).join()

        def statThree = new StatDetails(
                name: 'Headshots',
                minValue: 0,
                maxChange: 0,
                defaultValue: 0,
                incrementOnly: true
        )
        manager.registerService(statThree).join()

        def statFour = new StatDetails(
                name: 'KMs walked',
                minValue: 0,
                maxChange: 0.1,
                defaultValue: 0,
                incrementOnly: true
        )
        manager.registerService(statFour).join()
    }

    static genRooms()
    {
        def manager = Actor.getReference(RoomManager)

        def roomOne = new RoomDetails(
                name: '1vs1',
                rules: new Rules(players: 2, teams: 2, ranked: true)
        )
        def r1 = manager.registerService(roomOne).join()


        def roomTwo = new RoomDetails(
                name: '2vs2',
                rules: new Rules(players: 4, teams: 2, ranked: true)
        )
        def r2 = manager.registerService(roomTwo).join()

        /*
        r1.getEntry(Actor.getReference(Player, 'p1'))
        r1.getEntry(Actor.getReference(Player, 'p2'))
        r1.getEntry(Actor.getReference(Player, 'p3'))

        r2.getEntry(Actor.getReference(Player, 'p4'))
        r2.getEntry(Actor.getReference(Player, 'p5'))
        r2.getEntry(Actor.getReference(Player, 'p6'))
        r2.getEntry(Actor.getReference(Player, 'p7'))
        r2.getEntry(Actor.getReference(Player, 'p8'))
        r2.getEntry(Actor.getReference(Player, 'p9'))
        */

    }
}
