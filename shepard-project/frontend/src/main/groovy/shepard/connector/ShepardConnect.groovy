package shepard.connector

import cloud.orbit.actors.Actor
import cloud.orbit.actors.Stage
import com.ea.async.Async
import grails.util.Environment
import shepard.backend.service.achievement.manager.AchievementManager
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.backend.service.stats.manager.StatManager
import shepard.frontend.rest.Achievement
import shepard.frontend.rest.Leaderboard
import shepard.frontend.rest.Room
import shepard.frontend.rest.Stat

import static com.ea.async.Async.await

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 27/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class ShepardConnect
{
    private static Stage stage

    static start()
    {
        Async.init()
        stage = new Stage()
        switch(Environment.current)
        {
            case Environment.TEST:
                stage.setClusterName('testing-service')
                stage.setNodeName('testing-frontend')
                break
            default:
                stage.setClusterName('shepard-service')
                stage.setNodeName('shepard-frontend')
                break
        }
        stage.setMode(Stage.StageMode.CLIENT)
        stage.addExtension(new ShepardLifeTimeExtension())
        await(stage.start())
        stage.bind()

        //Insert sample data while developing
        if (Environment.current == Environment.DEVELOPMENT)
        {
            GenerateTestData.genAchievements()
            GenerateTestData.genLeaderboards()
            GenerateTestData.genStats()
            GenerateTestData.genRooms()
        }
    }

    static stop()
    {
        await(stage.stop())
    }

    static load()
    {

        await(Actor.getReference(AchievementManager).getServiceMap())
                .keySet().each { service -> Achievement.retrieve(service.identity) }

        await(Actor.getReference(LeaderboardManager).getServiceMap())
                .keySet().each { service -> Leaderboard.retrieve(service.identity) }

        await(Actor.getReference(StatManager).getServiceMap())
                .keySet().each { service -> Stat.retrieve(service.identity) }

        await(Actor.getReference(RoomManager).getServiceMap())
                .keySet().each { service -> Room.retrieve(service.identity) }
    }

    static startAndLoad()
    {
        start()
        load()
    }
}
