package shepard.frontend.command

import grails.validation.Validateable
import groovy.transform.ToString
import shepard.backend.util.glicko2.TeamResultResume
import shepard.frontend.rest.Player

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 22/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@ToString
class TeamResume implements Validateable
{
    Set<Player> team
    def score
    def wins
    def loses
    def draws
    def rounds

    TeamResume(TeamResultResume results)
    {
        team = results.teamMembers.collect { Player.retrieve(it) }
        score = results.score
        wins = results.wins
        loses = results.loses
        draws = results.draws
        rounds = results.rounds
    }

    def getAsEntry()
    {
        return [(team.collect{it.id}):["score":score,"wins":wins,"loses":loses,"draws":draws,"rounds":rounds]]
    }

}
