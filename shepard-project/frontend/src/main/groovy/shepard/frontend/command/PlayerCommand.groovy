package shepard.frontend.command

import grails.validation.Validateable
import groovy.transform.ToString

@ToString
class PlayerCommand implements Validateable
{
    String user
    String password
    String oldPassword
}