package shepard.frontend.command

import grails.validation.Validateable
import groovy.transform.ToString
import shepard.backend.util.glicko2.ResultResume
import shepard.frontend.rest.Player

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 22/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@ToString
class PlayerResume implements Validateable
{
    Player player
    def score
    def wins
    def loses
    def draws
    def rounds

    PlayerResume(ResultResume result)
    {
        player = Player.retrieve(result.player.identity)
        score = result.score
        wins = result.wins
        loses = result.loses
        draws = result.draws
        rounds = result.rounds
    }

    def getAsEntry()
    {
        return [(player.id):["score":score,"wins":wins,"loses":loses,"draws":draws,"rounds":rounds]]
    }

}
