package shepard.frontend.command

import grails.validation.Validateable
import groovy.transform.ToString
import shepard.backend.service.game.Game
import shepard.frontend.rest.Lobby

import static com.ea.async.Async.await

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 22/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@ToString(includeNames = true)
class GameResume implements Validateable
{
    Lobby lobby
    Boolean finished
    Map teams = [:]
    Map players = [:]

    GameResume(Game game)
    {
        finished = await(game.isFinished())
        lobby = Lobby.retrieve(await(game.lobby).identity)
        await(game.getTeamResumeRedux()).collect{ new TeamResume(it) }.each { teams << it.getAsEntry() }
        await(game.getResumeRedux()).collect{ new PlayerResume(it) }.each { players << it.getAsEntry() }
        //teamResume = await(game.getTeamResumeRedux()).collect { new TeamResume(it) }.collectEntries { [(it.team.collect{it.id}):it]}
        //playerResume = await(game.getResumeRedux()).collect { new PlayerResume(it) }.collectEntries { [(it.player.id):it] }
    }
}
