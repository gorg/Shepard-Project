package shepard.frontend.domain

import cloud.orbit.actors.Actor

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 24/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
trait RetrieveActor<A extends Actor>
{
    abstract A getActor()
}