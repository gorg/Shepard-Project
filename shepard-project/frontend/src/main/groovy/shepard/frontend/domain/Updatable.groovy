package shepard.frontend.domain

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 01/05/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
trait Updatable
{
    abstract void updateFromBackend()
}
