package shepard.frontend.services

import grails.artefact.Artefact

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 07/05/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
abstract class ShepardEntryCommons<E>
{
    abstract List<E> index()

    abstract E show()

    abstract E save()

}
