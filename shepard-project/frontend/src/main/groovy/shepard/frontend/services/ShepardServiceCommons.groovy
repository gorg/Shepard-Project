package shepard.frontend.services

import cloud.orbit.actors.Actor
import grails.transaction.Transactional
import grails.util.GrailsNameUtils
import shepard.backend.service.generic.ServiceManager

import static com.ea.async.Async.await
import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 06/05/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/

@Transactional
abstract class ShepardServiceCommons<D,E,M extends ServiceManager>
{
    Class<D> resource
    String resourceName
    String resourceClassName

    Class<M> manager
    Class<E> entry

    ShepardServiceCommons(Class<D> resource, Class<E> entry, Class<M> manager)
    {
        this.resource = resource
        this.manager = manager
        this.entry = entry

        resourceClassName = resource.simpleName
        resourceName = GrailsNameUtils.getPropertyName(resource)
    }

    List<D> index()
    {
        return resource.list()
    }

    D show(String id)
    {
        (id) ? resource.retrieve(id) : null
    }

    @Transactional
    D save(Map params)
    {
        D instance = null
        if(params.name)
        {
            def id = name2Id(params.name as String)
            if (!resource.exists(id))
            {
                instance = resource.newInstance(params)
                instance.validate()
                if (!instance.hasErrors())
                {
                    instance = instance.save flush:true
                }
                else transactionStatus.setRollbackOnly()
            }
            else instance = resource.get(id)
        }
        return instance
    }

    @Transactional
    D update(String id, Map params)
    {
        D instance = null
        if(id && params)
        {
            instance = resource.retrieve(id)
            if (instance)
            {
                params.remove('name')
                instance.properties = params
                instance.validate()
                if (!instance.hasErrors())
                {
                    instance = instance.save flush:true
                }
                else transactionStatus.setRollbackOnly()
            }
        }
        return instance
    }

    @Transactional
    boolean delete(String id)
    {
        if (id)
        {
            def instance = resource.retrieve(id)
            if (instance)
            {
                entry."findAllBy$resourceClassName"(instance).each { it.delete() }
                instance.delete flush: true
                return true
            }
        }
        return false
    }

    List<D> getEnabled()
    {
        resource.findAllByEnabled(true)
    }

    List<D> getDisabled()
    {
        resource.findAllByEnabled(false)
    }

    @Transactional
    Boolean enable(String id)
    {
        if (id)
        {
            def instance = resource.retrieve(id)
            if (instance)
            {
                def enable = await(Actor.getReference(manager).enableService(instance.actor))

                if (enable)
                {
                    instance.enabled = true
                    instance.save flush: true
                }
                return enable
            }
        }
        return null
    }

    @Transactional
    Boolean disable(String id)
    {
        if (id)
        {
            def instance = resource.retrieve(id)
            if (instance)
            {
                def enable = await(Actor.getReference(manager).disableService(instance.actor))

                if (enable)
                {
                    instance.enabled = false
                    instance.save flush: true
                }
                return enable
            }
        }
        return null
    }

}