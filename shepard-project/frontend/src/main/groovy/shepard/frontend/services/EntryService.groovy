package shepard.frontend.services

import grails.web.servlet.mvc.GrailsParameterMap
import shepard.frontend.rest.Player

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 11/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
trait EntryService<S,E>
{
    //Used with new entries
    abstract E setEntry(S service, Player player, GrailsParameterMap params)

    //Used to update entries
    abstract E updateEntry(E entry, GrailsParameterMap params)

    //Used to create initial entries if non found
    abstract E createEntry(S service, Player player, GrailsParameterMap params)
}
