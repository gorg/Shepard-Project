package shepard.frontend.controllers

import grails.artefact.Artefact
import grails.util.GrailsNameUtils
import grails.validation.ValidationException
import org.hibernate.engine.jdbc.spi.SqlExceptionHelper
import org.springframework.http.HttpStatus
import shepard.backend.service.generic.ServiceManager
import shepard.frontend.services.ShepardServiceCommons

import static org.springframework.http.HttpStatus.*

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 06/05/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Artefact("Controller")
abstract class ShepardServiceController<T>
{
    static responseFormats = ['json']
    static allowedMethods = [show: "GET",
                             save: "POST",
                             update: ["PUT", "POST"],
                             delete: "DELETE"]
    Class<T> resource
    String resourceName
    String resourceClassName

    ShepardServiceController(Class<T> resource)
    {
        this.resource = resource
        resourceClassName = resource.simpleName
        resourceName = GrailsNameUtils.getPropertyName(resource)
    }

    abstract ShepardServiceCommons<T,?,? extends ServiceManager> getService()

    def index()
    {
        showInstances(getService().index())
    }

    def show()
    {
        def instance = getService().show(getId())
        (instance) ? showInstance(instance) : notFound()
    }

    def save()
    {
        def instance = getService().save(mergeBodyAndParams())
        if (!instance)
            badRequest()
        else if (instance.hasErrors())
            respond instance.errors, [status: BAD_REQUEST, view: '../errors']
        else
            showInstance(instance, CREATED)
    }

    def update()
    {
        def instance = getService().update(getId(),mergeBodyAndParams())
        if (!instance) notFound()
        else if (instance.hasErrors()) respond instance.errors, [status: BAD_REQUEST, view: '../errors']
        else showInstance(instance)
    }

    def delete()
    {
        def deleted = getService().delete(getId())
        (deleted) ? noContent() : notFound()
    }

    def enable()
    {
        def enabled = getService().enable(getId())
        (enabled) ? show() : (enabled == null) ? notFound() : notModified()
    }

    def disable()
    {
        def disabled = getService().disable(getId())
        (disabled) ? show() : (disabled == null) ? notFound() : notModified()
    }

    def getEnabled()
    {
        showInstances(getService().enabled)
    }

    def getDisabled()
    {
        showInstances(getService().disabled)
    }

    private showInstance(T instance, HttpStatus code = OK)
    {
        respond instance, [status: code, view: "show" ]
        return
    }

    private showInstances(List<T> instances)
    {
        respond instances, [status: OK, view: "index" ]
        return
    }

    protected void notFound()
    {
        render status: NOT_FOUND
        return
    }

    protected void noContent()
    {
        render status: NO_CONTENT
        return
    }

    protected void badRequest()
    {
        render status: BAD_REQUEST
        return
    }

    protected void notModified()
    {
        render status: NOT_MODIFIED
        return
    }

    protected Map mergeBodyAndParams()
    {
        def values = [:]
        request.JSON.each { k,v -> if (v != null) values[k] = v }
        params.each { k,v -> if (v != null) values[k] = v }
        return values
    }

    protected String getId()
    {
        params.get( params.keySet().toList().find{ it.toString().toUpperCase().contains('ID') } )
    }

    def catchValidationException(final ValidationException ex)
    {
        badRequest()
    }

}
