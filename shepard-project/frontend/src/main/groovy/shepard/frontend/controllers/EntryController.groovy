package shepard.frontend.controllers

import grails.artefact.Artefact
import grails.transaction.Transactional
import grails.util.GrailsNameUtils
import org.springframework.http.HttpStatus
import shepard.frontend.rest.Player
import shepard.frontend.services.EntryService

import static org.springframework.http.HttpStatus.BAD_REQUEST
import static org.springframework.http.HttpStatus.OK

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 11/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 *
 * Based on Grails RestfulController
 **/
@Artefact("Controller")
@Transactional(readOnly = true)
abstract class EntryController<S,E>
{
    static allowedMethods = [save: "POST", update: ["PUT", "POST"]]
    static responseFormats = ['json']

    Class<E> resource
    String resourceName
    String resourceClassName

    Class<S> service
    String serviceName
    String serviceClassName

    boolean readOnly

    EntryController(Class<S> service, Class<E> resource)
    {
        this(service, resource, false)
    }

    EntryController(Class<S> service, Class<E> resource, boolean readOnly)
    {
        this.readOnly = readOnly

        this.resource = resource
        resourceClassName = resource.simpleName
        resourceName = GrailsNameUtils.getPropertyName(resource)

        this.service = service
        serviceClassName = service.simpleName
        serviceName = GrailsNameUtils.getPropertyName(service)
    }

    abstract protected EntryService getService()

    def index()
    {
        S instance = getServiceInstance()
        instance ? respond (getEntries(instance), [status:OK, view: 'index']) : notFound()
    }

    def show()
    {
        S instance = getServiceInstance()
        Player player = getPlayerInstance()

        if (player && instance)
        {
            E entry = getEntry(instance,player)
            entry = entry ?: getService().createEntry(instance,player,params) as E
            entry.updateFromBackend()
            respond entry, [status:OK, view: 'show']
        }
        else
            notFound()
    }

    def update()
    {
        S instance = getServiceInstance()
        Player player = getPlayerInstance()
        if (player && instance )
        {
            try
            {
                E entry = getService().setEntry(instance,player,params) as E
                if (entry.hasErrors())
                    respond entry.errors, [status: BAD_REQUEST, view: '../errors']
                else
                    respond entry, [status:OK, view: 'show']
            } catch(IllegalArgumentException e) { badRequest() }
        }
        else notFound()
    }

    def save()
    {
        update()
    }

    def catchIllegalArgument(final IllegalArgumentException ex)
    {
        badRequest()
    }

    private S getServiceInstance()
    {
        service.retrieve(params."${serviceName+"Id"}" as String)
    }

    private E getEntryInstance()
    {
        resource.retrieve(params.id as String)
    }

    private Player getPlayerInstance()
    {
        Player.retrieve(params.playerId as String)
    }

    def notFound = { render status: HttpStatus.NOT_FOUND }

    def badRequest = { render status: HttpStatus.BAD_REQUEST }


    private getEntries(S instance)
    {
        def entries = resource."findAllBy$serviceClassName"(instance)
        entries.each { entry -> entry.updateFromBackend() }
        return entries
    }

    private E getEntry(S instance, Player player)
    {
        resource."${"findBy$serviceClassName"+"AndPlayer"}"(instance,player)
    }

}
