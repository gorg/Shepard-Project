package shepard.frontend.rest.achievement

import cloud.orbit.actors.Actor
import groovyx.net.http.FromServer
import groovyx.net.http.HttpBuilder
import groovyx.net.http.HttpException
import shepard.backend.service.achievement.AchievementService
import shepard.backend.service.achievement.details.AchievementType
import shepard.backend.service.achievement.manager.AchievementManager
import shepard.frontend.rest.Achievement
import shepard.frontend.test.ShepardIntegrationSpec
import spock.lang.Shared
import spock.lang.Stepwise
import spock.lang.Unroll

import static groovyx.net.http.HttpBuilder.configure
import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 26/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class AchievementIntegrationSpec extends ShepardIntegrationSpec
{
    @Shared HttpBuilder http
    @Shared String path = '/achievements'

    def setup()
    {
        http = configure {
            request.uri = baseUrl
            request.contentType = 'application/json'
            request.accept = 'application/json'
        }
    }

    def "empty index"()
    {
        when:
        List<Achievement> result = http.get {
            request.uri.path = path
        }

        then:
        result.isEmpty()
    }

    @Unroll
    def "create invalid achievements"()
    {
        when:
        Achievement result = http.post {
            request.uri.path = path
            request.body = [name: name, description: desc, type: type]
        }

        then:
        HttpException ex = thrown()
        ex.statusCode == respCode

        where:
        name    |desc           |type       ||respCode
        null    |null           |null       ||400
        null    |"Description"  |null       ||400
        null    |"Description"  |'REVEALED' ||400
        null    |"Description"  |'HIDDEN'   ||400
    }


    @Unroll
    def "create #name achievement"()
    {
        given:
        def actor = Actor.getReference(AchievementService, name2Id(name))

        when:
        Achievement result = http.post {
            request.uri.path = path
            request.body = [name: name, description: desc, type: type]
        }

        then:
        result.name == name
        result.description ==  desc
        result.type == (type) ? AchievementType.valueOf(type) : AchievementType.REVEALED

        and:
        actor.getServiceDetails().join() == result.generateDetailsFromDomain()
        actor.isEnabled().join()

        where:
        name                    |desc                           |type
        "Test Achievement 1"    |null                           |null
        "Test Achievement 2"    |"Achievement 2 Description"    |null
        "Test Achievement 3"    |"Achievement 3 Description"    |'REVEALED'
        "Test Achievement 4"    |"Achievement 4 Description"    |'HIDDEN'
    }

    def "query index"()
    {
        when:
        List<Achievement> result = http.get {
            request.uri.path = path
        }

        then:
        result.size() == 4
        4.times {
            def name = "Test Achievement ${it+1}"
            assert result[it].id == name2Id(name)
            assert result[it].name == name
            assert result[it].enabled == true
        }
    }

    @Unroll
    def "show #name"()
    {
        when:
        Achievement result = http.get {
            request.uri.path = path+"/${name2Id(name)}"
        }

        then:
        result.name == name
        result.description ==  desc
        result.type == (type) ? AchievementType.valueOf(type) : AchievementType.REVEALED

        where:
        name                    |desc                           |type
        "Test Achievement 1"    |null                           |null
        "Test Achievement 2"    |"Achievement 2 Description"    |null
        "Test Achievement 3"    |"Achievement 3 Description"    |'REVEALED'
        "Test Achievement 4"    |"Achievement 4 Description"    |'HIDDEN'
    }

    @Unroll
    def "update #name"()
    {
        given:
        def id = name2Id(name)
        def actor = Actor.getReference(AchievementService, id)

        when:
        Achievement result = http.put {
            request.uri.path = path+"/$id"
            request.uri.query = [description: desc, type: type]
        }

        then:
        result.description == desc
        result.type == AchievementType.valueOf(type)

        and:
        actor.getServiceDetails().join() == result.generateDetailsFromDomain()
        actor.getServiceDetails().join().description == desc
        actor.getServiceDetails().join().type.toString() == type


        where:
        name                    |desc                           |type
        "Test Achievement 1"    |"Description of Achievement 1" |'HIDDEN'
        "Test Achievement 2"    |"Description of Achievement 2" |'REVEALED'
        "Test Achievement 3"    |"Description of Achievement 3" |'HIDDEN'
        "Test Achievement 4"    |"Description of Achievement 4" |'REVEALED'
    }

    @Unroll
    def "disable #name"()
    {
        given:
        def id = name2Id(name)
        def actor = Actor.getReference(AchievementService, id)

        when:
        Achievement result = http.delete {
            request.uri.path = path+"/$id/activate"
        }

        then:
        !result.enabled

        and:
        !actor.isEnabled().join()

        where:
        name << ["Test Achievement 1", "Test Achievement 2"]
    }

    def "get enabled"()
    {
        expect:
        Actor.getReference(AchievementManager).getAllEnabledServices().join().size() == 2

        when:
        List<Achievement> result = http.get {
            request.uri.path = path+"/enabled"
        }

        then:
        result.size() == 2
        result.name.containsAll(["Test Achievement 3", "Test Achievement 4"])
    }

    def "get disabled"()
    {
        expect:
        Actor.getReference(AchievementManager).getAllDisabledServices().join().size() == 2

        when:
        List<Achievement> result = http.get {
            request.uri.path = path+"/disabled"
        }

        then:
        result.size() == 2
        result.name.containsAll(["Test Achievement 1", "Test Achievement 2"])
    }

    @Unroll
    def "re-enable #name"()
    {
        given:
        def id = name2Id(name)

        when:
        int status = http.put() {
            request.uri.path = path+"/$id/activate"
            response.success { FromServer fs -> fs.statusCode }
        }

        then:
        status == 304

        where:
        name << ["Test Achievement 3", "Test Achievement 4"]
    }

    @Unroll
    def "re-disable #name"()
    {
        given:
        def id = name2Id(name)

        when:
        int status = http.delete() {
            request.uri.path = path+"/$id/activate"
            response.success { FromServer fs -> fs.statusCode }
        }

        then:
        status == 304

        where:
        name << ["Test Achievement 1", "Test Achievement 2"]
    }

    @Unroll
    def "enable the disabled #name"()
    {
        given:
        def id = name2Id(name)
        def actor = Actor.getReference(AchievementService, id)

        when:
        Map<String,String> result = http.put() {
            request.uri.path = path+"/$id/activate"
        }

        then:
        result["enabled"] == true


        and:
        actor.isEnabled().join()

        where:
        name << ["Test Achievement 1", "Test Achievement 2"]
    }

    @Unroll
    def "delete"()
    {
        given:
        def id = name2Id(name)

        when:
        Integer status = http.delete {
            request.uri.path = path+"/$id"
            response.success { FromServer fs -> fs.statusCode }
            response.failure { FromServer fs -> fs.statusCode }
        }

        then:
        status == respStatus

        and:
        if (respStatus == 204)
        {
            assert Actor.getReference(AchievementService, id).getServiceDetails().join() == null
        }


        where:
        name                    |respStatus
        "Test Achievement 1"    |204
        "Test Achievement 2"    |204
        "Test Achievement 3"    |204
        "Test Achievement 4"    |204
        "qwerty"                |404
        ""                      |404
        null                    |404
    }

}
