package shepard.frontend.rest.leaderboard

import cloud.orbit.actors.Actor
import groovyx.net.http.FromServer
import groovyx.net.http.HttpBuilder
import groovyx.net.http.HttpException
import shepard.backend.service.leaderboard.LeaderboardService
import shepard.backend.service.leaderboard.details.LeaderboardSort
import shepard.backend.service.leaderboard.details.LeaderboardType
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.frontend.rest.Leaderboard
import shepard.frontend.test.ShepardIntegrationSpec
import spock.lang.Shared
import spock.lang.Stepwise
import spock.lang.Unroll

import static groovyx.net.http.HttpBuilder.configure
import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 26/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class LeaderboardIntegrationSpec extends ShepardIntegrationSpec
{
    @Shared HttpBuilder http
    @Shared String path = '/leaderboards'

    def setup()
    {
        http = configure {
            request.uri = baseUrl
            request.contentType = 'application/json'
            request.accept = 'application/json'
        }
    }

    def "empty index"()
    {
        when:
        List<Leaderboard> result = http.get {
            request.uri.path = path
        }

        then:
        result.isEmpty()
    }

    @Unroll
    def "create invalid leaderboards"()
    {
        when:
        Leaderboard result = http.post {
            request.uri.path = path
            request.body = [name: name, lowerLimit: low, upperLimit: up, leaderboardType:type, leaderboardSort:sort]
        }

        then:
        HttpException ex = thrown()
        ex.statusCode == respStatus

        where:
        name    |low    |up     |type           |sort      || respStatus
        null    |null   |null   |null           |null      || 400
        't'     |-10    |-20    |null           |null      || 422  //upperLimit < lowerLimit
        't'     |0      |10     |'asdf'         |null      || 422  //bad enum
        't'     |0      |10     |'ABSOLUTE'     |'asdf'    || 422  //bad enum

    }


    @Unroll
    def "create #name leaderboard"()
    {
        given:
        def actor = Actor.getReference(LeaderboardService, name2Id(name))

        when:
        Leaderboard result = http.post {
            request.uri.path = path
            request.body = [name: name, lowerLimit: low, upperLimit: up, leaderboardType:type, leaderboardSort:sort]
        }

        then:
        result.enabled
        result.name == name
        result.lowerLimit == (low ?: 0)
        result.upperLimit == (up ?: Long.MAX_VALUE)
        result.leaderboardSort == (sort ? LeaderboardSort.valueOf(sort) : LeaderboardSort.HIGH)
        result.leaderboardType == (type ? LeaderboardType.valueOf(type) : LeaderboardType.ABSOLUTE)

        and:
        actor.getServiceDetails().join() == result.generateDetailsFromDomain()
        actor.isEnabled().join()

        where:
        name                |low    |up     |type           |sort
        'Leaderboard 1'     |null   |null   |null           |null
        'Leaderboard 2'     |50     |null   |null           |null
        'Leaderboard 3'     |null   |800    |null           |null
        'Leaderboard 4'     |1      |100    |null           |null
        'Leaderboard 5'     |2      |100    |'ABSOLUTE'     |null
        'Leaderboard 6'     |3      |100    |'INCREMENTAL'  |null
        'Leaderboard 7'     |4      |100    |null           |'HIGH'
        'Leaderboard 8'     |5      |100    |null           |'LOW'
        'Leaderboard 9'     |6      |100    |'ABSOLUTE'     |'HIGH'
    }

    def "query index"()
    {
        when:
        List<Leaderboard> result = http.get {
            request.uri.path = path
        }

        then:
        result.size() == 9
        9.times {
            def name = "Leaderboard ${it+1}"
            assert result[it].id == name2Id(name)
            assert result[it].name == name
            assert result[it].enabled == true
        }
    }


    @Unroll
    def "show #name"()
    {
        when:
        Leaderboard result = http.get {
            request.uri.path = path+"/${name2Id(name)}"
        }

        then:
        result.enabled
        result.name == name
        result.lowerLimit == (low ?: 0)
        result.upperLimit == (up ?: Long.MAX_VALUE)
        result.leaderboardSort == (sort ? LeaderboardSort.valueOf(sort) : LeaderboardSort.HIGH)
        result.leaderboardType == (type ? LeaderboardType.valueOf(type) : LeaderboardType.ABSOLUTE)

        where:
        name                |low    |up     |type           |sort
        'Leaderboard 1'     |null   |null   |null           |null
        'Leaderboard 2'     |50     |null   |null           |null
        'Leaderboard 3'     |null   |800    |null           |null
        'Leaderboard 4'     |1      |100    |null           |null
        'Leaderboard 5'     |2      |100    |'ABSOLUTE'     |null
        'Leaderboard 6'     |3      |100    |'INCREMENTAL'  |null
        'Leaderboard 7'     |4      |100    |null           |'HIGH'
        'Leaderboard 8'     |5      |100    |null           |'LOW'
        'Leaderboard 9'     |6      |100    |'ABSOLUTE'     |'HIGH'
    }

    @Unroll
    def "update #name"()
    {
        given:
        def id = name2Id(name)
        def actor = Actor.getReference(LeaderboardService, id)

        when:
        Leaderboard result = http.put {
            request.uri.path = path+"/$id"
            request.uri.query = [name: name, lowerLimit: low, upperLimit: up, leaderboardType:type, leaderboardSort:sort]
        }

        then:
        result.enabled
        result.name == name
        result.lowerLimit == low
        result.upperLimit == up
        result.leaderboardSort == LeaderboardSort.valueOf(sort)
        result.leaderboardType == LeaderboardType.valueOf(type)

        and:
        actor.getServiceDetails().join() == result.generateDetailsFromDomain()
        actor.getServiceDetails().join().lowerLimit == low
        actor.getServiceDetails().join().upperLimit == up
        actor.getServiceDetails().join().type.toString() == type
        actor.getServiceDetails().join().sort.toString() == sort


        where:
        name                |low    |up     |type           |sort
        'Leaderboard 1'     |-5     |5      |'INCREMENTAL'  |'LOW'
        'Leaderboard 2'     |1      |300    |'INCREMENTAL'  |'LOW'
        'Leaderboard 3'     |23     |802    |'INCREMENTAL'  |'LOW'
        'Leaderboard 4'     |1      |103    |'INCREMENTAL'  |'LOW'
        'Leaderboard 5'     |2      |104    |'ABSOLUTE'     |'LOW'
        'Leaderboard 6'     |3      |105    |'INCREMENTAL'  |'LOW'
        'Leaderboard 7'     |4      |106    |'ABSOLUTE'     |'HIGH'
        'Leaderboard 8'     |5      |107    |'ABSOLUTE'     |'LOW'
        'Leaderboard 9'     |6      |108    |'ABSOLUTE'     |'HIGH'
    }



    @Unroll
    def "disable #name"()
    {
        given:
        def id = name2Id(name)
        def actor = Actor.getReference(LeaderboardService, id)

        when:
        Leaderboard result = http.delete {
            request.uri.path = path+"/$id/activate"
        }

        then:
        !result.enabled

        and:
        !actor.isEnabled().join()

        where:
        name << ["Leaderboard 1", "Leaderboard 2"]
    }



    def "get enabled"()
    {
        expect:
        Actor.getReference(LeaderboardManager).getAllEnabledServices().join().size() == 7

        when:
        List<Leaderboard> result = http.get {
            request.uri.path = path+"/enabled"
        }

        then:
        result.size() == 7
        result.name.containsAll(["Leaderboard 3", "Leaderboard 4",
                                 "Leaderboard 5", "Leaderboard 6",
                                 "Leaderboard 7", "Leaderboard 8", "Leaderboard 9"])
    }

    def "get disabled"()
    {
        expect:
        Actor.getReference(LeaderboardManager).getAllDisabledServices().join().size() == 2

        when:
        List<Leaderboard> result = http.get {
            request.uri.path = path+"/disabled"
        }

        then:
        result.size() == 2
        result.name.containsAll(["Leaderboard 1", "Leaderboard 2"])
    }

    @Unroll
    def "re-enable #name"()
    {
        given:
        def id = name2Id(name)

        when:
        int status = http.put() {
            request.uri.path = path+"/$id/activate"
            response.success { FromServer fs -> fs.statusCode }
        }

        then:
        status == 304

        where:
        name << ["Leaderboard 3", "Leaderboard 4"]
    }

    @Unroll
    def "re-disable #name"()
    {
        given:
        def id = name2Id(name)

        when:
        int status = http.delete() {
            request.uri.path = path+"/$id/activate"
            response.success { FromServer fs -> fs.statusCode }
        }

        then:
        status == 304

        where:
        name << ["Leaderboard 1", "Leaderboard 2"]
    }

    @Unroll
    def "enable the disabled #name"()
    {
        given:
        def id = name2Id(name)
        def actor = Actor.getReference(LeaderboardService, id)

        when:
        Map<String,String> result = http.put() {
            request.uri.path = path+"/$id/activate"
        }

        then:
        result["enabled"] == true


        and:
        actor.isEnabled().join()

        where:
        name << ["Leaderboard 1", "Leaderboard 2"]
    }

    @Unroll
    def "delete #name"()
    {
        given:
        def id = name2Id(name)

        when:
        Integer status = http.delete {
            request.uri.path = path+"/$id"
            response.success { FromServer fs -> fs.statusCode }
            response.failure { FromServer fs -> fs.statusCode }
        }

        then:
        status == respStatus

        and:
        if (respStatus == 204)
        {
            assert Actor.getReference(LeaderboardService, id).getServiceDetails().join() == null
        }


        where:
        name                    |respStatus
        "qwerty"                |404
        ""                      |404
        null                    |404
        'Leaderboard 1'         |204
        'Leaderboard 2'         |204
        'Leaderboard 3'         |204
        'Leaderboard 4'         |204
        'Leaderboard 5'         |204
        'Leaderboard 6'         |204
        'Leaderboard 7'         |204
        'Leaderboard 8'         |204
        'Leaderboard 9'         |204
    }
}
