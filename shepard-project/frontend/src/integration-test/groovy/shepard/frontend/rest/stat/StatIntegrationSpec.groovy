package shepard.frontend.rest.stat

import cloud.orbit.actors.Actor
import groovyx.net.http.FromServer
import groovyx.net.http.HttpBuilder
import shepard.backend.service.stats.StatService
import shepard.backend.service.stats.manager.StatManager
import shepard.frontend.rest.Stat
import shepard.frontend.test.ShepardIntegrationSpec
import spock.lang.Shared
import spock.lang.Stepwise
import spock.lang.Unroll

import static groovyx.net.http.HttpBuilder.configure
import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 26/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class StatIntegrationSpec extends ShepardIntegrationSpec
{
    @Shared HttpBuilder http
    @Shared String path = '/stats'

    def setup()
    {
        http = configure {
            request.uri = baseUrl
            request.contentType = 'application/json'
            request.accept = 'application/json'
        }
    }

    def "empty index"()
    {
        when:
        List<Stat> result = http.get {
            request.uri.path = path
        }

        then:
        result.isEmpty()
    }

    @Unroll
    def "create invalid stats"()
    {
        when:
        int status = http.post {
            request.uri.path = path
            request.body = [name: name, minValue: low, maxValue: up, defaultValue:d, maxChange:change, incrementOnly:increment]
            response.failure { FromServer fs -> fs.statusCode }
        }

        then:
        status == respStatus

        where:
        name    |low   |up    |d    |change |increment  || respStatus
        null    |null  |null  |null |null   |null       || 400
        't'     |100   |10    |null |null   |null       || 422 //maxValue < minValue
        't'     |0     |10    |11   |null   |null       || 422 //defaultValue > maxValue
        't'     |0     |10    |-1   |null   |null       || 422 //defaultValue < minValue
        't'     |0     |10    |0    |11     |null       || 422 //maxChange > abs(low)+abs(up)
    }


    @Unroll
    def "create #name stat"()
    {
        given:
        def actor = Actor.getReference(StatService, name2Id(name))

        when:
        Stat result = http.post {
            request.uri.path = path
            request.body = [name: name, minValue: low, maxValue: up, defaultValue:d, maxChange:change, incrementOnly:increment]
        }

        then:
        result.name == name
        if (low == up)
        {
            assert result.minValue == 0
            assert result.maxValue == BigDecimal.valueOf(Long.MAX_VALUE, 4)
        }
        else
        {
            assert result.minValue == (low ?: 0)
            assert result.maxValue == (up == null ? BigDecimal.valueOf(Long.MAX_VALUE,4) : up)
        }
        result.defaultValue == (d ?: 0)
        result.maxChange == (change ?: 0)
        result.incrementOnly == (increment == null ? false : increment)

        and:
        actor.getServiceDetails().join() == result.generateDetailsFromDomain()
        actor.isEnabled().join()

        where:
        name         |low   |up    |d       |change |increment
        'Stat 1'     |null  |null  |null    |null   |null
        'Stat 2'     |0     |0     |null    |null   |null
        'Stat 3'     |0     |10    |null    |null   |null
        'Stat 4'     |-10   |0     |null    |null   |null
        'Stat 5'     |0     |10    |0       |null   |null
        'Stat 6'     |0     |10    |5       |null   |null
        'Stat 7'     |0     |10    |0       |0      |null
        'Stat 8'     |0     |10    |0       |5      |null
        'Stat 9'     |0     |10    |0       |0      |true
        'Stat 10'    |0     |10    |0       |0      |false
    }


    def "query index"()
    {
        when:
        List<Stat> result = http.get {
            request.uri.path = path
        }

        then:
        result.size() == 10
        10.times {
            def name = "Stat ${it+1}"
            assert result[it].id == name2Id(name)
            assert result[it].name == name
            assert result[it].enabled == true
        }
    }


    @Unroll
    def "show #name"()
    {
        when:
        Stat result = http.get {
            request.uri.path = path+"/${name2Id(name)}"
        }

        then:
        result.name == name
        if (low == up)
        {
            assert result.minValue == 0
            assert result.maxValue == BigDecimal.valueOf(Long.MAX_VALUE, 4).setScale(2,5)
        }
        else
        {
            assert result.minValue == (low ?: 0)
            assert result.maxValue == (up == null ? BigDecimal.valueOf(Long.MAX_VALUE,4).setScale(2,5) : up)
        }
        result.defaultValue == (d ?: 0)
        result.maxChange == (change ?: 0)
        result.incrementOnly == (increment == null ? false : increment)

        where:
        name         |low   |up    |d       |change |increment
        'Stat 1'     |null  |null  |null    |null   |null
        'Stat 2'     |0     |0     |null    |null   |null
        'Stat 3'     |0     |10    |null    |null   |null
        'Stat 4'     |-10   |0     |null    |null   |null
        'Stat 5'     |0     |10    |0       |null   |null
        'Stat 6'     |0     |10    |5       |null   |null
        'Stat 7'     |0     |10    |0       |0      |null
        'Stat 8'     |0     |10    |0       |5      |null
        'Stat 9'     |0     |10    |0       |0      |true
        'Stat 10'    |0     |10    |0       |0      |false
    }

    @Unroll
    def "update #name"()
    {
        given:
        def id = name2Id(name)
        def actor = Actor.getReference(StatService, id)

        when:
        Stat result = http.put {
            request.uri.path = path+"/$id"
            request.uri.query = [name: name, minValue: low, maxValue: up, defaultValue:d, maxChange:change, incrementOnly:increment]
        }

        then:
        result.name == name
        result.maxValue == up
        result.minValue == low
        result.defaultValue == d
        result.maxChange == change
        result.incrementOnly == increment

        and:
        actor.getServiceDetails().join() == result.generateDetailsFromDomain()


        where:
        name         |low   |up    |d       |change |increment
        'Stat 1'     |50    |1500  |55      |5      |false
        'Stat 2'     |1500  |2000  |1555    |5      |false
        'Stat 3'     |-1000 |10    |5       |5      |false
        'Stat 4'     |-10   |50    |5       |5      |false
        'Stat 5'     |11    |100   |95      |5      |false
        'Stat 6'     |22    |100   |95      |5      |false
        'Stat 7'     |33    |100   |90      |0      |false
        'Stat 8'     |44    |100   |90      |5      |false
        'Stat 9'     |55    |100   |90      |0      |false
        'Stat 10'    |66    |100   |90      |0      |true
    }



    @Unroll
    def "disable #name"()
    {
        given:
        def id = name2Id(name)
        def actor = Actor.getReference(StatService, id)

        when:
        Stat result = http.delete {
            request.uri.path = path+"/$id/activate"
        }

        then:
        !result.enabled

        and:
        !actor.isEnabled().join()

        where:
        name << ["Stat 1", "Stat 2"]
    }

    def "get enabled"()
    {
        expect:
        Actor.getReference(StatManager).getAllEnabledServices().join().size() == 8

        when:
        List<Stat> result = http.get {
            request.uri.path = path+"/enabled"
            response.failure {FromServer fs -> println "$fs.statusCode -> $fs.message" }
        }

        then:
        result.size() == 8
        result.name.containsAll(["Stat 3", "Stat 4",
                                 "Stat 5", "Stat 6",
                                 "Stat 7", "Stat 8",
                                 "Stat 9", "Stat 10"])
    }


    def "get disabled"()
    {
        expect:
        Actor.getReference(StatManager).getAllDisabledServices().join().size() == 2

        when:
        List<Stat> result = http.get {
            request.uri.path = path+"/disabled"
        }

        then:
        result.size() == 2
        result.name.containsAll(["Stat 1", "Stat 2"])
    }

    @Unroll
    def "re-enable #name"()
    {
        given:
        def id = name2Id(name)

        when:
        int status = http.put() {
            request.uri.path = path+"/$id/activate"
            response.success { FromServer fs -> fs.statusCode }
        }

        then:
        status == 304

        where:
        name << ["Stat 3", "Stat 4"]
    }

    @Unroll
    def "re-disable #name"()
    {
        given:
        def id = name2Id(name)

        when:
        int status = http.delete() {
            request.uri.path = path+"/$id/activate"
            response.success { FromServer fs -> fs.statusCode }
        }

        then:
        status == 304

        where:
        name << ["Stat 1", "Stat 2"]
    }

    @Unroll
    def "enable the disabled #name"()
    {
        given:
        def id = name2Id(name)
        def actor = Actor.getReference(StatService, id)

        when:
        Map<String,String> result = http.put() {
            request.uri.path = path+"/$id/activate"
        }

        then:
        result["enabled"] == true


        and:
        actor.isEnabled().join()

        where:
        name << ["Stat 1", "Stat 2"]
    }

    @Unroll
    def "delete #name"()
    {
        given:
        def id = name2Id(name)

        when:
        Integer status = http.delete {
            request.uri.path = path+"/$id"
            response.success { FromServer fs -> fs.statusCode }
            response.failure { FromServer fs -> fs.statusCode }
        }

        then:
        status == respStatus

        and:
        if (respStatus == 204)
        {
            assert Actor.getReference(StatService, id).getServiceDetails().join() == null
        }

        where:
        name             |respStatus
        "qwerty"         |404
        ""               |404
        null             |404
        'Stat 1'         |204
        'Stat 2'         |204
        'Stat 3'         |204
        'Stat 4'         |204
        'Stat 5'         |204
        'Stat 6'         |204
        'Stat 7'         |204
        'Stat 8'         |204
        'Stat 9'         |204
        'Stat 10'        |204
    }

}
