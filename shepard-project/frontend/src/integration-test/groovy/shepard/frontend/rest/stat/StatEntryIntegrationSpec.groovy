package shepard.frontend.rest.stat

import cloud.orbit.actors.Actor
import groovyx.net.http.FromServer
import groovyx.net.http.HttpBuilder
import groovyx.net.http.HttpException
import shepard.backend.service.stats.StatService
import shepard.backend.service.stats.manager.StatManager
import shepard.frontend.rest.Player
import shepard.frontend.rest.Stat
import shepard.frontend.rest.StatEntry
import shepard.frontend.test.ShepardIntegrationSpec
import spock.lang.Shared
import spock.lang.Unroll

import static groovyx.net.http.HttpBuilder.configure
import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 28/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class StatEntryIntegrationSpec extends ShepardIntegrationSpec
{
    @Shared HttpBuilder http
    @Shared String statName = 'Test Stat'
    @Shared String path = "/stats/${name2Id(statName)}"

    def setup()
    {
        http = configure {
            request.uri = baseUrl
            request.contentType = 'application/json'
            request.accept = 'application/json'
        }
    }

    def "create an stat service"()
    {
        when:
        Stat result = http.post {
            request.uri.path = '/stats'
            request.body = [name: statName, maxValue: 1000]
        }

        then:
        result.name == statName

        and:
        Actor.getReference(StatManager)
                .getAllEnabledServices().join().size() == 1

        Actor.getReference(StatManager)
                .getAllEnabledServices().join()
                .first().getServiceDetails().join() == result.generateDetailsFromDomain()
    }

    def "index entries"()
    {
        when:
        List<StatEntry> entries = http.get {
            request.uri.path = path+"/entries"
        }

        then:
        entries.size() == 0
    }

    @Unroll
    def "create players"()
    {
        when:
        Player player = http.post {
            request.uri.path = '/players'
            request.body = [user: user, password: pass]
        }

        then:
        assert player.rating == 1500
        assert player.games == 0

        where:
        user        |pass
        'Player 1'  |'12345'
        'Player 2'  |'12345'
        'Player 3'  |'12345'
        'Player 4'  |'12345'

    }



    @Unroll
    def "invalid url check player's entries"()
    {
        given:
        def playerId = sendPlayerId ? name2Id(user) : ''
        def statId = sendServiceId ? name2Id(statName) : ''
        def path = "/stats/$statId/player/$playerId"

        when:
        http.get {
            request.uri.path = path
        }

        then:
        def ex = thrown(HttpException)
        ex.statusCode == status


        where:
        sendServiceId   |sendPlayerId  |user          ||status
        false           |false         |"Player 1"    ||404
        true            |false         |"Player 1"    ||404
        false           |true          |"Player 1"    ||404
    }


    @Unroll
    def "check player's entries"()
    {
        given:
        def playerId = name2Id(user)
        def statId = name2Id(statName)
        def path = "/stats/$statId/player/$playerId"

        when:
        Map entry = http.get {
            request.uri.path = path
        }

        then:
        entry.player.id == playerId
        entry.stat.id == statId
        entry.score == score


        and:
        def backendEntry = Actor.getReference(StatService, statId)
                                .getEntry(Actor.getReference(shepard.backend.service.player.Player, playerId))
                                .join()

        backendEntry.player.join().identity == playerId
        backendEntry.service.join().identity == statId
        backendEntry.getStatValue().join() == entry.score


        where:
        user          ||score
        "Player 1"    ||0
        "Player 2"    ||0
        "Player 3"    ||0
        "Player 4"    ||0
    }

    @Unroll
    def "save entry"()
    {
        given:
        def playerId = name2Id(user)
        def statId = name2Id(statName)
        def path = "/stats/$statId/player/$playerId"

        when:
        Map entry = http.post {
            request.uri.path = path
            request.uri.query = [score:score]
        }

        then:
        entry.player.id == playerId
        entry.stat.id == statId
        entry.score == score


        and:
        def backendEntry = Actor.getReference(StatService, statId)
                                .getEntry(Actor.getReference(shepard.backend.service.player.Player, playerId))
                                .join()

        backendEntry.player.join().identity == playerId
        backendEntry.service.join().identity == statId
        backendEntry.getStatValue().join() == entry.score


        where:
        user          ||score
        "Player 1"    ||10
        "Player 2"    ||20.2
        "Player 3"    ||35.123
        "Player 4"    ||154.2
    }


    def "show all entries"()
    {
        when:
        List<StatEntry> entries = http.get {
            request.uri.path = path+'/entries'
        }

        then:
        entries.size() == 4
    }


    @Unroll
    def "show player's #user entry"()
    {
        given:
        def playerId = name2Id(user)

        when:
        Map entry = http.get {
            request.uri.path = path+"/player/$playerId"
        }

        then:
        entry.player.id == playerId
        entry.score == score

        where:
        user          ||score
        "Player 1"    ||10
        "Player 2"    ||20.2
        "Player 3"    ||35.123
        "Player 4"    ||154.2
    }

    @Unroll
    def "update entry"()
    {
        given:
        def playerId = name2Id(user)
        def statId = name2Id(statName)
        def path = "/stats/$statId/player/$playerId"

        when:
        Map entry = http.put {
            request.uri.path = path
            request.uri.query = [score:score]
        }

        then:
        entry.player.id == playerId
        entry.stat.id == statId
        entry.score == score


        and:
        def backendEntry = Actor.getReference(StatService, statId)
                                .getEntry(Actor.getReference(shepard.backend.service.player.Player, playerId))
                                .join()

        backendEntry.player.join().identity == playerId
        backendEntry.service.join().identity == statId
        backendEntry.getStatValue().join() == entry.score


        where:
        user          ||score
        "Player 1"    ||100.3124
        "Player 2"    ||200.3124
        "Player 3"    ||350.3124
        "Player 4"    ||400.3124
    }

    @Unroll
    def "update entry with bad values"()
    {
        given:
        def playerId = name2Id(user)
        def statId = name2Id(statName)
        def path = "/stats/$statId/player/$playerId"

        when:
        Map entry = http.put {
            request.uri.path = path
            request.uri.query = [score:score]
        }

        then:
        entry.player.id == playerId
        entry.stat.id == statId
        entry.score != score
        entry.score == oldScore


        and:
        def backendEntry = Actor.getReference(StatService, statId)
                .getEntry(Actor.getReference(shepard.backend.service.player.Player, playerId))
                .join()

        backendEntry.player.join().identity == playerId
        backendEntry.service.join().identity == statId
        backendEntry.getStatValue().join() == entry.score
        backendEntry.getStatValue().join() == oldScore
        backendEntry.getStatValue().join() != score


        where:
        user          |score    ||oldScore
        "Player 1"    |1001     ||100.3124
        "Player 2"    |-1       ||200.3124
        "Player 3"    |-0.1     ||350.3124
        "Player 4"    |1000.1   ||400.3124
    }


    def "remove stat"()
    {
        when:
        int status = http.delete {
            request.uri.path = path
            response.success { FromServer fs -> fs.statusCode}
        }

        then:
        status == 204
    }
}
