package shepard.frontend.rest.leaderboard

import cloud.orbit.actors.Actor
import groovyx.net.http.FromServer
import groovyx.net.http.HttpBuilder
import groovyx.net.http.HttpException
import shepard.backend.service.leaderboard.LeaderboardService
import shepard.backend.service.leaderboard.details.LeaderboardSort
import shepard.backend.service.leaderboard.details.LeaderboardType
import shepard.backend.service.leaderboard.manager.LeaderboardManager
import shepard.frontend.rest.AchievementEntry
import shepard.frontend.rest.Leaderboard
import shepard.frontend.rest.Player
import shepard.frontend.test.ShepardIntegrationSpec
import spock.lang.Shared
import spock.lang.Stepwise
import spock.lang.Unroll

import static groovyx.net.http.HttpBuilder.configure
import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 28/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class LeaderboardEntryIntegrationSpec extends ShepardIntegrationSpec
{
    @Shared HttpBuilder http
    @Shared String leaderboardName = 'Test Leaderboard'
    @Shared String path = "/leaderboards/${name2Id(leaderboardName)}"

    def setup()
    {
        http = configure {
            request.uri = baseUrl
            request.contentType = 'application/json'
            request.accept = 'application/json'
        }
    }

    def "create an achievement service"()
    {
        when:
        Leaderboard result = http.post {
            request.uri.path = '/leaderboards'
            request.body = [name: leaderboardName, lowerLimit: 0, upperLimit: 100]
        }

        then:
        result.name == leaderboardName
        result.lowerLimit == 0
        result.upperLimit == 100
        result.leaderboardType == LeaderboardType.ABSOLUTE
        result.leaderboardSort == LeaderboardSort.HIGH

        and:
        Actor.getReference(LeaderboardManager)
                .getAllEnabledServices().join().size() == 1

        Actor.getReference(LeaderboardManager)
                .getAllEnabledServices().join()
                .first().getServiceDetails().join() == result.generateDetailsFromDomain()
    }

    def "index entries"()
    {
        when:
        List<AchievementEntry> entries = http.get {
            request.uri.path = path+"/entries"
        }

        then:
        entries.size() == 0
    }

    @Unroll
    def "create players"()
    {
        when:
        Player player = http.post {
            request.uri.path = '/players'
            request.body = [user: user, password: pass]
        }

        then:
        assert player.rating == 1500
        assert player.games == 0

        where:
        user        |pass
        'Player 1'  |'12345'
        'Player 2'  |'12345'
        'Player 3'  |'12345'
        'Player 4'  |'12345'

    }

    @Unroll
    def "invalid url check player's entries"()
    {
        given:
        def playerId = sendPlayerId ? name2Id(user) : ''
        def achievementId = sendServiceId ? name2Id(leaderboardName) : ''
        def path = "/achievements/$achievementId/player/$playerId"

        when:
        http.get {
            request.uri.path = path
        }

        then:
        def ex = thrown(HttpException)
        ex.statusCode == status


        where:
        sendServiceId   |sendPlayerId  |user          ||status
        false           |false         |"Player 1"    ||404
        true            |false         |"Player 1"    ||404
        false           |true          |"Player 1"    ||404
    }


    @Unroll
    def "check player's entries"()
    {
        given:
        def playerId = name2Id(user)
        def leaderboardId = name2Id(leaderboardName)
        def path = "/leaderboards/$leaderboardId/player/$playerId"

        when:
        Map entry = http.get {
            request.uri.path = path
        }

        then:
        entry.player.id == playerId
        entry.leaderboard.id == leaderboardId
        entry.score == score
        entry.rank == rank
        entry.delta == delta

        and:
        def backendEntry = Actor.getReference(LeaderboardService, leaderboardId)
                                .getEntry(Actor.getReference(shepard.backend.service.player.Player, playerId))
                                .join()

        backendEntry.player.join().identity == playerId
        backendEntry.service.join().identity == leaderboardId
        backendEntry.score.join().rawScore.join() == score
        backendEntry.delta.join() == delta
        backendEntry.rank.join() == rank

        where:
        user          ||score   |rank   |delta
        "Player 1"    ||0       |0      |0
        "Player 2"    ||0       |0      |0
        "Player 3"    ||0       |0      |0
        "Player 4"    ||0       |0      |0
    }
    

    @Unroll
    def "save entry"()
    {
        given:
        def playerId = name2Id(user)
        def leaderboardId = name2Id(leaderboardName)
        def path = "/leaderboards/$leaderboardId/player/$playerId"

        when:
        Map entry = http.post {
            request.uri.path = path
            request.uri.query = [score: score]
        }

        then:
        entry.player.id == playerId
        entry.leaderboard.id == leaderboardId
        entry.score == score
        entry.rank == rank
        entry.delta == delta

        and:
        def backendEntry = Actor.getReference(LeaderboardService, leaderboardId)
                .getEntry(Actor.getReference(shepard.backend.service.player.Player, playerId))
                .join()

        backendEntry.player.join().identity == playerId
        backendEntry.service.join().identity == leaderboardId
        backendEntry.score.join().rawScore.join() == score
        backendEntry.delta.join() == delta
        backendEntry.rank.join() == rank

        where:
        user          ||score   |rank   |delta
        "Player 1"    ||10      |0      |0
        "Player 2"    ||8       |0      |0
        "Player 3"    ||75      |0      |0
        "Player 4"    ||21      |0      |0
    }


    def "show all entries"()
    {
        when:
        List<AchievementEntry> entries = http.get {
            request.uri.path = path+"/entries"
        }

        then:
        entries.size() == 4
    }

    def "perform a ranking operation"()
    {
        when:
        Integer status = http.put {
            request.uri.path = path+"/rank"
            response.success { FromServer fs -> fs.statusCode }
        }

        then:
        status == 200
    }

    @Unroll
    def "show player's #user entry"()
    {
        given:
        def leaderboardId = name2Id(leaderboardName)
        def playerId = name2Id(user)

        when:
        Map entry = http.get {
            request.uri.path = path+"/player/$playerId"
        }

        then:
        entry.player.id == playerId
        entry.score == score
        entry.rank == rank
        entry.delta == delta

        and:
        def backendEntry = Actor.getReference(LeaderboardService, leaderboardId)
                .getEntry(Actor.getReference(shepard.backend.service.player.Player, playerId))
                .join()

        backendEntry.player.join().identity == playerId
        backendEntry.service.join().identity == leaderboardId
        backendEntry.score.join().rawScore.join() == score
        backendEntry.delta.join() == delta
        backendEntry.rank.join() == rank

        where:
        user          ||score   |rank   |delta
        "Player 1"    ||10      |3      |-3
        "Player 2"    ||8       |4      |-4
        "Player 3"    ||75      |1      |-1
        "Player 4"    ||21      |2      |-2
    }


    @Unroll
    def "update entry"()
    {
        given:
        def leaderboardId = name2Id(leaderboardName)
        def playerId = name2Id(user)

        when:
        Map entry = http.put {
            request.uri.path = path+"/player/$playerId"
            request.uri.query = [score: score]
        }

        then:
        entry.player.id == playerId
        entry.score == score
        entry.rank == rank
        entry.delta == delta

        and:
        def backendEntry = Actor.getReference(LeaderboardService, leaderboardId)
                .getEntry(Actor.getReference(shepard.backend.service.player.Player, playerId))
                .join()

        backendEntry.player.join().identity == playerId
        backendEntry.service.join().identity == leaderboardId
        backendEntry.score.join().rawScore.join() == score
        backendEntry.delta.join() == delta
        backendEntry.rank.join() == rank

        where:
        user          ||score   |rank   |delta
        "Player 1"    ||42      |3      |-3
        "Player 2"    ||40      |4      |-4
        "Player 3"    ||85      |1      |-1
        "Player 4"    ||98      |2      |-2
    }

    def "perform a second ranking operation"()
    {
        when:
        Integer status = http.put {
            request.uri.path = path+"/rank"
            response.success { FromServer fs -> fs.statusCode }
        }

        then:
        status == 200
    }

    @Unroll
    def "second show player's #user entry"()
    {
        given:
        def leaderboardId = name2Id(leaderboardName)
        def playerId = name2Id(user)

        when:
        Map entry = http.get {
            request.uri.path = path+"/player/$playerId"
        }

        then:
        entry.player.id == playerId
        entry.score == score
        entry.rank == rank
        entry.delta == delta

        and:
        def backendEntry = Actor.getReference(LeaderboardService, leaderboardId)
                .getEntry(Actor.getReference(shepard.backend.service.player.Player, playerId))
                .join()

        backendEntry.player.join().identity == playerId
        backendEntry.service.join().identity == leaderboardId
        backendEntry.score.join().rawScore.join() == score
        backendEntry.delta.join() == delta
        backendEntry.rank.join() == rank

        where:
        user          ||score   |rank   |delta
        "Player 1"    ||42      |3      |0
        "Player 2"    ||40      |4      |0
        "Player 3"    ||85      |2      |-1
        "Player 4"    ||98      |1      |1
    }

    @Unroll
    def "update entry with invalid scores"()
    {
        given:
        def leaderboardId = name2Id(leaderboardName)
        def playerId = name2Id(user)

        when:
        Map entry = http.put {
            request.uri.path = path+"/player/$playerId"
            request.uri.query = [score: score]
        }

        then:
        entry.player.id == playerId
        entry.score != score
        entry.score == oldScore
        entry.rank == rank
        entry.delta == delta

        and:
        def backendEntry = Actor.getReference(LeaderboardService, leaderboardId)
                .getEntry(Actor.getReference(shepard.backend.service.player.Player, playerId))
                .join()

        backendEntry.player.join().identity == playerId
        backendEntry.service.join().identity == leaderboardId
        backendEntry.score.join().rawScore.join() != score
        backendEntry.score.join().rawScore.join() == oldScore
        backendEntry.delta.join() == delta
        backendEntry.rank.join() == rank

        where:
        user        |score     ||oldScore   |rank   |delta
        "Player 1"  |-1        ||42         |3      |0
        "Player 2"  |101       ||40         |4      |0
        "Player 3"  |1256      ||85         |2      |-1
        "Player 4"  |-1000     ||98         |1      |1
    }

    @Unroll
    def "get ranking #start - #end"()
    {
        when:
        int status = http.get {
            request.uri.path = path+"/ranking"
            request.uri.query = [start: start, end:end]
            response.success { FromServer fs -> fs.statusCode }
            response.failure { FromServer fs -> fs.statusCode }
        }

        then:
        status == respStatus

        and:
        if (status == 200)
        {
            List resp = http.get {
                request.uri.path = path + "/ranking"
                request.uri.query = [start: start, end: end]
            }

            assert resp.size() == size
            def ids = resp.collect { it.player }.collect { it.id }
            assert ids.containsAll(players.collect { name2Id(it) })
            //Check ranking order
            ids.eachWithIndex { def entry, int i -> assert entry == name2Id(players[i]) }
        }

        where:
        start   |end    ||respStatus    |size   |players
        0       |4      ||200           |4      |['Player 4', 'Player 3', 'Player 1', 'Player 2']
        0       |10     ||200           |4      |['Player 4', 'Player 3', 'Player 1', 'Player 2']
        2       |10     ||200           |2      |['Player 1', 'Player 2']
        10      |20     ||400           |null   |null
        -4      |2      ||200           |2      |['Player 4', 'Player 3']
        10      |5      ||422           |null   |null
    }

    def "delete leaderboard"()
    {
        when:
        int status = http.delete {
            request.uri.path = path
            response.success { FromServer fs -> fs.statusCode }
        }

        then:
        status == 204
    }

}
