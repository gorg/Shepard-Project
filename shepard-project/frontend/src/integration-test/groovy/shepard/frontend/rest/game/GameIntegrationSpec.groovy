package shepard.frontend.rest.game

import cloud.orbit.actors.Actor
import groovyx.net.http.FromServer
import groovyx.net.http.HttpBuilder
import shepard.backend.service.matchmaking.RoomService
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.frontend.rest.Player
import shepard.frontend.rest.Room
import shepard.frontend.test.ShepardIntegrationSpec
import spock.lang.Shared
import spock.lang.Unroll

import static groovyx.net.http.HttpBuilder.configure
import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 08/05/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class GameIntegrationSpec extends ShepardIntegrationSpec
{
    @Shared HttpBuilder http
    @Shared String path = '/game'
    @Shared String roomName = 'Game Test Room'
    @Shared String roomId = name2Id(roomName)
    @Shared String lobbyId = "${name2Id(roomName)}:lobby0"

    def setup()
    {
        http = configure {
            request.uri = baseUrl
            request.contentType = 'application/json'
            request.accept = 'application/json'
        }
    }

    def "create a room"()
    {
        when:
        Room result = http.post {
            request.uri.path = '/rooms'
            request.body = [name: roomName, teams: 2, players: 4, ranked:true, overlap:0.75]
        }

        then:
        result.teams == 2
        result.players == 4
        result.ranked == true
        result.overlap == 0.75

        and:
        Actor.getReference(RoomService, name2Id(roomName)).getServiceDetails().join() == result.generateDetailsFromDomain()
    }

    @Unroll
    def "create #name player"()
    {
        given:
        def actor = Actor.getReference(shepard.backend.service.player.Player, name2Id(name))

        when:
        Player result = http.post {
            request.uri.path = "/players"
            request.body = [user: name, password: pass]
        }

        then:
        result.games == 0
        result.rating == 1500

        and:
        actor.isPlayerSet().join()
        actor.validatePlayer(name,pass).join().identity == name2Id(name)
        actor.getCurrentLobby().join() == null
        actor.getCurrentGame().join() ==  null

        where:
        name               |pass
        'Game Player 1'    |'12345'
        'Game Player 2'    |'12345'
        'Game Player 3'    |'12345'
        'Game Player 4'    |'12345'
    }

    @Unroll
    def "player #name enters lobby"()
    {
        when:
        Map result = http.put {
            request.uri.path = "/rooms/$roomId/enter"
            request.uri.query = [playerId: [name2Id(name)]]
        }

        then:
        result.id == lobbyId
        result.rating == 1500
        result.deviation == 350
        result.players.id.containsAll(players.collect { name2Id(it)})

        where:
        name               ||players
        'Game Player 1'    ||['Game Player 1']
        'Game Player 2'    ||['Game Player 1','Game Player 2']
        'Game Player 3'    ||['Game Player 1','Game Player 2','Game Player 3']
        'Game Player 4'    ||['Game Player 1','Game Player 2','Game Player 3','Game Player 4']
    }

    def "index lobbies"()
    {
        when:
        List<Map> result = http.get {
            request.uri.path = "/rooms/$roomId/lobbies"
        }

        then:
        result.size() == 1

        and:
        result[0].id == lobbyId
    }

    def "show teams"()
    {
        when:
        Map result = http.get {
            request.uri.path = "/rooms/$roomId/lobbies/$lobbyId/team"
        }

        then:
        result."0".empty
        result."1".empty
    }

    @Unroll
    def "player #name enters team #team"()
    {
        when:
        Map result = http.put {
            request.uri.path = "/rooms/$roomId/lobbies/$lobbyId/team"
            request.uri.query = [playerId : name2Id(name), team: team]
        }

        then:
        result."0".id.containsAll ( members."0".collect{name2Id(it)})
        result."1".id.containsAll ( members."1".collect{name2Id(it)})

        where:
        name            |team   ||members
        "Game Player 2" |0      ||["0":["Game Player 2"], "1":[]]
        "Game Player 1" |0      ||["0":["Game Player 2","Game Player 1"], "1":[]]
        "Game Player 4" |1      ||["0":["Game Player 2","Game Player 1"], "1":["Game Player 4"]]
    }


    def "show game before it has started"()
    {
        when:
        int status = http.get {
            request.uri.path = "/rooms/$roomId/lobbies/$lobbyId/game"
            response.failure { FromServer fs -> fs.statusCode }
        }

        then:
        status == 404
    }

    def "start the game"()
    {
        when:
        Map result = http.post {
            request.uri.path = "/rooms/$roomId/lobbies/$lobbyId/game"
        }

        then:
        result.finished == false
        result.lobby.rating == 1500
        result.lobby.deviation == 350
        result.lobby.players.id.containsAll(['Game Player 1','Game Player 2','Game Player 3','Game Player 4'].collect { name2Id(it)})
    }

    def "post a draw"()
    {
        given:
        def players = ['Game Player 1','Game Player 2','Game Player 3','Game Player 4'].collect{name2Id(it)}

        when:
        Map result = http.put {
            request.uri.path = "/game/$lobbyId"
            request.uri.query = [draw: true]
        }

        then:
        result.finished == false
        result.lobby.id == lobbyId
        players.each { playerId ->
            assert result.players."$playerId".draws == 1
            assert result.players."$playerId".rounds == 1
            assert result.players."$playerId".score == 0.5
            assert result.players."$playerId".wins == 0
            assert result.players."$playerId".loses == 0
        }
    }

    def "post a win for first team"()
    {
        given:
        def players = ['Game Player 1','Game Player 2','Game Player 3','Game Player 4'].collect{name2Id(it)}

        when:
        Map result = http.put {
            request.uri.path = "/game/$lobbyId"
            request.uri.query = [winner: 0]
        }

        then:
        result.finished == false
        result.lobby.id == lobbyId
        players.each { playerId ->
            if (playerId == "game-player-1" || playerId == "game-player-2")
            {
                assert result.players."$playerId".draws == 1
                assert result.players."$playerId".rounds == 2
                assert result.players."$playerId".score == 1.5
                assert result.players."$playerId".wins == 1
                assert result.players."$playerId".loses == 0
            }
            else
            {
                assert result.players."$playerId".draws == 1
                assert result.players."$playerId".rounds == 2
                assert result.players."$playerId".score == 0.5
                assert result.players."$playerId".wins == 0
                assert result.players."$playerId".loses == 1
            }
        }
    }

    def "post a win for second team"()
    {
        given:
        def players = ['Game Player 1','Game Player 2','Game Player 3','Game Player 4'].collect{name2Id(it)}

        when:
        Map result = http.put {
            request.uri.path = "/game/$lobbyId"
            request.uri.query = [winner: 1]
        }

        then:
        result.finished == false
        result.lobby.id == lobbyId
        players.each { playerId ->
            assert result.players."$playerId".draws == 1
            assert result.players."$playerId".rounds == 3
            assert result.players."$playerId".score == 1.5
            assert result.players."$playerId".wins == 1
            assert result.players."$playerId".loses == 1
        }
    }

    def "post another draw"()
    {
        given:
        def players = ['Game Player 1','Game Player 2','Game Player 3','Game Player 4'].collect{name2Id(it)}

        when:
        Map result = http.put {
            request.uri.path = "/game/$lobbyId"
            request.uri.query = [draw: true]
        }

        then:
        result.finished == false
        result.lobby.id == lobbyId
        players.each { playerId ->
            assert result.players."$playerId".draws == 2
            assert result.players."$playerId".rounds == 4
            assert result.players."$playerId".score == 2
            assert result.players."$playerId".wins == 1
            assert result.players."$playerId".loses == 1
        }
    }

    def "post another win for first team"()
    {
        given:
        def players = ['Game Player 1','Game Player 2','Game Player 3','Game Player 4'].collect{name2Id(it)}

        when:
        Map result = http.put {
            request.uri.path = "/game/$lobbyId"
            request.uri.query = [winner: 0]
        }

        then:
        result.finished == false
        result.lobby.id == lobbyId
        players.each { playerId ->
            if (playerId == "game-player-1" || playerId == "game-player-2")
            {
                assert result.players."$playerId".draws == 2
                assert result.players."$playerId".rounds == 5
                assert result.players."$playerId".score == 3
                assert result.players."$playerId".wins == 2
                assert result.players."$playerId".loses == 1
            }
            else
            {
                assert result.players."$playerId".draws == 2
                assert result.players."$playerId".rounds == 5
                assert result.players."$playerId".score == 2
                assert result.players."$playerId".wins == 1
                assert result.players."$playerId".loses == 2
            }
        }
    }

    def "show game"()
    {
        given:
        def players = ['Game Player 1','Game Player 2','Game Player 3','Game Player 4'].collect{name2Id(it)}
        def teams = [["game-player-1", "game-player-2"],["game-player-3", "game-player-4"]]

        when:
        Map result = http.get {
            request.uri.path = "/rooms/$roomId/lobbies/$lobbyId/game"
        }

        then:

        result.finished == false
        result.lobby.id == lobbyId
        players.each { playerId ->
            if (playerId == "game-player-1" || playerId == "game-player-2")
            {
                assert result.players."$playerId".draws == 2
                assert result.players."$playerId".rounds == 5
                assert result.players."$playerId".score == 3
                assert result.players."$playerId".wins == 2
                assert result.players."$playerId".loses == 1
            }
            else
            {
                assert result.players."$playerId".draws == 2
                assert result.players."$playerId".rounds == 5
                assert result.players."$playerId".score == 2
                assert result.players."$playerId".wins == 1
                assert result.players."$playerId".loses == 2
            }
        }
        teams.each { teamsId ->
            if (teamsId == ["game-player-1", "game-player-2"])
            {
                assert result.teams."$teamsId".draws == 2
                assert result.teams."$teamsId".rounds == 5
                assert result.teams."$teamsId".score == 3
                assert result.teams."$teamsId".wins == 2
                assert result.teams."$teamsId".loses == 1
            }
            else
            {
                assert result.teams."$teamsId".draws == 2
                assert result.teams."$teamsId".rounds == 5
                assert result.teams."$teamsId".score == 2
                assert result.teams."$teamsId".wins == 1
                assert result.teams."$teamsId".loses == 2
            }
        }
    }


    def "finish game"()
    {
        given:
        def players = ['Game Player 1','Game Player 2','Game Player 3','Game Player 4'].collect{name2Id(it)}
        def teams = [["game-player-1", "game-player-2"],["game-player-3", "game-player-4"]]

        when:
        Map result = http.delete {
            request.uri.path = "/game/$lobbyId"
        }

        then:
        result.finished == true
        result.lobby.id == lobbyId
        players.each { playerId ->
            if (playerId == "game-player-1" || playerId == "game-player-2")
            {
                assert result.players."$playerId".draws == 2
                assert result.players."$playerId".rounds == 5
                assert result.players."$playerId".score == 3
                assert result.players."$playerId".wins == 2
                assert result.players."$playerId".loses == 1
            }
            else
            {
                assert result.players."$playerId".draws == 2
                assert result.players."$playerId".rounds == 5
                assert result.players."$playerId".score == 2
                assert result.players."$playerId".wins == 1
                assert result.players."$playerId".loses == 2
            }
        }
        teams.each { teamsId ->
            if (teamsId == ["game-player-1", "game-player-2"])
            {
                assert result.teams."$teamsId".draws == 2
                assert result.teams."$teamsId".rounds == 5
                assert result.teams."$teamsId".score == 3
                assert result.teams."$teamsId".wins == 2
                assert result.teams."$teamsId".loses == 1
            }
            else
            {
                assert result.teams."$teamsId".draws == 2
                assert result.teams."$teamsId".rounds == 5
                assert result.teams."$teamsId".score == 2
                assert result.teams."$teamsId".wins == 1
                assert result.teams."$teamsId".loses == 2
            }
        }

        and:
        players.each {
            def actor = Actor.getReference(shepard.backend.service.player.Player, it)
            assert actor.getCurrentLobby().join() == null
            assert actor.getCurrentGame().join() == null
        }

        and:
        assert Actor.getReference(RoomManager).getAllEnabledServices().join().size() == 1
        assert Actor.getReference(RoomManager).getAllEnabledServices().join().first().getLobbies().join().size() == 0
    }

    @Unroll
    def "check #player ratings"()
    {
        given:
        def id = name2Id(player)
        def actor = Actor.getReference(shepard.backend.service.player.Player, id)

        expect:
        actor.getSkill().join().rating.join().round(2).toBigDecimal() == rating
        actor.getSkill().join().ratingDeviation.join().round(2).toBigDecimal() == dev
        actor.getSkill().join().numberOfResults.join() == rounds
        actor.getCurrentGame().join() == null
        actor.getCurrentLobby().join() == null

        when:
        Map result = http.get {
            request.uri.path = "/players/$id"
        }

        then:
        (result.rating as Double).round(2).toBigDecimal() == rating
        result.games == rounds

        where:
        player              ||rating    |dev        |rounds
        'Game Player 1'     ||1585.13   |148.67     |10
        'Game Player 2'     ||1585.13   |148.67     |10
        'Game Player 3'     ||1414.87   |148.67     |10
        'Game Player 4'     ||1414.87   |148.67     |10
    }

    def "delete room"()
    {
        when:
        http.delete {
            request.uri.path = "/rooms/$roomId"
        }

        then:
        Actor.getReference(RoomManager).getAllEnabledServices().join().size() == 0
    }
}
