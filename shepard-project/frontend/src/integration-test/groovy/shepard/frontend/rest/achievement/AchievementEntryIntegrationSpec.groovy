package shepard.frontend.rest.achievement

import cloud.orbit.actors.Actor
import groovyx.net.http.FromServer
import groovyx.net.http.HttpBuilder
import groovyx.net.http.HttpException
import shepard.backend.service.achievement.AchievementService
import shepard.backend.service.achievement.manager.AchievementManager
import shepard.frontend.rest.Achievement
import shepard.frontend.rest.AchievementEntry
import shepard.frontend.rest.Player
import shepard.frontend.test.ShepardIntegrationSpec
import spock.lang.Shared
import spock.lang.Unroll

import java.time.LocalDateTime

import static groovyx.net.http.HttpBuilder.configure
import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 28/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class AchievementEntryIntegrationSpec extends ShepardIntegrationSpec
{
    @Shared HttpBuilder http
    @Shared String achievementName = 'Test Achievement'
    @Shared String path = "/achievements/${name2Id(achievementName)}"

    def setup()
    {
        http = configure {
            request.uri = baseUrl
            request.contentType = 'application/json'
            request.accept = 'application/json'
        }
    }

    def "create an achievement service"()
    {
        when:
        Achievement result = http.post {
            request.uri.path = '/achievements'
            request.body = [name: achievementName, description: 'Testing', type: 'REVEALED']
        }

        then:
        result.name == achievementName
        result.description == 'Testing'
        result.type.toString() == 'REVEALED'

        and:
        Actor.getReference(AchievementManager)
                .getAllEnabledServices().join().size() == 1

        Actor.getReference(AchievementManager)
                .getAllEnabledServices().join()
                .first().getServiceDetails().join() == result.generateDetailsFromDomain()
    }

    def "index entries"()
    {
        when:
        List<AchievementEntry> entries = http.get {
            request.uri.path = path+"/entries"
        }

        then:
        entries.size() == 0
    }

    @Unroll
    def "create players"()
    {
        when:
        Player player = http.post {
            request.uri.path = '/players'
            request.body = [user: user, password: pass]
        }

        then:
        assert player.rating == 1500
        assert player.games == 0

        where:
        user        |pass
        'Player 1'  |'12345'
        'Player 2'  |'12345'
        'Player 3'  |'12345'
        'Player 4'  |'12345'

    }

    @Unroll
    def "invalid url check player's entries"()
    {
        given:
        def playerId = sendPlayerId ? name2Id(user) : ''
        def achievementId = sendServiceId ? name2Id(achievementName) : ''
        def path = "/achievements/$achievementId/player/$playerId"

        when:
        http.get {
            request.uri.path = path
        }

        then:
        def ex = thrown(HttpException)
        ex.statusCode == status


        where:
        sendServiceId   |sendPlayerId  |user          ||status
        false           |false         |"Player 1"    ||404
        true            |false         |"Player 1"    ||404
        false           |true          |"Player 1"    ||404
    }

    @Unroll
    def "check player's entries"()
    {
        given:
        def playerId = name2Id(user)
        def achievementId = name2Id(achievementName)
        def path = "/achievements/$achievementId/player/$playerId"

        when:
        Map entry = http.get {
            request.uri.path = path
        }

        then:
        entry.player.id == playerId
        entry.achievement.id == achievementId
        entry.completed == completed
        entry.when == null

        and:
        def backendEntry = Actor.getReference(AchievementService, achievementId)
                                .getEntry(Actor.getReference(shepard.backend.service.player.Player, playerId))
                                .join()

        backendEntry.player.join().identity == playerId
        backendEntry.service.join().identity == achievementId
        backendEntry.isCompleted().join() == completed
        backendEntry.whenWasCompleted().join() == null

        where:
        user          ||completed
        "Player 1"    ||false
        "Player 2"    ||false
        "Player 3"    ||false
        "Player 4"    ||false
    }

    @Unroll
    def "save entry"()
    {
        given:
        def playerId = name2Id(user)
        def achievementId = name2Id(achievementName)
        def path = "/achievements/$achievementId/player/$playerId"

        when:
        Map entry = http.post {
            request.uri.path = path
        }

        then:
        entry.player.id == playerId
        entry.achievement.id == achievementId
        entry.completed == completed

        and:
        def backendEntry = Actor.getReference(AchievementService, achievementId)
                .getEntry(Actor.getReference(shepard.backend.service.player.Player, playerId))
                .join()

        backendEntry.player.join().identity == playerId
        backendEntry.service.join().identity == achievementId
        backendEntry.isCompleted().join() == completed
        backendEntry.whenWasCompleted().join() == LocalDateTime.parse(entry.when)

        where:
        user          ||completed
        "Player 1"    ||true
        "Player 2"    ||true
        "Player 3"    ||true
        "Player 4"    ||true
    }


    def "show all entries"()
    {
        when:
        List<AchievementEntry> entries = http.get {
            request.uri.path = path+'/entries'
        }

        then:
        entries.size() == 4
    }


    @Unroll
    def "show player's #user entry"()
    {
        given:
        def playerId = name2Id(user)

        when:
        Map entry = http.get {
            request.uri.path = path+"/player/$playerId"
        }

        then:
        entry.player.id == playerId
        entry.completed == true

        where:
        user << [ 'Player 1', 'Player 2', 'Player 3', 'Player 4' ]
    }


    @Unroll
    def "update entry"()
    {
        given:
        def playerId = name2Id(user)

        when:
        Map result = http.put {
            request.uri.path = path+"/player/$playerId"
        }

        then:
        result.player.id == playerId
        result.achievement.id == name2Id(achievementName)
        result.completed == true

        and:
        assert Actor.getReference(AchievementService, name2Id(achievementName))
                .getEntry(Actor.getReference(shepard.backend.service.player.Player, playerId)).join()
                .isCompleted().join()

        assert Actor.getReference(AchievementService, name2Id(achievementName))
                .getEntry(Actor.getReference(shepard.backend.service.player.Player, playerId)).join()
                .whenWasCompleted().join() == LocalDateTime.parse(result.when)

        where:
        user << [ 'Player 1', 'Player 2', 'Player 3', 'Player 4' ]
    }

    def "remove achievement"()
    {
        when:
        int status = http.delete {
            request.uri.path = path
            response.success { FromServer fs -> fs.statusCode}
        }

        then:
        status == 204
    }
}
