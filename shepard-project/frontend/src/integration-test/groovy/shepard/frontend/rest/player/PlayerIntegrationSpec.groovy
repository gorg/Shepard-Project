package shepard.frontend.rest.player

import cloud.orbit.actors.Actor
import groovyx.net.http.FromServer
import groovyx.net.http.HttpBuilder
import groovyx.net.http.HttpException
import shepard.frontend.rest.Player
import shepard.frontend.test.ShepardIntegrationSpec
import spock.lang.Shared
import spock.lang.Unroll

import static groovyx.net.http.HttpBuilder.configure
import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 29/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/

class PlayerIntegrationSpec extends ShepardIntegrationSpec
{
    @Shared HttpBuilder http
    @Shared String path = '/players'

    def setup()
    {
        http = configure {
            request.uri = baseUrl
            request.contentType = 'application/json'
            request.accept = 'application/json'
        }
    }

    def "no index"()
    {
        when:
        http.get { request.uri.path = path }

        then:
        def ex = thrown(HttpException)

        and:
        ex.statusCode == 404
    }

    @Unroll
    def "create invalid players"()
    {
        when:
        http.post {
            request.uri.path = path
            request.body = [user: name, password: pass]
        }

        then:
        def ex = thrown(HttpException)

        and:
        ex.statusCode == 400

        where:
        name    |pass
        null    |null
        null    |''
        ''      |null
        'a'     |null
        'a'     |''
        null    |'a'
        ''      |'a'
    }

    @Unroll
    def "create #name player"()
    {
        given:
        def actor = Actor.getReference(shepard.backend.service.player.Player, name2Id(name))

        when:
        Player result = http.post {
            request.uri.path = path
            request.body = [user: name, password: pass]
        }

        then:
        result.games == 0
        result.rating == 1500

        and:
        assert actor.isPlayerSet().join()
        assert actor.validatePlayer(name,pass).join().identity == name2Id(name)

        where:
        name               |pass
        'Test Player 1'    |'12345'
        'Test Player 2'    |'12345'
        'Test Player 3'    |'12345'
        'Test Player 4'    |'12345'
    }

    def "change #name password"()
    {
        given:
        def actor = Actor.getReference(shepard.backend.service.player.Player, name2Id(name))

        when:
        Player result = http.put {
            request.uri.path = path+"/${name2Id(name)}"
            request.uri.query = [user: name, password: newPass, oldPassword: oldPass]
        }

        then:
        result.games == 0
        result.rating == 1500

        and:
        assert actor.isPlayerSet().join()
        assert actor.validatePlayer(name,newPass).join().identity == name2Id(name)
        assert actor.validatePlayer(name,oldPass).join() == null

        where:
        name               |oldPass  |newPass
        'Test Player 1'    |'12345'  |'2'
    }

    @Unroll
    def "invalid password change"()
    {
        when:
        http.put {
            request.uri.path = path+"/${name2Id(name)}"
            request.uri.query = [user: name, password: newPass, oldPassword: oldPass]
        }

        then:
        def ex = thrown(HttpException)
        ex.statusCode == status

        where:
        name               |oldPass  |newPass    |status
        'Test Player 2'    |'12345'  |''         |400
        'Test Player 2'    |''       |''         |400
        'Test Player 2'    |''       |'1234'     |400
        ''                 |'12345'  |''         |400
    }

    @Unroll
    def "show #name player"()
    {
        when:
        Map result = http.get {
            request.uri.path = path+"/${name2Id(name)}"
        }

        then:
        result.id == name2Id(name)
        result.rating == 1500.0
        result.games == 0

        where:
        name << ['Test Player 1','Test Player 2','Test Player 3','Test Player 4']
    }


    @Unroll
    def "delete #name"()
    {
        given:
        def actor = Actor.getReference(shepard.backend.service.player.Player, name2Id(name))

        when:
        def result = http.delete {
            request.uri.path = path+"/${name2Id(name)}"
            request.uri.query = [user: name, password: pass]
            response.success { FromServer fs -> fs.statusCode }
        }

        then:
        result == 204

        and:
        assert !actor.isPlayerSet().join()

        where:
        name               |pass
        'Test Player 1'    |'2'
        'Test Player 2'    |'12345'
        'Test Player 3'    |'12345'
        'Test Player 4'    |'12345'
    }
}
