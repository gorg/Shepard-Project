package shepard.frontend.rest.room

import cloud.orbit.actors.Actor
import groovyx.net.http.FromServer
import groovyx.net.http.HttpBuilder
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.frontend.rest.Lobby
import shepard.frontend.rest.Player
import shepard.frontend.rest.Room
import shepard.frontend.test.ShepardIntegrationSpec
import spock.lang.Shared
import spock.lang.Stepwise
import spock.lang.Unroll

import static groovyx.net.http.HttpBuilder.configure
import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 02/05/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class LobbyIntegrationSpec extends ShepardIntegrationSpec
{
    @Shared HttpBuilder http
    @Shared String roomName = 'Test Room'
    @Shared String path = "/rooms/${name2Id(roomName)}"

    def setup()
    {
        http = configure {
            request.uri = baseUrl
            request.contentType = 'application/json'
            request.accept = 'application/json'
        }
    }

    def "create a room service"()
    {
        when:
        Room res = http.post {
            request.uri.path = '/rooms'
            request.body = [name: roomName, players: 2, teams: 2 ]
        }

        then:
        res.name == roomName
        res.players == 2
        res.teams == 2
        res.overlap == 0.75
        res.ranked == true

        and:
        Actor.getReference(RoomManager)
                .getAllEnabledServices().join().size() == 1

        Actor.getReference(RoomManager)
                .getAllEnabledServices().join()
                .first().getServiceDetails().join() == res.generateDetailsFromDomain()
    }

    @Unroll
    def "create players"()
    {
        when:
        Player player = http.post {
            request.uri.path = '/players'
            request.body = [user: user, password: pass]
        }

        then:
        assert player.rating == 1500
        assert player.games == 0

        where:
        user        |pass
        'Player 1'  |'12345'
        'Player 2'  |'12345'
        'Player 3'  |'12345'
        'Player 4'  |'12345'
        'Player 5'  |'12345'
    }

    @Unroll
    def "change players ratings"()
    {
        given:
        Actor.getReference(shepard.backend.service.player.Player, name2Id(user)).getSkill().join().setRating(rating).join()
        def id = name2Id(user)

        when:
        Player player = http.get {
            request.uri.path = "/players/$id"
            request.uri.query = [user: user]
        }

        then:
        assert player.rating == rating
        assert player.games == 0

        where:
        user        |rating
        'Player 1'  |3125
        'Player 2'  |3000
        'Player 3'  |2500
        'Player 4'  |2600
        'Player 5'  |1255
    }

    def "query lobbies"()
    {
        when:
        List<Lobby> res = http.get {
            request.uri.path = path+"/lobbies"
        }

        then:
        res.size() == 0
    }

    @Unroll
    def "check player #user is in a lobby"()
    {
        given:
        def id = name2Id(user)

        when:
        def res = http.get {
            request.uri.path = path+"/enter"
            request.uri.query = [playerId: id]
            response.success { FromServer fs -> fs.statusCode }
            response.failure { FromServer fs -> fs.statusCode }
        }

        then:
        res == 404  //Non of the player is in a lobby

        and:
        Actor.getReference(shepard.backend.service.player.Player,id).getCurrentLobby().join() == null
        Actor.getReference(shepard.backend.service.player.Player,id).getCurrentGame().join() == null

        where:
        user << ['Player 1', 'Player 2', 'Player 3', 'Player 4', 'Player 5']
    }

    @Unroll
    def "player #user enter lobby"()
    {
        given:
        def id = name2Id(user)

        when:
        Map res = http.put {
            request.uri.path = path+"/enter"
            request.uri.query = [playerId: id]
        }

        then:
        res.room.id == name2Id(roomName)
        res.players.id.containsAll( inLobby.collect { name2Id(it) } )
        Actor.getReference(shepard.backend.service.player.Player,id).getCurrentLobby().join().identity == res.id
        res.rating  == inLobby.collect {
            Actor.getReference(shepard.backend.service.player.Player,name2Id(it))
                    .getSkill().join()
                    .getRating().join()
            }.sum() / inLobby.size()

        res.deviation  == inLobby.collect {
            Actor.getReference(shepard.backend.service.player.Player,name2Id(it))
                    .getSkill().join()
                    .getRatingDeviation().join()
        }.sum() / inLobby.size()

        and:
        Actor.getReference(shepard.backend.service.player.Player,id).getCurrentGame().join() == null


        where:
        user        |inLobby
        'Player 1'  |['Player 1']
        'Player 2'  |['Player 1', 'Player 2']
        'Player 3'  |['Player 3']
        'Player 4'  |['Player 3', 'Player 4']
        'Player 5'  |['Player 5']
    }

    def "check with backend room service"()
    {
        expect:
        Actor.getReference(shepard.backend.service.matchmaking.RoomService, name2Id(roomName))
                .getLobbies().join().size() == 3
    }

    @Unroll
    def "player #user exit lobby"()
    {
        given:
        def id = name2Id(user)

        when:
        Map res = http.delete {
            request.uri.path = path+"/enter"
            request.uri.query = [playerId: id]
        }

        then:
        println res
        res.room.id == name2Id(roomName)
        res.players.id.containsAll( inLobby.collect { name2Id(it) } )

        and:
        Actor.getReference(shepard.backend.service.player.Player,id).getCurrentLobby().join() == null
        Actor.getReference(shepard.backend.service.player.Player,id).getCurrentGame().join() == null

        where:
        user        |inLobby
        'Player 1'  |['Player 2']
        'Player 2'  |[]
        'Player 3'  |['Player 4']
        'Player 4'  |[]
        'Player 5'  |[]
    }

    def "check"()
    {
        expect:
        Actor.getReference(shepard.backend.service.matchmaking.RoomService, name2Id(roomName))
                .getLobbies().join().size() == 0
    }


    @Unroll
    def "player #user re-enter lobby"()
    {
        given:
        def id = name2Id(user)

        when:
        Map res = http.put {
            request.uri.path = path+"/enter"
            request.uri.query = [playerId: id]
        }

        then:
        res.room.id == name2Id(roomName)
        res.players.id.containsAll( inLobby.collect { name2Id(it) } )
        res.rating  == inLobby.collect {
            Actor.getReference(shepard.backend.service.player.Player,name2Id(it))
                    .getSkill().join()
                    .getRating().join()
        }.sum() / inLobby.size()
        
        res.deviation  == inLobby.collect {
            Actor.getReference(shepard.backend.service.player.Player,name2Id(it))
                    .getSkill().join()
                    .getRatingDeviation().join()
        }.sum() / inLobby.size()
        
        and:
        Actor.getReference(shepard.backend.service.player.Player,id).getCurrentGame().join() == null
        Actor.getReference(shepard.backend.service.player.Player,id).getCurrentLobby().join().identity == res.id

        where:
        user        |inLobby
        'Player 5'  |['Player 5']
        'Player 3'  |['Player 3']
        'Player 4'  |['Player 3', 'Player 4']
        'Player 1'  |['Player 1']
        'Player 2'  |['Player 1', 'Player 2']
    }

    def "query lobbies "()
    {
        when:
        List<Lobby> res = http.get {
            request.uri.path = path+"/lobbies"
        }

        then:
        res.each { println it }
        res.size() == 3
    }
}
