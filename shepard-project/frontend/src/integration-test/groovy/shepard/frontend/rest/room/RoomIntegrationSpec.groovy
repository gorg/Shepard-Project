package shepard.frontend.rest.room

import cloud.orbit.actors.Actor
import groovyx.net.http.FromServer
import groovyx.net.http.HttpBuilder
import shepard.backend.service.matchmaking.RoomService
import shepard.backend.service.matchmaking.manager.RoomManager
import shepard.frontend.rest.Room
import shepard.frontend.test.ShepardIntegrationSpec
import spock.lang.Shared
import spock.lang.Stepwise
import spock.lang.Unroll

import static groovyx.net.http.HttpBuilder.configure
import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 26/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Stepwise
class RoomIntegrationSpec extends ShepardIntegrationSpec
{
    @Shared HttpBuilder http
    @Shared String path = '/rooms'

    def setup()
    {
        http = configure {
            request.uri = baseUrl
            request.contentType = 'application/json'
            request.accept = 'application/json'
        }
    }

    def "empty index"()
    {
        when:
        List<Room> result = http.get {
            request.uri.path = path
        }

        then:
        result.isEmpty()
    }

    @Unroll
    def "create invalid rooms"()
    {
        when:
        int status = http.post {
            request.uri.path = path
            request.body = [name: name, teams: t, players: p, ranked:r, overlap:o]
            response.failure { FromServer fs -> fs.statusCode }
        }

        then:
        status == respStatus

        where:
        name    |t    |p      |r      |o        || respStatus
        null    |null |null   |null   |null     || 400
        't'     |9    |null   |null   |null     || 422 // Teams > 8
        't'     |2    |65     |null   |null     || 422 // Players > 64
        't'     |2    |2      |true   |1.5      || 422 // overlap > 1
    }

    @Unroll
    def "create #name room"()
    {
        given:
        def actor = Actor.getReference(RoomService, name2Id(name))

        when:
        Room result = http.post {
            request.uri.path = path
            request.body = [name: name, teams: t, players: p, ranked:r, overlap:o]
        }

        then:
        result.name == name
        result.teams == (t ?: 2)
        result.players == (p ?: 8)
        result.ranked == (r == null ? true : r)
        result.overlap == (o ?: 0.75)


        and:
        actor.getServiceDetails().join() == result.generateDetailsFromDomain()
        actor.isEnabled().join()

        where:
        name        |t    |p      |r      |o        
        'Room 1'    |null |null   |null   |null     
        'Room 2'    |2    |null   |null   |null     
        'Room 3'    |0    |null   |null   |null     
        'Room 4'    |3    |2      |null   |null     
        'Room 5'    |4    |0      |null   |null     
        'Room 6'    |5    |3      |true   |null     
        'Room 7'    |6    |4      |false  |null     
        'Room 8'    |7    |10     |true   |0        
        'Room 9'    |8    |20     |true   |0.5      
    }

    def "query index"()
    {
        when:
        List<Room> result = http.get {
            request.uri.path = path
        }

        then:
        result.size() == 9
        9.times {
            def name = "Room ${it+1}"
            assert result[it].id == name2Id(name)
            assert result[it].name == name
            assert result[it].enabled == true
        }
    }


    @Unroll
    def "show #name"()
    {
        when:
        Room result = http.get {
            request.uri.path = path+"/${name2Id(name)}"
        }

        then:
        result.name == name
        result.teams == (t ?: 2)
        result.players == (p ?: 8)
        result.ranked == (r == null ? true : r)
        result.overlap == (o ?: 0.75)

        where:
        name        |t    |p      |r      |o
        'Room 1'    |null |null   |null   |null
        'Room 2'    |2    |null   |null   |null
        'Room 3'    |0    |null   |null   |null
        'Room 4'    |3    |2      |null   |null
        'Room 5'    |4    |0      |null   |null
        'Room 6'    |5    |3      |true   |null
        'Room 7'    |6    |4      |false  |null
        'Room 8'    |7    |10     |true   |0
        'Room 9'    |8    |20     |true   |0.5
    }
    
    @Unroll
    def "update #name"()
    {
        //Rooms can't be updated
        given:
        def id = name2Id(name)
        def actor = Actor.getReference(RoomService, id)
        Room original = http.get {
            request.uri.path = path+"/$id"
        }

        when:
        Room result = http.put {
            request.uri.path = path+"/$id"
            request.body = [name: name, teams: t, players: p, ranked:r, overlap:o]
        }

        then:
        result.name == original.name
        result.teams == original.teams
        result.players == original.players
        result.ranked == original.ranked
        result.overlap == original.overlap

        and:
        actor.getServiceDetails().join() == result.generateDetailsFromDomain()
        actor.getServiceDetails().join() == original.generateDetailsFromDomain()
        original.generateDetailsFromDomain() == result.generateDetailsFromDomain()

        where:
        name        |t    |p      |r      |o
        'Room 1'    |4    |2      |true   |0.68
        'Room 2'    |4    |2      |true   |0.68
        'Room 3'    |4    |2      |true   |0.68
        'Room 4'    |4    |2      |true   |0.68
        'Room 5'    |4    |2      |true   |0.68
        'Room 6'    |4    |2      |true   |0.68
        'Room 7'    |4    |2      |true   |0.68
        'Room 8'    |4    |2      |true   |0.68
        'Room 9'    |4    |2      |true   |0.68
    }



    @Unroll
    def "disable #name"()
    {
        given:
        def id = name2Id(name)
        def actor = Actor.getReference(RoomService, id)

        when:
        Room result = http.delete {
            request.uri.path = path+"/$id/activate"
        }

        then:
        !result.enabled

        and:
        !actor.isEnabled().join()

        where:
        name << ["Room 1", "Room 2"]
    }

    def "get enabled"()
    {
        expect:
        Actor.getReference(RoomManager).getAllEnabledServices().join().size() == 7

        when:
        List<Room> result = http.get {
            request.uri.path = path+"/enabled"
            response.failure {FromServer fs -> println "$fs.statusCode -> $fs.message" }
        }

        then:
        result.size() == 7
        result.name.containsAll(["Room 3", "Room 4",
                                 "Room 5", "Room 6",
                                 "Room 7", "Room 8",
                                 "Room 9"])
    }


    def "get disabled"()
    {
        expect:
        Actor.getReference(RoomManager).getAllDisabledServices().join().size() == 2

        when:
        List<Room> result = http.get {
            request.uri.path = path+"/disabled"
        }

        then:
        result.size() == 2
        result.name.containsAll(["Room 1", "Room 2"])
    }

    @Unroll
    def "re-enable #name"()
    {
        given:
        def id = name2Id(name)

        when:
        int status = http.put() {
            request.uri.path = path+"/$id/activate"
            response.success { FromServer fs -> fs.statusCode }
        }

        then:
        status == 304

        where:
        name << ["Room 3", "Room 4"]
    }

    @Unroll
    def "re-disable #name"()
    {
        given:
        def id = name2Id(name)

        when:
        int status = http.delete() {
            request.uri.path = path+"/$id/activate"
            response.success { FromServer fs -> fs.statusCode }
        }

        then:
        status == 304

        where:
        name << ["Room 1", "Room 2"]
    }

    @Unroll
    def "enable the disabled #name"()
    {
        given:
        def id = name2Id(name)
        def actor = Actor.getReference(RoomService, id)

        when:
        Map<String,String> result = http.put() {
            request.uri.path = path+"/$id/activate"
        }

        then:
        result["enabled"] == true


        and:
        actor.isEnabled().join()

        where:
        name << ["Room 1", "Room 2"]
    }


    @Unroll
    def "delete #name"()
    {
        given:
        def id = name2Id(name)

        when:
        Integer status = http.delete {
            request.uri.path = path+"/$id"
            response.success { FromServer fs -> fs.statusCode }
            response.failure { FromServer fs -> fs.statusCode }
        }

        then:
        status == respRoomus

        and:
        if (respRoomus == 204)
        {
            assert Actor.getReference(RoomService, id).getServiceDetails().join() == null
        }

        where:
        name             |respRoomus
        "qwerty"         |404
        ""               |404
        null             |404
        'Room 1'         |204
        'Room 2'         |204
        'Room 3'         |204
        'Room 4'         |204
        'Room 5'         |204
        'Room 6'         |204
        'Room 7'         |204
        'Room 8'         |204
        'Room 9'         |204
    }
}
