package shepard.frontend.test

import grails.test.mixin.integration.Integration
import spock.lang.Specification

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 31/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Integration
class ShepardIntegrationSpec extends Specification implements ServerUrl
{
    static String baseUrl

    def setupSpec()
    {
        TestBackend.start()
    }

    def setup()
    {
        baseUrl = integrationServerUrl()
    }
}
