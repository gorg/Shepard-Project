package shepard.frontend.test

import org.springframework.beans.factory.annotation.Value

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 31/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 *
 * http://stackoverflow.com/questions/38252059/how-to-get-the-url-where-grails-3-1-9-integration-tests-are-running
 **/
trait ServerUrl
{
    @Value('${local.server.port}')
    Integer serverPort

    String integrationServerUrl()
    {
        "http://localhost:$serverPort"
    }
}