package shepard.frontend.test

import cloud.orbit.actors.Stage
import cloud.orbit.actors.extensions.json.InMemoryJSONStorageExtension

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 31/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class TestBackend
{
    private static final Stage stage = new Stage()

    static start()
    {
        try{
            stage.setClusterName("testing-service")
            stage.addExtension(new InMemoryJSONStorageExtension())
            stage.setNodeName("testing-backend")
            stage.setMode(Stage.StageMode.HOST)
            stage.start().join()
        }
        catch(Exception e){}
    }
}
