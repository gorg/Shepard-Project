package shepard.backend.service.generic

import cloud.orbit.actors.Actor
import cloud.orbit.concurrent.Task

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/14/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */

interface ServiceCache<E extends Service> extends Actor
{
    Task<List<E>> retrieve()
}