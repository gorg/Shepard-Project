package shepard.backend.service.generic

import cloud.orbit.actors.Actor
import cloud.orbit.concurrent.Task
import shepard.backend.service.player.Player

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/21/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */

interface Entry<S extends Service, E extends Entry>
        extends Actor
{
    /**
     * Set a new entry for a given service and player
     *
     * @param service
     * @param player
     * @return A promise of the entry
     */
    Task<E> setupEntry(S service, Player player)

    /**
     * Retrieves the service responsible of the entry
     *
     * @return A promise of the service
     */
    Task<S> getService()

    /**
     * Retrieves the player to whom belongs the entry
     *
     * @return A promise of a player
     */
    Task<Player> getPlayer()

    /**
     * Collects all parameters of a given entry into a map. Used for entry propagation to the frontend
     *
     * @return A promise of map with all the entry parameters
     */
    Task<Map> collectEntry()
}
