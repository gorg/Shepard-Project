package shepard.backend.service.achievement.details

import groovy.transform.Canonical
import groovy.transform.ToString
import shepard.backend.service.generic.ServiceDetails

/**
*  Created by Gueorgui Tzvetoslavov Topalski on 15/10/16.
*  email: gueorgui.tzvetoslavov@gmail.com
*/
@Canonical
@ToString(includeNames = true, includePackage = false)
class AchievementDetails implements ServiceDetails
{
    String name
    String description
    AchievementType type = AchievementType.REVEALED
    String icon

    @Override
    boolean validate()
    {
        if(!name)
            return false

        if(!type)
            type = AchievementType.REVEALED

        return true
    }
}
