package shepard.backend.service.player.manager

import cloud.orbit.actors.Actor
import cloud.orbit.actors.annotation.NoIdentity
import cloud.orbit.concurrent.Task
import shepard.backend.service.player.Player

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 28/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@NoIdentity
interface PlayerManager extends Actor
{
    Task<Player> registerPlayer(String playerName, String password)

    Task<Player> changePassword(String playerName, String oldPassword, String newPassword)

    Task<Player> findByName(String playerName)

    Task<Player> findById(String id)

    Task<Boolean> removePlayer(String playerName, String password)
}
