package shepard.backend.service.leaderboard.score.details

import java.text.NumberFormat

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 22/10/16.
 * email: gueorgui.tzvetoslavov@gmail.com
 */
class NumericalScore extends AbstactScore
{
    NumericalScore()
    {
        super()
    }

    NumericalScore(double score)
    {
        super(score)
    }

    NumericalScore(int score)
    {
        super(score)
    }

    @Override
    getScoreLocale(Locale l = Locale.US) {
        def nf = NumberFormat.getNumberInstance(l)
        nf.format(super.score).toString()
    }
}
