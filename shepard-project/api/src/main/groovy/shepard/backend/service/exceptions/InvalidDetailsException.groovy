package shepard.backend.service.exceptions
/**
 * Created by Gueorgui Tzvetoslavov Topalski on 26/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class InvalidDetailsException extends Exception
{
    InvalidDetailsException()
    {
        super()
    }

    InvalidDetailsException(String message)
    {
        super(message)
    }

    InvalidDetailsException(String message, Throwable cause)
    {
        super(message, cause)
    }

    InvalidDetailsException(Throwable cause)
    {
        super(cause)
    }

    protected InvalidDetailsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace)
    }
}
