package shepard.backend.service.leaderboard.details

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 10/27/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
enum LeaderboardSort {
    HIGH('HIGH'),
    LOW('LOW')

    String name
    LeaderboardSort(String id)
    {
        this.name = id
    }
}