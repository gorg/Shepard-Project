package shepard.backend.service.generic.traits

import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 25/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
trait HasId implements HasName
{
    String id() { name2Id(name) }
}
