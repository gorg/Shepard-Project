package shepard.backend.service.stats.cache

import cloud.orbit.actors.annotation.NoIdentity
import cloud.orbit.actors.annotation.PreferLocalPlacement
import cloud.orbit.actors.annotation.StatelessWorker
import shepard.backend.service.generic.ServiceCache
import shepard.backend.service.stats.StatService

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/18/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
@NoIdentity
@StatelessWorker
@PreferLocalPlacement(percentile = 100)
interface StatCache extends ServiceCache<StatService>
{
}