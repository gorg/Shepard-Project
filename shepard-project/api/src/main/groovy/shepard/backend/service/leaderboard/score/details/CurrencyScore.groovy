package shepard.backend.service.leaderboard.score.details

import java.text.NumberFormat

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 22/10/16.
 * email: gueorgui.tzvetoslavov@gmail.com
 */
class CurrencyScore extends AbstactScore {

    final static long base = 1000000

    CurrencyScore()
    {
        super()
    }

    CurrencyScore(long score)
    {
        super(score)
    }

    @Override
    getScoreLocale(Locale l) {
        NumberFormat.getCurrencyInstance(l)
            .format((super.score.toDouble() / base))
            .toString()
    }
}
