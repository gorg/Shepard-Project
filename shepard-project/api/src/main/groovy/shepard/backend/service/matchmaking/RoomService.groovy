package shepard.backend.service.matchmaking

import cloud.orbit.concurrent.Task
import shepard.backend.service.generic.Service
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.entry.Lobby
import shepard.backend.service.player.Player

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/25/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
interface RoomService extends Service<RoomService, RoomDetails, RoomData, Lobby>
{
    Task<List<Lobby>> getLobbies()
    Task<Map<Double,Lobby>> getRatingsAndLobbies()
    Task<Void> removeLobby(Lobby lobby,Set<Player> lobbyPlayers)

    Task<Lobby> checkForEntry(Player player)

    Task<Lobby> exitLobby(Player player)
    Task<Lobby> checkForPlayer(Player player)
    Task<Lobby> enterLobby(Player player)
}