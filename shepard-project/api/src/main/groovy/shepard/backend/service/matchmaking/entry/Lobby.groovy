package shepard.backend.service.matchmaking.entry

import cloud.orbit.concurrent.Task
import shepard.backend.service.game.Game
import shepard.backend.service.generic.Entry
import shepard.backend.service.matchmaking.RoomService
import shepard.backend.service.matchmaking.details.Rules
import shepard.backend.service.matchmaking.util.Team
import shepard.backend.service.player.Player

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 12/13/16.
 * email: gueorgui.tzvetoslavov@gmail.com
 */
interface Lobby extends Entry<RoomService, Lobby>
{
    Task<Lobby> setupEntry(RoomService service, Player player, Rules rules)
    Task<Void> setRules(Rules rules)
    Task<Rules> getLobbyRules()

    Task<Boolean> addPlayer(Player player)
    Task<Boolean> removePlayer(Player player)
    Task<Boolean> canPlay(Player player)
    Task<Boolean> hasFreeSpot()

    Task<Double> getLobbyRating()
    Task<Double> getLobbyRatingDevation()
    Task<Double> getArea(Player player)

    Task<Game> startGame()
    Task<Boolean> isGameStarted()
    Task<Game> getGame()
    Task<Void> populateTeams()

    Task<List<Team>> getTeams()
    Task<List<Team>> getOtherTeams(Team team)
    Task<Set<Player>> getOpponents(Team team)

    Task<Team> getTeam(int team)
    Task<Boolean> selectTeam(Player player, int team)
    Task<Boolean> leaveTeam(Player player)



    Task<Set<Player>> getLobbyPLayers()

    Task<Void> finishMatch()
}
