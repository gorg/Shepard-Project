package shepard.backend.service.leaderboard

import cloud.orbit.tuples.Pair
import groovy.transform.Canonical
import shepard.backend.service.generic.ServiceData
import shepard.backend.service.leaderboard.ranking.Rank
import shepard.backend.service.leaderboard.score.Score
import shepard.backend.service.leaderboard.score.updater.ScoreUpdater

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 26/01/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Canonical
class LeaderboarData implements ServiceData
{
    Comparator<Pair<Rank, Score>> comparator
    ScoreUpdater updater
    HashSet<String> players
    ArrayList<String> ranking
}
