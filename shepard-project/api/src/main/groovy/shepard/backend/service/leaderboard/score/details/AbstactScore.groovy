package shepard.backend.service.leaderboard.score.details

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 22/10/16.
 * email: gueorgui.tzvetoslavov@gmail.com
 */
@ToString(includeNames = true)
@EqualsAndHashCode
abstract class AbstactScore implements Comparable<AbstactScore>, Serializable
{
    Number score = 0

    AbstactScore()
    {
        score = 0
    }

    AbstactScore(Number score)
    {
        this.score = score
    }

    abstract getScoreLocale(Locale l = Locale.US)

    @Override
    int compareTo(AbstactScore score)
    {
        this.score <=> score.score
    }
}
