package shepard.backend.service.leaderboard.score.details

import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 22/10/16.
 * email: gueorgui.tzvetoslavov@gmail.com
 */
class TimeScore extends AbstactScore
{
    TimeScore()
    {
        super()
    }

    TimeScore(long score)
    {
        super(score)
    }

    @Override
    getScoreLocale(Locale l = Locale.US) {
        def formatter = DateTimeFormatter.ISO_LOCAL_TIME.withLocale(l)
        def time = LocalTime.MIDNIGHT.plus(super.score.toLong(), ChronoUnit.MILLIS)
        time.format(formatter).toString()
    }
}
