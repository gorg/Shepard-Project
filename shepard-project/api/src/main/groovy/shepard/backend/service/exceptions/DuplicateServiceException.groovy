package shepard.backend.service.exceptions
/**
 * Created by Gueorgui Tzvetoslavov Topalski on 26/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class DuplicateServiceException extends Exception
{
    DuplicateServiceException()
    {
        super()
    }

    DuplicateServiceException(String message)
    {
        super(message)
    }

    DuplicateServiceException(String message, Throwable cause)
    {
        super(message, cause)
    }

    DuplicateServiceException(Throwable cause)
    {
        super(cause)
    }

    protected DuplicateServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace)
    }
}
