package shepard.backend.service.leaderboard.ranking

import groovy.transform.Canonical
import groovy.transform.ToString
import groovy.transform.builder.Builder
import shepard.backend.service.leaderboard.score.details.AbstactScore

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/3/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
@Canonical
@ToString(includeNames = true, ignoreNulls = true, includePackage = false)
@Builder
class RankData
{
    long currentRank = 0
    AbstactScore score
    long delta = 0

    long changeRank(long newRank)
    {
        delta = currentRank - newRank
        currentRank = newRank
        return delta
    }

}
