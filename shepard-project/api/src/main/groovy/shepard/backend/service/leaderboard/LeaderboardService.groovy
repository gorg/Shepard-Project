package shepard.backend.service.leaderboard

import cloud.orbit.concurrent.Task
import shepard.backend.service.generic.Service
import shepard.backend.service.leaderboard.details.LeaderboardDetails
import shepard.backend.service.leaderboard.ranking.Rank
import shepard.backend.service.leaderboard.score.Score
import shepard.backend.service.player.Player

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 23/10/16.
 * email: gueorgui.tzvetoslavov@gmail.com
 */
interface LeaderboardService
        extends Service<LeaderboardService, LeaderboardDetails, LeaderboarData, Rank>
{

    Task<Score> insertScore(Player player, long points)

    Task<Long> rank()

    Task<List<Rank>> getRankedLeaderboard()

    Task<List<Rank>> getRankedTop(int end)

    Task<List<Rank>> getRankedRelativeLeaderboard(int start, int end)
}