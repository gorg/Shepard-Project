package shepard.backend.service.stats.details

import groovy.transform.Canonical
import groovy.transform.ToString
import shepard.backend.service.generic.ServiceDetails

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/17/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */

@Canonical
@ToString(includeNames = true, includePackage = false)
class StatDetails implements ServiceDetails
{
    String name
    BigDecimal maxChange = 0
    BigDecimal minValue = 0
    BigDecimal maxValue = BigDecimal.valueOf(Long.MAX_VALUE,4)
    BigDecimal defaultValue = 0
    boolean incrementOnly = true

    def test(BigDecimal newValue, BigDecimal oldValue)
    {
        if (newValue == null && oldValue == null)
            return false

        if (newValue == null)
            return false

        if (oldValue == null)
            oldValue = defaultValue

        if (newValue == oldValue)
            return false

        if (maxValue)
        {
            if (oldValue > maxValue)
                return false

            if (newValue > maxValue)
                return false
        }

        if (oldValue < minValue)
            return false

        if (newValue < minValue)
            return false


        if (maxChange) // maxChange != 0 or !null
            if ((newValue-oldValue).abs() > maxChange.abs())
                return false

        if (incrementOnly)
        {
            if(oldValue > newValue)
                return false
        }

        return true
    }

    @Override
    boolean validate()
    {
        if(!name)
            return false

        if (minValue >= maxValue)
            return false

        if (defaultValue < minValue || defaultValue > maxValue)
            return false

        return true
    }
}