package shepard.backend.service.stats.entry

import cloud.orbit.concurrent.Task
import shepard.backend.service.generic.Entry
import shepard.backend.service.stats.StatService

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/18/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
interface StatEntry extends Entry<StatService, StatEntry>
{
    Task<BigDecimal> getStatValue()
    Task<StatEntry> updateStatValue(BigDecimal newValue)
}
