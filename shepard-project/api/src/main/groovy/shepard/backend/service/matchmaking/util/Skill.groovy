package shepard.backend.service.matchmaking.util

import cloud.orbit.actors.Actor
import cloud.orbit.concurrent.Task
import shepard.backend.service.player.Player

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/25/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
interface Skill extends Actor
{
    Task<Skill> setup(Player player)

    Task<Double> getRating()
    Task<Void> setRating(double rating)

    Task<Double> getGlicko2Rating()
    Task<Void> setGlicko2Rating(double glicko2Rating)

    Task<Double> getRatingDeviation()
    Task<Void> setRatingDeviation(double ratingDeviation)

    Task<Double> getGlicko2RatingDeviation()
    Task<Void> setGlicko2RatingDeviation(double glicko2RatingDeviation)

    Task<Double> getVolatility()
    Task<Void> setVolatility(double volatility)

    Task<Void> incrementNumberOfResults(int increment)
    Task<Integer> getNumberOfResults()

    Task<Void> setWorkingRating(double workingRating)
    Task<Void> setWorkingRatingDeviation(double workingRatingDeviation)
    Task<Void> setWorkingVolatility(double workingVolatility)

    Task<Void> finishRating()

    Task<SkillRange> getSkillRange68()
    Task<SkillRange> getSkillRange95()
    Task<SkillRange> getSkillRange99()

    Task<String> resume()
}