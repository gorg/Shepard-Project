package shepard.backend.service.leaderboard.ranking

import cloud.orbit.concurrent.Task
import shepard.backend.service.generic.Entry
import shepard.backend.service.leaderboard.LeaderboardService
import shepard.backend.service.leaderboard.score.Score
import shepard.backend.service.player.Player

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 10/26/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
interface Rank extends Entry<LeaderboardService,Rank>
{

    Task<Score> getScore()

    Task<Score> updateScore(long points)

    Task<Long> getRank()

    Task<Rank> changeRank(long newRank)

    Task<Player> getPlayer()

    Task<Long> getDelta()
}