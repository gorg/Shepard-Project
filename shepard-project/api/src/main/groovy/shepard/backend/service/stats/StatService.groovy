package shepard.backend.service.stats

import cloud.orbit.actors.annotation.Reentrant
import cloud.orbit.concurrent.Task
import shepard.backend.service.generic.Service
import shepard.backend.service.player.Player
import shepard.backend.service.stats.details.StatDetails
import shepard.backend.service.stats.entry.StatEntry

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/14/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
interface StatService extends Service<StatService, StatDetails, StatData, StatEntry>
{
    @Reentrant
    Task<StatEntry> update(BigDecimal newValue, Player player)

    @Reentrant
    Task<Boolean> test(BigDecimal newValue, BigDecimal oldValue)
}