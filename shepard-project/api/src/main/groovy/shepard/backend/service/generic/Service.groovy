package shepard.backend.service.generic

import cloud.orbit.actors.Actor
import cloud.orbit.concurrent.Task
import shepard.backend.service.player.Player

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/21/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */

interface Service<S extends Service, D extends ServiceDetails, Z extends ServiceData, E extends Entry>
        extends Actor
{
    Task<S> setupService(D serviceDetails)

    Task<S> updateServiceDetails(D serviceDetails)

    Task<E> getEntry(Player player)

    Task<D> getServiceDetails()

    Task<Void> removeService()

    Task<Boolean> isEnabled()
}