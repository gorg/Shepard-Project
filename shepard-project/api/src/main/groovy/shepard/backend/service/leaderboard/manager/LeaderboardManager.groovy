package shepard.backend.service.leaderboard.manager

import cloud.orbit.actors.annotation.NoIdentity
import shepard.backend.service.generic.ServiceManager
import shepard.backend.service.leaderboard.LeaderboardService
import shepard.backend.service.leaderboard.details.LeaderboardDetails

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 23/10/16.
 * email: gueorgui.tzvetoslavov@gmail.com
 */

@NoIdentity
interface LeaderboardManager
        extends ServiceManager<LeaderboardService, LeaderboardDetails>
{
}