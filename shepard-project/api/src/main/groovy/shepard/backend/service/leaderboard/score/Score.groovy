package shepard.backend.service.leaderboard.score

import cloud.orbit.actors.Actor
import cloud.orbit.concurrent.Task
import shepard.backend.service.leaderboard.score.details.CurrencyScore
import shepard.backend.service.leaderboard.score.details.NumericalScore
import shepard.backend.service.leaderboard.score.details.TimeScore

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/5/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
interface Score extends Actor
{
    Task<Score> setScore(long score)
    Task<Score> getScore()

    Task<Long> getRawScore()
    Task<NumericalScore> getScoreAsNumericalScore()
    Task<CurrencyScore> getScoreAsCurrencyScore()
    Task<TimeScore> getScoreAsTimeScore()
}
