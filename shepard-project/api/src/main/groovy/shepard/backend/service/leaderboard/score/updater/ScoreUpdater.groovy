package shepard.backend.service.leaderboard.score.updater

import cloud.orbit.concurrent.Task
import shepard.backend.service.leaderboard.ranking.Rank
import shepard.backend.service.leaderboard.score.Score

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/8/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
interface ScoreUpdater extends Serializable
{
    Task<Score> updateScore(Task<Rank> toUpdate, Long points)
}
