package shepard.backend.service.leaderboard.comparators

import cloud.orbit.tuples.Pair
import shepard.backend.service.leaderboard.ranking.Rank
import shepard.backend.service.leaderboard.score.Score


/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/5/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class FirstHighComparator implements Comparator<Pair<Rank, Score>>,Serializable
{
    FirstHighComparator(){}

    @Override
    int compare(Pair<Rank, Score> rankScorePair, Pair<Rank, Score> otherRankScorePair)
    {
        def firstScore = rankScorePair.right.getRawScore().join()
        def secondScore = otherRankScorePair.right.getRawScore().join()

        if (firstScore > secondScore)
            return -1
        else if (firstScore < secondScore)
            return 1
        else
            return 0
    }
}
