package shepard.backend.service.achievement.manager

import cloud.orbit.actors.annotation.NoIdentity
import shepard.backend.service.achievement.AchievementService
import shepard.backend.service.achievement.details.AchievementDetails
import shepard.backend.service.generic.ServiceManager

/**
*  Created by Gueorgui Tzvetoslavov Topalski on 15/10/16.
*  email: gueorgui.tzvetoslavov@gmail.com
*/

@NoIdentity
interface AchievementManager
        extends ServiceManager<AchievementService,AchievementDetails>
{
}