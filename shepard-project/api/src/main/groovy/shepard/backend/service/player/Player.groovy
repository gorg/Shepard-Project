package shepard.backend.service.player

import cloud.orbit.actors.Actor
import cloud.orbit.concurrent.Task
import shepard.backend.service.achievement.AchievementService
import shepard.backend.service.achievement.entry.Achievement
import shepard.backend.service.game.Game
import shepard.backend.service.leaderboard.LeaderboardService
import shepard.backend.service.leaderboard.ranking.Rank
import shepard.backend.service.leaderboard.score.Score
import shepard.backend.service.matchmaking.entry.Lobby
import shepard.backend.service.matchmaking.util.Skill
import shepard.backend.service.stats.StatService
import shepard.backend.service.stats.entry.StatEntry

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 10/12/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
interface Player extends Actor
{
    Task<Game> getCurrentGame()
    Task<Void> joinGame(Game game)
    Task<Void> leaveGame(Game game)

    Task<Achievement> isAchievementCompleted(AchievementService achievement)

    Task<Void> setCurrentLobby(Lobby lobby)
    Task<Lobby> getCurrentLobby()

    Task<Rank> getRankForLeaderboard(LeaderboardService leaderboard)
    Task<Score> getScoreForLeaderboard(LeaderboardService leaderboard)

    Task<StatEntry> getStat(StatService stat)
    Task<Skill> getSkill()

    Task<List<StatEntry>> getStatList()
    Task<List<Achievement>> getAchievementsList()
    Task<List<Rank>> getRanksList()

    Task<Player> registerPlayer(String username, String password)
    Task<Player> validatePlayer(String username, String password)
    Task<Player> changePassword(String oldPassword, String newPassword)
    Task<Boolean> removePlayer(String username, String password)
    Task<Boolean> isPlayerSet()
}