package shepard.backend.service.matchmaking.util

import cloud.orbit.actors.Actor
import cloud.orbit.actors.annotation.NoIdentity
import cloud.orbit.actors.annotation.StatelessWorker
import cloud.orbit.concurrent.Task
import shepard.backend.util.glicko2.ResultPeriod

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/25/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
@NoIdentity
@StatelessWorker
interface Glicko2 extends Actor
{
    Task<Double> getDefaultRating()
    Task<Double> getDefaultRatingDeviation()
    Task<Double> getDefaultVolatility()

    Task<Void> updateRating(ResultPeriod results)

    Task<Double> convertRatingToGlicko2Scale(double rating)
    Task<Double> convertRatingToOriginalGlickoScale(double rating)
    Task<Double> convertRatingDeviationToGlicko2Scale(double rating)
    Task<Double> convertRatingDeviationToOriginalGlickoScale(double rating)

    Task<Void> setup(double tau, double volatility)
}
