package shepard.backend.service.matchmaking.details

import groovy.transform.Canonical
import groovy.transform.ToString

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 12/25/16.
 * email: gueorgui.tzvetoslavov@gmail.com*/
@Canonical
@ToString (ignoreNulls = true, includePackage = false, includeNames = true)
class Rules implements Serializable
{
    int teams = 2
    int players = 8
    boolean ranked = true
    double overlap = 0.75d
}
