package shepard.backend.service.achievement.details

/**
*  Created by Gueorgui Tzvetoslavov Topalski on 15/10/16.
*  email: gueorgui.tzvetoslavov@gmail.com
*/
enum AchievementType
{
    HIDDEN("HIDDEN"),
    REVEALED("REVEALED")

    String name
    AchievementType(String name)
    {
        this.name = name
    }
}