package shepard.backend.service.achievement

import cloud.orbit.concurrent.Task
import shepard.backend.service.achievement.details.AchievementDetails
import shepard.backend.service.achievement.entry.Achievement
import shepard.backend.service.generic.Service
import shepard.backend.service.player.Player

import java.time.LocalDateTime

/**
*  Created by Gueorgui Tzvetoslavov Topalski on 15/10/16.
*  email: gueorgui.tzvetoslavov@gmail.com
*/
interface AchievementService
        extends Service<AchievementService, AchievementDetails, AchievementServiceData, Achievement>
{
    //Marks the achievements entry for the player as completed
    Task<Boolean> completeAchievement(Player player)

    //Queries the entry
    Task<Boolean> isCompleted(Player player)

    //Time information about the completion of the achievements
    Task<LocalDateTime> whenWasCompleted(Player player)
}