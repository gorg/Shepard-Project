package shepard.backend.service.leaderboard.score.details

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/5/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class EmptyScore extends AbstactScore
{
    EmptyScore()
    {
        super()
    }

    @Override
    getScoreLocale(Locale l)
    {
        return null
    }
}
