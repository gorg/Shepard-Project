package shepard.backend.service.matchmaking.manager

import cloud.orbit.actors.annotation.NoIdentity
import cloud.orbit.concurrent.Task
import shepard.backend.service.generic.ServiceManager
import shepard.backend.service.matchmaking.RoomService
import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.entry.Lobby

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 12/26/16.
 * email: gueorgui.tzvetoslavov@gmail.com
 */

@NoIdentity
interface RoomManager
        extends ServiceManager<RoomService, RoomDetails>
{
    Task<Lobby> findPlayer(String playerId)
}