package shepard.backend.service.game

import cloud.orbit.actors.Actor
import cloud.orbit.concurrent.Task
import shepard.backend.service.matchmaking.entry.Lobby
import shepard.backend.service.matchmaking.util.Team
import shepard.backend.util.glicko2.ResultPeriod
import shepard.backend.util.glicko2.ResultResume
import shepard.backend.util.glicko2.TeamResultResume

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 10/12/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
interface Game extends Actor
{
    //Called for game initialization
    Task<Game> setup(Lobby lobby)

    //Results
    Task<Game> addWin(Team winner)
    Task<Game> addWin(Team winner, Team loser)
    Task<Game> addWin(Integer winner)
    Task<Game> addWin(Integer winner, Integer loser)
    Task<Game> addDraw()

    Task<List<ResultResume>> getResume()
    Task<List<ResultResume>> getResumeRedux()
    Task<List<TeamResultResume>> getTeamResume()
    Task<List<TeamResultResume>> getTeamResumeRedux()

    Task<ResultPeriod> getResults()

    //Match
    Task<Game> finishMatch()
    Task<Boolean> isFinished()

    Task<Game> addParticipant(Team team)
    Task<Lobby> getLobby()
}