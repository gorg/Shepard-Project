package shepard.backend.service.leaderboard.score.updater

import cloud.orbit.concurrent.Task
import shepard.backend.service.leaderboard.ranking.Rank
import shepard.backend.service.leaderboard.score.Score

import java.util.concurrent.CompletionStage
import java.util.function.Function

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/8/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class IncrementalScoreUpdater implements ScoreUpdater
{
    long lowerLimit = Long.MIN_VALUE
    long upperLimit = Long.MAX_VALUE

    @Override
    Task<Score> updateScore(Task<Rank> toUpdate, Long points)
    {
        toUpdate.thenCompose( new Function<Rank, CompletionStage<Score>>() {
            @Override
            CompletionStage<Score> apply(Rank rank)
            {
                rank.score.thenCompose(new Function<Score, CompletionStage<Score>>()
                {
                    @Override
                    CompletionStage<Score> apply(Score score)
                    {
                        score.getRawScore().thenCompose(new Function<Long, CompletionStage<Score>>() {
                            @Override
                            CompletionStage<Score> apply(Long aLong) {
                                def temp = aLong + points
                                if(temp >= lowerLimit && temp <= upperLimit)
                                    score.setScore(temp)
                                else
                                    score.getScore()
                            }
                        })
                    }
                })
            }
        })
    }
}
