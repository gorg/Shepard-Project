package shepard.backend.service.achievement.cache

import cloud.orbit.actors.annotation.NoIdentity
import cloud.orbit.actors.annotation.PreferLocalPlacement
import cloud.orbit.actors.annotation.StatelessWorker
import shepard.backend.service.achievement.AchievementService
import shepard.backend.service.generic.ServiceCache

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/8/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
@NoIdentity
@StatelessWorker
@PreferLocalPlacement(percentile = 100)
interface AchievementCache extends ServiceCache<AchievementService>
{
}
