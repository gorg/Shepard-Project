package shepard.backend.service.matchmaking.util

import groovy.transform.Canonical
import groovy.transform.ToString

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 10/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Canonical
@ToString
class SkillRange implements Serializable
{
    def low
    def high
}
