package shepard.backend.service.stats.manager

import cloud.orbit.actors.annotation.NoIdentity
import shepard.backend.service.generic.ServiceManager
import shepard.backend.service.stats.StatService
import shepard.backend.service.stats.details.StatDetails

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/14/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */

@NoIdentity
interface StatManager
        extends ServiceManager<StatService, StatDetails>
{
}