package shepard.backend.service.leaderboard.details

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/8/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
enum LeaderboardType {
    INCREMENTAL('INCREMENTAL'),
    ABSOLUTE('ABSOLUTE'),
    ONLY_HIGHER('ONLY HIGHER'),
    ONLY_LOWER('ONLY LOWER')

    String name
    LeaderboardType(String id)
    {
        this.name = id
    }
}