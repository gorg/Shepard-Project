package shepard.backend.service.generic

import shepard.backend.service.generic.traits.HasIcon
import shepard.backend.service.generic.traits.HasId
import shepard.backend.service.generic.traits.Validate

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/16/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */

interface ServiceDetails extends Serializable, HasId, Validate, HasIcon
{
}