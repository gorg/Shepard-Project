package shepard.backend.service.generic.traits

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 26/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
trait Validate
{
    abstract boolean validate()
}
