package shepard.backend.service.matchmaking.details

import groovy.transform.Canonical
import groovy.transform.ToString
import shepard.backend.service.generic.ServiceDetails

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 25/01/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 */
@Canonical
@ToString(includeNames = true, includePackage = false)
class RoomDetails implements ServiceDetails
{
    String name
    Rules rules = new Rules()

    @Override
    boolean validate()
    {
        if(!name)
            return false

        if (rules.players < 2 || rules.players > 64)
            return false

        if (rules.teams < 2 || rules.teams > 8)
            return false

        if (rules.overlap < 0.25 || rules.overlap > 1)
            return false

        if (rules.ranked == null)
            rules.ranked = true

        return true
    }
}
