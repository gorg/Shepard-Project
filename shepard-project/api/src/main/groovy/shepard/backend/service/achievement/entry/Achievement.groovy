package shepard.backend.service.achievement.entry

import cloud.orbit.concurrent.Task
import shepard.backend.service.achievement.AchievementService
import shepard.backend.service.generic.Entry

import java.time.LocalDateTime

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/8/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
interface Achievement extends Entry<AchievementService,Achievement>
{
    Task<Boolean> complete()
    Task<Boolean> isCompleted()
    Task<LocalDateTime> whenWasCompleted()
}
