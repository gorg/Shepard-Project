package shepard.backend.service.matchmaking

import cloud.orbit.actors.Actor
import groovy.transform.Canonical
import shepard.backend.service.generic.ServiceData
import shepard.backend.service.matchmaking.entry.Lobby
import shepard.backend.service.player.Player

import java.util.stream.Collectors

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 26/01/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@Canonical
class RoomData implements ServiceData
{
    int counter = 0
    TreeMap<Double, String> lobbies = new TreeMap<>()
    HashMap<String, String> playerMap = new HashMap<>()

    def addLobby(Double rating, Lobby lobby)
    {
        while(lobbies.get(rating)) rating++

        lobbies.put(rating, lobby.identity)
    }

    def addPlayer(Player player, Lobby lobby)
    {
        playerMap.put(player.identity, lobby.identity)
    }

    def getPlayersLobby(Player player)
    {
        playerMap.containsKey(player.identity) ? Actor.getReference(Lobby, playerMap.get(player.identity)) : null
    }

    def removePlayer(Player player)
    {
        playerMap.remove(player.identity)
    }

    def getLobbyList()
    {
        lobbies.values().stream()
                .map { id -> Actor.getReference(Lobby, id) }
                .collect(Collectors.toList()) as List<Lobby>
    }

    def getRatingLobbyMap()
    {
        lobbies.collectEntries { k, v -> [(k): Actor.getReference(Lobby, v)] } as Map<Double, Lobby>
    }

    def removeLobby(Lobby lobby)
    {
        lobbies.values().remove(lobby.identity)
    }

}


