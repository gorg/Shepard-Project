package shepard.backend.service.generic

import cloud.orbit.actors.Actor
import cloud.orbit.concurrent.Task

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/16/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */

interface ServiceManager<E extends Service, D extends ServiceDetails>
        extends Actor
{
    Task<E> registerService(D details)

    Task<Boolean> disableService(E service)
    Task<Boolean> enableService(E service)
    Task<Boolean> removeService(E service)

    Task<Boolean> isValidId(E id)

    Task<Map<E,Boolean>> getServiceMap()
    Task<Set<E>> getAllEnabledServices()
    Task<Set<E>> getAllDisabledServices()

    Task<Boolean> isEnabled(E service)

    Task<List<Entry>> getAllEntries(String playerId)
}