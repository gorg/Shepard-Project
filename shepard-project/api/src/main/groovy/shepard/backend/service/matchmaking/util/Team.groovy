package shepard.backend.service.matchmaking.util

import cloud.orbit.actors.Actor
import cloud.orbit.concurrent.Task
import shepard.backend.service.player.Player

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/25/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
interface Team extends Actor
{
    Task<Boolean> addTeamMember(Player player)

    Task<Boolean> removeTeamMember(Player player)

    Task<Boolean> isInTeam(Player player)

    Task<Set<Player>> getTeamMembers()

    Task<Integer> teamSize()
}