package shepard.backend.service.matchmaking.cache

import cloud.orbit.actors.annotation.NoIdentity
import cloud.orbit.actors.annotation.PreferLocalPlacement
import cloud.orbit.actors.annotation.StatelessWorker
import shepard.backend.service.generic.ServiceCache
import shepard.backend.service.matchmaking.RoomService

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 28/01/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@NoIdentity
@StatelessWorker
@PreferLocalPlacement(percentile = 100)
interface LobbyCache extends ServiceCache<RoomService>
{}
