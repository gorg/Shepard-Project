package shepard.backend.service.leaderboard.details

import groovy.transform.Canonical
import groovy.transform.ToString
import shepard.backend.service.generic.ServiceDetails

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 23/10/16.
 * email: gueorgui.tzvetoslavov@gmail.com
 */
@Canonical
@ToString(includeNames = true, includePackage = false)
class LeaderboardDetails implements ServiceDetails
{
    String name
    long upperLimit = Long.MAX_VALUE
    long lowerLimit = 0.longValue()
    LeaderboardSort sort = LeaderboardSort.HIGH
    LeaderboardType type = LeaderboardType.ABSOLUTE


    @Override
    boolean validate()
    {
        if(!name)
            return false

        if (lowerLimit >= upperLimit)
            return false

        if(!sort)
            sort = LeaderboardSort.HIGH

        if(!type)
            type = LeaderboardType.ABSOLUTE

        return true
    }
}
