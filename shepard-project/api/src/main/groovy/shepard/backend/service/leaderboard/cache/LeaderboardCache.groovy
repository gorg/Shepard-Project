package shepard.backend.service.leaderboard.cache

import cloud.orbit.actors.annotation.NoIdentity
import cloud.orbit.actors.annotation.PreferLocalPlacement
import cloud.orbit.actors.annotation.StatelessWorker
import shepard.backend.service.generic.ServiceCache
import shepard.backend.service.leaderboard.LeaderboardService

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/7/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
@NoIdentity
@StatelessWorker
@PreferLocalPlacement(percentile = 100)
interface LeaderboardCache extends ServiceCache<LeaderboardService>
{
}