package shepard.backend.util.glicko2

import cloud.orbit.actors.Actor
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import shepard.backend.service.player.Player

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/25/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
@ToString(includeNames=true, includeFields = true, includePackage = false, excludes = ['metaClass'])
@EqualsAndHashCode(includeFields = true)
class Result
{
    private static final double POINTS_FOR_WIN = 1.0
    private static final double POINTS_FOR_LOSS = 0.0
    private static final double POINTS_FOR_DRAW = 0.5

    private boolean isDraw = false
    private String winner
    private String loser

    Result(Player winner, Player loser)
    {
        if (winner.getIdentity() == loser.getIdentity())
            throw new IllegalArgumentException()

        this.winner = winner.identity
        this.loser = loser.identity
    }

    Result(Player playerA, Player playerB, boolean isDraw)
    {
        if (playerA.getIdentity() == playerB.getIdentity() || !isDraw)
            throw new IllegalArgumentException()

        this.winner = playerA.identity
        this.loser = playerB.identity
        this.isDraw = isDraw
    }

    def participated(Player player)
    {
        (player.identity == winner || player.identity == loser)
    }

    def getScore(Player player)
    {
        def score

        if (player.identity == winner)
            score = POINTS_FOR_WIN
        else if (player.identity == loser)
            score = POINTS_FOR_LOSS
        else
            throw new IllegalArgumentException()

        if (isDraw)
            score = POINTS_FOR_DRAW

        return score
    }

    Player getWinner()
    {
        Actor.getReference(Player,winner)
    }

    Player getLoser()
    {
        Actor.getReference(Player,loser)
    }

    Player getOpponent(Player player)
    {
        if (player.identity == winner)
            return getLoser()
        else if (player.identity == loser)
            return getWinner()
        else
            throw new IllegalArgumentException("$player has no oppnent")
    }

    boolean isDraw() { return isDraw }
}
