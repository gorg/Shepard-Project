package shepard.backend.util.glicko2

import cloud.orbit.actors.Actor
import groovy.transform.Canonical
import groovy.transform.InheritConstructors
import groovy.transform.ToString
import shepard.backend.service.matchmaking.util.Team
import shepard.backend.service.player.Player

import java.math.MathContext
import java.util.function.BinaryOperator

import static com.ea.async.Async.await

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 21/04/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
@InheritConstructors
@Canonical
@ToString(includeNames = true, includePackage = false)
class TeamResultResume
{
    List<String> teamMembers
    double score
    int wins
    int loses
    int draws
    int rounds
    String team

    TeamResultResume(Team team, List<Result> results )
    {
        def actors = await(team.getTeamMembers())
        this.teamMembers = actors.collect{ it.identity }
        this.team = team.identity
        def size = teamMembers.size()

        score = actors.stream()
                        .map{ player ->
                            results.stream()
                                .filter{ res -> res.participated(player) }
                                .map{ it.getScore(player) }
                                .reduce( {a,b -> a + b} as BinaryOperator)}
                        .map{ it.get() }
                        .reduce( { a,b -> a + b }  as BinaryOperator).get() / size

        wins = results.stream()
                        .filter { res -> actors.contains(res.winner) }
                        .filter { res-> !res.isDraw() }
                        .distinct()
                        .count() / (size)

        loses = results.stream()
                        .filter { res -> actors.contains(res.loser) }
                        .filter { res-> !res.isDraw() }
                        .distinct()
                        .count() / (size)

        draws = results.count{ it.isDraw() } / size
        rounds = results.count { true } / size
    }

    @Deprecated
    TeamResultResume redux(int pit)
    {
        def res = new TeamResultResume()
        res.teamMembers = this.teamMembers
        res.team = this.getTeam()
        res.score = this.score / pit
        res.wins = (this.wins / pit).toInteger()
        res.loses = (this.loses / pit).toInteger()
        res.draws = (this.draws / pit).toInteger()
        res.rounds = (this.rounds / pit).toInteger()
        return res
    }


    Team getTeam()
    {
        Actor.getReference(Team,team)
    }

    void setTeam(Team team)
    {
        this.team = team.identity
        this.teamMembers = await(team.getTeamMembers()).collect{ it.identity }
    }

    Set<Player> getTeamMembes()
    {
        teamMembers?.collect{ Actor.getReference(Player,it) }
    }
}
