package shepard.backend.util.glicko2

import cloud.orbit.actors.Actor
import groovy.transform.Canonical
import groovy.transform.InheritConstructors
import groovy.transform.ToString
import shepard.backend.service.player.Player

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 21/04/17.
 * email= gueorgui.tzvetoslavov@gmail.com
 **/
@InheritConstructors
@Canonical
@ToString(includeNames = true, includePackage = false)
class ResultResume
{
    String player
    double score
    int wins
    int loses
    int draws
    int rounds

    ResultResume(Player player)
    {
        this.player = player.identity
        score = 0.0d
        wins = 0
        loses = 0
        draws = 0
        rounds = 0
    }

    ResultResume(Player player, List<Result> results)
    {
        this.player = player.identity
        score = 0.0d
        wins = 0
        loses = 0
        draws = 0
        rounds = 0
        this.update(results)
    }

    ResultResume(Player player, Result results)
    {
        this.player = player.identity
        score = 0.0d
        wins = 0
        loses = 0
        draws = 0
        rounds = 0
        this.update(results)
    }

    def update(List<Result> results)
    {
        results.each { result -> update(result) }
    }

    def update(Result result)
    {

        if (result.participated(getPlayer()))
        {
            score += result.getScore(getPlayer())

            if (result.isDraw())
                draws++
            else if (result.winner == getPlayer())
                wins++
            else
                loses++

            rounds++
        }
    }

    Player getPlayer()
    {
        Actor.getReference(Player,player)
    }

    void setPlayer(Player player)
    {
        this.player == player.identity
    }

    @Deprecated
    def redux(int pit)
    {
        def res = new ResultResume(player= this.getPlayer())
        res.score = this.score/pit
        res.wins = (this.wins/pit).toInteger()
        res.loses = (this.loses/pit).toInteger()
        res.draws = (this.draws/pit).toInteger()
        res.rounds = (this.rounds/pit).toInteger()
        return res
    }


}
