package shepard.backend.util.id

import shepard.backend.service.matchmaking.entry.Lobby

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 11/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class TeamIdGenerator
{
    static String generateTeamID(Lobby lobby, int counter)
    {
        "$lobby.identity:$counter"
    }
}
