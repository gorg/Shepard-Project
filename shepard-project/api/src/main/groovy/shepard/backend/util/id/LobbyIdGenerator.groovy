package shepard.backend.util.id
/**
 * Created by Gueorgui Tzvetoslavov Topalski on 24/01/17.
 * email: gueorgui.tzvetoslavov@gmail.com*/
class LobbyIdGenerator
{
    static String generateLobbyId(String lobbyId,int counter)
    {
        "$lobbyId:lobby$counter"
    }
}
