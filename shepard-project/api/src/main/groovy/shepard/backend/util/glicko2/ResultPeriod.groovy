package shepard.backend.util.glicko2

import cloud.orbit.actors.Actor
import groovy.transform.ToString
import shepard.backend.service.matchmaking.util.Team
import shepard.backend.service.player.Player

import java.util.stream.Collectors

import static com.ea.async.Async.await

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/25/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
@ToString(includeNames=true, includeFields=true, ignoreNulls = true)
class ResultPeriod
{
    def results = new ArrayList<Result>()
    def players = new HashSet<String>()

    def addResult(Player winner, Player loser)
    {
        if (winner && loser)
        {
            players.add(winner.identity)
            players.add(loser.identity)
            results.add(new Result(winner,loser))
        }
    }

    def addDraw(Player playerA, Player playerB)
    {
        if (playerA && playerB)
        {
            players.add(playerA.identity)
            players.add(playerB.identity)
            results.add(new Result(playerA,playerB,true))
        }
    }

    List<Result> getResults(Player player)
    {
        results.stream().filter{ it.participated(player) }.collect(Collectors.toList())
    }

    List<Result> getAllResults()
    {
        results
    }

    List<Player> getParticipants()
    {
       players.collect { Actor.getReference(Player, it) }
    }

    def addParticipant(Player participant)
    {
        players.add(participant.identity)
    }

    def clear()
    {
        results.clear()
    }

    List<ResultResume> getResume()
    {
        players.stream()
                .sorted()
                .map{ Actor.getReference(Player,it)}
                .map{ player -> new ResultResume(player,getResults(player))}
                .collect(Collectors.toList())
    }

    TeamResultResume getTeamResume(Team team)
    {
        def members = await(team.getTeamMembers())
        
        TeamResultResume resume     

        if ( players.containsAll(members.collect{ it.identity}))
        {
            def results = members.stream()
                        .map{ player -> getResults(player)}
                        .flatMap{ it.stream() }
                        .distinct()
                        .collect(Collectors.toList())

            resume = new TeamResultResume(team, results)
        }
        return resume
    }
}
