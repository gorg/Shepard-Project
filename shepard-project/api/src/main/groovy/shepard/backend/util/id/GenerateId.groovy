package shepard.backend.util.id

import cloud.orbit.actors.Actor
import shepard.backend.service.player.Player

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/21/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class GenerateId
{
    static String generateId(Actor actor, Player player)
    {
        actor.getIdentity() + ":" + player.getIdentity()
    }
}
