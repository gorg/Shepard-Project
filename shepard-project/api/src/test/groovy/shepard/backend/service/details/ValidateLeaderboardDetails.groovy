package shepard.backend.service.details

import shepard.backend.service.leaderboard.details.LeaderboardDetails
import shepard.backend.service.leaderboard.details.LeaderboardSort
import shepard.backend.service.leaderboard.details.LeaderboardType
import spock.lang.Specification

import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 25/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class ValidateLeaderboardDetails extends Specification
{
    def "Empty"()
    {
        when:
        def details = new LeaderboardDetails()

        then:
        details.name == null
        details.icon == null
        details.type == LeaderboardType.ABSOLUTE
        details.sort == LeaderboardSort.HIGH
        details.lowerLimit == 0
        details.upperLimit == Long.MAX_VALUE
        !details.validate()
    }

    def "With icon"()
    {
        when:
        def details = new LeaderboardDetails(icon: "Should fail Leaderboard")

        then:
        details.icon == "Should fail Leaderboard"
        details.name == null
        details.type == LeaderboardType.ABSOLUTE
        details.sort == LeaderboardSort.HIGH
        details.lowerLimit == 0
        details.upperLimit == Long.MAX_VALUE
        !details.validate()
    }

    def "With type"()
    {
        when:
        def details = new LeaderboardDetails(type: "Should fail Leaderboard")

        then:
        thrown(IllegalArgumentException)
    }

    def "With name"()
    {
        when:
        def details = new LeaderboardDetails(name: "Test Leaderboard")

        then:
        details.id() == name2Id("Test Leaderboard")
        details.name == "Test Leaderboard"
        details.icon == null
        details.type == LeaderboardType.ABSOLUTE
        details.sort == LeaderboardSort.HIGH
        details.lowerLimit == 0
        details.upperLimit == Long.MAX_VALUE
        details.validate()
    }

    def "ABSOLUTE"()
    {
        when:
        def details = new LeaderboardDetails(name: "Test Leaderboard", type: LeaderboardType.ABSOLUTE)

        then:
        details.id() == name2Id("Test Leaderboard")
        details.name == "Test Leaderboard"
        details.icon == null
        details.type == LeaderboardType.ABSOLUTE
        details.sort == LeaderboardSort.HIGH
        details.lowerLimit == 0
        details.upperLimit == Long.MAX_VALUE
        details.validate()
    }

    def "INCREMENTAL"()
    {
        when:
        def details = new LeaderboardDetails(name: "Test Leaderboard", type: LeaderboardType.INCREMENTAL)

        then:
        details.id() == name2Id("Test Leaderboard")
        details.name == "Test Leaderboard"
        details.icon == null
        details.type == LeaderboardType.INCREMENTAL
        details.sort == LeaderboardSort.HIGH
        details.lowerLimit == 0
        details.upperLimit == Long.MAX_VALUE
        details.validate()
    }

    def "ONLY_HIGHER"()
    {
        when:
        def details = new LeaderboardDetails(name: "Test Leaderboard", type: LeaderboardType.ONLY_HIGHER)

        then:
        details.id() == name2Id("Test Leaderboard")
        details.name == "Test Leaderboard"
        details.icon == null
        details.type == LeaderboardType.ONLY_HIGHER
        details.sort == LeaderboardSort.HIGH
        details.lowerLimit == 0
        details.upperLimit == Long.MAX_VALUE
        details.validate()
    }

    def "ONLY_LOWER"()
    {
        when:
        def details = new LeaderboardDetails(name: "Test Leaderboard", type: LeaderboardType.ONLY_LOWER)

        then:
        details.id() == name2Id("Test Leaderboard")
        details.name == "Test Leaderboard"
        details.icon == null
        details.type == LeaderboardType.ONLY_LOWER
        details.sort == LeaderboardSort.HIGH
        details.lowerLimit == 0
        details.upperLimit == Long.MAX_VALUE
        details.validate()
    }

    def "Random string as type"()
    {
        when:
        def details = new LeaderboardDetails(name: "Test Leaderboard", type: "91sdpaikhjcas")

        then:
        thrown(IllegalArgumentException)
    }

    def "Empty string as type"()
    {
        when:
        def details = new LeaderboardDetails(name: "Test Leaderboard", type: '')

        then:
        thrown(IllegalArgumentException)
    }

    def "Null string as type"()
    {
        when:
        def details = new LeaderboardDetails(name: "Test Leaderboard", type: null)

        then:
        details.id() == name2Id("Test Leaderboard")
        details.name == "Test Leaderboard"
        details.icon == null
        details.type == null
        details.sort == LeaderboardSort.HIGH
        details.lowerLimit == 0
        details.upperLimit == Long.MAX_VALUE
        details.validate()
        details.type == LeaderboardType.ABSOLUTE

    }

    def "HIGH"()
    {
        when:
        def details = new LeaderboardDetails(name: "Test Leaderboard", sort: LeaderboardSort.HIGH)

        then:
        details.id() == name2Id("Test Leaderboard")
        details.name == "Test Leaderboard"
        details.icon == null
        details.type == LeaderboardType.ABSOLUTE
        details.sort == LeaderboardSort.HIGH
        details.lowerLimit == 0
        details.upperLimit == Long.MAX_VALUE
        details.validate()
    }

    def "LOW"()
    {
        when:
        def details = new LeaderboardDetails(name: "Test Leaderboard", sort: LeaderboardSort.LOW)

        then:
        details.id() == name2Id("Test Leaderboard")
        details.name == "Test Leaderboard"
        details.icon == null
        details.type == LeaderboardType.ABSOLUTE
        details.sort == LeaderboardSort.LOW
        details.lowerLimit == 0
        details.upperLimit == Long.MAX_VALUE
        details.validate()
    }

    def "Equal limits"()
    {

        when:
        def details = new LeaderboardDetails(name: "Test Leaderboard", lowerLimit: 5, upperLimit: 5)

        then:
        details.id() == name2Id("Test Leaderboard")
        details.name == "Test Leaderboard"
        details.icon == null
        details.type == LeaderboardType.ABSOLUTE
        details.sort == LeaderboardSort.HIGH
        details.lowerLimit == 5
        details.upperLimit == 5
        !details.validate()
    }


    def "Lower limit with bigger value than the upper limit"()
    {

        when:
        def details = new LeaderboardDetails(name: "Test Leaderboard", lowerLimit: 100, upperLimit: -100)

        then:
        details.id() == name2Id("Test Leaderboard")
        details.name == "Test Leaderboard"
        details.icon == null
        details.type == LeaderboardType.ABSOLUTE
        details.sort == LeaderboardSort.HIGH
        details.lowerLimit == 100
        details.upperLimit == -100
        !details.validate()
    }
}
