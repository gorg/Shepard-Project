package shepard.backend.service.leaderboard.score

import shepard.backend.service.leaderboard.score.details.CurrencyScore
import shepard.backend.service.leaderboard.score.details.NumericalScore
import shepard.backend.service.leaderboard.score.details.TimeScore
import spock.lang.Specification

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 10/27/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class CurrencyScoreSpec extends Specification
{
    def "Empty currency score"()
    {
        given:
        def score = new CurrencyScore()

        expect:
        score.getScoreLocale() == "\$0.00"

    }

    def "Value currency score"()
    {
        given:
        def score = new CurrencyScore(5000)

        expect:
        //The value is 1/1000000 of the base
        score.getScoreLocale() == "\$0.01"

    }

    def "Unit currency score"()
    {
        given:
        def score = new CurrencyScore(1000000)

        expect:
        //The value is 1/1000000 of the base
        score.getScoreLocale() == "\$1.00"

    }

    def "Locale currency score"()
    {
        given:
        def score = new CurrencyScore(555444333222)

        expect:
        score.getScoreLocale(a) == b

        where:
        a                      || b
        Locale.US              || "\$555,444.33"
        new Locale("es", "ES") || "555.444,33 €"
        Locale.CHINA           || "￥555,444.33"
    }

    def "Compare scores"()
    {
        given:
        def score = new CurrencyScore(555444333)

        expect:
        score.compareTo(a) == b
        a.compareTo(score) == c

        where:
        a                             || b  || c
        new CurrencyScore(1122)       || 1  || -1
        new CurrencyScore(555444333)  || 0  || 0
        new CurrencyScore(666444333)  || -1 || 1
        new NumericalScore(555444333) || 0  || 0
        new TimeScore(555444333)      || 0  || 0

    }
}
