package shepard.backend.service.details

import shepard.backend.service.matchmaking.details.RoomDetails
import shepard.backend.service.matchmaking.details.Rules
import spock.lang.Specification

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 25/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class ValidateRoomDetailsSpec extends Specification
{
    def "Empty"()
    {
        when:
        def details = new RoomDetails()

        then:
        details.name == null
        details.icon == null
        details.rules == new Rules()
        details.rules.ranked == true
        details.rules.players == 8
        details.rules.teams == 2
        details.rules.overlap == 0.75
    }

    def "With icon"()
    {
        when:
        def details = new RoomDetails(icon: "Should fail Lobby")

        then:
        details.name == null
        details.icon == "Should fail Lobby"
        details.rules == new Rules()
        details.rules.ranked == true
        details.rules.players == 8
        details.rules.teams == 2
        details.rules.overlap == 0.75
    }

    def "With name"()
    {
        when:
        def details = new RoomDetails(name: "Test Lobby")

        then:
        details.name == "Test Lobby"
        details.icon == null
        details.rules == new Rules()
        details.rules.ranked == true
        details.rules.players == 8
        details.rules.teams == 2
        details.rules.overlap == 0.75
        details.validate()
    }

    def "With 1 player"()
    {
        when:
        def rules = new Rules(players: 1)
        def details = new RoomDetails(name: "Test Lobby", rules: rules)

        then:
        details.name == "Test Lobby"
        details.icon == null
        details.icon == null
        details.rules.ranked == true
        details.rules.players == 1
        details.rules.teams == 2
        details.rules.overlap == 0.75
        !details.validate()
    }

    def "With 4 players"()
    {
        when:
        def rules = new Rules(players: 4)
        def details = new RoomDetails(name: "Test Lobby", rules: rules)

        then:
        details.name == "Test Lobby"
        details.icon == null
        details.icon == null
        details.rules.ranked == true
        details.rules.players == 4
        details.rules.teams == 2
        details.rules.overlap == 0.75
        details.validate()
    }

    def "With 33 players"()
    {
        when:
        def rules = new Rules(players: 65)
        def details = new RoomDetails(name: "Test Lobby", rules: rules)

        then:
        details.name == "Test Lobby"
        details.icon == null
        details.icon == null
        details.rules.ranked == true
        details.rules.players == 65
        details.rules.teams == 2
        details.rules.overlap == 0.75
        !details.validate()
    }

    def "With 1 team"()
    {
        when:
        def rules = new Rules(teams: 1)
        def details = new RoomDetails(name: "Test Lobby", rules: rules)

        then:
        details.name == "Test Lobby"
        details.icon == null
        details.icon == null
        details.rules.ranked == true
        details.rules.players == 8
        details.rules.teams == 1
        details.rules.overlap == 0.75
        !details.validate()
    }

    def "With 2 team"()
    {
        when:
        def rules = new Rules(teams: 2)
        def details = new RoomDetails(name: "Test Lobby", rules: rules)

        then:
        details.name == "Test Lobby"
        details.icon == null
        details.icon == null
        details.rules.ranked == true
        details.rules.players == 8
        details.rules.teams == 2
        details.rules.overlap == 0.75
        details.validate()
    }

    def "With 9 team"()
    {
        when:
        def rules = new Rules(teams: 9)
        def details = new RoomDetails(name: "Test Lobby", rules: rules)

        then:
        details.name == "Test Lobby"
        details.icon == null
        details.icon == null
        details.rules.ranked == true
        details.rules.players == 8
        details.rules.teams == 9
        details.rules.overlap == 0.75
        !details.validate()
    }


}
