package shepard.backend.service.leaderboard.score

import shepard.backend.service.leaderboard.score.details.CurrencyScore
import shepard.backend.service.leaderboard.score.details.NumericalScore
import shepard.backend.service.leaderboard.score.details.TimeScore
import spock.lang.Specification

class TimeScoreSpec extends Specification
{

    def "Empty time score"()
    {
        given:
        def score = new TimeScore()

        expect:
        score.getScoreLocale() == "00:00:00"

    }


    def "Value time score"()
    {
        given:
        def score = new TimeScore(66501)

        expect:
        score.getScoreLocale() == "00:01:06.501"

    }

    def "Big value time score"()
    {
        given:
        def score = new TimeScore(555444333222)

        expect:
        score.getScoreLocale() == "18:05:33.222"
    }


    def "Big value time score with locale"()
    {
        given:
        def score = new TimeScore(555444333222)

        expect:
        score.getScoreLocale(a) == b

        where:
        a || b
        Locale.US               || "18:05:33.222"
        new Locale("es", "ES")  || "18:05:33.222"
        Locale.CHINA            || "18:05:33.222"

    }

    def "Compare scores"()
    {
        given:
        def score = new TimeScore(555444333)

        expect:
        score.compareTo(a) == b
        a.compareTo(score) == c

        where:
        a || b || c
        new TimeScore(1122)             || 1    || -1
        new TimeScore(555444333)        || 0    || 0
        new TimeScore(666444333)        || -1   || 1
        new NumericalScore(555444333)   || 0    || 0
        new CurrencyScore(555444333)    || 0    || 0
    }
}

