package shepard.backend.service.leaderboard.score

import shepard.backend.service.leaderboard.score.details.CurrencyScore
import shepard.backend.service.leaderboard.score.details.NumericalScore
import shepard.backend.service.leaderboard.score.details.TimeScore
import spock.lang.Specification

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 22/10/16.
 * email: gueorgui.tzvetoslavov@gmail.com
 */
class NumericalScoreSpec extends Specification
{

    def "Empty score"()
    {
        when:
        def score = new NumericalScore()

        then:
        score.getScoreLocale() == "0"
    }

    def "Numerical score"()
    {
        when:
        def score = new NumericalScore(10)

        then:
        score.getScoreLocale() == "10"
    }

    def "Locale score"()
    {
        given:
        def score = new NumericalScore(555444333222)

        expect:
        score.getScoreLocale(a) == b

        where:
        a                      || b
        Locale.US              || "555,444,333,222"
        new Locale("es", "ES") || "555.444.333.222"
        Locale.CHINA           || "555,444,333,222"
    }

    def "Void locale score"()
    {
        given:
        def score = new NumericalScore(555444333222)

        expect:
        score.getScoreLocale() == "555,444,333,222"
    }

    def "Double locale score"()
    {
        given:
        def score = new NumericalScore(555444333.22)

        expect:
        score.getScoreLocale(a) == b

        where:
        a                      || b
        Locale.US              || "555,444,333.22"
        new Locale("es", "ES") || "555.444.333,22"
        Locale.CHINA           || "555,444,333.22"
    }

    def "Void double locale score"()
    {
        given:
        def score = new NumericalScore(555444333.22)

        expect:
        score.getScoreLocale() == "555,444,333.22"
    }

    def "Compare scores"()
    {
        given:
        def score = new NumericalScore(555444333)

        expect:
        score.compareTo(a) == b
        a.compareTo(score) == c

        where:
        a                             || b  || c
        new NumericalScore(1122)      || 1  || -1
        new NumericalScore(555444333) || 0  || 0
        new NumericalScore(666444333) || -1 || 1
        new TimeScore(555444333)      || 0  || 0
        new CurrencyScore(555444333)  || 0  || 0

    }


}
