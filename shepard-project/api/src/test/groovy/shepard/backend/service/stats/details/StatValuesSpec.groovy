package shepard.backend.service.stats.details

import spock.lang.Specification
import spock.lang.Unroll

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/17/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class StatValuesSpec extends Specification
{
    @Unroll("Default stat new:#newValue old:#oldValue -> #valid")
    def "Default stat test"()
    {
        setup:
        def stat = new StatDetails()

        expect:
        assert valid == stat.test(newValue,oldValue)

        where:
        newValue    |oldValue   ||valid
        0           |0          ||false
        null        |null       ||false
        -1          |0          ||false //Default is increment only
        -5          |0          ||false
        0           |-1000      ||false //Default min value == 0
        0           |1000       ||false
        5           |6          ||false
        6           |6          ||false
        7           |6          ||true
        8           |null       ||true
        null        |8          ||false
        3.14        |null       ||true
        null        |7.5        ||false
        6.52        |5.71       ||true
        6.53339     |6.53338    ||true
    }

    @Unroll("Limit test: -5.3 < #oldValue < #newValue < 6 -> #valid")
    def "Limit stat test"()
    {
        expect:
        def stat = new StatDetails(minValue: -5.3, maxValue: 6)
        assert valid == stat.test(newValue,oldValue)

        where:
        newValue    |oldValue   ||valid
        0           |0          ||false
        null        |null       ||false
        -8          |-6         ||false //Values below lower limit
        7           |8.3        ||false //Values above upper limit
        1           |0.52       ||true
        5.99        |5          ||true
        6           |0          ||true
        6.00000001  |0          ||false
        null        |8          ||false
        null        |5          ||false
        8           |null       ||false //Old value is null and new value is above limit so default value is returned
        5           |null       ||true
        -7.2        |null       ||false //Old value is null and new value is below limit so default value is returned
        -4          |null       ||false //When null old value is 0 (the default) and 0 > -4
    }

    @Unroll("Max change test: abs(#newValue-#oldValue) < 4 -> #valid")
    def "Max change test"()
    {
        expect:
        def stat = new StatDetails(maxChange: 4)
        assert valid == stat.test(newValue,oldValue)
    
        where:
        newValue    | oldValue  || valid
        0           | 0         || false    //If equal values, valid will be false
        2.45        | 2.45      || false    //If equal values, valid will be false
        1           | 0         || true
        2           | 0         || true
        3           | 0         || true
        4           | 0         || true
        4.5         | 0         || false
        5           | 0         || false
        4.5         | 1         || true
        5           | 1         || true
        0           | 4         || false    //Increment only by default
    }

    @Unroll("Can decrement stats #newValue #oldValue -> #valid")
    def "Decrement tests"()
    {
        expect:
        def stat = new StatDetails(incrementOnly: false)
        assert valid == stat.test(newValue,oldValue)

        where:
        newValue    | oldValue  || valid
        0           | 0         || false    //If equal values, valid will be false
        -1          | -1        || false    //If equal values, valid will be false
        1           | 0         || true
        2           | 0         || true
        1           | 5         || true
        2           | 6         || true
        1           | 7         || true
        2           | 8         || true
        -1          | 0         || false    //default minValue == 0
        -5          | 0         || false
        -100        | -5        || false
        -50.569     | -6        || false
        1           | -558.56   || false    //invalid old value
        -0.556      | 100000    || false
    }

    @Unroll("Can decrement with maxChange(1.25) #newValue #oldValue -> #valid")
    def "Decrement tests with max change"()
    {
        expect:
        def stat = new StatDetails(incrementOnly: false, maxChange: 1.25)
        assert valid == stat.test(newValue,oldValue)

        where:
        newValue    | oldValue  || valid
        0           | 0         || false    //If equal values, valid will be false
        -1          | -1        || false    //If equal values, valid will be false
        1           | 0         || true
        2           | 0         || false
        -1          | 0         || false    //default min value == 0
        -5          | 0         || false
        -1.24       | 0         || false
        -1.25       | 0         || false
        -1.26       | 0         || false
        3.24        | 2         || true
        3.25        | 2         || true
        3.26        | 2         || false
        0.76        | 2         || true
        0.75        | 2         || true
        0.74        | 2         || false
    }

    @Unroll("Full spec test #newValue,#oldValue -> #valid")
    def "Full spec stat test"()
    {
        expect:
        def stat = new StatDetails(
                minValue: -62.1,
                maxValue: 100.523,
                maxChange: 5.5,
                defaultValue: 12.25,
                incrementOnly: false)
        assert valid == stat.test(newValue,oldValue)

        where:
        newValue    | oldValue  || valid
        0           | null      || false    //Null old is like default. Fails for maxChange
        9           | 9         || false
        4.5         | 0         || true
        5           | 0         || true
        5.5         | 0         || true
        5.6         | 0         || false
        100.523     | 95.023    || true
        100.524     | 95.023    || false
        99          | 100.523   || true
        98          | 100.523   || true
        97          | 100.523   || true
        96          | 100.523   || true
        95.024      | 100.523   || true
        95.023      | 100.523   || true
        95.022      | 100.523   || false
        95          | 100.523   || false
        101         | 100.523   || false
        100.524     | 100.523   || false
        -56.4       | -62.1     || false
        -56.5       | -62.1     || false
        -56.6       | -62.1     || true
        -56.7       | -62.1     || true
        -63         | -62.1     || false
        -62.2       | -62.1     || false
    }
}
