package shepard.backend.service.details

import shepard.backend.service.stats.details.StatDetails
import spock.lang.Specification

import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 25/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class ValidateStatDetailsSpec extends Specification
{
    def "Empty"()
    {
        when:
        def details = new StatDetails()

        then:
        details.name == null
        details.icon == null
        details.minValue == 0
        details.maxValue == BigDecimal.valueOf(Long.MAX_VALUE,4)
        details.maxChange == 0
        details.defaultValue == 0
        details.incrementOnly == true
        !details.validate()
    }

    def "With icon"()
    {
        when:
        def details = new StatDetails(icon: "Should fail Stat")

        then:
        details.name == null
        details.icon == "Should fail Stat"
        details.minValue == 0
        details.maxValue == BigDecimal.valueOf(Long.MAX_VALUE,4)
        details.maxChange == 0
        details.defaultValue == 0
        details.incrementOnly == true
        !details.validate()
    }

    def "With name"()
    {
        when:
        def details = new StatDetails(name: "Test Stat")

        then:
        details.id() == name2Id("Test Stat")
        details.name == "Test Stat"
        details.minValue == 0
        details.maxValue == BigDecimal.valueOf(Long.MAX_VALUE,4)
        details.maxChange == 0
        details.defaultValue == 0
        details.incrementOnly == true
        details.validate()
    }

    def "INTEGER with bad values"()
    {
        when:
        def details = new StatDetails(name: "Test Stat", defaultValue: -5)

        then:
        details.id() == name2Id("Test Stat")
        details.name == "Test Stat"
        details.minValue == 0
        details.maxValue == BigDecimal.valueOf(Long.MAX_VALUE,4)
        details.maxChange == 0
        details.defaultValue == -5
        details.incrementOnly == true
        !details.validate()
    }

    def "FLOAT with bad values"()
    {
        when:
        def details = new StatDetails(name: "Test Stat", defaultValue: -5)

        then:
        details.id() == name2Id("Test Stat")
        details.name == "Test Stat"
        details.minValue == 0
        details.maxValue == BigDecimal.valueOf(Long.MAX_VALUE,4)
        details.maxChange == 0
        details.defaultValue == -5
        details.incrementOnly == true
        !details.validate()
    }

    def "INTEGER with bad min/max"()
    {
        when:
        def details = new StatDetails(name: "Test Stat", maxValue: 10, minValue: 20)

        then:
        details.id() == name2Id("Test Stat")
        details.name == "Test Stat"
        details.minValue == 20
        details.maxValue == 10
        details.maxChange == 0
        details.defaultValue == 0
        details.incrementOnly == true
        !details.validate()
    }

    def "FLOAT with bad min/max"()
    {
        when:
        def details = new StatDetails(name: "Test Stat", maxValue: 0.5, minValue: 1.1)

        then:
        details.id() == name2Id("Test Stat")
        details.name == "Test Stat"
        details.minValue == 1.1
        details.maxValue == 0.5
        details.maxChange == 0
        details.defaultValue == 0
        details.incrementOnly == true
        !details.validate()
    }

}
