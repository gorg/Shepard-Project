package shepard.backend.service.stats.details

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import spock.lang.Specification

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 10/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class StatDetailsSpec extends Specification
{
    def "Default details"()
    {
        setup:
        def details = new StatDetails(name: "Sample Details")

        expect:
        assert details.name == "Sample Details"
        assert details.maxChange == 0
        assert details.minValue == 0
        assert details.maxValue == BigDecimal.valueOf(Long.MAX_VALUE,4)
        assert details.defaultValue == 0
        assert details.incrementOnly
    }

    def "Integer type"()
    {
        setup:
        def details = new StatDetails(name: "Sample Integer",
                maxChange: 5,
                minValue: -10,
                maxValue: 1000,
                defaultValue: 10,
                incrementOnly: false)

        expect:
        assert details.name == "Sample Integer"
        assert details.maxChange == 5
        assert details.minValue == -10
        assert details.maxValue == 1000
        assert details.defaultValue == 10
        assert !details.incrementOnly
    }

    def "Float type"()
    {
        setup:
        def details = new StatDetails(name: "Sample Integer",
                maxChange: 5.1,
                minValue: -10.2,
                maxValue: 1000.3,
                defaultValue: 10.4,
                incrementOnly: false)

        expect:
        assert details.name == "Sample Integer"
        assert details.maxChange == 5.1
        assert details.minValue == -10.2
        assert details.maxValue == 1000.3
        assert details.defaultValue == 10.4
        assert !details.incrementOnly
    }

    def "Test method with Integers"()
    {
        setup:
        def details = new StatDetails(name: "Sample Integer",
                maxChange: 0,
                minValue: -5,
                maxValue: 5,
                defaultValue: 0,
                incrementOnly: false)

        expect:
        assert !details.test(null, null)

        //Limit testing
        assert details.test(5, null)
        assert !details.test(6, null)
        assert details.test(-5, null)
        assert !details.test(-6, null)

        //Old value out of bounds
        assert !details.test(5, 20)
        assert !details.test(6, 20)
        assert !details.test(-5, 20)
        assert !details.test(-6, 20)


        assert !details.test(-6, 0) //Out of limits
        assert details.test(-5, 0)
        assert details.test(-4, 0)
        assert details.test(-3, 0)
        assert details.test(-2, 0)
        assert details.test(-1, 0)
        assert !details.test(0, 0)
        assert details.test(1, 0)
        assert details.test(2, 0)
        assert details.test(3, 0)
        assert details.test(4, 0)
        assert details.test(5, 0)
        assert !details.test(6, 0) //Out of limits

    }

    def "Test method with Floats"()
    {
        setup:
        def details = new StatDetails(name: "Sample float",
                maxChange: 0,
                minValue: -1.1,
                maxValue: 1.1,
                defaultValue: 0,
                incrementOnly: false)

        expect:
        assert !details.test(null, null)

        //Limit testing
        assert details.test(1.1, null)
        assert !details.test(1.100000001, null)
        assert details.test(-1.1, null)
        assert !details.test(-1.100000001, null)

        //Old value out of bounds
        assert !details.test(1, 2)
        assert !details.test(1, 2)
        assert !details.test(-1, 2)
        assert !details.test(-1, 2)

        assert !details.test(-1.11, 0) //Out of limits
        assert details.test(-1.10, 0)
        assert details.test(-1.09, 0)
        assert details.test(-1.08, 0)
        assert details.test(-1.07, 0)
        assert details.test(-1.06, 0)
        assert details.test(-1.05, 0)
        assert details.test(-1.04, 0)
        assert details.test(-1.03, 0)
        assert details.test(-1.02, 0)
        assert details.test(-1.01, 0)
        assert details.test(-1.0, 0)
        assert details.test(-0.9, 0)
        assert details.test(-0.8, 0)
        assert details.test(-0.7, 0)
        assert details.test(-0.6, 0)
        assert details.test(-0.5, 0)
        assert details.test(-0.4, 0)
        assert details.test(-0.3, 0)
        assert details.test(-0.2, 0)
        assert details.test(-0.1, 0)
        assert !details.test(0, 0)

        assert !details.test(1.11, 0) //Out of limits
        assert details.test(1.10, 0)
        assert details.test(1.09, 0)
        assert details.test(1.08, 0)
        assert details.test(1.07, 0)
        assert details.test(1.06, 0)
        assert details.test(1.05, 0)
        assert details.test(1.04, 0)
        assert details.test(1.03, 0)
        assert details.test(1.02, 0)
        assert details.test(1.01, 0)
        assert details.test(1.0, 0)
        assert details.test(0.9, 0)
        assert details.test(0.8, 0)
        assert details.test(0.7, 0)
        assert details.test(0.6, 0)
        assert details.test(0.5, 0)
        assert details.test(0.4, 0)
        assert details.test(0.3, 0)
        assert details.test(0.2, 0)
        assert details.test(0.1, 0)
    }

    def "Max change test"()
    {
        setup:
        def details = new StatDetails(name: "Sample Integer",
                maxChange: 2,
                minValue: -5,
                maxValue: 5,
                defaultValue: 0,
                incrementOnly: false)

        expect:
        assert details.test(1,0)
        assert details.test(2,0)
        assert !details.test(3,0) //  |3-0|=3 > 2 (maxChange)
        assert !details.test(4,0)

        assert details.test(-1,0)
        assert details.test(-2,0)
        assert !details.test(-3,0) // |-3-0|=3 > 2 (maxChange)
        assert !details.test(-4,0)
    }

    def "Max change test with increment only"()
    {
        setup:
        def details = new StatDetails(name: "Sample Integer",
                maxChange: 2,
                minValue: -5,
                maxValue: 5,
                defaultValue: 0,
                incrementOnly: true)

        expect:
        assert details.test(1,0)
        assert details.test(2,0)
        assert !details.test(3,0) //  |3-0|=3 > 2 (maxChange)
        assert !details.test(4,0)

        assert !details.test(-1,0)
        assert !details.test(-2,0)
        assert !details.test(-3,0)
        assert !details.test(-4,0)
    }

    def "Max change test with floats"()
    {
        setup:
        def details = new StatDetails(name: "Sample float",
                maxChange: 1.1,
                minValue: -5.1,
                maxValue: 5.1,
                defaultValue: 0,
                incrementOnly: false)

        expect:
        assert details.test(1,0)
        assert details.test(1.1,0)
        assert !details.test(1.2,0) //  |1.2-0|=1.2 > 1.1 (maxChange)
        assert !details.test(2.2,0)

        assert details.test(-1,0)
        assert details.test(-1.1,0)
        assert !details.test(-1.2,0) // |-1.2-0|=1.2 > 2 (maxChange)
        assert !details.test(-2.2,0)
    }

    def "Max change test with floats and increment only"()
    {
        setup:
        def details = new StatDetails(name: "Sample float",
                maxChange: 1.1,
                minValue: -5.1,
                maxValue: 5.1,
                defaultValue: 0,
                incrementOnly: true)

        expect:
        assert details.test(1,0)
        assert details.test(1.1,0)
        assert !details.test(1.2,0) //  |1.2-0|=1.2 > 1.1 (maxChange)
        assert !details.test(2.2,0)

        assert !details.test(-1,0)
        assert !details.test(-1.1,0)
        assert !details.test(-1.2,0) // |-1.2-0|=1.2 > 2 (maxChange)
        assert !details.test(-2.2,0)
    }

    def "JSON serialization/deserialization"()
    {
        setup:
        def details = new StatDetails(name: "Sample float",
                maxChange: 1.1,
                minValue: -5.1,
                maxValue: 5.1,
                defaultValue: 0,
                incrementOnly: true)

        def json = JsonOutput.toJson(details)
        def jsonSlurper = new JsonSlurper()


        when:
        def sd = jsonSlurper.parseText(json)

        then:
        assert sd instanceof Map
        assert sd.maxChange.class == BigDecimal
        assert sd.minValue.class == BigDecimal
        assert sd.maxValue.class == BigDecimal
        assert sd.defaultValue.class == Integer
        assert sd.incrementOnly.class == Boolean

        when:
        def parsedDetails = sd as StatDetails

        then:
        parsedDetails == details
    }
}