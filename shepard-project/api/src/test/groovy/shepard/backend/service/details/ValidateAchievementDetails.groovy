package shepard.backend.service.details

import shepard.backend.service.achievement.details.AchievementDetails
import shepard.backend.service.achievement.details.AchievementType
import spock.lang.Specification

import static shepard.util.string.StringUtils.name2Id

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 25/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class ValidateAchievementDetails extends Specification
{
    def "Empty"()
    {
        when:
        def details = new AchievementDetails()

        then:
        details.name == null
        details.description == null
        details.type == AchievementType.REVEALED
        details.icon == null
        !details.validate()
    }

    def "With description"()
    {
        when:
        def details = new AchievementDetails(description: "Should fail Achievement")

        then:
        details.name == null
        details.description == "Should fail Achievement"
        details.type == AchievementType.REVEALED
        details.icon == null
        !details.validate()
    }

    def "With icon"()
    {
        when:
        def details = new AchievementDetails(icon: "Should fail Achievement")

        then:
        details.name == null
        details.description == null
        details.type == AchievementType.REVEALED
        details.icon == "Should fail Achievement"
        !details.validate()
    }

    def "With type"()
    {
        when:
        def details = new AchievementDetails(type: "Should fail Achievement")

        then:
        thrown(IllegalArgumentException)
    }

    def "With name"()
    {
        when:
        def details = new AchievementDetails(name: "Test Achievement")

        then:
        details.id() == name2Id("Test Achievement")
        details.name == "Test Achievement"
        details.description == null
        details.icon == null
        details.type == AchievementType.REVEALED
        details.validate()
    }

    def "REVEALED"()
    {
        when:
        def details = new AchievementDetails(name: "Test Achievement", type: AchievementType.REVEALED)

        then:
        details.id() == name2Id("Test Achievement")
        details.name == "Test Achievement"
        details.description == null
        details.icon == null
        details.type == AchievementType.REVEALED
        details.validate()
    }

    def "HIDDEN"()
    {
        when:
        def details = new AchievementDetails(name: "Test Achievement", type: AchievementType.HIDDEN)

        then:
        details.id() == name2Id("Test Achievement")
        details.name == "Test Achievement"
        details.description == null
        details.icon == null
        details.type == AchievementType.HIDDEN
        details.validate()
    }

    def "Random string as type"()
    {
        when:
        def details = new AchievementDetails(name: "Test Achievement", type: "91sdpaikhjcas")

        then:
        thrown(IllegalArgumentException)
    }

    def "Empty string as type"()
    {
        when:
        def details = new AchievementDetails(name: "Test Achievement", type: '')

        then:
        thrown(IllegalArgumentException)
    }

    def "Null string as type"()
    {
        when:
        def details = new AchievementDetails(name: "Test Achievement", type: null)

        then:
        details.id() == name2Id("Test Achievement")
        details.name == "Test Achievement"
        details.description == null
        details.icon == null
        details.type == null
        details.validate()
        details.type == AchievementType.REVEALED

    }

    def "All params"()
    {
        when:
        def details = new AchievementDetails(name: "Test Achievement", type: AchievementType.HIDDEN, description: "Test Description", icon: "http://random.url")

        then:
        details.id() == name2Id("Test Achievement")
        details.name == "Test Achievement"
        details.description == "Test Description"
        details.icon == "http://random.url"
        details.type == AchievementType.HIDDEN
        details.validate()
    }

}
