package shepard.backend.service.matchmaking

import shepard.backend.service.matchmaking.details.Rules
import spock.lang.Specification

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 12/26/16.
 * email: gueorgui.tzvetoslavov@gmail.com*/
class RulesTest extends Specification
{
    def "Default ruleset"()
    {
        def rules = new Rules()

        expect:
        rules.ranked == true
        rules.overlap == 0.75
        rules.players == 8
        rules.teams == 2

    }

    def "Custom ruleset"()
    {
        def rules = new Rules(players: 6, teams: 3, overlap: 0.5d, ranked:true)

        expect:
        rules.ranked == true
        rules.overlap == 0.5
        rules.players == 6
        rules.teams == 3

    }

    def "Custom ruleset with defaults values"()
    {
        def rules = new Rules(ranked:true)

        expect:
        rules.ranked == true
        rules.overlap == 0.75
        rules.players == 8
        rules.teams == 2

    }
}
