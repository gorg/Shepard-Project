package shepard.backend.util.glicko2

import shepard.backend.service.player.Player
import spock.lang.Shared
import spock.lang.Specification

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/27/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class ResultSpec extends Specification
{
    @Shared Player playerOne = Stub { getIdentity() >> "player-one" }
    @Shared Player playerTwo = Stub { getIdentity() >> "player-two" }
    @Shared Player playerThree = Stub { getIdentity() >> "player-three" }

    def "Creating results"()
    {
        when:
        def result = new Result(playerOne,playerTwo)

        then:
        result.getWinner().identity == playerOne.identity
        result.getLoser().identity == playerTwo.identity


        when:
        new Result(playerOne,playerOne)

        then:
        thrown(IllegalArgumentException)

        when:
        new Result(playerTwo,playerTwo)

        then:
        thrown(IllegalArgumentException)

        when:
        new Result(playerOne,playerTwo,false)

        then:
        thrown(IllegalArgumentException)

    }

    def "Getting scores for winning and losing"()
    {
        setup:
        def result = new Result(playerOne,playerTwo)

        expect:
        result.getScore(a) == b

        where:
        a | b
        playerOne | 1.0d
        playerTwo | 0.0d
    }

    def "Getting scores for draw"()
    {
        setup:
        def draw = new Result(playerOne,playerTwo,true)

        expect:
        draw.getScore(a) == b

        where:
        a | b
        playerOne | 0.5d
        playerTwo | 0.5d
    }

    def "Participation"()
    {
        setup:
        def result = new Result(playerOne,playerTwo)

        expect:
        result.participated(playerOne)
        result.participated(playerTwo)
        !result.participated(playerThree)

    }

    def "Getting opponents"()
    {
        setup:
        def result = new Result(playerOne,playerTwo)

        expect:
        result.getOpponent(playerOne).identity == playerTwo.identity
        result.getOpponent(playerTwo).identity == playerOne.identity

        when:
        result.getOpponent(playerThree)

        then:
        thrown(IllegalArgumentException)
    }
}
