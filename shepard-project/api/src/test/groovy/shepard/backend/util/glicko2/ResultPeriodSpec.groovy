package shepard.backend.util.glicko2

import shepard.backend.service.player.Player
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/27/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class ResultPeriodSpec extends Specification
{
    @Shared
    Player playerOne = Stub { getIdentity() >> "player-one" }
    @Shared
    Player playerTwo = Stub { getIdentity() >> "player-two" }
    @Shared
    Player playerThree = Stub { getIdentity() >> "player-three" }

    @Unroll
    def "test result period"()
    {
        given:
        def res = new ResultPeriod()

        when:
        res.addResult(p1, p2)

        then:
        res.players.size() == size
        if(size == 2)
        {
            res.players.contains(p1.identity)
            res.players.contains(p2.identity)
        }

        where:
        p1        | p2          | size
        playerOne | playerTwo   | 2
        playerTwo | playerThree | 2
        null      | null        | 0
        playerTwo | null        | 0
        null      | playerOne   | 0

    }

    @Unroll
    def "test result period with same players"()
    {
        given:
        def res = new ResultPeriod()

        when:
        res.addResult(p1, p2)

        then:
        thrown(IllegalArgumentException)

        where:
        p1        | p2
        playerOne | playerOne
        playerTwo | playerTwo
    }

    @Unroll
    def "test for winner"()
    {
        given:
        def res = new ResultPeriod()

        when:
        res.addResult(winner, loser)

        then:
        if (!winner || !loser)
        {
            res.getAllResults().size() == 0
        }
        else res.getAllResults().size() == 1

        and:
        if (winner && loser)
        {
            res.getAllResults().each
            {
                assert it.winner.identity == winner.identity
                assert it.loser.identity == loser.identity
                assert it.getScore(winner) == 1.0d
                assert it.getScore(loser) == 0.0d
            }
        }

        where:
        winner      | loser
        playerOne   | playerTwo
        playerTwo   | playerThree
        playerThree | playerOne
        null        | null
        playerOne   | null
        null        | playerTwo
    }

    @Unroll
    def "test for draw"()
    {

        given:
        def res = new ResultPeriod()

        when:
        res.addDraw(winner, loser)

        then:
        if (winner&&loser)
        {
            res.getAllResults().each
            {
                assert it.getScore(winner) == 0.5d
                assert it.getScore(loser) == 0.5d
            }
        }
        else
        {
            assert res.getAllResults().size() == 0
        }

        where:
        winner      | loser
        playerOne   | playerTwo
        playerTwo   | playerThree
        playerThree | playerOne
        null        | null
        playerOne   | null
        null        | playerTwo
    }

    def "complex result periods"()
    {
        setup:
        def result = new ResultPeriod()

        when:
        result.addResult(playerOne,playerTwo)
        result.addResult(playerTwo,playerThree)

        then:
        result.getResults(playerOne).size() == 1
        result.getResults(playerOne).first().getWinner().identity == playerOne.identity
        result.getResults(playerOne).first().getLoser().identity == playerTwo.identity

        result.getResults(playerTwo).size() == 2
        result.getResults(playerTwo).get(0).getWinner().identity == playerOne.identity
        result.getResults(playerTwo).get(0).getLoser().identity == playerTwo.identity
        result.getResults(playerTwo).get(1).getWinner().identity == playerTwo.identity
        result.getResults(playerTwo).get(1).getLoser().identity == playerThree.identity

        result.getResults(playerThree).size() == 1
        result.getResults(playerThree).first().getWinner().identity == playerTwo.identity
        result.getResults(playerThree).first().getLoser().identity == playerThree.identity

        result.getResults(playerOne).first() == result.getResults(playerTwo).first()
        result.getResults(playerThree).first() == result.getResults(playerTwo).last()
    }
}
