#!/usr/bin/env bash

ps -ef | grep "shepard.backend.BackendMain" | grep -v grep | awk '{print $2}' | xargs kill -6

