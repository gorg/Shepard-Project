package shepard.util.string
/**
 * Created by Gueorgui Tzvetoslavov Topalski on 18/10/16.
 * email: gueorgui.tzvetoslavov@gmail.com
 */
class StringUtils
{
    static String name2Id(String s)
    {
        if (s != null && !s.isEmpty() && !s.isAllWhitespace())
        {
            s = s.trim()
            s = s.replaceAll(/\B[A-Z]/) { '-' + it }.toLowerCase()
            s = s.replace(' ','-')
            s = s.toLowerCase()
            return s
        }
        else
            null
    }

    static String id2Name(String s)
    {
        if (s != null && !s.isEmpty() && !s.isAllWhitespace())
        {
            s = s.replaceAll('-', ' ')
            s = s.capitalize()
            return s
        }
        else
            null
    }
}
