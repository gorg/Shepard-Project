package shepard.util.hashing

import org.mindrot.jbcrypt.BCrypt

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 21/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class BCHash
{
    static String hash(String plain)
    {
        return BCrypt.hashpw(plain, BCrypt.gensalt())
    }

    static boolean check(String plain, String hash)
    {
        return BCrypt.checkpw(plain,hash)
    }

}
