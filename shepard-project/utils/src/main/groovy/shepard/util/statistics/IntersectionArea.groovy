package shepard.util.statistics

import org.apache.commons.math3.distribution.NormalDistribution

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 12/20/16.
 * email: gueorgui.tzvetoslavov@gmail.com*/
class IntersectionArea
{
    static double intersect(double mu1, double sigma1, double mu2, double sigma2)
    {
        NormalDistribution normalDistribution1 =
                new NormalDistribution(mu1, sigma1)

        NormalDistribution normalDistribution2 =
                new NormalDistribution(mu2, sigma2)

        double min = Math.min(mu1 - 6 * sigma1, mu2 - 6 * sigma2)
        double max = Math.max(mu1 + 6 * sigma1, mu2 + 6 * sigma2)
        double range = max - min

        int resolution = (int) (range/Math.min(sigma1, sigma2))
        double partwidth = range / resolution
        double intersectionArea = 0


        int begin = (int)((Math.max(mu1 - 6 * sigma1, mu2 - 6 * sigma2)-min)/partwidth)
        int end = (int)((Math.min(mu1 + 6 * sigma1, mu2 + 6 * sigma2)-min)/partwidth)

        // Divide the range into N partitions
        for (int ii = begin; ii < end; ii++) {

            double partMin = partwidth * ii
            double partMax = partwidth * (ii + 1)

            double areaOfDist1 = normalDistribution1.probability(partMin, partMax)
            double areaOfDist2 = normalDistribution2.probability(partMin, partMax)

            intersectionArea += Math.min(areaOfDist1, areaOfDist2)
        }
        return intersectionArea
    }
}
