package shepard.util

import spock.lang.Specification

import static shepard.util.string.StringUtils.id2Name
import static shepard.util.string.StringUtils.name2Id

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/17/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class Id2Name extends Specification
{
    def "Expected"()
    {
        setup:
        def value = "This is the name of a new achievements"
        def id = name2Id(value)

        expect:
        id == "this-is-the-name-of-a-new-achievements"
        id2Name(id) == value
    }

    def "Empty"()
    {
        when: def test = id2Name("")
        then: test == null
    }

    def "Null"()
    {
        when: def test = id2Name(null)
        then: test == null
    }

    def "All white"()
    {
        when: def test = id2Name("                  ")
        then: test == null
    }

}
