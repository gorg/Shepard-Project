package shepard.util

import spock.lang.Specification

import static shepard.util.string.StringUtils.name2Id

/**
 *  Created by Gueorgui Tzvetoslavov Topalski on 11/17/16.
 *  email: gueorgui.tzvetoslavov@gmail.com
 */
class Name2Id extends Specification
{
    def "Empty"()
    {
        when: def test = name2Id("")
        then: test == null
    }

    def "Null"()
    {
        when: def test = name2Id(null)
        then: test == null
    }

    def "All white"()
    {
        when: def test = name2Id("                  ")
        then: test == null
    }


    def "Whitespace at beginig"()
    {
        when: def test = name2Id("     testString")
        then: test == "test-string"
    }

    def "Whitespace at end"()
    {
        when: def test = name2Id("testString     ")
        then: test == "test-string"
    }

    def "Whitespace to dash"()
    {
        when: def test = name2Id("test string")
        then: test == "test-string"
    }

    def "Expected"()
    {
        when: def test = name2Id("New Actor Id")
        then: test == "new-actor-id"
    }

    def "Expected with camel case"()
    {
        when: def test = name2Id("someKind of new Actor Id")
        then: test == "some-kind-of-new-actor-id"
    }

}
