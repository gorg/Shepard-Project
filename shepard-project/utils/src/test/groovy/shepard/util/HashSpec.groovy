package shepard.util

import shepard.util.hashing.BCHash
import spock.lang.Specification

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 21/03/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class HashSpec extends Specification
{

    def "Check hashing"()
    {
        setup:
        def plain = "Plain string to be hashed"
        def hash = BCHash.hash(plain)

        expect:
        BCHash.check(plain,hash)
    }

    def "Random hashing"()
    {
        def random = new Random()

        expect:
        10.times {
            def plain = random.nextDouble() as String
            def hash = BCHash.hash(plain)
            assert BCHash.check(plain,hash)
        }
    }

}
