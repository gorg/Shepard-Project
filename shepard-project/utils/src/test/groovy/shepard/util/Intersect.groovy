package shepard.util

import shepard.util.statistics.IntersectionArea
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by Gueorgui Tzvetoslavov Topalski on 15/05/17.
 * email: gueorgui.tzvetoslavov@gmail.com
 **/
class Intersect extends Specification
{

    @Unroll
    def "Intersection test"()
    {
        expect:
        IntersectionArea.intersect(mu1,sigma1,mu2,sigma2).round(2) == area

        where:
        mu1     |sigma1     |mu2    |sigma2     ||area
        1500    |300        |1500   |250        ||0.91
        1750    |150        |1700   |150        ||0.36
        1500    |300        |1600   |175        ||0.72
        1500    |300        |1200   |80         ||0.30
        1500    |300        |1500   |70         ||0.40
        1500    |300        |1600   |60         ||0.30
        1650    |300        |1500   |250        ||0.79
        1550    |150        |1750   |100        ||0.28
        1550    |150        |1800   |400        ||0.48
    }



}
