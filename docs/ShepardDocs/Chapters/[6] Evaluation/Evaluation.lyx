#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\master ../../ShepardProject.lyx
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Part
Evaluation
\end_layout

\begin_layout Section
Testing
\end_layout

\begin_layout Standard
To ensure the correctness of the project implementation, Shepard comes with
 a large set of unit integration and functional tests.
 During the development of the project the TDD
\begin_inset Foot
status open

\begin_layout Plain Layout
Test Driven Development
\end_layout

\end_inset

 philosophy was embraced resulting in more than a 1000 test cases.
 
\end_layout

\begin_layout Subsection
API module
\end_layout

\begin_layout Standard
The API module contains 190 unit test cases.
 All of them target the auxiliary data structures and objects used in the
 project.
 The Actor interfaces aren’t tested here, the corresponding tests are made
 in the backend module.
 
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename API.png
	width 150text%

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
Testing the API module
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Util module
\end_layout

\begin_layout Standard
The utilities module contains only 23 test cases.
 Although the provided functions are used all over the project the module
 it self provides only four methods in total, making it the smallest module
 of them all.
 
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename UTIL.png
	width 150text%

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
Testing the Util module
\end_layout

\end_inset


\end_layout

\end_inset


\begin_inset Newpage newpage
\end_inset


\end_layout

\begin_layout Subsection
Backend module
\end_layout

\begin_layout Standard
The backend contains 239 test cases.
 All Actor logic is tested here and it’s tested in a integration test fashion.
 For each actor test a real stage is started to test the actor functionalities
 providing a slower but more reliable validation.
 There two specially noteworthy test types in the backend module: 
\end_layout

\begin_layout Itemize

\series bold
Workflow specifications
\series default
: Provides a step by step workflow test, that goes through service registration,
 cache notifications and player-service interactions.
 
\end_layout

\begin_layout Itemize

\series bold
Multi-stage specifications
\series default
: Where correct service functionality is tested with more than one stage.
 Stages leave and enter the cluster testing the recover capability of the
 actor framework and the provided services.
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\begin_inset Graphics
	filename BACKEND.png
	width 150text%

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
Testing the Backend module
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Frontend module
\end_layout

\begin_layout Standard
The frontend module contains 1118 test cases.
 The fact that the frontend is the only way of consuming the provided services
 makes testing key a part of development.
 Unit tests against the domains, controllers and services form the majority
 of the test cases here.
 But the most important are the integration test which provide end-to-end
 testing with both the frontend and backend.
 In these, both a backend stage and the Grails frontend are initiated and
 the testing is done by performing real REST calls against the provided
 API.
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename FRONTEND.png
	width 150text%

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
Testing the Frontend module
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Metrics
\end_layout

\begin_layout Subsection
Class count
\end_layout

\begin_layout Standard
\begin_inset Float table
placement H
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="11" columns="3">
<features tabularvalignment="middle">
<column alignment="left" valignment="top">
<column alignment="left" valignment="top">
<column alignment="left" valignment="top">
<row>
<cell alignment="left" valignment="top" bottomline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Module
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Classes
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="3" alignment="left" valignment="middle" bottomline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Production
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
API
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
69
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="4" alignment="left" valignment="top" rotate="90" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Backend
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
30
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="4" alignment="left" valignment="top" rotate="90" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Frontend
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
129
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="4" alignment="left" valignment="top" rotate="90" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Utils
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
4
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="4" alignment="left" valignment="top" rotate="90" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Other
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
18
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="3" alignment="left" valignment="middle" bottomline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Test
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
API
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
12
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="4" alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Backend
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
40
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="4" alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Frontend
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
39
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="4" alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Utils
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
4
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Total
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
345
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
Class Count Metrics
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Lines of code
\end_layout

\begin_layout Standard
\begin_inset Float table
placement H
wide false
sideways false
status collapsed

\begin_layout Plain Layout
\align center
\begin_inset Tabular
<lyxtabular version="3" rows="11" columns="3">
<features tabularvalignment="middle">
<column alignment="left" valignment="top">
<column alignment="left" valignment="top">
<column alignment="left" valignment="top">
<row>
<cell alignment="left" valignment="top" bottomline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Module
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
LoC
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="3" alignment="left" valignment="middle" bottomline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Production
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
API
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1707
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="4" alignment="left" valignment="top" rotate="90" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Backend
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2914
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="4" alignment="left" valignment="top" rotate="90" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Frontend
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
2917
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="4" alignment="left" valignment="top" rotate="90" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Utils
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
100
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="4" alignment="left" valignment="top" rotate="90" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Other
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
23
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="3" alignment="left" valignment="middle" bottomline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Test
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
API
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
1447
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="4" alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Backend
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
4316
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="4" alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Frontend
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
7587
\end_layout

\end_inset
</cell>
</row>
<row>
<cell multirow="4" alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" bottomline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Utils
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
141
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
Total
\end_layout

\end_inset
</cell>
<cell alignment="left" valignment="top" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout

\series bold
21152
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset Caption Standard

\begin_layout Plain Layout
Lines of Code Metrics
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\end_body
\end_document
