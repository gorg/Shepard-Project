#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\begin_preamble
\AtBeginDocument{\renewcommand{\lstlistingname}{Snippet}}


\usepackage{listings}
\renewcommand{\lstlistingname}{Snippet}
\usepackage{longtable}

\usepackage{color}
\usepackage{listings}

\lstloadlanguages{% Check Dokumentation for further languages ...
C,
C++,
csh,
Java
}

\definecolor{red}{rgb}{0,0,0} % for strings
\definecolor{blue}{rgb}{0,0,0.6}
\definecolor{green}{rgb}{0,0.8,0}
\definecolor{cyan}{rgb}{0.0,0.6,0.6}

\lstset{
language=Java,
basicstyle=\footnotesize\ttfamily,
numbers=left,
numberstyle=\tiny,
numbersep=5pt,
tabsize=2,
extendedchars=true,
breaklines=true,
frame=b,
stringstyle=\color{blue}\ttfamily,
showspaces=false,
showtabs=false,
xleftmargin=4pt,
framexleftmargin=5pt,
framexrightmargin=5pt,
framexbottommargin=4pt,
commentstyle=\color{green},
morecomment=[l]{//}, %use comment-line-style!
morecomment=[s]{/*}{*/}, %for multiline comments
showstringspaces=false,
morekeywords={ abstract, event, new, struct,
as, explicit, null, switch,
base, extern, object, this,
bool, false, operator, throw,
break, finally, true,
byte, fixed, override, try,
case, float, params, typeof,
catch, for, private, uint,
char, foreach, protected, ulong,
checked, goto, public, unchecked,
class, if, readonly, unsafe,
const, implicit, ref, ushort,
continue, in, return, using,
decimal, int, sbyte, virtual,
default, interface, sealed, volatile,
delegate, internal, short, void,
do, is, sizeof, while,
double, lock, stackalloc,
else, long, static,
enum, namespace},
keywordstyle=\color{cyan},
identifierstyle=\color{red},
belowcaptionskip=1\baselineskip
}
\usepackage{caption}
\DeclareCaptionFont{white}{\color{white}}
\DeclareCaptionFormat{listing}{\colorbox{blue}{\parbox{\textwidth}{\hspace{15pt}#1#2#3}}}
\captionsetup[lstlisting]{format=listing,labelfont=white,textfont=white, singlelinecheck=false, margin=0pt, font={bf,footnotesize}}
\end_preamble
\options sort
\use_default_options true
\maintain_unincluded_children false
\begin_local_layout
Format 60
PackageOptions natbib compress
\end_local_layout
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "cmtl" "Hack"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize 12
\spacing single
\use_hyperref true
\pdf_title "Project Shepard"
\pdf_author "Gueorgui Tzvetoslavov Topalski"
\pdf_bookmarks false
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder false
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle false
\papersize a4paper
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine natbib
\cite_engine_type numerical
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 3cm
\topmargin 2.5cm
\rightmargin 2.5cm
\bottommargin 2.5cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset toc
LatexCommand lstlistoflistings

\end_inset


\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset FloatList figure

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Standard
\begin_inset FloatList table

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand include
filename "Chapters/[2] Project Objectives /Project Objectives.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand include
filename "Chapters/[3] Project Specification /Project Specification.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand include
filename "Chapters/[4] Design/Design.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand include
filename "Chapters/[5] Development/Development.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand include
filename "Chapters/[6] Evaluation/Evaluation.lyx"

\end_inset


\end_layout

\begin_layout Standard
\begin_inset CommandInset include
LatexCommand include
filename "Chapters/[7] Conclusion/Conclusion.lyx"

\end_inset


\begin_inset CommandInset include
LatexCommand include
filename "Chapters/[8] Used Resources/Used Resources.lyx"

\end_inset


\begin_inset CommandInset include
LatexCommand include
filename "Chapters/[9] Manual/Manuals.lyx"

\end_inset


\end_layout

\end_body
\end_document
